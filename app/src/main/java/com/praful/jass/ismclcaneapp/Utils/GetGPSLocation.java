package com.praful.jass.ismclcaneapp.Utils;

import android.content.Context;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;

import static android.content.Context.LOCATION_SERVICE;

/**
 * Created by Praful on 08-05-2018.
 */

public class GetGPSLocation implements LocationListener {
    Context MyContext;
    LocationManager locationManager;
    public Location location = null;
    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    boolean isDialogShow = false;
    double latitude, longitude;
    private FusedLocationProviderClient mFusedLocationClient;
    double accuracy = 0.0;

    private Location fn_getlocation(Context context) {
        MyContext = context;
        locationManager = (LocationManager) context.getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        try {
            if (!isGPSEnable && !isNetworkEnable) {
//                buildAlertMessageNoGps();
            } else {
                if (isNetworkEnable) {
                    if (ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(context, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        return location;
                    }
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 1000, 1, this); // Integer.parseInt(Preferences.getTrackingTime())
                    if (locationManager != null) {
                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    // Logic to handle location object
                                    if (location.getAccuracy() < 30) {
//                                        location = location;
                                    }
                                }
                            }
                        });
//                        }
                    }
                }
            }
            try {
                if (isGPSEnable) {
                    Location location1 = null;
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 1000, 0, this);
                    if (locationManager != null) {

                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(context);
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    // Logic to handle location object

                                    if (location.getAccuracy() < 30) {
//                                        location1 = location;
                                    }
                                }
                            }
                        });
                    }
                }
            } catch (Exception ex) {
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return location;
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }
}
