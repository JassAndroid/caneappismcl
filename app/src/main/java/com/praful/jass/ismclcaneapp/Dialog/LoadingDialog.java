package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.graphics.drawable.ColorDrawable;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;

public class LoadingDialog extends Dialog {
    private TextView txtTitle;
    private Context context;

    public LoadingDialog(Context context) {
        super(context);
        this.context = context;
    }

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_loadingbar);
            getWindow().setBackgroundDrawable(new ColorDrawable(android.graphics.Color.TRANSPARENT));
            txtTitle = (TextView) findViewById(R.id.loading_dialog_title);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onSetTitle(String strTitle) {
        try {
            if (txtTitle != null) {
                txtTitle.setText(strTitle.trim());
                txtTitle.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onHideTitle() {
        try {
            if (txtTitle != null) {
                txtTitle.setVisibility(View.GONE);
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void onShowTitle() {
        try {
            if (txtTitle != null) {
                txtTitle.setVisibility(View.VISIBLE);
            }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}