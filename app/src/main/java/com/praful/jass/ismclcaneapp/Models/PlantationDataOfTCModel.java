package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 07-10-2017.
 */

public class PlantationDataOfTCModel {
    private String SR_NO_TC, TOD_CHITTI_ID, PLANTATION_CODE_TC, FARMER_CODE_TC, FARMER_NAME_TC, FARMER_VILLAGE_TC, FARMER_GUT_TC, FARMER_SUBGUT_TC, FARMER_TALUKA_TC, SHIVAR_VILLAGE_TC, SHIVAR_GUT_TC, SURVEY_NUMBER_TC, IRRIGATION_TC, CANE_VERITY_TC, PLANTATION_DATE_TC, PLANTATION_TYPE_TC, VEHICAL_NUMBER_TC, VEHICAL_TYPE_TC, TRANSPORTER_NAME_TC, HARVEStER_CODE_TC, TRAILER_1, TRAILER_2, FLAG, OBJECT_ID, FARMER_ID, VNO_OBJECT_ID, TRANSPORTER_ID, VEHICAL_TYPE, DATE_TIME_TC, FLAG_ONLINE_SUBMIT_TC, FACTORY_NAME_TC, LAT_TC, LONG_TC, CUTTING_OBJECT_ID_TC, HRV_CODE_TC, HRV_NAME_TC, PLOT_START_DATE_TC, PLANT_GUT_ID_TC, CANE_QUALITY_TC, DRIVER_NAME_TC, ROP_TYPE_TC;

    public String getCANE_VERITY_TC() {
        return CANE_VERITY_TC;
    }

    public void setCANE_VERITY_TC(String CANE_VERITY_TC) {
        this.CANE_VERITY_TC = CANE_VERITY_TC;
    }

    public String getFARMER_CODE_TC() {
        return FARMER_CODE_TC;
    }

    public void setFARMER_CODE_TC(String FARMER_CODE_TC) {
        this.FARMER_CODE_TC = FARMER_CODE_TC;
    }

    public String getFARMER_GUT_TC() {
        return FARMER_GUT_TC;
    }

    public void setFARMER_GUT_TC(String FARMER_GUT_TC) {
        this.FARMER_GUT_TC = FARMER_GUT_TC;
    }

    public String getFARMER_NAME_TC() {
        return FARMER_NAME_TC;
    }

    public void setFARMER_NAME_TC(String FARMER_NAME_TC) {
        this.FARMER_NAME_TC = FARMER_NAME_TC;
    }

    public String getFARMER_SUBGUT_TC() {
        return FARMER_SUBGUT_TC;
    }

    public void setFARMER_SUBGUT_TC(String FARMER_SUBGUT_TC) {
        this.FARMER_SUBGUT_TC = FARMER_SUBGUT_TC;
    }

    public String getFARMER_TALUKA_TC() {
        return FARMER_TALUKA_TC;
    }

    public void setFARMER_TALUKA_TC(String FARMER_TALUKA_TC) {
        this.FARMER_TALUKA_TC = FARMER_TALUKA_TC;
    }

    public String getFARMER_VILLAGE_TC() {
        return FARMER_VILLAGE_TC;
    }

    public void setFARMER_VILLAGE_TC(String FARMER_VILLAGE_TC) {
        this.FARMER_VILLAGE_TC = FARMER_VILLAGE_TC;
    }

    public String getHARVEStER_CODE_TC() {
        return HARVEStER_CODE_TC;
    }

    public void setHARVEStER_CODE_TC(String HARVEStER_CODE_TC) {
        this.HARVEStER_CODE_TC = HARVEStER_CODE_TC;
    }

    public String getIRRIGATION_TC() {
        return IRRIGATION_TC;
    }

    public void setIRRIGATION_TC(String IRRIGATION_TC) {
        this.IRRIGATION_TC = IRRIGATION_TC;
    }

    public String getPLANTATION_CODE_TC() {
        return PLANTATION_CODE_TC;
    }

    public void setPLANTATION_CODE_TC(String PLANTATION_CODE_TC) {
        this.PLANTATION_CODE_TC = PLANTATION_CODE_TC;
    }

    public String getPLANTATION_DATE_TC() {
        return PLANTATION_DATE_TC;
    }

    public void setPLANTATION_DATE_TC(String PLANTATION_DATE_TC) {
        this.PLANTATION_DATE_TC = PLANTATION_DATE_TC;
    }

    public String getPLANTATION_TYPE_TC() {
        return PLANTATION_TYPE_TC;
    }

    public void setPLANTATION_TYPE_TC(String PLANTATION_TYPE_TC) {
        this.PLANTATION_TYPE_TC = PLANTATION_TYPE_TC;
    }

    public String getSHIVAR_GUT_TC() {
        return SHIVAR_GUT_TC;
    }

    public void setSHIVAR_GUT_TC(String SHIVAR_GUT_TC) {
        this.SHIVAR_GUT_TC = SHIVAR_GUT_TC;
    }

    public String getSHIVAR_VILLAGE_TC() {
        return SHIVAR_VILLAGE_TC;
    }

    public void setSHIVAR_VILLAGE_TC(String SHIVAR_VILLAGE_TC) {
        this.SHIVAR_VILLAGE_TC = SHIVAR_VILLAGE_TC;
    }

    public String getSR_NO_TC() {
        return SR_NO_TC;
    }

    public void setSR_NO_TC(String SR_NO_TC) {
        this.SR_NO_TC = SR_NO_TC;
    }

    public String getSURVEY_NUMBER_TC() {
        return SURVEY_NUMBER_TC;
    }

    public void setSURVEY_NUMBER_TC(String SURVEY_NUMBER_TC) {
        this.SURVEY_NUMBER_TC = SURVEY_NUMBER_TC;
    }

    public String getTOD_CHITTI_ID() {
        return TOD_CHITTI_ID;
    }

    public void setTOD_CHITTI_ID(String TOD_CHITTI_ID) {
        this.TOD_CHITTI_ID = TOD_CHITTI_ID;
    }

    public String getTRAILER_1() {
        return TRAILER_1;
    }

    public void setTRAILER_1(String TRAILER_1) {
        this.TRAILER_1 = TRAILER_1;
    }

    public String getTRAILER_2() {
        return TRAILER_2;
    }

    public void setTRAILER_2(String TRAILER_2) {
        this.TRAILER_2 = TRAILER_2;
    }

    public String getTRANSPORTER_NAME_TC() {
        return TRANSPORTER_NAME_TC;
    }

    public void setTRANSPORTER_NAME_TC(String TRANSPORTER_NAME_TC) {
        this.TRANSPORTER_NAME_TC = TRANSPORTER_NAME_TC;
    }

    public String getVEHICAL_NUMBER_TC() {
        return VEHICAL_NUMBER_TC;
    }

    public void setVEHICAL_NUMBER_TC(String VEHICAL_NUMBER_TC) {
        this.VEHICAL_NUMBER_TC = VEHICAL_NUMBER_TC;
    }

    public String getVEHICAL_TYPE_TC() {
        return VEHICAL_TYPE_TC;
    }

    public void setVEHICAL_TYPE_TC(String VEHICAL_TYPE_TC) {
        this.VEHICAL_TYPE_TC = VEHICAL_TYPE_TC;
    }

    public String getFLAG() {
        return FLAG;
    }

    public void setFLAG(String FLAG) {
        this.FLAG = FLAG;
    }

    public String getFARMER_ID() {
        return FARMER_ID;
    }

    public void setFARMER_ID(String FARMER_ID) {
        this.FARMER_ID = FARMER_ID;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getTRANSPORTER_ID() {
        return TRANSPORTER_ID;
    }

    public void setTRANSPORTER_ID(String TRANSPORTER_ID) {
        this.TRANSPORTER_ID = TRANSPORTER_ID;
    }

    public String getVEHICAL_TYPE() {
        return VEHICAL_TYPE;
    }

    public void setVEHICAL_TYPE(String VEHICAL_TYPE) {
        this.VEHICAL_TYPE = VEHICAL_TYPE;
    }

    public String getVNO_OBJECT_ID() {
        return VNO_OBJECT_ID;
    }

    public void setVNO_OBJECT_ID(String VNO_OBJECT_ID) {
        this.VNO_OBJECT_ID = VNO_OBJECT_ID;
    }

    public String getDATE_TIME_TC() {
        return DATE_TIME_TC;
    }

    public void setDATE_TIME_TC(String DATE_TIME_TC) {
        this.DATE_TIME_TC = DATE_TIME_TC;
    }

    public String getFLAG_ONLINE_SUBMIT_TC() {
        return FLAG_ONLINE_SUBMIT_TC;
    }

    public void setFLAG_ONLINE_SUBMIT_TC(String FLAG_ONLINE_SUBMIT_TC) {
        this.FLAG_ONLINE_SUBMIT_TC = FLAG_ONLINE_SUBMIT_TC;
    }

    public String getFACTORY_NAME_TC() {
        return FACTORY_NAME_TC;
    }

    public void setFACTORY_NAME_TC(String FACTORY_NAME_TC) {
        this.FACTORY_NAME_TC = FACTORY_NAME_TC;
    }

    public String getLAT_TC() {
        return LAT_TC;
    }

    public void setLAT_TC(String LAT_TC) {
        this.LAT_TC = LAT_TC;
    }

    public String getLONG_TC() {
        return LONG_TC;
    }

    public void setLONG_TC(String LONG_TC) {
        this.LONG_TC = LONG_TC;
    }

    public String getCUTTING_OBJECT_ID_TC() {
        return CUTTING_OBJECT_ID_TC;
    }

    public void setCUTTING_OBJECT_ID_TC(String CUTTING_OBJECT_ID_TC) {
        this.CUTTING_OBJECT_ID_TC = CUTTING_OBJECT_ID_TC;
    }

    public String getHRV_CODE_TC() {
        return HRV_CODE_TC;
    }

    public void setHRV_CODE_TC(String HRV_CODE_TC) {
        this.HRV_CODE_TC = HRV_CODE_TC;
    }

    public String getHRV_NAME_TC() {
        return HRV_NAME_TC;
    }

    public void setHRV_NAME_TC(String HRV_NAME_TC) {
        this.HRV_NAME_TC = HRV_NAME_TC;
    }

    public String getPLOT_START_DATE_TC() {
        return PLOT_START_DATE_TC;
    }

    public void setPLOT_START_DATE_TC(String PLOT_START_DATE_TC) {
        this.PLOT_START_DATE_TC = PLOT_START_DATE_TC;
    }

    public String getPLANT_GUT_ID_TC() {
        return PLANT_GUT_ID_TC;
    }

    public String getCANE_QUALITY_TC() {
        return CANE_QUALITY_TC;
    }

    public void setCANE_QUALITY_TC(String CANE_QUALITY_TC) {
        this.CANE_QUALITY_TC = CANE_QUALITY_TC;
    }

    public String getDRIVER_NAME_TC() {
        return DRIVER_NAME_TC;
    }

    public void setDRIVER_NAME_TC(String DRIVER_NAME_TC) {
        this.DRIVER_NAME_TC = DRIVER_NAME_TC;
    }

    public String getROP_TYPE_TC() {
        return ROP_TYPE_TC;
    }

    public void setROP_TYPE_TC(String ROP_TYPE_TC) {
        this.ROP_TYPE_TC = ROP_TYPE_TC;
    }

    public void setPLANT_GUT_ID_TC(String PLANT_GUT_ID_TC) {
        this.PLANT_GUT_ID_TC = PLANT_GUT_ID_TC;


    }
}


