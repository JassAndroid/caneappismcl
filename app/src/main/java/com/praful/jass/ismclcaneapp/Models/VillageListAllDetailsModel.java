package com.praful.jass.ismclcaneapp.Models;

public class VillageListAllDetailsModel {
    private String VILLAGE;

    private String DISTRICT;

    private String TALUKA;

    private String VILLAGE_ID;

    private String SUB_GUT;

    private String GUT;

    public String getVILLAGE() {
        return VILLAGE;
    }

    public void setVILLAGE(String VILLAGE) {
        this.VILLAGE = VILLAGE;
    }

    public String getDISTRICT() {
        return DISTRICT;
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public String getTALUKA() {
        return TALUKA;
    }

    public void setTALUKA(String TALUKA) {
        this.TALUKA = TALUKA;
    }

    public String getVILLAGE_ID() {
        return VILLAGE_ID;
    }

    public void setVILLAGE_ID(String VILLAGE_ID) {
        this.VILLAGE_ID = VILLAGE_ID;
    }

    public String getSUB_GUT() {
        return SUB_GUT;
    }

    public void setSUB_GUT(String SUB_GUT) {
        this.SUB_GUT = SUB_GUT;
    }

    public String getGUT() {
        return GUT;
    }

    public void setGUT(String GUT) {
        this.GUT = GUT;
    }

    @Override
    public String toString() {
        return "ClassPojo [VILLAGE = " + VILLAGE + ", DISTRICT = " + DISTRICT + ", TALUKA = " + TALUKA + ", VILLAGE_ID = " + VILLAGE_ID + ", SUB_GUT = " + SUB_GUT + ", GUT = " + GUT + "]";
    }
}
