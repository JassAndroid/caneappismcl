package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.ApproveModel;
import com.praful.jass.ismclcaneapp.Models.CandidateWorkDataModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApprovalPlantationActivity extends AppCompatActivity {

    ImageView go_back;
    CandidateWorkDataModel candidateWorkDataModel;
    EditText edt_plantation_code, edt_grower_name, edt_village, edt_gut_id, edt_survey_number, edt_plantation_date, edt_plantation_id, edt_plantation_area, edt_shivar_village, edt_shivar_gut, edt_regi_type;
    Button btn_work_detail_approve, btn_work_detail_deny;
    Context context;
    String device = "";
    TelephonyManager telephonyManager;
    SystemDateTime dateTime = new SystemDateTime();

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_plantation);
        init();
    }

    private void init() {
        context = this;
        Preferences.appContext = context;
        go_back = findViewById(R.id.go_back);
        edt_plantation_code = findViewById(R.id.edt_plantation_code);
        edt_grower_name = findViewById(R.id.edt_grower_name);
        edt_shivar_village = findViewById(R.id.edt_shivar_v_name);
        edt_shivar_gut = findViewById(R.id.edt_shivar_v_gut);
        edt_village = findViewById(R.id.edt_village);
        edt_gut_id = findViewById(R.id.edt_gut_id);
        edt_survey_number = findViewById(R.id.edt_survey_number);
        edt_plantation_date = findViewById(R.id.edt_plantation_date);
        edt_plantation_id = findViewById(R.id.edt_plantation_id);
        edt_plantation_area = findViewById(R.id.edt_plantation_area);
        btn_work_detail_approve = findViewById(R.id.btn_work_detail_approve);
        btn_work_detail_deny = findViewById(R.id.btn_work_detail_deny);
        edt_regi_type = findViewById(R.id.edt_plantation_regi_type);
        candidateWorkDataModel = new Gson().fromJson(getIntent().getStringExtra("model"), CandidateWorkDataModel.class);
        telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
//        locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
        device = getDeviceID();
        //Method Calling
        //getDeviceID();
        setListeners();
        setData();
    }

    private String getDeviceID() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return null;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            device = telephonyManager.getDeviceId(2);
        } else {
            device = telephonyManager.getDeviceId();//getDeviceId(1);
        }
        return device;
    }

    private void setListeners() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
        btn_work_detail_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveOrder("A");
            }
        });

        btn_work_detail_deny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveOrder("C");
            }
        });
    }

    private void setData() {
        edt_plantation_code.setText(candidateWorkDataModel.getPLANTATION_CODE());
        edt_grower_name.setText(candidateWorkDataModel.getFARMER_NAME());
        edt_village.setText(candidateWorkDataModel.getVG_NAME());
        edt_gut_id.setText(candidateWorkDataModel.getGUT_NAME());
        edt_survey_number.setText(candidateWorkDataModel.getSARVE_NO());
        edt_plantation_date.setText(candidateWorkDataModel.getPLANTATION_DATE());
        edt_plantation_id.setText(candidateWorkDataModel.getPLANTATION_ID());
        edt_plantation_area.setText(candidateWorkDataModel.getPLANTATION_AREA());
        edt_regi_type.setText(candidateWorkDataModel.getPLANT_REG_TYPE());
        edt_shivar_village.setText(candidateWorkDataModel.getSHIVAR_VG_NAME());
        edt_shivar_gut.setText(candidateWorkDataModel.getSHIVAR_GUT_NAME());
    }

    /**
     * API CALLING
     **/
    private void approveOrder(String flag) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject param = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        String lat = "", longg = "";

        try {
//            CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
//            lat = locationModel.getLatitude() + "";
//            longg = locationModel.getLongitude() + "";
            param.put("p_object_id", edt_plantation_id.getText().toString());
            param.put("p_user", Preferences.getCurrentUserId());
            param.put("p_flag", flag);
            param.put("p_device", device);

            //param.put("p_out", Preferences.getSeasonID());
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("PlantationApprove", jsonArray);


            new APIRestManager().postAPI(AppConstants.URL_PLANTATION_APPROVE, jsonObject, context, ApproveModel.class, new APIRestManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    progressDialog.dismiss();
                    Toast.makeText(context, "Approve Successfully", Toast.LENGTH_SHORT).show();
                    ((Activity) context).finish();
                }

                @Override
                public void onError(String strError) {
                    progressDialog.dismiss();
                    Toast.makeText(context, strError, Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception ex) {
            progressDialog.dismiss();
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
