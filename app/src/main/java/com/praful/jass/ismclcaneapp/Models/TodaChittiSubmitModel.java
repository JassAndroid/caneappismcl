package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 26-10-2017.
 */

public class TodaChittiSubmitModel {
    String slip_id, slip_key;

    public String getSlip_id() {
        return slip_id;
    }

    public void setSlip_id(String slip_id) {
        this.slip_id = slip_id;
    }

    public String getSlip_key() {
        return slip_key;
    }

    public void setSlip_key(String slip_key) {
        this.slip_key = slip_key;
    }
}
