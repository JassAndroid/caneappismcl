package com.praful.jass.ismclcaneapp.DBHelper;

import android.content.Context;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;

import com.praful.jass.ismclcaneapp.Utils.Preferences;

/**
 * Created by Praful on 17-09-2017.
 */

public class SQLLiteHelperPhase_II extends SQLiteOpenHelper

{

    /**
     * Initialization of variables Start
     */

    //    =============================================Database Name and Version===================================================
    private final static String DATABASE_NAME = "CANE_BUDDY_APP_1";
    private final static int DATABASE_VERSION = 1;


    //    =============================================Create Database for Farmer_List===================================================
//    private final String TBL_FARMER_LIST_TC = "TBL_FARMER_LIST_TC";
//    private final String ID_FARMER_TC = "ID_TC";
//    private final String FARMER_LIST_TC = "FARMER_LIST_TC";

    //    =============================================Create Database for PLANTATION_TYPE===================================================
    private final String TBL_PLANTATION_LIST_TC = "TBL_PLANTATION_LIST_TC";
    private final String ID_PLANTATION_TYPE = "ID_TC";
    private final String PLANTATION_TYPE_LIST = "PLANTATION_TYPE_LIST";

    //    =============================================Create Database for VEHICLE_TYPE===================================================
    private final String TBL_VEHICLE_LIST_TC = "TBL_VEHICLE_LIST_TC";
    private final String ID_VEHICLE_TYPE = "ID_TC";
    private final String VEHICLE_TYPE_LIST = "VEHICLE_TYPE_LIST";

    //    =============================================Create Database for Get_HARVESTER_TYPE===================================================
    private final String TBL_HARVESTER_LIST_TC = "TBL_HARVESTER_LIST_TC";
    private final String ID_HARVESTER_TYPE = "ID_TC";
    private final String HARVESTER_TYPE_LIST = "HARVESTER_TYPE_LIST";

    //    =============================================Create Database for TOD CHITTI DATA===================================================
    private final String TBL_TOD_CHITTI_DATA_SUBMIT = "TBL_TOD_CHITTI_DATA_SUBMIT";
    private final String SR_NO_TC = "SR_NO_TC";
    private final String TOD_CHITTI_ID_TC = "TOD_CHITTI_ID_TC";
    private final String PLANTATION_CODE_TC = "PLANTATION_CODE_TC";
    private final String FARMER_CODE_TC = "FARMER_CODE_TC";
    private final String FARMER_NAME_TC = "FARMER_NAME_TC";
    private final String FARMER_VILLAGE_TC = "FARMER_VILLAGE_TC";
    private final String FARMER_GUT_TC = "FARMER_GUT_TC";
    private final String FARMER_SUBGUT_TC = "FARMER_SUBGUT_TC";
    private final String FARMER_TALUKA_TC = "FARMER_TALUKA_TC";
    private final String SHIVAR_VILLAGE_TC = "SHIVAR_VILLAGE_TC";
    private final String SHIVAR_GUT_TC = "SHIVAR_GUT_TC";
    private final String SURVEY_NUMBER_TC = "SURVEY_NUMBER_TC";
    private final String IRRIGATION_TC = "IRRIGATION_TC";
    private final String CANE_VERITY_TC = "CANE_VERITY_TC";
    private final String PLANTATION_DATE_TC = "PLANTATION_DATE_TC";
    private final String PLANTATION_TYPE_TC = "PLANTATION_TYPE_TC";
    private final String VEHICAL_NUMBER_TC = "VEHICAL_NUMBER_TC";
    private final String VEHICAL_TYPE_TC = "VEHICAL_TYPE_TC";
    private final String TRANSPORTER_NAME_TC = "TRANSPORTER_NAME_TC";
    private final String HARVEStER_CODE_TC = "HARVEStER_CODE_TC";
    private final String TRAILER_1 = "TRAILER_1";
    private final String TRAILER_2 = "TRAILER_2";
    private final String FLAG = "FLAG";
    private final String OBJECT_ID_TC = "OBJECT_ID_TC";
    private final String FARMER_ID_TC = "FARMER_ID_TC";
    private final String VNO_OBJECT_ID_TC = "VNO_OBJECT_ID_TC";
    private final String TRANSPORTER_ID_TC = "TRANSPORTER_ID_TC";
    private final String VEHICAL_TYPE_NO_TC = "VEHICAL_TYPE_NO_TC";
    private final String DATE_TIME_TC = "DATE_TIME_TC";
    private final String FLAG_ONLINE_SUBMIT_TC = "FLAG_ONLINE_SUBMIT_TC";


    /**
     * Initialization of variables End
     */

    public SQLLiteHelperPhase_II(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
    }

    @Override
    public void onOpen(SQLiteDatabase sqLiteDatabase) {
        if (Preferences.getFirstTimeStatus()) {
//            createTableFarmerList(sqLiteDatabase);
//            createTablePlantationList(sqLiteDatabase);
//            createTableVehicleList(sqLiteDatabase);
//            createTableHarvesterList(sqLiteDatabase);
//            createTableTodChittiData(sqLiteDatabase);
        }
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
//        createTableFarmerList(sqLiteDatabase);
//        createTablePlantationList(sqLiteDatabase);
//        createTableVehicleList(sqLiteDatabase);
//        createTableHarvesterList(sqLiteDatabase);
//        createTableTodChittiData(sqLiteDatabase);

    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int i, int i1) {

    }


    /**
     * Table Create Functions Start
     */

    //==================================== METHOD FOR CREATTING TABLE FOR Farmer LIST  =====================================
//    public void createTableFarmerList(SQLiteDatabase db) {
//
//        String sql = "CREATE TABLE " + TBL_FARMER_LIST_TC + " ( "
//                + ID_FARMER_TC + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
//                + FARMER_LIST_TC + " TEXT );";
//        db.execSQL(sql);
//
//    }

    //==================================== METHOD FOR CREATTING TABLE FOR PLANTATION LIST  =====================================
    public void createTablePlantationList(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TBL_PLANTATION_LIST_TC + " ( "
                + ID_PLANTATION_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + PLANTATION_TYPE_LIST + " TEXT );";
        db.execSQL(sql);

    }

    //==================================== METHOD FOR CREATTING TABLE FOR Vehicle LIST  =====================================
    public void createTableVehicleList(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TBL_VEHICLE_LIST_TC + " ( "
                + ID_VEHICLE_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + VEHICLE_TYPE_LIST + " TEXT );";
        db.execSQL(sql);

    }

    //==================================== METHOD FOR CREATTING TABLE FOR Harvester LIST  =====================================
    public void createTableHarvesterList(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TBL_HARVESTER_LIST_TC + " ( "
                + ID_HARVESTER_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + HARVESTER_TYPE_LIST + " TEXT );";
        db.execSQL(sql);

    }

    //==================================== METHOD FOR CREATTING TABLE FOR TOD CHITT DATA  =====================================
    public void createTableTodChittiData(SQLiteDatabase db) {

        String sql = "CREATE TABLE " + TBL_TOD_CHITTI_DATA_SUBMIT + " ( "
                + SR_NO_TC + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                + TOD_CHITTI_ID_TC + " TEXT ,"
                + PLANTATION_CODE_TC + " TEXT ,"
                + FARMER_CODE_TC + " TEXT ,"
                + FARMER_NAME_TC + " TEXT ,"
                + FARMER_VILLAGE_TC + " TEXT ,"
                + FARMER_GUT_TC + " TEXT ,"
                + FARMER_SUBGUT_TC + " TEXT ,"
                + FARMER_TALUKA_TC + " TEXT ,"
                + SHIVAR_VILLAGE_TC + " TEXT ,"
                + SHIVAR_GUT_TC + " TEXT ,"
                + SURVEY_NUMBER_TC + " TEXT ,"
                + IRRIGATION_TC + " TEXT ,"
                + CANE_VERITY_TC + " TEXT ,"
                + PLANTATION_DATE_TC + " TEXT ,"
                + PLANTATION_TYPE_TC + " TEXT ,"
                + VEHICAL_NUMBER_TC + " TEXT ,"
                + VEHICAL_TYPE_TC + " TEXT ,"
                + TRANSPORTER_NAME_TC + " TEXT ,"
                + HARVEStER_CODE_TC + " TEXT ,"
                + TRAILER_1 + " TEXT ,"
                + TRAILER_2 + " TEXT ,"
                + FLAG + " TEXT ,"
                + OBJECT_ID_TC + " TEXT ,"
                + FARMER_ID_TC + " TEXT ,"
                + VNO_OBJECT_ID_TC + " TEXT ,"
                + TRANSPORTER_ID_TC + " TEXT ,"
                + VEHICAL_TYPE_NO_TC + " TEXT ,"
                + DATE_TIME_TC + " TEXT ,"
                + FLAG_ONLINE_SUBMIT_TC + " TEXT );";
        db.execSQL(sql);
    }

    /**
     * Table Create Functions End */


    /**
     * Insert Query Functions Start
     */

    //====================================METHOD FOR INSERTING DATA INTO FARMER LIST =======================
//    public void insertFarmerList(String farmerList) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_FARMER_LIST_TC + ";";
//        db.execSQL(deleteQuery);
//        ContentValues cv = new ContentValues();
//        cv.put(FARMER_LIST_TC, farmerList);
//        db.insert(TBL_FARMER_LIST_TC, null, cv);
//    }

    //====================================METHOD FOR INSERTING DATA INTO PLANTATION LIST =======================
//    public void insertPlantationList(String farmerList) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_PLANTATION_LIST_TC + ";";
//        db.execSQL(deleteQuery);
//        ContentValues cv = new ContentValues();
//        cv.put(PLANTATION_TYPE_LIST, farmerList);
//        db.insert(TBL_PLANTATION_LIST_TC, null, cv);
//    }
//
//    //====================================METHOD FOR INSERTING DATA INTO VEHICLE LIST =======================
//    public void insertVehicleList(String farmerList) {
//        try
//        {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String deleteQuery = "DELETE FROM " + TBL_VEHICLE_LIST_TC + ";";
//            db.execSQL(deleteQuery);
//            ContentValues cv = new ContentValues();
//            cv.put(VEHICLE_TYPE_LIST, farmerList);
//            db.insert(TBL_VEHICLE_LIST_TC, null, cv);
//        } catch (Exception e) {
//            e.printStackTrace();
//        }
//    }
//
//    //====================================METHOD FOR INSERTING DATA INTO HARVESTER LIST =======================
//    public void insertHarvesterList(String farmerList) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_HARVESTER_LIST_TC + ";";
//        db.execSQL(deleteQuery);
//        ContentValues cv = new ContentValues();
//        cv.put(HARVESTER_TYPE_LIST, farmerList);
//        db.insert(TBL_HARVESTER_LIST_TC, null, cv);
//    }
//
//    //====================================METHOD FOR INSERTING DATA INTO TOD CHITTI DATA =======================
//    public boolean insertTodChittiData(String ID_TC, String plantation_TC, String farmerCode, String farmerName, String farmerVillage, String farmerGut, String farmerSubgut, String farmerTaluka, String shivarVillage, String shivarGut, String surveyNumber, String irrigation, String caneVerity, String plantationDate, String plantationType, String vehicalNumber, String vehicalType, String transporterName, String harvesterCode, String trailer_1, String trailer_2, String plantation_id, String farmer_id, String vnoID, String transporter_id, String vehical_id, String dateTime) {
//        SQLiteDatabase db = this.getWritableDatabase();
////        String deleteQuery = "DELETE FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + ";";
////        db.execSQL(deleteQuery);
//        try {
//            ContentValues cv = new ContentValues();
//            cv.put(TOD_CHITTI_ID_TC, ID_TC);
//            cv.put(PLANTATION_CODE_TC, plantation_TC);
//            cv.put(FARMER_CODE_TC, farmerCode);
//            cv.put(FARMER_NAME_TC, farmerName);
//            cv.put(FARMER_VILLAGE_TC, farmerVillage);
//            cv.put(FARMER_GUT_TC, farmerGut);
//            cv.put(FARMER_SUBGUT_TC, farmerSubgut);
//            cv.put(FARMER_TALUKA_TC, farmerTaluka);
//            cv.put(SHIVAR_VILLAGE_TC, shivarVillage);
//            cv.put(SHIVAR_GUT_TC, shivarGut);
//            cv.put(SURVEY_NUMBER_TC, surveyNumber);
//            cv.put(IRRIGATION_TC, irrigation);
//            cv.put(CANE_VERITY_TC, caneVerity);
//            cv.put(PLANTATION_DATE_TC, plantationDate);
//            cv.put(PLANTATION_TYPE_TC, plantationType);
//            cv.put(VEHICAL_NUMBER_TC, vehicalNumber);
//            cv.put(VEHICAL_TYPE_TC, vehicalType);
//            cv.put(TRANSPORTER_NAME_TC, transporterName);
//            cv.put(HARVEStER_CODE_TC, harvesterCode);
//            cv.put(TRAILER_1, trailer_1);
//            cv.put(TRAILER_2, trailer_2);
//            cv.put(FLAG, "N");
//            cv.put(OBJECT_ID_TC, plantation_id);
//            cv.put(FARMER_ID_TC, farmer_id);
//            cv.put(VNO_OBJECT_ID_TC, vnoID);
//            cv.put(TRANSPORTER_ID_TC, transporter_id);
//            cv.put(VEHICAL_TYPE_NO_TC, vehical_id);
//            cv.put(DATE_TIME_TC, dateTime);
//            cv.put(FLAG_ONLINE_SUBMIT_TC, "N");
//            db.insert(TBL_TOD_CHITTI_DATA_SUBMIT, null, cv);
//            return true;
//        } catch (Exception ex) {
//            return false;
//        }
//    }
//
//
////9595953721
//
//    /**
//     * Insert Query Functions End */
//
//
//    /**
//     * Get Data Query Functions Start
//     */
//
//    //====================================METHOD FOR GETTING DATA OF FARMER LIST =======================
////    public String getFarmerList() {
////        String value;
////        try {
////            SQLiteDatabase db = this.getWritableDatabase();
////            String sql = "SELECT * FROM " + TBL_FARMER_LIST_TC + ";";
////            Cursor cursor = db.rawQuery(sql, null);
////            cursor.moveToNext();
////            value = cursor.getString(1);
////            System.out.print(value);
////        } catch (Exception e) {
////            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
////        }
////        return value;
////
////    }
//
//    //====================================METHOD FOR GETTING DATA OF PLANTATION LIST =======================
//    public String getPlanatationList() {
//        String value;
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_PLANTATION_LIST_TC + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToNext();
//            value = cursor.getString(1);
//            System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//    }
//
//    //====================================METHOD FOR GETTING DATA OF VEHICLE LIST =======================
//    public String getVehicleList() {
//        String value;
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_VEHICLE_LIST_TC + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToNext();
//            value = cursor.getString(1);
//            System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//    }
//
//    //====================================METHOD FOR GETTING DATA OF HARVESTER LIST =======================
//    public String getHarvesterList() {
//        String value;
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_HARVESTER_LIST_TC + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToNext();
//            value = cursor.getString(1);
//            System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//    }
//
//    //====================================METHOD FOR GETTING DATA OF TOD CHITTI LIST =======================
//    public ArrayList<PlantationDataOfTCModel> getTodChittiList() {
//        ArrayList<PlantationDataOfTCModel> value = new ArrayList<>();
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            while (cursor.moveToNext()) {
//                PlantationDataOfTCModel plantationDataOfTCModel = new PlantationDataOfTCModel();
//                try {
//                    plantationDataOfTCModel.setTOD_CHITTI_ID(cursor.getString(1));
//                    plantationDataOfTCModel.setPLANTATION_CODE_TC(cursor.getString(2));
//                    plantationDataOfTCModel.setFARMER_CODE_TC(cursor.getString(3));
//                    plantationDataOfTCModel.setFARMER_NAME_TC(cursor.getString(4));
//                    plantationDataOfTCModel.setFARMER_VILLAGE_TC(cursor.getString(5));
//                    plantationDataOfTCModel.setFARMER_GUT_TC(cursor.getString(6));
//                    plantationDataOfTCModel.setFARMER_SUBGUT_TC(cursor.getString(7));
//                    plantationDataOfTCModel.setFARMER_TALUKA_TC(cursor.getString(8));
//                    plantationDataOfTCModel.setSHIVAR_VILLAGE_TC(cursor.getString(9));
//                    plantationDataOfTCModel.setSHIVAR_GUT_TC(cursor.getString(10));
//                    plantationDataOfTCModel.setSURVEY_NUMBER_TC(cursor.getString(11));
//                    plantationDataOfTCModel.setIRRIGATION_TC(cursor.getString(12));
//                    plantationDataOfTCModel.setCANE_VERITY_TC(cursor.getString(13));
//                    plantationDataOfTCModel.setPLANTATION_DATE_TC(cursor.getString(14));
//                    plantationDataOfTCModel.setPLANTATION_TYPE_TC(cursor.getString(15));
//                    plantationDataOfTCModel.setVEHICAL_NUMBER_TC(cursor.getString(16));
//                    plantationDataOfTCModel.setVEHICAL_TYPE_TC(cursor.getString(17));
//                    plantationDataOfTCModel.setTRANSPORTER_NAME_TC(cursor.getString(18));
//                    plantationDataOfTCModel.setHARVEStER_CODE_TC(cursor.getString(19));
//                    plantationDataOfTCModel.setTRAILER_1(cursor.getString(20));
//                    plantationDataOfTCModel.setTRAILER_2(cursor.getString(21));
//                    plantationDataOfTCModel.setFLAG(cursor.getString(22));
//                    plantationDataOfTCModel.setOBJECT_ID(cursor.getString(23));
//                    plantationDataOfTCModel.setFARMER_ID(cursor.getString(24));
//                    plantationDataOfTCModel.setVNO_OBJECT_ID(cursor.getString(25));
//                    plantationDataOfTCModel.setTRANSPORTER_ID(cursor.getString(26));
//                    plantationDataOfTCModel.setVEHICAL_TYPE(cursor.getString(27));
//                    plantationDataOfTCModel.setDATE_TIME_TC(cursor.getString(28));
//                    plantationDataOfTCModel.setFLAG_ONLINE_SUBMIT_TC(cursor.getString(29));
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//                value.add(plantationDataOfTCModel);
//            }
//            System.out.print(value);
//        } catch (Exception e) {
////            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//    }
//
//    //====================================METHOD FOR GETTING DATA OF TOD CHITTI LIST =======================
//    public String getTodChittiListSubmit(String deviceID) {
//        String value = "";
//        JSONObject jsonObject = new JSONObject();
//        JSONArray jsonArray = new JSONArray();
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + " WHERE " + FLAG_ONLINE_SUBMIT_TC + " = 'N' ;";
//            Cursor cursor = db.rawQuery(sql, null);
//            while (cursor.moveToNext()) {
//                try {
//                    JSONObject innerjsonObject = new JSONObject();
//                    innerjsonObject.put("p_date", cursor.getString(28));
//                    innerjsonObject.put("p_device_id", deviceID);//dd-MMM-yyyy HH:mm:ss);
//                    innerjsonObject.put("p_user_id", cursor.getString(2));
//                    innerjsonObject.put("p_farmer_id", cursor.getString(24));
//                    innerjsonObject.put("p_plantation", cursor.getString(23));
//                    innerjsonObject.put("p_trasporter_id", cursor.getString(26));//dd-MMM-yyyy HH:mm:ss);
//                    innerjsonObject.put("p_veh_id", cursor.getString(25));
//                    innerjsonObject.put("p_harvestor_id", cursor.getString(19));
//                    innerjsonObject.put("p_tr_1", cursor.getString(20));
//                    innerjsonObject.put("p_tr_2", cursor.getString(21));//dd-MMM-yyyy HH:mm:ss);
//                    innerjsonObject.put("p_slip", cursor.getString(1));
//                    jsonArray.put(innerjsonObject);
//                } catch (Exception e) {
//                    e.printStackTrace();
//                }
//            }
//            value = jsonObject.put("SlipSubmit", jsonArray).toString();
//
//            System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//    }
//
//    /**
//     * Get Data Query Functions End */
//
//
//    /**
//     * Delete Data Query Functions Start
//     */
//
////  ========
//
//    /**
//     * Delete Data Query Functions End */
//
//    /**
//     * Update Recorde Functions Start
//     */
//
//    //====================================METHOD FOR UPDATE LIST OF PLANTATION IMAGES =======================
//    public String updateTodChittiData(String plantationID, String flag) {
//        String value = "";
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            ContentValues newValues = new ContentValues();
//            newValues.put(FLAG, flag);
//            int update = db.update(TBL_TOD_CHITTI_DATA_SUBMIT, newValues, OBJECT_ID_TC + "=" + plantationID, null);
////            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
////            Cursor cursor = db.rawQuery(sql, null);
//            value = "DONE" + update;
//            System.out.print(value);
//        } catch (Exception e) {
//            value = "FAIL";
//        }
//        return value;
//    }
//
//
//    //====================================METHOD FOR UPDATE LIST OF PLANTATION IMAGES =======================
//    public String updateTodChittiOnlineFlag(String slipId, String flag) {
//        String value = "";
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            ContentValues newValues = new ContentValues();
//            newValues.put(FLAG_ONLINE_SUBMIT_TC, flag);
//            int update = db.update(TBL_TOD_CHITTI_DATA_SUBMIT, newValues, TOD_CHITTI_ID_TC + "=" + slipId, null);
////            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
////            Cursor cursor = db.rawQuery(sql, null);
//            value = "DONE" + update;
//            System.out.print(value);
//        } catch (Exception e) {
//            value = "FAIL";
//        }
//        return value;
//    }

//  ========

    /**
     * Update Recorde Functions End */


}
