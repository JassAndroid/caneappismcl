package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-07-2017.
 */

public class ShivarVillageListModel {
    private String GUT_NAME;

    private String VILLAGE_NAME;

    private String OBJECT_ID;

    private String GUT_ID;

    public String getGUT_NAME() {
        return GUT_NAME;
    }

    public void setGUT_NAME(String GUT_NAME) {
        this.GUT_NAME = GUT_NAME;
    }

    public String getVILLAGE_NAME() {
        return VILLAGE_NAME;
    }

    public void setVILLAGE_NAME(String VILLAGE_NAME) {
        this.VILLAGE_NAME = VILLAGE_NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getGUT_ID() {
        return GUT_ID;
    }

    public void setGUT_ID(String GUT_ID) {
        this.GUT_ID = GUT_ID;
    }

    @Override
    public String toString() {
        return "ClassPojo [GUT_NAME = " + GUT_NAME + ", VILLAGE_NAME = " + VILLAGE_NAME + ", OBJECT_ID = " + OBJECT_ID + ", GUT_ID = " + GUT_ID + "]";
    }
}

