package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 10-09-2017.
 */

public class AllImageDataDeleteCheckModel {
    boolean value;
    String img_name;

    public boolean isValue() {
        return value;
    }

    public void setValue(boolean value) {
        this.value = value;
    }

    public String getImg_name() {
        return img_name;
    }

    public void setImg_name(String img_name) {
        this.img_name = img_name;
    }
}
