package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;

public class PrivacyPolicy extends AppCompatActivity {

    ImageView goBackImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_privacy_policy);

            goBackImageView = (ImageView)findViewById(R.id.privacy_policy_go_back);
            goBackImageView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    finish();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
