package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.AlertDialog;
import android.app.ProgressDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.annotation.NonNull;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Dialog.MessageDidplayDialog;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.CalculateAreaFromGeoLocation;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.services.GoogleService;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;
import java.util.UUID;

public class GetGeoLocationAndImagesActivity extends AppCompatActivity {
    TextView first_lat, first_long, second_lat, second_long, third_lat, third_long, fourth_lat, fourth_long;
    CheckBox img_first, img_second, img_three, img_four;
    ImageView backActivity;
    private Uri file;
    ArrayList<Double> lat = new ArrayList();
    ArrayList<Double> longs = new ArrayList();
    String firstImgName = "";
    String secondImgName = "";
    String threeImgName = "";
    String fourImgName = "";
    Button btn_geo_submit;
    String plantationID;
    String flag = "";
    String selectedNumberOfFile = "";
    String URL = "";
    public ProgressDialog pdLoading;
    public String filePathName = "";
    //storage permission code
    private static final int STORAGE_PERMISSION_CODE = 123;
    String imgFileName = "";
    private SQLiteHelper sqLiteHelper;
    public static double latit = 0, longi = 0;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_get_geo_location_and_images);
            ignoreURIExposure();
            init();
            Intent intent = getIntent();
            Preferences.appContext = this;
            plantationID = intent.getStringExtra("plantationID");
            flag = intent.getStringExtra("flag");
            if (flag.equals(AppConstants.PLANTATION_CONSTANT)) {
                URL = AppConstants.UPLOAD_FOUR_IMAGE_PLANTATION;
            } else if (flag.equals(AppConstants.RUJVAT_CONSTANT)) {
                URL = AppConstants.UPLOAD_FOUR_IMAGE_RUJVAT;
            }

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }

            try {
                img_first.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        try {
                            if (img_first.isChecked()) {
                                Preferences.appContext = GetGeoLocationAndImagesActivity.this;
                                CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);

                                first_lat.setText(locationModel.getLatitude() + "");
                                first_long.setText(locationModel.getLongitude() + "");

                               /* first_lat.setText(GoogleService.location.getLatitude() + "");
                                first_long.setText(GoogleService.location.getLongitude() + "");*/
                                lat.add(locationModel.getLatitude());
                                longs.add(locationModel.getLongitude());

                                selectedNumberOfFile = AppConstants.FIRST_IMAGE;
                                takePicture(AppConstants.FIRST_IMAGE, flag);
                            }
                        } catch (Exception e) {
                            e.printStackTrace();
                            Toast.makeText(GetGeoLocationAndImagesActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
//                    first_lat.setText(AppConstants.latitude + "");
//                    first_long.setText(AppConstants.longitude + "");
                        }

                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            try {
                img_second.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        try {
                            if (img_second.isChecked()) {
//                    getCurrentLocation(second_lat, second_long);
//                    secondLocation.setEnabled(false);
                                Preferences.appContext = GetGeoLocationAndImagesActivity.this;
                                CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);

                                second_lat.setText(locationModel.getLatitude() + "");
                                second_long.setText(locationModel.getLongitude() + "");

                                lat.add(locationModel.getLatitude());
                                longs.add(locationModel.getLongitude());

                                selectedNumberOfFile = AppConstants.SECOND_IMAGE;
                                takePicture(AppConstants.SECOND_IMAGE, flag);
                            }
                        } catch (Exception e) {
                            Toast.makeText(GetGeoLocationAndImagesActivity.this, e.getMessage() + "", Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            try {
                img_three.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        try {
                            if (img_three.isChecked()) {
//                    getCurrentLocation(third_lat, third_long);
//                    thirdLocation.setEnabled(false);
                                /*third_lat.setText(GoogleService.location.getLatitude() + "");
                                third_long.setText(GoogleService.location.getLongitude() + "");
                                lat.add(AppConstants.latitude);
                                longs.add(AppConstants.longitude);*/
                                Preferences.appContext = GetGeoLocationAndImagesActivity.this;
                                CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);

                                third_lat.setText(locationModel.getLatitude() + "");
                                third_long.setText(locationModel.getLongitude() + "");

                                lat.add(locationModel.getLatitude());
                                longs.add(locationModel.getLongitude());

                                selectedNumberOfFile = AppConstants.THREE_IMAGE;
                                takePicture(AppConstants.THREE_IMAGE, flag);
                            }
                        } catch (Exception e) {
                            Toast.makeText(GetGeoLocationAndImagesActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        }


                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            try {
                img_four.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        try {
                            if (img_four.isChecked()) {
//                    getCurrentLocation(fourth_lat, fourth_long);
//                    fourthLocation.setEnabled(false);
                               /* fourth_lat.setText(GoogleService.location.getLatitude() + "");
                                fourth_long.setText(GoogleService.location.getLongitude() + "");
                                lat.add(AppConstants.latitude);
                                longs.add(AppConstants.longitude);*/

                                Preferences.appContext = GetGeoLocationAndImagesActivity.this;
                                CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);

                                fourth_lat.setText(locationModel.getLatitude() + "");
                                fourth_long.setText(locationModel.getLongitude() + "");

                                lat.add(locationModel.getLatitude());
                                longs.add(locationModel.getLongitude());

                                double area = new CalculateAreaFromGeoLocation().getArea(lat, longs, GetGeoLocationAndImagesActivity.this);
                                Toast.makeText(getApplicationContext(), "Area " + area, Toast.LENGTH_LONG).show();

                                selectedNumberOfFile = AppConstants.FOUR_IMAGE;
                                takePicture(AppConstants.FOUR_IMAGE, flag);
                            }
                        } catch (Exception e) {
                            Toast.makeText(GetGeoLocationAndImagesActivity.this, e.getMessage().toString(), Toast.LENGTH_SHORT).show();
                        }
                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            try {
                backActivity.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        finish();
                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }

            try {
                btn_geo_submit.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {

                        if (validateData()) {
                            pdLoading.show();
//                    boolean b = uploadMultipart();
                            boolean b = saveDataIntoDataBase();
                            if (b) {
                                if (flag.equals(AppConstants.PLANTATION_CONSTANT)) {
                                    pdLoading.dismiss();
                                    Preferences.setPlantationUploadStatus(false);
                                    Preferences.setImageRenameStatus(false);
                                    Preferences.setAllImageUploadStatus(false);
                                    callMeaasgeDialog(getString(R.string.plantation_successful));
                                } else if (flag.equals(AppConstants.RUJVAT_CONSTANT)) {
                                    pdLoading.dismiss();
                                    Preferences.setRujvatUploadStatus(false);
                                    Preferences.setRujvatImgStatus(false);
                                    callMeaasgeDialog(getString(R.string.rujvat_successful));
                                }
                            } else {
                                if (flag.equals(AppConstants.PLANTATION_CONSTANT)) {
                                    callMeaasgeDialog(getString(R.string.plantation_fail));
                                } else if (flag.contains(AppConstants.RUJVAT_CONSTANT)) {
                                    callMeaasgeDialog(getString(R.string.rujvat_fail));
                                }
                            }
                        }
                    }
                });
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private void ignoreURIExposure() {
        try {
            if (Build.VERSION.SDK_INT >= 24) {
                StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
                StrictMode.setVmPolicy(builder.build());
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void callMeaasgeDialog(String message) {
        try {
            final MessageDidplayDialog messageDidplayDialog = new MessageDidplayDialog(GetGeoLocationAndImagesActivity.this);
            messageDidplayDialog.show();
            messageDidplayDialog.setCancelable(false);
            messageDidplayDialog.message_txt.setText(message);
            messageDidplayDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                                   @Override
                                                                   public void onClick(final View view) {
                                                                       Intent intent = new Intent(GetGeoLocationAndImagesActivity.this, MenuActivityPhase1.class);
                                                                       intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TASK | Intent.FLAG_ACTIVITY_CLEAR_TOP);
                                                                       startActivity(intent);
                                                                       overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                                                                       pdLoading.dismiss();
                                                                       finish();
                                                                   }
                                                               }
            );
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            Intent intent = new Intent(this, GoogleService.class);
            getApplication().startService(intent);

            btn_geo_submit = (Button) findViewById(R.id.btn_geo_submit);
            first_lat = (TextView) findViewById(R.id.first_lat);
            first_long = (TextView) findViewById(R.id.first_long);
            second_lat = (TextView) findViewById(R.id.second_lat);
            second_long = (TextView) findViewById(R.id.second_long);
            third_lat = (TextView) findViewById(R.id.third_lat);
            third_long = (TextView) findViewById(R.id.third_long);
            fourth_lat = (TextView) findViewById(R.id.fourth_lat);
            fourth_long = (TextView) findViewById(R.id.fourth_long);
            img_first = (CheckBox) findViewById(R.id.img_first);
            img_second = (CheckBox) findViewById(R.id.img_second);
            img_three = (CheckBox) findViewById(R.id.img_three);
            img_four = (CheckBox) findViewById(R.id.img_four);
            backActivity = (ImageView) findViewById(R.id.backActivity);
            sqLiteHelper = new SQLiteHelper(this);
            pdLoading = new ProgressDialog(GetGeoLocationAndImagesActivity.this);
            pdLoading.setCancelable(false);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean uploadMultipart() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CaneApp");
        //Uploading code
        try {
            String uploadId = UUID.randomUUID().toString();
            //Creating a multi part request
            new MultipartUploadRequest(this, uploadId, URL)
                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + firstImgName, "img_1") //Adding file
                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + secondImgName, "img_2") //Adding file
                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + threeImgName, "img_3") //Adding file
                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + fourImgName, "img_4") //Adding file
                    .addParameter("p_plant_id", plantationID + "") //Adding text parameter to the request
                    .addParameter("image_1", firstImgName) //Adding text parameter to the request
                    .addParameter("image_2", secondImgName) //Adding text parameter to the request
                    .addParameter("image_3", threeImgName) //Adding text parameter to the request
                    .addParameter("image_4", fourImgName) //Adding text parameter to the request
                    .addParameter("lat_1", first_lat.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("lat_2", second_lat.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("lat_3", third_lat.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("lat_4", fourth_lat.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("log_1", first_long.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("log_2", second_long.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("log_3", third_long.getText().toString().trim()) //Adding text parameter to the request
                    .addParameter("log_4", fourth_long.getText().toString().trim()) //Adding text parameter to the request
                    .setNotificationConfig(new UploadNotificationConfig())
                    .setMaxRetries(2)
                    .startUpload(); //Starting the upload

            return true;
        } catch (Exception exc) {
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public boolean saveDataIntoDataBase() {
        try {
            sqLiteHelper.insertPlantationAndRujvatImageSubmit(plantationID + "", firstImgName, secondImgName, threeImgName, fourImgName, first_lat.getText().toString().trim(), second_lat.getText().toString().trim(), third_lat.getText().toString().trim(), fourth_lat.getText().toString().trim(), first_long.getText().toString().trim(), second_long.getText().toString().trim(), third_long.getText().toString().trim(), fourth_long.getText().toString().trim(), flag);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    //Requesting permission
    private void requestStoragePermission() {
        try {
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.READ_EXTERNAL_STORAGE) == PackageManager.PERMISSION_GRANTED)
                return;
            if (ActivityCompat.shouldShowRequestPermissionRationale(this, Manifest.permission.READ_EXTERNAL_STORAGE)) {
                //If the user has denied the permission previously your code will come to this block
                //Here you can explain why you need this permission
                //Explain here why you need this permission
            }
            //And finally ask for the permission
            ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.READ_EXTERNAL_STORAGE}, STORAGE_PERMISSION_CODE);

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //This method will be called when the user will tap on allow or deny
    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        try {
            //Checking the request code of our request
            if (requestCode == STORAGE_PERMISSION_CODE) {

                //If permission is granted
                if (grantResults.length > 0 && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                    //Displaying a toast
                    Toast.makeText(this, getString(R.string.permission_granted), Toast.LENGTH_LONG).show();
                } else {
                    //Displaying another toast if permission is not granted
                    Toast.makeText(this, getString(R.string.permission_denied), Toast.LENGTH_LONG).show();
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void takePicture(String img, String flag) {
        try {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            file = Uri.fromFile(getOutputMediaFile(img, flag));
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
            startActivityForResult(intent, 1888);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private File getOutputMediaFile(String img, String flag) {
        File file = new File("");
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CameraDemo", getString(R.string.failed_create_directory));
                    return null;
                }
            } else {
                File fileIn = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                try {
                    if (fileIn.exists())
                        Log.d("Trp Camera File", "File Already Exists");
                    else
                        fileIn.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }

            String timeStamp = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
            if (selectedNumberOfFile.equals(AppConstants.FIRST_IMAGE)) {
                firstImgName = img + plantationID + "_" + flag + "_" + timeStamp + ".jpg";
                filePathName = mediaStorageDir.getPath() + File.separator + firstImgName;
                imgFileName = firstImgName;
                file = new File(filePathName);
            } else if (selectedNumberOfFile.equals(AppConstants.SECOND_IMAGE)) {
                secondImgName = img + plantationID + "_" + flag + "_" + timeStamp + ".jpg";
                filePathName = mediaStorageDir.getPath() + File.separator + secondImgName;
                imgFileName = secondImgName;
                file = new File(filePathName);
            } else if (selectedNumberOfFile.equals(AppConstants.THREE_IMAGE)) {
                threeImgName = img + plantationID + "_" + flag + "_" + timeStamp + ".jpg";
                filePathName = mediaStorageDir.getPath() + File.separator + threeImgName;
                imgFileName = threeImgName;
                file = new File(filePathName);
            } else if (selectedNumberOfFile.equals(AppConstants.FOUR_IMAGE)) {
                fourImgName = img + plantationID + "_" + flag + "_" + timeStamp + ".jpg";
                filePathName = mediaStorageDir.getPath() + File.separator + fourImgName;
                imgFileName = fourImgName;
                file = new File(filePathName);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return file;
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = 0;
        int height = 0;
        try {
            width = image.getWidth();
            height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }


    private void saveImage(Bitmap finalBitmap) {
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CaneApp", getString(R.string.failed_create_directory));
                }
            }
            File file = new File(mediaStorageDir.getPath() + File.separator + imgFileName);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1888) {
                if (resultCode == RESULT_OK) {
//                imageView.setImageURI(file);
//                if (requestCode == RESULT_OK && data != null && data.getData() != null) {
//                if (requestCode == RESULT_OK) {
                    Bitmap photo = null;
                    try {
                        photo = MediaStore.Images.Media.getBitmap(this.getContentResolver(), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    saveImage(getResizedBitmap(photo, 800));
                } else {
                    if (selectedNumberOfFile.equals(AppConstants.FIRST_IMAGE)) {
                        firstImgName = "";
                    } else if (selectedNumberOfFile.equals(AppConstants.SECOND_IMAGE)) {
                        secondImgName = "";
                    } else if (selectedNumberOfFile.equals(AppConstants.THREE_IMAGE)) {
                        threeImgName = "";
                    } else if (selectedNumberOfFile.equals(AppConstants.FOUR_IMAGE)) {
                        fourImgName = "";
                    }
                    filePathName = "";
                    imgFileName = "";
                }
            } else {
                Toast.makeText(this, getString(R.string.request_not_match), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateData() {
        try {
            if (first_lat.getText().toString().equals(getString(R.string.latitude)) || first_long.getText().toString().equals(getString(R.string.longitude))) {
                Toast.makeText(this, getString(R.string.enter_location_1), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (firstImgName.equals("")) {
                Toast.makeText(this, getString(R.string.first_picture), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (second_lat.getText().toString().equals(getString(R.string.latitude)) || second_long.getText().toString().equals(getString(R.string.longitude))) {
                Toast.makeText(this, getString(R.string.enter_location_2), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (secondImgName.equals("")) {
                Toast.makeText(this, getString(R.string.second_picture), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (third_lat.getText().toString().equals(getString(R.string.latitude)) || third_long.getText().toString().equals(getString(R.string.longitude))) {
                Toast.makeText(this, getString(R.string.enter_location_3), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (threeImgName.equals("")) {
                Toast.makeText(this, getString(R.string.third_picture), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (fourth_lat.getText().toString().equals(getString(R.string.latitude)) || fourth_long.getText().toString().equals(getString(R.string.longitude))) {
                Toast.makeText(this, getString(R.string.enter_location_4), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (fourImgName.equals("")) {
                Toast.makeText(this, getString(R.string.fourth_picture), Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    @Override
    public void onBackPressed() {
        try {
            final AlertDialog.Builder builder = new AlertDialog.Builder(this);
            builder.setTitle(getString(R.string.confirm_back));
            builder.setMessage(getString(R.string.exit_plantation));
            builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    dialogInterface.dismiss();
                }
            });

            builder.setPositiveButton(getString(R.string.Yes), new DialogInterface.OnClickListener() {
                @Override
                public void onClick(DialogInterface dialogInterface, int i) {
                    finish();
                }
            });
            builder.show();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
