package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Activity.Phase1.GetGeoLocationAndImagesActivity;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Dialog.PlantationCodeDialog;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.XMLManager;
import com.praful.jass.ismclcaneapp.Models.CaneTypeListModel;
import com.praful.jass.ismclcaneapp.Models.PlantationDataForRujvatModel;
import com.praful.jass.ismclcaneapp.Models.RujvatUpdationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;
import com.praful.jass.ismclcaneapp.services.CallSoap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class RujvatFragment extends Fragment {
    private LinearLayout layout_rujvat_reason;
    private TextView rujvat_reason, r_grower_name, r_village, r_gut_id, r_sub_gut_id, r_taluka, r_shivar_village, r_shivar_gut, r_survey_number, r_irrigation_source, r_cane_variety, r_plantation_date, r_plantation_type, r_plantation_area, r_drip;
    private Button btn_rujvat_submit;
    private EditText r_rujvat_area, r_brix_meter_reading, r_cane_stick_count, r_cane_stick_length, r_cane_tonage;
    private Toolbar toolbar;
    int selectListType = 0;
    private SQLiteHelper sqLiteHelper;
    private CallSoap cs;
    private XMLManager xmlManager;
    private FragmentManager fragmentManager;
    private OptionListFragment fragment;
    private ArrayList<String> namLlist = new ArrayList();
    private ArrayList<CaneTypeListModel> deatilsList;
    private FragmentTransaction fragmentTransaction;
    String plantationIDForRujvat = "";
    public ProgressDialog pdLoading;
    private PlantationDataForRujvatModel plantationDataForRujvatModel;
    private View view1;
    private String rujvatReasonID = "";
    private SystemDateTime dateTime = new SystemDateTime();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view1 = inflater.inflate(R.layout.fragment_rujvat, container, false);
        init(view1);

        getPlantationDialog(view1);

        btn_rujvat_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
//                if (CheckInternet.isNetworkAvailable(getContext())) {
                if (validateData()) {
//                        submitRujvatData(AppConstants.URL_UPDATE_RUJVAT_REASON, RujvatUpdationModel.class);
                    submitRujvatData();
                    Snackbar.make(view1, getString(R.string.submitted_successfully), Snackbar.LENGTH_LONG).show();
                    Intent intent = new Intent(getContext(), GetGeoLocationAndImagesActivity.class);
                    intent.putExtra("plantationID", plantationIDForRujvat);
                    intent.putExtra("flag", AppConstants.RUJVAT_CONSTANT);
                    startActivity(intent);
                }
//                } else {
//                    Snackbar.make(view1.findViewById(R.id.fragment_rujvat), "Check Internet Connection", Snackbar.LENGTH_LONG).show();
//                }
            }
        });

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.nav_rujvat));

        layout_rujvat_reason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectListType = AppConstants.RUJVAT_REASON_SELECT;
                new AsyncCaller().execute();
            }
        });

        r_brix_meter_reading.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_brix_meter_reading.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_brix_meter_reading.setTypeface(null, Typeface.BOLD);
            }
        });
        r_cane_stick_count.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_cane_stick_count.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_cane_stick_count.setTypeface(null, Typeface.BOLD);
            }
        });
        r_cane_stick_length.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_cane_stick_length.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_cane_stick_length.setTypeface(null, Typeface.BOLD);
            }
        });
        r_cane_tonage.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_cane_tonage.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_cane_tonage.setTypeface(null, Typeface.BOLD);
            }
        });
        r_drip.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_drip.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_drip.setTypeface(null, Typeface.BOLD);
            }
        });
        r_rujvat_area.setOnFocusChangeListener(new View.OnFocusChangeListener() {
            @Override
            public void onFocusChange(View view, boolean b) {
                r_rujvat_area.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                r_rujvat_area.setTypeface(null, Typeface.BOLD);
            }
        });

        // Inflate the layout for this fragment
        return view1;
    }
//    199109

    public void init(View view) {
        btn_rujvat_submit = (Button) view.findViewById(R.id.btn_rujvat_submit);
        layout_rujvat_reason = (LinearLayout) view.findViewById(R.id.layout_rujvat_reason);
        rujvat_reason = (TextView) view.findViewById(R.id.rujvat_reason);
        r_grower_name = (TextView) view.findViewById(R.id.r_grower_name);
        r_village = (TextView) view.findViewById(R.id.r_village);
        r_gut_id = (TextView) view.findViewById(R.id.r_gut_id);
        r_sub_gut_id = (TextView) view.findViewById(R.id.r_sub_gut_id);
        r_taluka = (TextView) view.findViewById(R.id.r_taluka);
        r_shivar_village = (TextView) view.findViewById(R.id.r_shivar_village);
        r_shivar_gut = (TextView) view.findViewById(R.id.r_shivar_gut);
        r_survey_number = (TextView) view.findViewById(R.id.r_survey_number);
        r_irrigation_source = (TextView) view.findViewById(R.id.r_irrigation_source);
        r_cane_variety = (TextView) view.findViewById(R.id.r_cane_variety);
        r_plantation_date = (TextView) view.findViewById(R.id.r_plantation_date);
        r_plantation_type = (TextView) view.findViewById(R.id.r_plantation_type);
        r_plantation_area = (TextView) view.findViewById(R.id.r_plantation_area);
        r_brix_meter_reading = (EditText) view.findViewById(R.id.r_brix_meter_reading);
        r_cane_stick_count = (EditText) view.findViewById(R.id.r_cane_stick_count);
        r_cane_stick_length = (EditText) view.findViewById(R.id.r_cane_stick_length);
        r_cane_tonage = (EditText) view.findViewById(R.id.r_cane_tonage);
        r_drip = view.findViewById(R.id.r_drip);
        r_rujvat_area = (EditText) view.findViewById(R.id.r_rujvat_area);

        sqLiteHelper = new SQLiteHelper(getContext());
        pdLoading = new ProgressDialog(getActivity());
        pdLoading.setCancelable(false);
        cs = new CallSoap();
        fragmentManager = getActivity().getSupportFragmentManager();
        xmlManager = new XMLManager();
    }

    private void getPlantationDialog(final View view1) {

        final PlantationCodeDialog plantationCodeDialog = new PlantationCodeDialog(getContext());
        plantationCodeDialog.show();
        plantationCodeDialog.setCancelable(false);
        plantationCodeDialog.btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
                plantationCodeDialog.dismiss();
            }
        });

        plantationCodeDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(final View view) {
                if (plantationCodeDialog.plantationCode.getText().toString().equals("")) {
//                    runOnUiThread(new Runnable() {
//                        @Override
//                        public void run() {
//                            Toast.makeText(getActivity().getApplicationContext(), "Please Enter Plantation Code" , Toast.LENGTH_LONG).show();
                    Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.enter_plantation_code), Snackbar.LENGTH_LONG).show();
//                        }
//                    });
                } else {
//                    if (CheckInternet.isNetworkAvailable(getContext())) {
                    plantationIDForRujvat = plantationCodeDialog.plantationCode.getText().toString();
//                    getPlantationDataForRujvat(AppConstants.URL_PLANTATION_DATA_FOR_RUJVAT, PlantationDataForRujvatModel.class);
                    getPlantationDataForRujvatFromDB(plantationIDForRujvat);
                    plantationCodeDialog.dismiss();
//                    } else {
//                        Snackbar.make(view1.findViewById(R.id.fragment_rujvat), "Check Internet Connection", Snackbar.LENGTH_LONG).show();
//                    }
//                    Snackbar.make(view1.findViewById(R.id.fragment_rujvat), plantationIDForRujvat + "", Snackbar.LENGTH_LONG).show();

                }
            }
        });
    }

    private ArrayList<String> getJsoneManager(String jsone) {

        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), CaneTypeListModel.class, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    deatilsList = new ArrayList();
                    if (resultObj instanceof ArrayList) {
                        if (selectListType == AppConstants.RUJVAT_REASON_SELECT) {
                            namLlist = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getReason());
                            }
                        }
                    }
                }

                @Override
                public void onError(String strError) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return namLlist;
    }


    private void showOptionList(ArrayList<String> listArray, final TextView textView) {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(listArray, textView.getText().toString());
        fragment.setListTitle("kljklkllk");

        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                removeOptionList();
            }

            @Override
            public void onListItemSelected(Object model) {


                // you can use this model to show selected value in the text
                if (model instanceof String) {

                    String value = (String) model;
                    textView.setText(value);
                    textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    textView.setTypeface(null, Typeface.BOLD);
                    rujvatReasonID = deatilsList.get(AppConstants.selectItemIndex).getReasonID();
                }
                AppConstants.selectItemIndex = 0;
                removeOptionList();
            }
        };
        ft.add(R.id.fragment_rujvat, fragment);
        ft.commit();
        pdLoading.dismiss();

    }

    void removeOptionList() {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        ft.remove(fragment).commit();
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void> {


//        pdLoading=new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\t" + getString(R.string.loading));
            pdLoading.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
            if (selectListType == AppConstants.RUJVAT_REASON_SELECT) {
                String rujvatReason = sqLiteHelper.getRujvatReason();
                showOptionList(getJsoneManager(rujvatReason), rujvat_reason);
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //this method will be running on UI thread

            pdLoading.dismiss();
        }

    }

    //    {
//        "user_id":11,
//            "plantation":19,
//            "season":7
//    }
    void getPlantationDataForRujvatFromDB(String plCode) {
        ArrayList<String> rujvatDataRJ = sqLiteHelper.getRujvatDataRJ(plCode);
        try {
            r_grower_name.setText(rujvatDataRJ.get(1));
            r_village.setText(rujvatDataRJ.get(3));
            r_gut_id.setText(rujvatDataRJ.get(4));
            r_sub_gut_id.setText(rujvatDataRJ.get(5));
            r_taluka.setText(rujvatDataRJ.get(6));
            r_shivar_village.setText(rujvatDataRJ.get(7));
            r_shivar_gut.setText(rujvatDataRJ.get(8));
            r_survey_number.setText(rujvatDataRJ.get(7));
            r_irrigation_source.setText(rujvatDataRJ.get(8));
            r_cane_variety.setText(rujvatDataRJ.get(11));
            r_plantation_date.setText(rujvatDataRJ.get(12));
            r_plantation_type.setText(rujvatDataRJ.get(13));
            r_plantation_area.setText(rujvatDataRJ.get(14));
        } catch (Exception e) {
//            Toast.makeText(getContext(), plCode + " is not available", Toast.LENGTH_SHORT).show();
            getPlantationDialog(r_cane_variety);
        }

    }

    void getPlantationDataForRujvat(String apiUrl, Class modelType) {
        JSONObject jsonObject = new JSONObject();
        jsonObject = new JSONObject();

        try {
            jsonObject.put("user_id", Preferences.getCurrentUserId());
            jsonObject.put("plantation", plantationIDForRujvat);
            jsonObject.put("season", Integer.parseInt(Preferences.getSeasonID()));
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Show loading bar
        pdLoading.show();

        new APIRestManager().postAPI(apiUrl, jsonObject, getActivity(), modelType, new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObj) {

                if (resultObj instanceof PlantationDataForRujvatModel) {
                    plantationDataForRujvatModel = (PlantationDataForRujvatModel) resultObj;
                    r_grower_name.setText(plantationDataForRujvatModel.getFARMER_NAME());
                    r_village.setText(plantationDataForRujvatModel.getVILLAGE_NAME());
                    r_gut_id.setText(plantationDataForRujvatModel.getGUT_NAME());
                    r_sub_gut_id.setText(plantationDataForRujvatModel.getSUB_GUT_NAME());
                    r_taluka.setText(plantationDataForRujvatModel.getTALUKA_NAME());
                    r_shivar_village.setText(plantationDataForRujvatModel.getSHIVAR_VILLAGE_NAME());
                    r_shivar_gut.setText(plantationDataForRujvatModel.getSUB_GUT_NAME());
                    r_survey_number.setText(plantationDataForRujvatModel.getSARVE_NO());
                    r_irrigation_source.setText(plantationDataForRujvatModel.getIRRE_NAME());
                    r_cane_variety.setText(plantationDataForRujvatModel.getVARITY());
                    r_plantation_date.setText(plantationDataForRujvatModel.getPLANTATION_DATE());
                    r_plantation_type.setText(plantationDataForRujvatModel.getPLANTATION_TYPE());
                    r_plantation_area.setText(plantationDataForRujvatModel.getPLANTATION_AREA());
                } else if (resultObj instanceof String) {
                    Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.no_record_found), Snackbar.LENGTH_LONG).show();
                    getPlantationDialog(view1);
                }
                pdLoading.dismiss();
            }

            @Override
            public void onError(String strError) {
                //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
                Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                getPlantationDialog(view1);
                pdLoading.dismiss();

            }
        });
    }

    void submitRujvatData() {
        sqLiteHelper.insertRjvatReasonData(plantationIDForRujvat, r_rujvat_area.getText().toString(), rujvatReasonID, dateTime.systemDateTime("dd-MMM-yyyy"), Preferences.getCurrentUserId() + "", r_brix_meter_reading.getText().toString(), r_cane_stick_count.getText().toString(), r_cane_stick_length.getText().toString(), r_cane_tonage.getText().toString(), "Y");
    }

    void submitRujvatData(String apiUrl, Class modelType) {
        JSONObject jsonObject = new JSONObject();
        JSONObject outJsonObject = new JSONObject();

        try {
            jsonObject.put("p_object_id", plantationDataForRujvatModel.getOBJECT_ID());
            jsonObject.put("p_area", r_rujvat_area.getText().toString());
            jsonObject.put("p_reason", rujvatReasonID);

            JSONArray jsonArray = new JSONArray();
            jsonArray.put(jsonObject);

            outJsonObject.put("RujvatData", jsonArray);

        } catch (JSONException e) {
            e.printStackTrace();
        }

        //Show loading bar
        pdLoading.show();

        new APIRestManager().postRujvatData(apiUrl, outJsonObject, getActivity(), modelType, new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObj) {

                if (resultObj instanceof ArrayList) {
                    ArrayList<RujvatUpdationModel> arrayList = (ArrayList<RujvatUpdationModel>) resultObj;
                    if (arrayList.get(0).getValue().equals("1")) {
                        Intent intent = new Intent(getContext(), GetGeoLocationAndImagesActivity.class);
                        intent.putExtra("plantationID", plantationIDForRujvat);
                        intent.putExtra("flag", AppConstants.RUJVAT_CONSTANT);
                        startActivity(intent);
                    } else {
                        Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();

                    }
                } else {
                    Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                }
                pdLoading.dismiss();
            }

            @Override
            public void onError(String strError) {
                //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
                Snackbar.make(view1.findViewById(R.id.fragment_rujvat), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                getPlantationDialog(view1);
                pdLoading.dismiss();

            }
        });
    }

    private boolean validateData() {
        if (r_brix_meter_reading.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (r_cane_stick_count.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (r_cane_stick_length.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (r_cane_tonage.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (r_drip.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (r_rujvat_area.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.enter_rujvat_area), Toast.LENGTH_SHORT).show();
            return false;
        }
        if (rujvat_reason.getText().toString().equals("")) {
            Toast.makeText(getContext(), getString(R.string.select_reason_rujvat), Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }
}
