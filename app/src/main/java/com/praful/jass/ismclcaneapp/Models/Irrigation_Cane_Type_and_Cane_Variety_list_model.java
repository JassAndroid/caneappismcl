package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-07-2017.
 */

public class Irrigation_Cane_Type_and_Cane_Variety_list_model {
    private String NAME;

    private String OBJECT_ID;

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    @Override
    public String toString() {
        return "ClassPojo [NAME = " + NAME + ", OBJECT_ID = " + OBJECT_ID + "]";
    }
}
