package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Activity.Phase1.UpdateDatabase;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.Models.AllImageDataDeleteCheckModel;
import com.praful.jass.ismclcaneapp.Models.KYCModel;
import com.praful.jass.ismclcaneapp.Models.PlantationKeyAndIDModel;
import com.praful.jass.ismclcaneapp.Models.RujvatReasonAndKYCUpdateOutputModel;
import com.praful.jass.ismclcaneapp.Models.SubmitPlantationDataModel;
import com.praful.jass.ismclcaneapp.Models.TripModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.CheckInternet;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.ServerResponse;
import net.gotev.uploadservice.UploadInfo;
import net.gotev.uploadservice.UploadNotificationConfig;
import net.gotev.uploadservice.UploadStatusDelegate;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.UUID;

public class UpdateServerFragment extends Fragment {

    private Toolbar toolbar;
    private TextView txt_update_trip_info, txt_plantation_update, txt_rename_files, txt_rujvat_update, txt_uploads_images, txt_delete_records_images, txt_uploads_rujvat_images, txt_upload_trip_image, txt_kyc_info, txt_kyc_images, txt_update_local_db;
    private SQLiteHelper sqLiteHelper;
    public ProgressDialog pdLoading;
    int selectButton = 0;
    File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CaneApp");
    View view;
    ArrayList deatilsList;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            // Inflate the layout for this fragment
            View view = inflater.inflate(R.layout.fragment_update_server, container, false);
            this.view = view;
            Preferences.appContext = getContext();
            init(view);

            txt_update_trip_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectButton = AppConstants.UPLOAD_TRIP_INFORMATION;
                    if (CheckInternet.isNetworkAvailable(getContext())) {
                        pdLoading.show();
                        submitEmployeeTripDataOnServer();
                    } else {
                        Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            txt_upload_trip_image.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (CheckInternet.isNetworkAvailable(getContext())) {
                        if (!txt_update_trip_info.isEnabled()) {
                            pdLoading.show();
                            submitEmployeeTripImgage();
                        } else
                            Snackbar.make(v, getString(R.string.upload_trip_info), Snackbar.LENGTH_LONG).show();
                    } else {
                        Snackbar.make(v, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                    }
                }
            });

            txt_plantation_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectButton = AppConstants.UPDATE_PLANTATION_SELECT;
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            uploadPlantationData();
                        } else {
                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_rename_files.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            if (!txt_plantation_update.isEnabled()) {
                                pdLoading.show();
                                renameAllfileNameInDatabase();
                            } else {
                                Snackbar.make(view, getString(R.string.upload_plantation_update), Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_rujvat_update.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectButton = AppConstants.UPDATE_RUJVAT_SELECT;
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            uploadRujvatData();
                        } else {
                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_uploads_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            if (!txt_rename_files.isEnabled() && !txt_rename_files.isEnabled()) {
                                uploadPlantationImageWithData();
                            } else {
                                Snackbar.make(view, getString(R.string.rename_plantation), Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_uploads_rujvat_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            if (!txt_rename_files.isEnabled()) {
                                uploadRujvatImageWithData();
                            } else {
                                Snackbar.make(v, getString(R.string.upload_rujvat_information), Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(v, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });

            txt_kyc_info.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectButton = AppConstants.UPLOAD_KYC_INFORMATION;
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            uploadKycData();
                        } else {
                            Snackbar.make(v, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_kyc_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            if (!txt_kyc_info.isEnabled()) {
                                uploadKYCImage();
                            } else {
                                Snackbar.make(v, getString(R.string.upload_kyc_information), Snackbar.LENGTH_LONG).show();
                            }
                        } else {
                            Snackbar.make(v, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_delete_records_images.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip_update), Snackbar.LENGTH_LONG).show();
                    } else {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            deleteImageAndAllSQLData();
                        } else {
                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }
            });
            txt_update_local_db.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    startActivity(new Intent(getContext(), UpdateDatabase.class));
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return view;
    }


//    public Notification getNotification() {
//        String channel;
//        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O)
//            channel = createChannel();
//        else {
//            channel = "";
//        }
//        NotificationCompat.Builder mBuilder = new NotificationCompat.Builder(getContext(), channel).setSmallIcon(android.R.drawable.ic_menu_mylocation).setContentTitle("snap map fake location");
//        Notification notification = mBuilder
//                .setPriority(Notification.PRIORITY_LOW)
//                .setCategory(Notification.CATEGORY_SERVICE)
//                .build();
//        return notification;
//    }

//    @NonNull
//    @TargetApi(26)
//    private synchronized String createChannel() {
//        NotificationManager mNotificationManager = (NotificationManager) getContext().getSystemService(Context.NOTIFICATION_SERVICE);
//
//        String name = "snap map fake location ";
//        int importance = NotificationManager.IMPORTANCE_LOW;
//
//        NotificationChannel mChannel = new NotificationChannel("snap map channel", name, importance);
//
//        mChannel.enableLights(true);
//        mChannel.setLightColor(Color.BLUE);
//        if (mNotificationManager != null) {
//            mNotificationManager.createNotificationChannel(mChannel);
//        } else {
////            stopSelf();
//        }
//        return "snap map channel";
//    }

    public void init(View view) {
        try {
            toolbar = getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.update_server));
            txt_update_trip_info = view.findViewById(R.id.txt_update_trip_info);
            txt_plantation_update = view.findViewById(R.id.txt_plantation_update);
            txt_rename_files = view.findViewById(R.id.txt_rename_files);
            txt_rujvat_update = view.findViewById(R.id.txt_rujvat_update);
            txt_uploads_images = view.findViewById(R.id.txt_uploads_plantation_images);
            txt_uploads_rujvat_images = view.findViewById(R.id.txt_uploads_rujvat_images);
            txt_upload_trip_image = view.findViewById(R.id.txt_upload_trip_image);
            txt_delete_records_images = view.findViewById(R.id.txt_delete_records_images);
            txt_kyc_info = view.findViewById(R.id.txt_kyc_info);
            txt_kyc_images = view.findViewById(R.id.txt_kyc_img);
            txt_update_local_db = view.findViewById(R.id.txt_get_updates);
            sqLiteHelper = new SQLiteHelper(getActivity());
            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);

            if (Preferences.getPlantationUploadStatus()) {
                txt_plantation_update.setEnabled(false);
                txt_plantation_update.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getImageRenameStatus()) {
                txt_rename_files.setEnabled(false);
                txt_rename_files.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getRujvatUploadStatus()) {
                txt_rujvat_update.setEnabled(false);
                txt_rujvat_update.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getAllImageUploadStatus()) {
                txt_uploads_images.setEnabled(false);
                txt_uploads_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (sqLiteHelper.getEmployeeTripData().get(0).length < 1) {
                txt_update_trip_info.setEnabled(false);
                txt_update_trip_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getRujvatImgStatus()) {
                txt_uploads_rujvat_images.setEnabled(false);
                txt_uploads_rujvat_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getTripImgStatus()) {
                txt_upload_trip_image.setEnabled(false);
                txt_upload_trip_image.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getKycInfoStatus()) {
                txt_kyc_info.setEnabled(false);
                txt_kyc_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
            if (Preferences.getKycImgStatus()) {
                txt_kyc_images.setEnabled(false);
                txt_kyc_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadPlantationData() {
//        String plantationData = sqLiteHelper.getPlantationData();
        try {
            String plantationData = sqLiteHelper.getPlantationData();
            if (plantationData.equals(AppConstants.TABLE_NOT_AVAILABLE) || plantationData.equals("{\"PlantationSubmit\":[]}")) {
                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.no_plantation_found), Snackbar.LENGTH_LONG).show();
            } else {
                submitBulkDataToServer(AppConstants.URL_PLANTATION_DATA_SUBMIT, new JSONObject(plantationData), SubmitPlantationDataModel.class);
            }
        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void renameAllfileNameInDatabase() {
        try {
            ArrayList<PlantationKeyAndIDModel> imageNameFromPlantationKey = sqLiteHelper.getPlantationKey();
            for (int i = 0; i < imageNameFromPlantationKey.size(); i++) {
                ArrayList<String> newNameList = new ArrayList();
                ArrayList<String> plantationImageDataToRename = sqLiteHelper.getPlantationImageDataToRename(imageNameFromPlantationKey.get(i).getKey());
                for (int j = 0; j < plantationImageDataToRename.size(); j++) {
                    newNameList.add(renameFile(plantationImageDataToRename.get(j), imageNameFromPlantationKey.get(i).getKey(), imageNameFromPlantationKey.get(i).getId()));
                }
                if (newNameList.size() > 0)
                    sqLiteHelper.updatePlantationImageName(imageNameFromPlantationKey.get(i).getKey(), newNameList.get(0), newNameList.get(1), newNameList.get(2), newNameList.get(3), imageNameFromPlantationKey.get(i).getId());
            }
            Toast.makeText(getContext(), getString(R.string.done), Toast.LENGTH_SHORT).show();
            pdLoading.dismiss();
        } catch (Exception ex) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadRujvatData() {
        try {
            String rujvatReasonData = sqLiteHelper.getRujvatReasonData();
            if (rujvatReasonData.equals("{\"RujvatData\":[]}")) {
                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.record_not_found_for_rujvat), Snackbar.LENGTH_LONG).show();
                txt_rujvat_update.setEnabled(true);
                txt_rujvat_update.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                Preferences.setRujvatUploadStatus(false);
            } else
                submitBulkDataToServer(AppConstants.URL_UPDATE_RUJVAT_REASON, new JSONObject(rujvatReasonData), RujvatReasonAndKYCUpdateOutputModel.class);
        } catch (JSONException ex) {
            ex.printStackTrace();
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadPlantationImageWithData() {
        try {
            pdLoading.show();
            uploadAllImagesAndData(AppConstants.PLANTATION_CONSTANT, AppConstants.UPLOAD_FOUR_IMAGE_PLANTATION_WITH_ARRAY);
        } catch (Exception ex) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadRujvatImageWithData() {
        try {
            pdLoading.show();
            uploadAllImagesAndData(AppConstants.RUJVAT_CONSTANT, AppConstants.UPLOAD_FOUR_IMAGE_RUJVAT_WITH_ARRAY);
        } catch (Exception ex) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadKYCImage() {
        try {
            pdLoading.show();
            uploadAllKycImages(AppConstants.URL_UPLOAD_KYC_IMAGE_ARRAY);
        } catch (Exception ex) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadKycData() {
        try {
            String kycData = sqLiteHelper.getKycData();
            if (kycData.equals("{\"KYCInfo\":[]}")) {
                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.record_not_found_for_kyc), Snackbar.LENGTH_LONG).show();
                txt_kyc_info.setEnabled(true);
                txt_kyc_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                Preferences.setKycInfoStatus(false);
            } else
                submitBulkDataToServer(AppConstants.URL_KYC_SUBMIT, new JSONObject(kycData), RujvatReasonAndKYCUpdateOutputModel.class);
        } catch (JSONException ex) {
            ex.printStackTrace();
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void deleteImageAndAllSQLData() {
        try {
            if (!txt_update_trip_info.isEnabled() && !txt_plantation_update.isEnabled() && !txt_rename_files.isEnabled() && !txt_rujvat_update.isEnabled() && !txt_uploads_images.isEnabled()) {
                sqLiteHelper.deletePlantationDataSubmit();
                sqLiteHelper.deleteRujvatReasonData();
                String allPlantationAndRujvatImageList = sqLiteHelper.getAllPlantationAndRujvatImageList();
                if (allPlantationAndRujvatImageList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE) || allPlantationAndRujvatImageList.equals("{\"imgNameList\":[]}"))
                    Snackbar.make(view, getString(R.string.image_record_not_found), Snackbar.LENGTH_LONG).show();
                else {
                    try {
                        selectButton = AppConstants.DELETE_ALL_DATA_SELECT;
                        submitBulkDataToServer(AppConstants.URL_AllPLANTATION_AND_RUJVAT_IMAGE_VERIFY, new JSONObject(allPlantationAndRujvatImageList), AllImageDataDeleteCheckModel.class);
                    } catch (Exception e) {//JSONException
                        e.printStackTrace();
                    }
                }
            } else {
                Snackbar.make(view, getString(R.string.perform_above_all_actions), Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadAllImagesAndData(final String flag, String URL) {
        try {
            String uploadId = UUID.randomUUID().toString();
//            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CaneApp");
            ArrayList<String[]> plantationAndRujvatImageData = sqLiteHelper.getPlantationAndRujvatImageData(flag);
            //Creating a multi part request
            if (plantationAndRujvatImageData.get(0).length > 0) {
                if (URL.contains("null")) {
                    URL = URL.replace("null", Preferences.getServerIP());
                }
                try {
                    MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(getContext(), uploadId, URL);
//            new MultipartUploadRequest(this, uploadId, AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY)
//                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9), "img_1") //Adding file

                    for (int i = 0; i < plantationAndRujvatImageData.get(9).length; i++) { //
                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + plantationAndRujvatImageData.get(1)[i], "img_1[]"); //Adding file
                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + plantationAndRujvatImageData.get(2)[i], "img_2[]"); //Adding file
                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + plantationAndRujvatImageData.get(3)[i], "img_3[]"); //Adding file
                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + plantationAndRujvatImageData.get(4)[i], "img_4[]"); //Adding file
                        multipartUploadRequest.addParameter("p_plant_id[]", plantationAndRujvatImageData.get(0)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("image_1[]", plantationAndRujvatImageData.get(1)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("image_2[]", plantationAndRujvatImageData.get(2)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("image_3[]", plantationAndRujvatImageData.get(3)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("image_4[]", plantationAndRujvatImageData.get(4)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("lat_1[]", plantationAndRujvatImageData.get(5)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("lat_2[]", plantationAndRujvatImageData.get(6)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("lat_3[]", plantationAndRujvatImageData.get(7)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("lat_4[]", plantationAndRujvatImageData.get(8)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("log_1[]", plantationAndRujvatImageData.get(9)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("log_2[]", plantationAndRujvatImageData.get(10)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("log_3[]", plantationAndRujvatImageData.get(11)[i]); //Adding text parameter to the request
                        multipartUploadRequest.addParameter("log_4[]", plantationAndRujvatImageData.get(12)[i]); //Adding text parameter to the request
                    }
//                    multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    } else {
                        multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
                    }
                    multipartUploadRequest.setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(UploadInfo uploadInfo) {

                        }

                        @Override
                        public void onError(UploadInfo uploadInfo, Exception exception) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.server_error_uploading), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {

                            if (flag.equalsIgnoreCase(AppConstants.PLANTATION_CONSTANT)) {
                                txt_uploads_images.setEnabled(false);
                                txt_uploads_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                Preferences.setAllImageUploadStatus(true);

                            } else if (flag.equalsIgnoreCase(AppConstants.RUJVAT_CONSTANT)) {
                                txt_uploads_rujvat_images.setEnabled(false);
                                txt_uploads_rujvat_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                Preferences.setRujvatImgStatus(true);
                            }
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_complete_successfully), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(UploadInfo uploadInfo) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_cancel_by_user), Toast.LENGTH_SHORT).show();
                        }
                    });
                    multipartUploadRequest.setMaxRetries(2);
                    multipartUploadRequest.startUpload();//Starting the upload
//            Toast.makeText(this, s + " Trip Upload", Toast.LENGTH_SHORT).show();
//                sqLiteHelper.deleteEmployeeTripData();
                } catch (Exception ex) {
                    pdLoading.dismiss();
                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception exc) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void uploadAllKycImages(String URL) {
        try {
            String uploadId = UUID.randomUUID().toString();
            ArrayList<KYCModel> kycImageData = sqLiteHelper.getKycDataArrayList();
            if (!kycImageData.get(0).getP_first_name().equals("")) {
                if (URL.contains("null")) {
                    URL = URL.replace("null", Preferences.getServerIP());
                }
                try {
                    MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(getContext(), uploadId, URL);
                    for (int i = 0; i < kycImageData.size(); i++) { //
                        if (!kycImageData.get(i).getP_aadhar_name().equals(""))
                            multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + kycImageData.get(i).getP_aadhar_name(), "img_1[]"); //Adding file
                        if (!kycImageData.get(i).getP_bank_name().equals(""))
                            multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + kycImageData.get(i).getP_bank_name(), "img_2[]"); //Adding file
                        if (!kycImageData.get(i).getP_vote_name().equals(""))
                            multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + kycImageData.get(i).getP_vote_name(), "img_3[]"); //Adding file
                        if (!kycImageData.get(i).getP_pan_name().equals(""))
                            multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + kycImageData.get(i).getP_pan_name(), "img_4[]"); //Adding file
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    } else {
                        multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
                    }

                    multipartUploadRequest.setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(UploadInfo uploadInfo) {

                        }

                        @Override
                        public void onError(UploadInfo uploadInfo, Exception exception) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.server_error_uploading), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {

                            txt_kyc_images.setEnabled(false);
                            txt_kyc_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                            Preferences.setKycImgStatus(true);
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_complete_successfully), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(UploadInfo uploadInfo) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_cancel_by_user), Toast.LENGTH_SHORT).show();
                        }
                    });
                    multipartUploadRequest.setMaxRetries(2);
                    multipartUploadRequest.startUpload();//Starting the upload
                } catch (Exception ex) {
                    pdLoading.dismiss();
                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        } catch (Exception exc) {
            pdLoading.dismiss();
            Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String renameFile(String name, String from, String to) {
        String newFileName = "";
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");
            ArrayList<String> childranList = new ArrayList<>(Arrays.asList(mediaStorageDir.list()));
            if (childranList.contains(name)) {
                File from_img = new File(mediaStorageDir, name);
                newFileName = changeStringCarector(name, from, to);
                File to_img = new File(mediaStorageDir, newFileName);
                from_img.renameTo(to_img);
            }
//            Toast.makeText(getContext(), "Done", Toast.LENGTH_SHORT).show();
            txt_rename_files.setEnabled(false);
            txt_rename_files.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            Preferences.setImageRenameStatus(true);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return newFileName;
    }

    private String changeStringCarector(String string, String from, String to) {
        String replace = "";
        try {
            String fromTemp = "_" + from + "_";
            String toTemp = "_" + to + "_";
            replace = string.replace(fromTemp, toTemp);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return replace;
    }

    void submitBulkDataToServer(String apiUrl, JSONObject jsonObject, Class modelType) {
        //Show loading bar
        try {
            pdLoading.show();

            new APIRestManager().postAPI(apiUrl, jsonObject, getActivity(), modelType, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {

                    if (selectButton == AppConstants.UPLOAD_TRIP_INFORMATION) {
                        if (resultObj instanceof ArrayList) {
                            ArrayList<TripModel> tripModels = (ArrayList<TripModel>) resultObj;
                            txt_update_trip_info.setEnabled(false);
                            txt_update_trip_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_complete_successfully), Toast.LENGTH_SHORT).show();
                        } else if (resultObj instanceof String) {
                            Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                        pdLoading.dismiss();
                    } else if (selectButton == AppConstants.UPDATE_PLANTATION_SELECT) {
                        if (resultObj instanceof ArrayList) {
                            ArrayList<SubmitPlantationDataModel> plantationData = (ArrayList<SubmitPlantationDataModel>) resultObj;
                            int i = 0;
                            for (i = 0; i < plantationData.size(); i++) {
                                sqLiteHelper.insertPlantationKeyAndID(plantationData.get(i).plantation_key, plantationData.get(i).Code);
                            }
                            if (i > 0) {
                                txt_plantation_update.setEnabled(false);
                                txt_plantation_update.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                Preferences.setPlantationUploadStatus(true);
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_complete_successfully), Snackbar.LENGTH_LONG).show();
                            }
                        } else if (resultObj instanceof String) {
                            Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                        pdLoading.dismiss();
                    } else if (selectButton == AppConstants.UPDATE_RUJVAT_SELECT) {
                        boolean status = false;
                        if (resultObj instanceof ArrayList) {
                            ArrayList<RujvatReasonAndKYCUpdateOutputModel> rujvatOutput = (ArrayList<RujvatReasonAndKYCUpdateOutputModel>) resultObj;
                            for (int i = 0; i < rujvatOutput.size(); i++) {
                                String temp = rujvatOutput.get(i).getValue();
                                if (temp.equals("1")) {
                                    status = true;
                                } else
                                    status = false;
                            }
                            if (status) {
                                txt_rujvat_update.setEnabled(false);
                                txt_rujvat_update.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                Preferences.setRujvatUploadStatus(true);
                                pdLoading.dismiss();
                                txt_rujvat_update.setEnabled(false);
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_complete_successfully), Snackbar.LENGTH_LONG).show();
                            } else
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_fail), Snackbar.LENGTH_LONG).show();

                        } else if (resultObj instanceof String) {
                            Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                    } else if (selectButton == AppConstants.UPLOAD_KYC_INFORMATION) {
                        boolean status = false;
                        if (resultObj instanceof ArrayList) {
                            ArrayList<RujvatReasonAndKYCUpdateOutputModel> rujvatAndKycOutput = (ArrayList<RujvatReasonAndKYCUpdateOutputModel>) resultObj;
                            for (int i = 0; i < rujvatAndKycOutput.size(); i++) {
                                String temp = rujvatAndKycOutput.get(i).getValue();
                                if (temp.equals("1")) {
                                    status = true;
                                } else
                                    status = false;
                            }
                            if (status) {
                                txt_kyc_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                Preferences.setKycInfoStatus(true);
                                pdLoading.dismiss();
                                txt_kyc_info.setEnabled(false);
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_complete_successfully), Snackbar.LENGTH_LONG).show();
                            } else
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_fail), Snackbar.LENGTH_LONG).show();

                        } else if (resultObj instanceof String) {
                            Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                    } else if (selectButton == AppConstants.DELETE_ALL_DATA_SELECT) {
                        boolean status = false;
                        ArrayList<String> notAvailableImageOnServer = new ArrayList<String>();
                        if (resultObj instanceof ArrayList) {
                            ArrayList<AllImageDataDeleteCheckModel> imageListOutput = (ArrayList<AllImageDataDeleteCheckModel>) resultObj;
                            for (int i = 0; i < imageListOutput.size(); i++) {
                                if (imageListOutput.get(i).isValue()) {
                                    status = true;
                                } else {
//                                status = false;
                                    notAvailableImageOnServer.add(imageListOutput.get(i).getImg_name());
                                }
                            }
                            if (notAvailableImageOnServer.size() == 0) {
                                sqLiteHelper.deletePlantationAndRujvatImageData();
                                boolean deleteImagOutput = deleteRecursive(mediaStorageDir);
                                if (deleteImagOutput) {
                                    Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.all_data_image_deleted), Snackbar.LENGTH_LONG).show();
                                    File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                                    try {
                                        if (file.exists())
                                            Log.d("Trp Camera File", "File Already Exists");
                                        else
                                            file.createNewFile();
                                    } catch (IOException e) {
                                        e.printStackTrace();
                                    }
                                } else
                                    Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.no_image_deleted), Snackbar.LENGTH_LONG).show();
                                txt_delete_records_images.setEnabled(false);
                                txt_delete_records_images.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                                pdLoading.dismiss();
                            } else {
                                pdLoading.dismiss();
                                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.upload_all_images_and_data), Snackbar.LENGTH_LONG).show();
                            }
                        } else if (resultObj instanceof String) {
                            pdLoading.dismiss();
                            Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onError(String strError) {
                    //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
                    if (strError == null)
                        Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.something_wrong_with_data), Snackbar.LENGTH_LONG).show();
                    else
                        Snackbar.make(view.findViewById(R.id.activity_update_server), strError, Snackbar.LENGTH_LONG).show();
                    pdLoading.dismiss();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public boolean deleteRecursive(File fileOrDirectory) {
        boolean delete = false;
        try {
            if (fileOrDirectory.isDirectory())
                for (File child : fileOrDirectory.listFiles()) {
                    delete = child.delete();
                }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return delete;
    }


    void submitEmployeeTripDataOnServer() {

        try {
            String tripData = sqLiteHelper.getEmployeeTripDataInJson();

            if (tripData.equals("{\"TripData\":[]}")) {
                Snackbar.make(view.findViewById(R.id.activity_update_server), getString(R.string.no_trip_found), Snackbar.LENGTH_LONG).show();
                txt_update_trip_info.setEnabled(false);
                txt_update_trip_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
            } else
                submitBulkDataToServer(AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY, new JSONObject(tripData), TripModel.class);
        } catch (JSONException ex) {
            ex.printStackTrace();
            pdLoading.dismiss();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//        try {
//            String uploadId = UUID.randomUUID().toString();
//            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CaneApp");
//            ArrayList<String[]> employeeTripData = sqLiteHelper.getEmployeeTripData();
//            //Creating a multi part request
//            String URL = "";
//            if (employeeTripData.get(0).length > 0) {
//                if (AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY.contains("null")) {
//                    URL = AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY.replace("null", Preferences.getServerIP());
//                } else {
//                    URL = AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY;
//                }
//                try {
//                    MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(getContext(), uploadId, URL);
////            new MultipartUploadRequest(this, uploadId, AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY)
////                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9), "img_1") //Adding file8
//                    for (int i = 0; i < employeeTripData.get(9).length; i++) { //
//                        multipartUploadRequest.addParameter("p_device[]", employeeTripData.get(0)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_date[]", employeeTripData.get(1)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_start[]", employeeTripData.get(2)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_start_time[]", employeeTripData.get(3)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_end[]", employeeTripData.get(4)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_end_time[]", employeeTripData.get(5)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_start_pic[]", employeeTripData.get(6)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_end_pic[]", employeeTripData.get(7)[i]); //Adding text parameter to the request
//                        multipartUploadRequest.addParameter("p_flag[]", employeeTripData.get(8)[i]); //Adding text parameter to the request
////                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9)[i], "userfile[]");
//                    }
//                    multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
//                    multipartUploadRequest.setDelegate(new UploadStatusDelegate() {
//                        @Override
//                        public void onProgress(UploadInfo uploadInfo) {
//
//                        }
//
//                        @Override
//                        public void onError(UploadInfo uploadInfo, Exception exception) {
//                            pdLoading.dismiss();
//                            Toast.makeText(getContext(), "Server error in uploading, Try again.", Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
////                            sqLiteHelper.deleteEmployeeTripData();
//                            txt_update_trip_info.setEnabled(false);
//                            txt_update_trip_info.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
//                            pdLoading.dismiss();
//                            Toast.makeText(getContext(), "Upload Complete Successfully ", Toast.LENGTH_SHORT).show();
//                        }
//
//                        @Override
//                        public void onCancelled(UploadInfo uploadInfo) {
//                            pdLoading.dismiss();
//                            Toast.makeText(getContext(), "Upload Cancel by user", Toast.LENGTH_SHORT).show();
//                        }
//
//                    });
//                    multipartUploadRequest.setMaxRetries(2);
//                    String s = multipartUploadRequest.startUpload();//Starting the uploadwh
////            Toast.makeText(this, s + " Trip Upload", Toast.LENGTH_SHORT).show();
//                } catch (Exception ex) {
//                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
//                }
//
//            } else {
//                pdLoading.dismiss();
//                Toast.makeText(getContext(), "Trip data not available", Toast.LENGTH_SHORT).show();
//            }
//        } catch (Exception exc) {
//            Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
//        }
//    }

    void submitEmployeeTripImgage() {
        try {
            String uploadId = UUID.randomUUID().toString();
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CaneApp");
            ArrayList<String[]> employeeTripData = sqLiteHelper.getEmployeeTripData();
            //Creating a multi part request
            String URL = "";
            if (employeeTripData.get(0).length > 0) {
                if (AppConstants.URL_EMPLOYEE_TRIP_IMG_UPLOAD_IN_ARRY.contains("null")) {
                    URL = AppConstants.URL_EMPLOYEE_TRIP_IMG_UPLOAD_IN_ARRY.replace("null", Preferences.getServerIP());
                } else {
                    URL = AppConstants.URL_EMPLOYEE_TRIP_IMG_UPLOAD_IN_ARRY;
                }
                try {
                    MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(getContext(), uploadId, URL);
//            new MultipartUploadRequest(this, uploadId, AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY)
//                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9), "img_1") //Adding file8
                    for (int i = 0; i < employeeTripData.get(9).length; i++) {
                        multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9)[i], "userfile[]");
                    }
                    if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.O) {
                    } else {
                        multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
                    }
                    multipartUploadRequest.setDelegate(new UploadStatusDelegate() {
                        @Override
                        public void onProgress(UploadInfo uploadInfo) {
                            Toast.makeText(getContext(), getString(R.string.server_error_uploading), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onError(UploadInfo uploadInfo, Exception exception) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.server_error_uploading), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCompleted(UploadInfo uploadInfo, ServerResponse serverResponse) {
                            sqLiteHelper.deleteEmployeeTripData();
                            txt_upload_trip_image.setEnabled(false);
                            txt_upload_trip_image.setBackground(getResources().getDrawable(R.drawable.background_disable_update_tab));
                            Preferences.setTripImgStatus(true);
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_complete_successfully), Toast.LENGTH_SHORT).show();
                        }

                        @Override
                        public void onCancelled(UploadInfo uploadInfo) {
                            pdLoading.dismiss();
                            Toast.makeText(getContext(), getString(R.string.upload_cancel_by_user), Toast.LENGTH_SHORT).show();
                        }

                    });
                    multipartUploadRequest.setMaxRetries(2);
                    String s = multipartUploadRequest.startUpload();//Starting the uploadwh
//            Toast.makeText(this, s + " Trip Upload", Toast.LENGTH_SHORT).show();
                } catch (Exception ex) {
                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }

            } else {
                pdLoading.dismiss();
                Toast.makeText(getContext(), getString(R.string.trip_data_not_available), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception exc) {
            Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<KYCModel> getJsoneManager(String jsone, Class KYCModel) {
        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), KYCModel, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    deatilsList = new ArrayList();
                    deatilsList = (ArrayList<KYCModel>) resultObj;
                }

                @Override
                public void onError(String strError) {
                    Toast.makeText(getContext(), strError, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return deatilsList;
    }
}
