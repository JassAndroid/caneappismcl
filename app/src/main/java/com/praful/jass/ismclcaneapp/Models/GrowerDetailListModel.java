package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-07-2017.
 */


public class GrowerDetailListModel {
    private String FARMER_CODE;

    private String TALUKA_NAME;

    private String TALUKA_ID;

    private String SUBGUT_ID;

    private String GUT_NAME;

    private String VILLAGE_NAME;

    private String OBJECT_ID;

    private String GUT_ID;

    private String FARMER_NAME;

    private String FARMER_ID;

    private String SUB_GUT_NAME;

    public String getFARMER_CODE() {
        return FARMER_CODE;
    }

    public void setFARMER_CODE(String FARMER_CODE) {
        this.FARMER_CODE = FARMER_CODE;
    }

    public String getTALUKA_NAME() {
        return TALUKA_NAME;
    }

    public void setTALUKA_NAME(String TALUKA_NAME) {
        this.TALUKA_NAME = TALUKA_NAME;
    }

    public String getTALUKA_ID() {
        return TALUKA_ID;
    }

    public void setTALUKA_ID(String TALUKA_ID) {
        this.TALUKA_ID = TALUKA_ID;
    }

    public String getSUBGUT_ID() {
        return SUBGUT_ID;
    }

    public void setSUBGUT_ID(String SUBGUT_ID) {
        this.SUBGUT_ID = SUBGUT_ID;
    }

    public String getGUT_NAME() {
        return GUT_NAME;
    }

    public void setGUT_NAME(String GUT_NAME) {
        this.GUT_NAME = GUT_NAME;
    }

    public String getVILLAGE_NAME() {
        return VILLAGE_NAME;
    }

    public void setVILLAGE_NAME(String VILLAGE_NAME) {
        this.VILLAGE_NAME = VILLAGE_NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getGUT_ID() {
        return GUT_ID;
    }

    public void setGUT_ID(String GUT_ID) {
        this.GUT_ID = GUT_ID;
    }

    public String getFARMER_NAME() {
        return FARMER_NAME;
    }

    public void setFARMER_NAME(String FARMER_NAME) {
        this.FARMER_NAME = FARMER_NAME;
    }

    public String getFARMER_ID() {
        return FARMER_ID;
    }

    public void setFARMER_ID(String FARMER_ID) {
        this.FARMER_ID = FARMER_ID;
    }

    public String getSUB_GUT_NAME() {
        return SUB_GUT_NAME;
    }

    public void setSUB_GUT_NAME(String SUB_GUT_NAME) {
        this.SUB_GUT_NAME = SUB_GUT_NAME;
    }

    @Override
    public String toString() {
        return "ClassPojo [FARMER_CODE = " + FARMER_CODE + ", TALUKA_NAME = " + TALUKA_NAME + ", TALUKA_ID = " + TALUKA_ID + ", SUBGUT_ID = " + SUBGUT_ID + ", GUT_NAME = " + GUT_NAME + ", VILLAGE_NAME = " + VILLAGE_NAME + ", OBJECT_ID = " + OBJECT_ID + ", GUT_ID = " + GUT_ID + ", FARMER_NAME = " + FARMER_NAME + ", FARMER_ID = " + FARMER_ID + ", SUB_GUT_NAME = " + SUB_GUT_NAME + "]";
    }
}