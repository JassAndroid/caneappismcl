package com.praful.jass.ismclcaneapp.services;

import android.Manifest;
import android.annotation.SuppressLint;
import android.app.Service;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.Handler;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AlertDialog;
import android.support.v7.view.ContextThemeWrapper;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.android.gms.location.FusedLocationProviderClient;
import com.google.android.gms.location.LocationServices;
import com.google.android.gms.tasks.OnSuccessListener;
import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.Activity.Phase1.GetGeoLocationAndImagesActivity;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by Jass on 7/18/2017.
 */

public class GoogleService extends Service implements LocationListener {
    final Context MyContext = this;

    boolean isGPSEnable = false;
    boolean isNetworkEnable = false;
    boolean isDialogShow = false;
    double latitude, longitude;
    LocationManager locationManager;
    public static Location location;
    private Handler mHandler = new Handler();
    private Timer mTimer = null;
    long peroid_after_task_done = 10000;
    long delay_befour_start = 5;
    public static String str_receiver = "servicetutorial.service.receiver";
    Intent intent;
    TelephonyManager telephonyManager;
    SystemDateTime dateTime = new SystemDateTime();
    SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
    //    LocationRequest locationRequest;
    private FusedLocationProviderClient mFusedLocationClient;
    double accuracy = 0.0;

    public GoogleService() {
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    @SuppressLint("RestrictedApi")
    @Override
    public void onCreate() {
        super.onCreate();
        try {
//            fn_getlocation();
            mTimer = new Timer();
            mTimer.schedule(new TimerTaskToGetLocation(), delay_befour_start, peroid_after_task_done);

//            locationRequest = new LocationRequest();
//            locationRequest.setInterval(100);
//            locationRequest.setSmallestDisplacement(0.05f);
//            locationRequest.setFastestInterval(100);
//            locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);

            intent = new Intent(str_receiver);
            telephonyManager = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);
            Preferences.appContext = this;

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {

    }

    @Override
    public void onStatusChanged(String provider, int status, Bundle extras) {

    }

    @Override
    public void onProviderEnabled(String provider) {

    }

    @Override
    public void onProviderDisabled(String provider) {

    }

    void getListFromServer(String lat, String longg) {
        try {
            if (lat == null || longg == null) {
                return;
            }
            String deviceID = "";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    return;
                }
                deviceID = telephonyManager.getDeviceId(2);
            } else {
                deviceID = telephonyManager.getDeviceId();
            }
            sqLiteHelper.insertTraceEmployeeData(deviceID, dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss"), lat, longg);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void fn_getlocation() {
        locationManager = (LocationManager) this.getSystemService(LOCATION_SERVICE);
        isGPSEnable = locationManager.isProviderEnabled(LocationManager.GPS_PROVIDER);
        isNetworkEnable = locationManager.isProviderEnabled(LocationManager.NETWORK_PROVIDER);
        try {
            if (!isGPSEnable && !isNetworkEnable) {
                buildAlertMessageNoGps();
            } else {
                if (isNetworkEnable) {
                    location = null;
                    if (ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED && ActivityCompat.checkSelfPermission(this, android.Manifest.permission.ACCESS_COARSE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        return;
                    }
                    locationManager.requestLocationUpdates(LocationManager.NETWORK_PROVIDER, 10000, 1, this); // Integer.parseInt(Preferences.getTrackingTime())
                    if (locationManager != null) {
//                        location = locationManager.getLastKnownLocation(LocationManager.NETWORK_PROVIDER);
//                        if (location != null) {
                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    // Logic to handle location object
                                    if (location.getAccuracy() < 30) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();

                                        CustomLocationModel locationModel = new CustomLocationModel();
                                        locationModel.setLatitude(latitude);
                                        locationModel.setLongitude(longitude);

                                        Preferences.appContext = MyContext;
                                        Preferences.setUserLocation(new Gson().toJson(locationModel));

//                                                Toast.makeText(MyContext, location.getAccuracy() + " " + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();
//                                                    Intent intent = new Intent("matrimonialapp.jass.example.com.locationlistenerdemo");
//                                                    intent.putExtra("lat", latitude);
//                                                    intent.putExtra("lag", longitude);
//                                                    sendBroadcast(intent);
                                        fn_update(location);

                                        Log.d("latitude", location.getLatitude() + "");
                                        Log.d("longitude", location.getLongitude() + "");
                                        GetGeoLocationAndImagesActivity.latit = location.getLatitude();
                                        GetGeoLocationAndImagesActivity.longi = location.getLongitude();
                                        AppConstants.latitude = location.getLatitude();
                                        AppConstants.longitude = location.getLongitude();


                                        if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                                        } else {
                                            getListFromServer(latitude + "", longitude + "");
                                        }
                                    }
                                }
                            }
                        });
//                        }
                    }
                }
            }
            try {
                if (isGPSEnable) {
                    location = null;
                    locationManager.requestLocationUpdates(LocationManager.GPS_PROVIDER, 10000, 0, this);
                    if (locationManager != null) {

                        mFusedLocationClient = LocationServices.getFusedLocationProviderClient(this);
                        mFusedLocationClient.getLastLocation().addOnSuccessListener(new OnSuccessListener<Location>() {
                            @Override
                            public void onSuccess(final Location location) {
                                if (location != null) {
                                    // Logic to handle location object

                                    if (location.getAccuracy() < 30) {
                                        latitude = location.getLatitude();
                                        longitude = location.getLongitude();

                                        CustomLocationModel locationModel = new CustomLocationModel();
                                        locationModel.setLatitude(latitude);
                                        locationModel.setLongitude(longitude);

                                        Preferences.appContext = MyContext;
                                        Preferences.setUserLocation(new Gson().toJson(locationModel));

//                                                Toast.makeText(MyContext, location.getAccuracy() + " " + location.getLatitude() + " " + location.getLongitude(), Toast.LENGTH_SHORT).show();
//                                                    Intent intent = new Intent("matrimonialapp.jass.example.com.locationlistenerdemo");
//                                                    intent.putExtra("lat", latitude);
//                                                    intent.putExtra("lag", longitude);
//                                                    sendBroadcast(intent);
                                        fn_update(location);

                                        Log.d("latitude", location.getLatitude() + "");
                                        Log.d("longitude", location.getLongitude() + "");
                                        GetGeoLocationAndImagesActivity.latit = location.getLatitude();
                                        GetGeoLocationAndImagesActivity.longi = location.getLongitude();
                                        AppConstants.latitude = location.getLatitude();
                                        AppConstants.longitude = location.getLongitude();

                                        try {
                                            if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                                            } else {
                                                getListFromServer(latitude + "", longitude + "");
                                            }
                                        } catch (Exception ex) {
                                            Toast.makeText(GoogleService.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                        }
                                    }
                                }
                            }
                        });

//                        location = locationManager.getLastKnownLocation(LocationManager.GPS_PROVIDER);
//                        if (location != null) {
//                            Log.e("latitude", location.getLatitude() + "");
//                            Log.e("longitude", location.getLongitude() + "");
//                            latitude = location.getLatitude();
//                            longitude = location.getLongitude();
//                            fn_update(location);
//                        }
                    }
                }
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void buildAlertMessageNoGps() {
        try {
            if (!isDialogShow) {
                @SuppressLint("RestrictedApi") AlertDialog.Builder builder = new AlertDialog.Builder(new ContextThemeWrapper(this, R.style.myDialog));
                builder.setMessage("Your GPS seems to be disabled, you have to enable it")  //Your GPS seems to be disabled, do you want to enable it?
                        .setCancelable(false)
                        .setPositiveButton("OK", new DialogInterface.OnClickListener() {
                            public void onClick(final DialogInterface dialog, final int id) {
                                startActivity(new Intent(android.provider.Settings.ACTION_LOCATION_SOURCE_SETTINGS));
//                            Intent dialogIntent = new Intent(this, MenuActivityPhase1.);
//                            dialogIntent.addFlags(Intent.FLAG_ACTIVITY_NEW_TASK);
//                            startActivity(dialogIntent);
                            }
                        });
//                .setNegativeButton("No", new DialogInterface.OnClickListener() {
//                    public void onClick(final DialogInterface dialog, final int id) {
//                        dialog.cancel();
//                    }
//                });
                AlertDialog alert = builder.create();
                alert.getWindow().setType(WindowManager.LayoutParams.TYPE_TOAST);
                alert.show();
                isDialogShow = true;
                alert.setOnDismissListener(new DialogInterface.OnDismissListener() {
                    @Override
                    public void onDismiss(DialogInterface dialogInterface) {
//                    isDialogShow = false;
                    }
                });
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private class TimerTaskToGetLocation extends TimerTask {
        @Override
        public void run() {
            try {
                mHandler.post(new Runnable() {
                    @Override
                    public void run() {
                        fn_getlocation();
                    }
                });
            } catch (Exception ex) {
                Toast.makeText(GoogleService.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

    }

    private void fn_update(Location location) {
        try {
            intent.putExtra("latutide", location.getLatitude() + "");
            intent.putExtra("longitude", location.getLongitude() + "");
            sendBroadcast(intent);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            accuracy = intent.getDoubleExtra("accuracy", 0.0);
            return super.onStartCommand(intent, flags, startId);
        } catch (Exception e) {
            e.printStackTrace();
            return super.onStartCommand(intent, flags, startId);
        }
    }
}