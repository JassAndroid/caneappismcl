package com.praful.jass.ismclcaneapp.Utils;

/**
 * Created by Jass on 2/10/2017.
 */
public class Validator {
    public static boolean isValidString(String... str){
        boolean result = false;
        for(int i=0; i<str.length;i++){
            if(str[i] == null || str[i].isEmpty() || str[i].equals("null")){
                return   false;
            }
        }
        return true;
    }
}
