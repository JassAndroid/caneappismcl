package com.praful.jass.ismclcaneapp.Models;

public class CuttingOrderStartModel {

    private String FARMER_VILLAGE;

    private String FARMER_CODE;

    private String VARITY;

    private String FARMER_GUT;

    private String PLANTATION_DATE;

    private String PLANTATION_ID;

    private String SHIVAR_VILLAGE_NAME;

    private String PLANTATION_TYPE;

    private String PLANTATION_CODE;

    private String FARMER_TALUKA;

    private String FARMER_NAME;

    private String IRRE_NAME;

    private String SHIVAR_GUT_NAME;

    private String SARVE_NO;

    private String FARMER_SUBGUT;

    private String FARMER_ID;

    private String SUB_GUT_NAME;
    private String CUTTING_OBJECT_ID;
    private String HRV_CODE;
    private String HRV_NAME;
    private String PLOT_START_DATE;
    private String GUT_ID;

    public String getFARMER_VILLAGE() {
        return FARMER_VILLAGE;
    }

    public void setFARMER_VILLAGE(String FARMER_VILLAGE) {
        this.FARMER_VILLAGE = FARMER_VILLAGE;
    }

    public String getFARMER_CODE() {
        return FARMER_CODE;
    }

    public void setFARMER_CODE(String FARMER_CODE) {
        this.FARMER_CODE = FARMER_CODE;
    }

    public String getVARITY() {
        return VARITY;
    }

    public void setVARITY(String VARITY) {
        this.VARITY = VARITY;
    }

    public String getFARMER_GUT() {
        return FARMER_GUT;
    }

    public void setFARMER_GUT(String FARMER_GUT) {
        this.FARMER_GUT = FARMER_GUT;
    }

    public String getPLANTATION_DATE() {
        return PLANTATION_DATE;
    }

    public void setPLANTATION_DATE(String PLANTATION_DATE) {
        this.PLANTATION_DATE = PLANTATION_DATE;
    }

    public String getPLANTATION_ID() {
        return PLANTATION_ID;
    }

    public void setPLANTATION_ID(String PLANTATION_ID) {
        this.PLANTATION_ID = PLANTATION_ID;
    }

    public String getSHIVAR_VILLAGE_NAME() {
        return SHIVAR_VILLAGE_NAME;
    }

    public void setSHIVAR_VILLAGE_NAME(String SHIVAR_VILLAGE_NAME) {
        this.SHIVAR_VILLAGE_NAME = SHIVAR_VILLAGE_NAME;
    }

    public String getPLANTATION_TYPE() {
        return PLANTATION_TYPE;
    }

    public void setPLANTATION_TYPE(String PLANTATION_TYPE) {
        this.PLANTATION_TYPE = PLANTATION_TYPE;
    }

    public String getPLANTATION_CODE() {
        return PLANTATION_CODE;
    }

    public void setPLANTATION_CODE(String PLANTATION_CODE) {
        this.PLANTATION_CODE = PLANTATION_CODE;
    }

    public String getFARMER_TALUKA() {
        return FARMER_TALUKA;
    }

    public void setFARMER_TALUKA(String FARMER_TALUKA) {
        this.FARMER_TALUKA = FARMER_TALUKA;
    }

    public String getFARMER_NAME() {
        return FARMER_NAME;
    }

    public void setFARMER_NAME(String FARMER_NAME) {
        this.FARMER_NAME = FARMER_NAME;
    }

    public String getIRRE_NAME() {
        return IRRE_NAME;
    }

    public void setIRRE_NAME(String IRRE_NAME) {
        this.IRRE_NAME = IRRE_NAME;
    }

    public String getSHIVAR_GUT_NAME() {
        return SHIVAR_GUT_NAME;
    }

    public void setSHIVAR_GUT_NAME(String SHIVAR_GUT_NAME) {
        this.SHIVAR_GUT_NAME = SHIVAR_GUT_NAME;
    }

    public String getSARVE_NO() {
        return SARVE_NO;
    }

    public void setSARVE_NO(String SARVE_NO) {
        this.SARVE_NO = SARVE_NO;
    }

    public String getFARMER_SUBGUT() {
        return FARMER_SUBGUT;
    }

    public void setFARMER_SUBGUT(String FARMER_SUBGUT) {
        this.FARMER_SUBGUT = FARMER_SUBGUT;
    }

    public String getFARMER_ID() {
        return FARMER_ID;
    }

    public void setFARMER_ID(String FARMER_ID) {
        this.FARMER_ID = FARMER_ID;
    }

    public String getSUB_GUT_NAME() {
        return SUB_GUT_NAME;
    }

    public void setSUB_GUT_NAME(String SUB_GUT_NAME) {
        this.SUB_GUT_NAME = SUB_GUT_NAME;
    }

    public String getCUTTING_OBJECT_ID() {
        return CUTTING_OBJECT_ID;
    }

    public void setCUTTING_OBJECT_ID(String CUTTING_OBJECT_ID) {
        this.CUTTING_OBJECT_ID = CUTTING_OBJECT_ID;
    }

    public String getHRV_CODE() {
        return HRV_CODE;
    }

    public void setHRV_CODE(String HRV_CODE) {
        this.HRV_CODE = HRV_CODE;
    }

    public String getHRV_NAME() {
        return HRV_NAME;
    }

    public void setHRV_NAME(String HRV_NAME) {
        this.HRV_NAME = HRV_NAME;
    }

    public String getPLOT_START_DATE() {
        return PLOT_START_DATE;
    }

    public void setPLOT_START_DATE(String PLOT_START_DATE) {
        this.PLOT_START_DATE = PLOT_START_DATE;
    }

    public String getGUT_ID() {
        return GUT_ID;
    }

    public void setGUT_ID(String GUT_ID) {
        this.GUT_ID = GUT_ID;
    }

    @Override
    public String toString() {
        return "ClassPojo [FARMER_VILLAGE = " + FARMER_VILLAGE + ", FARMER_CODE = " + FARMER_CODE + ", VARITY = " + VARITY + ", FARMER_GUT = " + FARMER_GUT + ", PLANTATION_DATE = " + PLANTATION_DATE + ", PLANTATION_ID = " + PLANTATION_ID + ", SHIVAR_VILLAGE_NAME = " + SHIVAR_VILLAGE_NAME + ", PLANTATION_TYPE = " + PLANTATION_TYPE + ", PLANTATION_CODE = " + PLANTATION_CODE + ", FARMER_TALUKA = " + FARMER_TALUKA + ", FARMER_NAME = " + FARMER_NAME + ", IRRE_NAME = " + IRRE_NAME + ", SHIVAR_GUT_NAME = " + SHIVAR_GUT_NAME + ", SARVE_NO = " + SARVE_NO + ", FARMER_SUBGUT = " + FARMER_SUBGUT + ", FARMER_ID = " + FARMER_ID + ", SUB_GUT_NAME = " + SUB_GUT_NAME + "]";
    }

}
