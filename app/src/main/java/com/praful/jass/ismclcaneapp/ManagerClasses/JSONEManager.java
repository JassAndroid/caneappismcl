package com.praful.jass.ismclcaneapp.ManagerClasses;

import com.google.gson.Gson;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;


public class JSONEManager {

    public interface APIManagerInterface {
        public void onSuccess(Object resultObj);

        public void onError(String strError);
    }


    public void getJSONEResults(JSONObject params, final Class classType, final APIManagerInterface listner) {


        Gson gson = new Gson();

//        try {
//            if (params.getString("status").equalsIgnoreCase("SUCCESS")) {
                try {
                    Object json = params.get("response");


                    //if multiple objects
                    if (json instanceof JSONArray) {
                        ArrayList resultArray = new ArrayList();

                        JSONArray list = params.getJSONArray("response");
                        for (int i = 0; i < list.length(); i++) {
                            JSONObject object = list.getJSONObject(i);
                            Object model = gson.fromJson(object.toString(), classType);
//                            model = ModelParser.parseModel(object, model);

                            resultArray.add(model);

                        }

                        if (listner != null) {
                            listner.onSuccess(resultArray);
                        }
                    } else {
//                        String jsonStr = "";
//                        Object result = null;
//
//                        if (classType == CommonResponseModel.class) {
//                            jsonStr = response.toString();
//                        } else {
//                            jsonStr = response.getString("response");
//                        }
//
//                        result = gson.fromJson(jsonStr, classType);
//
////                        result = ModelParser.parseModel((JSONObject) json, result);
//
//                        if (listner != null) {
//                            listner.onSuccess(result);
//                        }
                    }
                } catch (Exception e) {
                    e.printStackTrace();
                    listner.onError(e.getLocalizedMessage());
                }
//            }
// else if (response.getString("status").equalsIgnoreCase("Error")) {
//                String msg = response.getString("message");
//                if (listner != null) {
//                    listner.onError(msg);
//                }
//            }
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
    }

}

