package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.InputFilter;
import android.text.Spanned;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class IPUpdateDialog extends Dialog {
    public EditText changeIP;
    public Button btn_submit, btn_cancle;
    private Context context;
//    private TextWatcher ipAddressWatcher;

    public IPUpdateDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_ip_update);
            init();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            changeIP = (EditText) findViewById(R.id.changeIP);
            changeIP.setText("");
            btn_submit = (Button) findViewById(R.id.btn_submit_ip);
            btn_cancle = (Button) findViewById(R.id.btn_cancle_ip);
            InputFilter[] filters = new InputFilter[1];
            filters[0] = new InputFilter() {
                public CharSequence filter(CharSequence source, int start, int end, Spanned dest, int dstart, int dend) {
                    if (end > start) {
                        String destTxt = dest.toString();
                        String resultingTxt = destTxt.substring(0, dstart) + source.subSequence(start, end) + destTxt.substring(dend);
                        if (!resultingTxt.matches("^\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3}(\\.(\\d{1,3})?)?)?)?)?)?")) {
                            return "";
                        } else {
                            String[] splits = resultingTxt.split("\\.");
                            for (int i = 0; i < splits.length; i++) {
                                if (Integer.valueOf(splits[i]) > 255) {
                                    return "";
                                }
                            }
                        }
                    }
                    return null;
                }
            };
            changeIP.setFilters(filters);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
