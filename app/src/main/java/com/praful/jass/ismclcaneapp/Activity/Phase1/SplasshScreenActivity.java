package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.os.Handler;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.SeasonNameListModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.ChangeLocaleLanguage;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.ArrayList;

public class SplasshScreenActivity extends AppCompatActivity {

    TextView version;

    @Override

    protected void onCreate(final Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_splassh_screen);

            //Make window full screen
            getWindow().getDecorView().setSystemUiVisibility(
                    View.SYSTEM_UI_FLAG_LAYOUT_STABLE
                            | View.SYSTEM_UI_FLAG_LAYOUT_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_LAYOUT_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_HIDE_NAVIGATION
                            | View.SYSTEM_UI_FLAG_FULLSCREEN
                            | View.SYSTEM_UI_FLAG_IMMERSIVE);

            init();
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(SplasshScreenActivity.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                }
            }
            callSeasonList();
            new Handler().postDelayed(new Runnable() {
                @Override
                public void run() {
                    try {
                        Intent intent = new Intent(SplasshScreenActivity.this, LoginActivityTemp.class);
                        startActivity(intent);
                        finish();
                    } catch (Exception ex) {
                        Toast.makeText(SplasshScreenActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            }, 3000);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            Preferences.appContext = getApplicationContext();
//            Preferences.setLocalLanguage("MR");
            version = (TextView) findViewById(R.id.version);
            ChangeLocaleLanguage changeLocaleLanguage = new ChangeLocaleLanguage();
            changeLocaleLanguage.changeLocale(Preferences.getLocalLanguage(), this);
            try {
                version.setText(getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void callSeasonList() {
        try {
            JSONObject jsonObject = new JSONObject();
            new APIRestManager().postAPI(AppConstants.URL_GET_ALL_SEASON_LIST, jsonObject, SplasshScreenActivity.this, SeasonNameListModel.class, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {
                    try {
                        JSONArray jsonArray = new JSONArray();
                        if (resultObj instanceof ArrayList) {
                            ArrayList<SeasonNameListModel> seasonNameListModel = (ArrayList<SeasonNameListModel>) resultObj;
                            for (int i = 0; i < seasonNameListModel.size(); i++) {
                                JSONObject jsonObject = new JSONObject();
                                jsonObject.put("seasonID", seasonNameListModel.get(i).getOBJECT_ID());
                                jsonObject.put("seasonName", seasonNameListModel.get(i).getNAME());
                                jsonArray.put(jsonObject);
                            }
                            JSONObject jsonObject1 = new JSONObject();
                            jsonObject1.put("seasonList", jsonArray);
                            Preferences.setSeasonList(jsonObject1.toString());
//                            Context context1 = ;
//                            Toast.makeText(SplasshScreenActivity.this, "Done dana done", Toast.LENGTH_SHORT).show();
                        } else {
//                            Toast.makeText(SplasshScreenActivity.this, "Done dana done1", Toast.LENGTH_SHORT).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(SplasshScreenActivity.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(String strError) {
                    Toast.makeText(SplasshScreenActivity.this, strError, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
