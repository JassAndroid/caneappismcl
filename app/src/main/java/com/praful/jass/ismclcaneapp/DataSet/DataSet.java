package com.praful.jass.ismclcaneapp.DataSet;

import android.content.Context;

import com.praful.jass.ismclcaneapp.Models.DailyHistoryModel;
import com.praful.jass.ismclcaneapp.Models.RopTypeModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import java.util.ArrayList;

/**
 * Created by JASS-3 on 9/4/2017.
 */

public class DataSet {


    ArrayList<String> listmarital = new ArrayList<>();

    public ArrayList<String> getListmarital() {
        listmarital.add("Ramesh");
        listmarital.add("Motilal");
        listmarital.add("Govind");
        listmarital.add("Shyama");
        listmarital.add("Mukul");
        return listmarital;
    }

    public ArrayList<String> listLanguage = new ArrayList<>();

    public ArrayList<String> getListLanguage(Context context) {
        listLanguage.add(context.getString(R.string.txt_english));
        listLanguage.add(context.getString(R.string.txt_marathi));
        return listLanguage;
    }

    public ArrayList<String> listSeason = new ArrayList<>();

    public ArrayList<String> getListSeason(Context context) {
        listSeason.add(context.getString(R.string.txt_season1));
        listSeason.add(context.getString(R.string.txt_season2));
        return listSeason;
    }

    public ArrayList<String> listLanguageCode = new ArrayList<>();

    public ArrayList<String> getListLanguageCode() {
        listLanguage.add(AppConstants.ENGLISH);
        listLanguage.add(AppConstants.MARATHI);
        return listLanguage;
    }

    ArrayList<String> plantationCode = new ArrayList<>();

    public ArrayList<String> getplantationCode() {
        plantationCode.add("0000740");
        plantationCode.add("0000741");
        plantationCode.add("0000742");
        plantationCode.add("0000743");
        plantationCode.add("0000744");
        plantationCode.add("0000745");
        plantationCode.add("0000746");
        plantationCode.add("0000747");
        plantationCode.add("0000748");
        plantationCode.add("0000749");
        plantationCode.add("0000750");
        plantationCode.add("0000751");
        plantationCode.add("0000752");
        plantationCode.add("0000753");
        return plantationCode;
    }

    ArrayList<String> shivarVillage = new ArrayList<>();

    public ArrayList<String> getshivarVillage() {
        shivarVillage.add("AAKUR");
        shivarVillage.add("AALAGI");
        shivarVillage.add("ADHEGAON");
        shivarVillage.add("AAKUR");
        shivarVillage.add("UMARANI");
        shivarVillage.add("AAKUR");
        shivarVillage.add("UMARANI");
        shivarVillage.add("ADHEGAON");
        shivarVillage.add("ADHEGAON");
        shivarVillage.add("AAKUR");
        shivarVillage.add("AALAGI");
        shivarVillage.add("HATTALI(PURV)");
        shivarVillage.add("HATTALI(PURV)");
        shivarVillage.add("UMARANI");
        return shivarVillage;
    }

    ArrayList<String> shivarGut = new ArrayList<>();

    public ArrayList<String> getshivarGut() {
        shivarGut.add("INDI");
        shivarGut.add("AKKALAKOT");
        shivarGut.add("MADHA");
        shivarGut.add("INDI");
        shivarGut.add("UMARANI");
        shivarGut.add("INDI");
        shivarGut.add("UMARANI");
        shivarGut.add("MADHA");
        shivarGut.add("MADHA");
        shivarGut.add("INDI");
        shivarGut.add("AKKALAKOT");
        shivarGut.add("UMARANI");
        shivarGut.add("UMARANI");
        shivarGut.add("UMARANI");
        return shivarGut;
    }

    ArrayList<String> surveyNumber = new ArrayList<>();

    public ArrayList<String> getsurveyNumber() {
        surveyNumber.add("0000740");
        surveyNumber.add("0000741");
        surveyNumber.add("0000742");
        surveyNumber.add("0000743");
        surveyNumber.add("0000744");
        surveyNumber.add("0000745");
        surveyNumber.add("0000746");
        surveyNumber.add("0000747");
        surveyNumber.add("0000748");
        surveyNumber.add("0000749");
        surveyNumber.add("0000750");
        surveyNumber.add("0000751");
        surveyNumber.add("0000752");
        surveyNumber.add("0000753");
        return surveyNumber;
    }

    ArrayList<String> surveyDate = new ArrayList<>();

    public ArrayList<String> getsurveyDate() {
        surveyDate.add("10/Jul/2016");
        surveyDate.add("10/Jul/2016");
        surveyDate.add("12/Jul/2016");
        surveyDate.add("25/Jul/2016");
        surveyDate.add("30/Jul/2016");
        surveyDate.add("23/Jul/2016");
        surveyDate.add("14/Jul/2016");
        surveyDate.add("15/Jul/2016");
        surveyDate.add("25/Jul/2016");
        surveyDate.add("12/Jul/2016");
        surveyDate.add("12/Jul/2016");
        surveyDate.add("30/Jul/2016");
        surveyDate.add("14/Jul/2016");
        surveyDate.add("14/Jul/2016");
        return plantationCode;
    }

    ArrayList<String> irrigationSource = new ArrayList<>();

    public ArrayList<String> getirrigationSource() {
        irrigationSource.add("WELL");
        irrigationSource.add("RIVER");
        irrigationSource.add("BOREWEL & WELL");
        irrigationSource.add("CANAL");
        irrigationSource.add("CANAL");
        irrigationSource.add("WELL");
        irrigationSource.add("RIVER");
        irrigationSource.add("RIVER");
        irrigationSource.add("WELL");
        irrigationSource.add("CANAL");
        irrigationSource.add("WELL");
        irrigationSource.add("WELL");
        irrigationSource.add("RIVER");
        irrigationSource.add("CANAL");
        return irrigationSource;
    }

    ArrayList<String> caneVariety = new ArrayList<>();

    public ArrayList<String> getcaneVariety() {
        caneVariety.add("434");
        caneVariety.add("671");
        caneVariety.add("9805");
        caneVariety.add("9805");
        caneVariety.add("9805");
        caneVariety.add("434");
        caneVariety.add("671");
        caneVariety.add("0265");
        caneVariety.add("434");
        caneVariety.add("9805");
        caneVariety.add("671");
        caneVariety.add("434");
        caneVariety.add("671");
        caneVariety.add("9805");
        return caneVariety;
    }

    ArrayList<String> caneType = new ArrayList<>();

    public ArrayList<String> getcaneType() {
        caneType.add("ADSALI");
        caneType.add("SEASONAL");
        caneType.add("SEASONAL");
        caneType.add("RATOON");
        caneType.add("SEASONAL");
        caneType.add("RATOON");
        caneType.add("SEASONAL");
        caneType.add("ADSALI");
        caneType.add("PRESEASONAL");
        caneType.add("PRESEASONAL");
        caneType.add("ADSALI");
        caneType.add("PRESEASONAL");
        caneType.add("ADSALI");
        caneType.add("RATOON");
        return caneType;
    }

    ArrayList<String> plantationType = new ArrayList<>();

    public ArrayList<String> getplantationType() {
        plantationType.add("RATOON");
        plantationType.add("NEW PLANT");
        plantationType.add("NEW PLANT");
        plantationType.add("RATOON-3");
        plantationType.add("NEW PLANT");
        plantationType.add("RATOON-1");
        plantationType.add("NEW PLANT");
        plantationType.add("RATOON");
        plantationType.add("RATOON-2");
        plantationType.add("RATOON-3");
        plantationType.add("NEW PLANT");
        plantationType.add("RATOON-1");
        plantationType.add("RATOON-2");
        plantationType.add("RATOON");
        return plantationType;
    }

    ArrayList<String> plantationArea = new ArrayList<>();

    public ArrayList<String> getplantationArea() {
        plantationArea.add("2.5");
        plantationArea.add("1.5");
        plantationArea.add("2.2");
        plantationArea.add("2.7");
        plantationArea.add("2.4");
        plantationArea.add("2.6");
        plantationArea.add("1.8");
        plantationArea.add("1.6");
        plantationArea.add("1.4");
        plantationArea.add("1.9");
        plantationArea.add("1.8");
        plantationArea.add("1.2");
        plantationArea.add("1.6");
        plantationArea.add("2.4");
        return plantationArea;
    }

//    ArrayList<String> caneType = new ArrayList<>();
//
//    public ArrayList<String> getcaneType() {
//        caneType.add("0000740");
//        caneType.add("0000741");
//        caneType.add("0000742");
//        caneType.add("0000743");
//        caneType.add("0000744");
//        caneType.add("0000745");
//        caneType.add("0000746");
//        caneType.add("0000747");
//        caneType.add("0000748");
//        caneType.add("0000749");
//        caneType.add("0000750");
//        caneType.add("0000751");
//        caneType.add("0000752");
//        caneType.add("0000753");
//        return plantationCode;
//    }


    ArrayList<String> vehicalNo = new ArrayList<>();

    public ArrayList<String> getvehicalNo() {
        vehicalNo.add("MH 13 S 1620");
        vehicalNo.add("MH 13 M 4521");
        vehicalNo.add("MH 13 L 8564");
        vehicalNo.add("MH 13 D 7458");
        vehicalNo.add("MH 13 A 9874");
        vehicalNo.add("MH 13 X 4125");
        vehicalNo.add("MH 13 W 8452");
        vehicalNo.add("MH 13 Q 5214");
        vehicalNo.add("MH 13 A 9658");
        vehicalNo.add("MH 13 S 2154");
        vehicalNo.add("MH 13 F 4568");
        return vehicalNo;
    }


    ArrayList<String> vehicalType = new ArrayList<>();

    public ArrayList<String> getvehicalType() {
        vehicalType.add("Truck");
        vehicalType.add("Mini Tractor");
        vehicalType.add("Mini Tractor");
        vehicalType.add("Tractor");
        vehicalType.add("Truck");
        vehicalType.add("Tractor");
        vehicalType.add("Mini Tractor");
        vehicalType.add("Truck");
        vehicalType.add("Tractor");
        vehicalType.add("Mini Tractor");
        vehicalType.add("Tractor");
        return vehicalType;
    }

    ArrayList<String> transportercode = new ArrayList<>();

    public ArrayList<String> gettransportercode() {
        transportercode.add("Amit");
        transportercode.add("Jitendra");
        transportercode.add("Praful");
        transportercode.add("Praful");
        transportercode.add("Amit");
        transportercode.add("Jitendra");
        transportercode.add("Praful");
        transportercode.add("Jitendra");
        transportercode.add("Jitendra");
        transportercode.add("Praful");
        transportercode.add("Amit");
        return transportercode;
    }

    ArrayList<String> troly = new ArrayList<>();

    public ArrayList<String> gettroly(Context context) {
        troly.add(context.getString(R.string.above));
        troly.add(context.getString(R.string.middle));
        troly.add(context.getString(R.string.lower));
        troly.add(context.getString(R.string.above_all));
        troly.add(context.getString(R.string.none));
        return troly;
    }

    ArrayList<String> tripBoyOptionsList = new ArrayList<>();

    public ArrayList<String> getTripBoyOptions(Context context) {
        tripBoyOptionsList.add("Plantation");
        tripBoyOptionsList.add("Rujvat");
        tripBoyOptionsList.add("KYC");
        tripBoyOptionsList.add("Tokenchitti Approval");
        return tripBoyOptionsList;
    }

    ArrayList<RopTypeModel> ropTypeList = new ArrayList<>();

    public ArrayList<RopTypeModel> getRopType(Context context) {
        RopTypeModel ropTypeModel = new RopTypeModel();
        ropTypeModel.setR_type_code("S");
        ropTypeModel.setR_type_name("Single");
        RopTypeModel ropTypeModel1 = new RopTypeModel();
        ropTypeModel1.setR_type_code("D");
        ropTypeModel1.setR_type_name("Double");
        ropTypeList.add(ropTypeModel);
        ropTypeList.add(ropTypeModel1);

        return ropTypeList;
    }

    ArrayList<DailyHistoryModel> tripBoyOptionsDataList = new ArrayList<>();

    public ArrayList<DailyHistoryModel> getTripBoyOptionsData(Context context) {
        DailyHistoryModel model;

        model = new DailyHistoryModel();
        model.setNAME("Abhishek");
        model.setWORK_TYPE("Crop");
        tripBoyOptionsDataList.add(model);

        model = new DailyHistoryModel();
        model.setNAME("Ben");
        model.setWORK_TYPE("Crop");
        tripBoyOptionsDataList.add(model);

        model = new DailyHistoryModel();
        model.setNAME("Clinton");
        model.setWORK_TYPE("Crop");
        tripBoyOptionsDataList.add(model);

        model = new DailyHistoryModel();
        model.setNAME("Deny");
        model.setWORK_TYPE("Crop");
        tripBoyOptionsDataList.add(model);
        return tripBoyOptionsDataList;
    }
}
