package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.Activity;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.Build;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.ApproveModel;
import com.praful.jass.ismclcaneapp.Models.CandidateWorkDataModel;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import org.json.JSONArray;
import org.json.JSONObject;

public class ApprovalCandidateWorkDetailActivity extends AppCompatActivity {

    ImageView go_back;
    CandidateWorkDataModel candidateWorkDataModel;
    EditText edt_plantation_code, edt_grower_name, edt_village, edt_gut_id, edt_survey_number, edt_plantation_date, edt_plantation_type, edt_plantation_area, edt_start_time, edt_harvester;
    Button btn_work_detail_approve, btn_work_detail_deny;
    Context context;
    String device = "";
    TelephonyManager telephonyManager;
    SystemDateTime dateTime = new SystemDateTime();


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_approval_candidate_work_detail);
        init();
    }

    private void init() {
        context = this;
        Preferences.appContext = context;
        go_back = findViewById(R.id.go_back);
        edt_plantation_code = findViewById(R.id.edt_plantation_code);
        edt_grower_name = findViewById(R.id.edt_grower_name);
        edt_village = findViewById(R.id.edt_village);
        edt_gut_id = findViewById(R.id.edt_gut_id);
        edt_survey_number = findViewById(R.id.edt_survey_number);
        edt_plantation_date = findViewById(R.id.edt_plantation_date);
        edt_plantation_type = findViewById(R.id.edt_plantation_type);
        edt_plantation_area = findViewById(R.id.edt_plantation_area);
        btn_work_detail_approve = findViewById(R.id.btn_work_detail_approve);
        btn_work_detail_deny = findViewById(R.id.btn_work_detail_deny);
        edt_start_time = findViewById(R.id.edt_start_time);
        edt_harvester = findViewById(R.id.edt_harvester);
        candidateWorkDataModel = new Gson().fromJson(getIntent().getStringExtra("model"), CandidateWorkDataModel.class);
        telephonyManager = (TelephonyManager) context.getSystemService(TELEPHONY_SERVICE);
//        locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
        device = getDeviceID();
        //Method Calling
        //getDeviceID();
        setListeners();
        setData();
    }

    private String getDeviceID() {
        if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return null;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            device = telephonyManager.getDeviceId(2);
        } else {
            device = telephonyManager.getDeviceId();//getDeviceId(1);
        }
        return device;
    }

    private void setListeners() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });

        btn_work_detail_approve.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveOrder("A", AppConstants.URL_CUTTING_ORDER_APPROVE);
            }
        });

        btn_work_detail_deny.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                approveOrder("D", AppConstants.URL_CUTTING_ORDER_DENY);
            }
        });
    }

    private void setData() {
        edt_plantation_code.setText(candidateWorkDataModel.getPLANTATION_CODE());
        edt_grower_name.setText(candidateWorkDataModel.getFARMER_NAME());
        edt_village.setText(candidateWorkDataModel.getFARMER_VILLAGE());
        edt_gut_id.setText(candidateWorkDataModel.getFARMER_GUT());
        edt_survey_number.setText(candidateWorkDataModel.getSARVE_NO());
        edt_plantation_date.setText(candidateWorkDataModel.getPLANTATION_DATE());
        edt_plantation_type.setText(candidateWorkDataModel.getPLANTATION_TYPE());
        edt_plantation_area.setText(candidateWorkDataModel.getFARMER_TALUKA());
        edt_start_time.setText(candidateWorkDataModel.getPLOT_START_DATE());
        edt_harvester.setText(candidateWorkDataModel.getHRV_NAME());
    }

    /**
     * API CALLING
     **/
    private void approveOrder(final String flag, String URL) {
        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.setCancelable(false);
        progressDialog.show();
        JSONObject param = new JSONObject();
        JSONObject jsonObject = new JSONObject();
        String lat = "", longg = "";

        try {
            CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
            lat = locationModel.getLatitude() + "";
            longg = locationModel.getLongitude() + "";
            param.put("p_season", Preferences.getSeasonID());
            param.put("p_date", dateTime.systemDateTime("dd-MMM-yyyy"));
            param.put("p_order_id", candidateWorkDataModel.getCUTTING_OBJECT_ID());
            param.put("p_user_id", Preferences.getCurrentUserId());
            param.put("p_device", device);
            param.put("p_lat", lat);
            param.put("p_long", longg);
            //param.put("p_out", Preferences.getSeasonID());
            JSONArray jsonArray = new JSONArray();
            jsonArray.put(param);
            jsonObject.put("CuttingOrder", jsonArray);


            new APIRestManager().postAPI(URL, jsonObject, context, ApproveModel.class, new APIRestManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    progressDialog.dismiss();
                    if (flag.equalsIgnoreCase("A"))
                        Toast.makeText(context, "Approve Successfully", Toast.LENGTH_SHORT).show();
                    else
                        Toast.makeText(context, "Cutting Order Deny", Toast.LENGTH_SHORT).show();

                    ((Activity) context).finish();
                }

                @Override
                public void onError(String strError) {
                    progressDialog.dismiss();
                    Toast.makeText(context, strError, Toast.LENGTH_SHORT).show();
                }
            });

        } catch (Exception ex) {
            progressDialog.dismiss();
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
//352345080195400
//352345080512695 AGRIRBM
//KMM

//357278089539358
//357278089539358ksl  352345080418042

//352345080067203 AGRIVMP