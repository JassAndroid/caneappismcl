package com.praful.jass.ismclcaneapp.services;

import android.Manifest;
import android.app.Activity;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.location.GpsSatellite;
import android.location.GpsStatus;
import android.location.Location;
import android.location.LocationListener;
import android.location.LocationManager;
import android.os.Bundle;
import android.os.IBinder;
import android.preference.PreferenceManager;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.View;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Dialog.GPSDisabledMessageDialog;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import java.util.Timer;
import java.util.TimerTask;

/**
 * Created by fabio on 30/01/2016.
 */
public class SensorService extends Service implements LocationListener, GpsStatus.Listener {
    public int counter = 0;

    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    //    GPSTracker gps;
    private LocationManager mLocationManager;
    private static Data data;
    TelephonyManager telephonyManager;
    SystemDateTime dateTime = new SystemDateTime();
    SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
    Location location;
    private Data.onGpsServiceUpdate onGpsServiceUpdate;
    private SharedPreferences sharedPreferences;
    Context applicationContex;
    Activity activity;

    public SensorService(Context applicationContext) {
        super();
        this.applicationContex = applicationContext;
        Log.i("HERE", "here I am!");
    }

    public SensorService(Activity activity) {
        super();
        this.activity = activity;
        Log.i("HERE", "here I am!");
    }

    public SensorService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            super.onStartCommand(intent, flags, startId);
//        gps = new GPSTracker(this);
            Preferences.appContext = this;
            telephonyManager = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);


            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(activity, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
            data = new Data(onGpsServiceUpdate);

            sharedPreferences = PreferenceManager.getDefaultSharedPreferences(this);

            mLocationManager = (LocationManager) this.getSystemService(Context.LOCATION_SERVICE);
            startTimer();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Log.i("EXIT", "ondestroy!");
            Intent broadcastIntent = new Intent("link");
            sendBroadcast(broadcastIntent);
            stoptimertask();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        try {
            //set a new Timer
            timer = new Timer();
            //initialize the TimerTask's job
            initializeTimerTask();
            //schedule the timer, to wake up every 1 second
            timer.schedule(timerTask, 500, Long.parseLong(Preferences.getTrackingTime())); //
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        try {
            timerTask = new TimerTask() {
                public void run() {
                    Log.i("in timer", "GPS:- " + " , " + " :-" + (counter++));
                    try {
                        if (location.hasAccuracy()) {
//                        AppConstants.location = location;
//                        getListFromServer(location.getLatitude() + "", location.getLongitude() + "");
                            Log.i("in timer", "GPS:- " + location.getLatitude() + " , " + location.getLongitude() + " :-" + (counter++));
                            Toast.makeText(getApplication(), "Your Location is - \nLat: " + location.getLatitude() + "\nLong: " + location.getLongitude(), Toast.LENGTH_LONG).show();
                        } else {
                            showGpsDisabledDialog();
                        }

                    } catch (Exception e) {
                        e.printStackTrace();
                    }
                }
            };
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        //stop the timer, if it's not already null
        try {
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

//    void getListFromServer(String apiUrl, Class modelType, String lat, String longg) {
//        JSONObject jsonObject = new JSONObject();
//        JSONArray jsonArray = new JSONArray();
//        String deviceID = "";
//        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
//            deviceID = telephonyManager.getDeviceId(2);
//        } else {
//            deviceID = telephonyManager.getDeviceId();
//        }
//        try {
//            JSONObject innerjsonObject = new JSONObject();
//            innerjsonObject.put("p_device", deviceID);
//            innerjsonObject.put("p_date", dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss"));//dd-MMM-yyyy HH:mm:ss);
//            innerjsonObject.put("p_lat", lat);
//            innerjsonObject.put("p_lon", longg);
//            jsonArray.put(innerjsonObject);
//            jsonObject.put("TrackEntry", jsonArray);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        new APIRestManager().postAPI(apiUrl, jsonObject, this, modelType, new APIRestManager.APIManagerInterface() {
//
//            @Override
//            public void onSuccess(Object resultObj) {
//
//                if (resultObj instanceof TrackingResultModel) {
//                }
//            }
//
//            @Override
//            public void onError(String strError) {
//                //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
//            }
//        });
//    }

    void getListFromServer(String lat, String longg) {
        try {
            String deviceID = "";
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                deviceID = telephonyManager.getDeviceId(2);
            } else {
                deviceID = telephonyManager.getDeviceId();
            }
            sqLiteHelper.insertTraceEmployeeData(deviceID, dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss"), lat, longg);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void showGpsDisabledDialog() {
        try {
            final GPSDisabledMessageDialog gpsDisabledMessageDialog = new GPSDisabledMessageDialog(this, getResources().getString(R.string.gps_disabled), getResources().getString(R.string.please_enable_gps));
//        Dialog dialog = new Dialog(this, getResources().getString(R.string.gps_disabled), getResources().getString(R.string.please_enable_gps));

            gpsDisabledMessageDialog.btn_accept.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    startActivity(new Intent("android.settings.LOCATION_SOURCE_SETTINGS"));
                    gpsDisabledMessageDialog.dismiss();
                }
            });
            gpsDisabledMessageDialog.show();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public static Data getData() {
        return data;
    }

    @Override
    public void onGpsStatusChanged(int event) {
        try {
            switch (event) {
                case GpsStatus.GPS_EVENT_SATELLITE_STATUS:
                    if (ActivityCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                        // TODO: Consider calling
                        //    ActivityCompat#requestPermissions
                        // here to request the missing permissions, and then overriding
                        //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                        //                                          int[] grantResults)
                        // to handle the case where the user grants the permission. See the documentation
                        // for ActivityCompat#requestPermissions for more details.
                        return;
                    }
                    GpsStatus gpsStatus = mLocationManager.getGpsStatus(null);
                    int satsInView = 0;
                    int satsUsed = 0;
                    Iterable<GpsSatellite> sats = gpsStatus.getSatellites();
                    for (GpsSatellite sat : sats) {
                        satsInView++;
                        if (sat.usedInFix()) {
                            satsUsed++;
                        }
                    }
                    if (satsUsed == 0) {
                        data.setRunning(false);
                        stopService(new Intent(this, GpsServices.class));
//                    firstfix = true;
                    }
                    break;

                case GpsStatus.GPS_EVENT_STOPPED:
                    if (!mLocationManager.isProviderEnabled(LocationManager.GPS_PROVIDER)) {
                        showGpsDisabledDialog();
                    }
                    break;
                case GpsStatus.GPS_EVENT_FIRST_FIX:
                    break;
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onLocationChanged(Location location) {
//        AppConstants.location = location;
        this.location = location;
    }

    @Override
    public void onStatusChanged(String s, int i, Bundle bundle) {

    }

    @Override
    public void onProviderEnabled(String s) {

    }

    @Override
    public void onProviderDisabled(String s) {

    }
}