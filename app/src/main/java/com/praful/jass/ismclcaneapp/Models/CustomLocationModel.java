package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 05-05-2018.
 */

public class CustomLocationModel {

    double latitude;
    double longitude;

    public double getLatitude() {
        return latitude;
    }

    public void setLatitude(double latitude) {
        this.latitude = latitude;
    }

    public double getLongitude() {
        return longitude;
    }

    public void setLongitude(double longitude) {
        this.longitude = longitude;
    }
}
