package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.net.ConnectivityManager;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Fragments.Phase2.Phase2GenerateTokenFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase2.Phase2TransferTokenFragment;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.CheckInternet;
import com.praful.jass.ismclcaneapp.Utils.Preferences;


public class MenuFragment extends Fragment {

    RelativeLayout layout_plantation, layout_rujvat, layout_employeeTrip, layout_addPlantation, layout_dailyHistory, layout_todaywork, layout_update_server, layout_work_history;
    RelativeLayout menu_slip_boy, menu_tod_chitti_start, menu_tod_chitti_end;
    RelativeLayout rlGenerateToken;
    RelativeLayout rlTransferToken;
    TextView userIdTextView, userStatusTextView, serverIdTextView, seasonIdTextView;
    TextView lbl_user_id, lbl_user_status, lbl_user_sessionId, lbl_user_ip;

    Preferences myPreferences;
//    private static Locale myLocale;

    //    Bundle bundle;
    FragmentTransaction fragmentTransaction;
    public static Phase2TransferTokenFragment transferTokenFragment = new Phase2TransferTokenFragment();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_menu, container, false);
        try {
            // Inflate the layout for this fragment
            myPreferences = new Preferences();
            init(view);
            Onclick();
//            changeLocale("hi", view.getContext());
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return view;
    }


    public void init(View view) {
        try {
            layout_plantation = (RelativeLayout) view.findViewById(R.id.layout_plantation);
            layout_rujvat = (RelativeLayout) view.findViewById(R.id.layout_rujvat);
            layout_employeeTrip = (RelativeLayout) view.findViewById(R.id.menu_employee_trip);
            layout_addPlantation = (RelativeLayout) view.findViewById(R.id.menu_add_plantation);
//        layout_dailyHistory = (RelativeLayout) view.findViewById(R.id.menu_daily_history);
//        layout_todaywork = (RelativeLayout) view.findViewById(R.id.menu_today_work);
            layout_update_server = (RelativeLayout) view.findViewById(R.id.menu_update_server);
            layout_work_history = view.findViewById(R.id.menu_work_history);
            menu_slip_boy = (RelativeLayout) view.findViewById(R.id.menu_slip_boy);
            menu_tod_chitti_start = (RelativeLayout) view.findViewById(R.id.menu_tod_chitti_start);
            menu_tod_chitti_end = (RelativeLayout) view.findViewById(R.id.menu_tod_chitti_end);
            rlGenerateToken = (RelativeLayout) view.findViewById(R.id.menu_generate_token);
            rlTransferToken = (RelativeLayout) view.findViewById(R.id.menu_transfer_token);
            userIdTextView = (TextView) view.findViewById(R.id.user_id);
            userStatusTextView = (TextView) view.findViewById(R.id.user_status);
            userIdTextView.setText(Preferences.getUserName() + " (" + String.valueOf(Preferences.getCurrentUserId()) + ")");
            serverIdTextView = view.findViewById(R.id.user_ip);
            seasonIdTextView = (TextView) view.findViewById(R.id.user_sessionId);
            serverIdTextView.setText(Preferences.getServerIP());
            seasonIdTextView.setText(Preferences.getSeasonName());

            lbl_user_id = view.findViewById(R.id.lbl_user_id);
            lbl_user_status = view.findViewById(R.id.lbl_user_status);
            lbl_user_sessionId = view.findViewById(R.id.lbl_user_sessionId);
            lbl_user_ip = view.findViewById(R.id.lbl_user_ip);

            BroadcastReceiver statusUpdator = new BroadcastReceiver() {
                @Override
                public void onReceive(Context context, Intent intent) {
                    if (getContext() != null) {
                        if (CheckInternet.isNetworkAvailable(getContext())) {
                            userStatusTextView.setText("Online");
                        } else {
                            userStatusTextView.setText("Offline");
                        }
                    }
                }
            };

            getActivity().registerReceiver(statusUpdator, new IntentFilter(ConnectivityManager.CONNECTIVITY_ACTION));
            FragmentManager fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void Onclick() {
        try {


            layout_work_history.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        SlipBoyListFragment slipBoyListFragment = new SlipBoyListFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("Purpose", "history");
                        slipBoyListFragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.frame, slipBoyListFragment);
                        fragmentTransaction.commit();
                    }
                }
            });
            menu_slip_boy.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        SlipBoyListFragment slipBoyListFragment = new SlipBoyListFragment();
                        Bundle bundle = new Bundle();
                        bundle.putString("Purpose", "approve");
                        slipBoyListFragment.setArguments(bundle);
                        fragmentTransaction.replace(R.id.frame, slipBoyListFragment);
                        fragmentTransaction.commit();
                    }
                }
            });

            menu_tod_chitti_start.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        CuttingOrderStartFragment cuttingOrderStartFragment = new CuttingOrderStartFragment();
                        fragmentTransaction.replace(R.id.frame, cuttingOrderStartFragment);
                        fragmentTransaction.commit();
                    }
                }
            });

            menu_tod_chitti_end.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        CuttingOrderEndFragment cuttingOrderEndFragment = new CuttingOrderEndFragment();
                        fragmentTransaction.replace(R.id.frame, cuttingOrderEndFragment);
                        fragmentTransaction.commit();
                    }
                }
            });


            layout_plantation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
//                fragmentTransaction.setCustomAnimations(R.anim.slide_left, R.anim.slide_right);
//                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        PlantationRegistrationFragment plantationRegistrationFragment = new PlantationRegistrationFragment();
                        fragmentTransaction.replace(R.id.frame, plantationRegistrationFragment);
//                bundle.putInt("registrationType", AppConstants.PLANTATION_REGISTRATION);
//                plantationRegistrationFragment.setArguments(bundle);
                        fragmentTransaction.commit();
//                Intent intent = new Intent(getContext(), PlantationRegistrationActivity.class);
//                intent.putExtra("registrationType", AppConstants.PLANTATION_REGISTRATION);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                    }
                }
            });

            layout_rujvat.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        RujvatFragment rujvatFragment = new RujvatFragment();
                        fragmentTransaction.replace(R.id.frame, rujvatFragment);
//                bundle.putInt("registrationType", AppConstants.RUJVAT_REGISTRATION);
//                plantationRegistrationFragment.setArguments(bundle);
                        fragmentTransaction.commit();

//                Intent intent = new Intent(getContext(), PlantationRegistrationActivity.class);
//                intent.putExtra("registrationType", AppConstants.RUJVAT_REGISTRATION);
//                startActivity(intent);
//                overridePendingTransition(R.anim.slide_left, R.anim.slide_right);
                    }
                }
            });
            layout_employeeTrip.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    EmployeeTripFragment employeeTripFragment = new EmployeeTripFragment();
                    fragmentTransaction.replace(R.id.frame, employeeTripFragment);
                    fragmentTransaction.commit();
                }
            });
//        layout_dailyHistory.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                DailyHistoryFragment dailyHistoryFragment = new DailyHistoryFragment();
//                fragmentTransaction.replace(R.id.frame, dailyHistoryFragment);
//                fragmentTransaction.commit();
//
////                Intent intent = new Intent(getContext(), DailyHistoryActivity.class);
////                startActivity(intent);
//            }
//        });
//        layout_todaywork.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                TodaysWorkoutFragment todaysWorkoutFragment = new TodaysWorkoutFragment();
//                fragmentTransaction.replace(R.id.frame, todaysWorkoutFragment);
//                fragmentTransaction.commit();
//
////                Intent intent = new Intent(getContext(), TodaysWorkoutActivity.class);
////                startActivity(intent);
//            }
//        });

            layout_update_server.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    UpdateServerFragment updateServerFragment = new UpdateServerFragment();
                    fragmentTransaction.replace(R.id.frame, updateServerFragment);
                    fragmentTransaction.commit();
                }
            });

            rlGenerateToken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        Phase2GenerateTokenFragment Phase2GenerateTokenFragment = new Phase2GenerateTokenFragment();
                        fragmentTransaction.replace(R.id.frame, Phase2GenerateTokenFragment);
                        fragmentTransaction.commit();
                    }
                }
            });

            rlTransferToken.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(v, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        fragmentTransaction.replace(R.id.frame, transferTokenFragment);
                        fragmentTransaction.commit();
                    }
                }
            });

            layout_addPlantation.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP)) {
                        Snackbar.make(view, getString(R.string.start_trip), Snackbar.LENGTH_LONG).show();
                    } else {
                        KYCFragment kycFragment = new KYCFragment();
                        fragmentTransaction.replace(R.id.frame, kycFragment);
                        fragmentTransaction.commit();
                    }
                }
            });

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


   /* //Change Locale
    public void changeLocale(String lang, Context context) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);//Set Selected Locale
//        saveLocale(lang);//Save the selected locale
        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());//Update the config
        updateTexts();//Update texts according to locale
    }

    private void updateTexts() {
        lbl_user_id.setText(R.string.user_id_labal);
        lbl_user_status.setText(R.string.user_serverip_labal);
        lbl_user_sessionId.setText(R.string.user_sessionId_labal);
        lbl_user_ip.setText(R.string.user_status_labal);
    }*/
}
