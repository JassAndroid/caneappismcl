package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;

import com.praful.jass.ismclcaneapp.R;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;


public class EmergencyBackupActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_emergency_backup);
        try {
            ServerSocket sersock = new ServerSocket(3033);    //instruction7 System.out.println("Server ready for chatting");       //instruction 8
            Socket sock = sersock.accept();                                //instruction 9
            BufferedReader keyRead = new BufferedReader(new InputStreamReader(System.in));
            OutputStream ostream = sock.getOutputStream();
            PrintWriter pwrite = new PrintWriter(ostream, true);
            InputStream istream = sock.getInputStream();
            BufferedReader receiveRead = new BufferedReader(new InputStreamReader(istream));
            String receiveMessage, sendMessage;
            while (true) {
                if ((receiveMessage = receiveRead.readLine()) != null) {
                    System.out.println(receiveMessage);
                    sendMessage = keyRead.readLine();
                    pwrite.println(sendMessage);
                    pwrite.flush();
                }
            }

        } catch (Exception ex) {
            ex.printStackTrace();
        }
    }
}
