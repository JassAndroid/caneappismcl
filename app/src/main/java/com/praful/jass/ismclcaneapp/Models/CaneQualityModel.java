package com.praful.jass.ismclcaneapp.Models;

public class CaneQualityModel {
    String NAME, QUALITY_ID;

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getQUALITY_ID() {
        return QUALITY_ID;
    }

    public void setQUALITY_ID(String QUALITY_ID) {
        this.QUALITY_ID = QUALITY_ID;
    }
}
