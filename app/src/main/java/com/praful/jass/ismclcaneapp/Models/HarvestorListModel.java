package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-09-2017.
 */

public class HarvestorListModel {
    String H_NAME, CODE, OBJECT_ID;

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getH_NAME() {
        return H_NAME;
    }

    public void setH_NAME(String h_NAME) {
        H_NAME = h_NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }
}
