package com.praful.jass.ismclcaneapp.Utils;

/**
 * Created by Praful on 11-05-2017.
 */


import android.content.Context;
import android.widget.Toast;

import java.util.ArrayList;

public class CalculateAreaFromGeoLocation {
//    public static void main(String[] args) throws java.lang.Exception {
//        ArrayList<Double> lat = new ArrayList();
//        ArrayList<Double> longs = new ArrayList();
//
//        lat.add(21.110512);
//        longs.add(79.070251);
//
//        lat.add(21.105828);
//        longs.add(79.088147);
//
//        lat.add(21.094107);
//        longs.add(79.084317);
//
//        lat.add(21.098741);
//        longs.add(79.066132);
//
//        double area = new CalculateAreaFromGeoLocation().getArea(lat, longs);
//        System.out.println(area);
//    }

    public double getArea(ArrayList<Double> lats, ArrayList<Double> lons, Context context) {
        double sum = 0;
        try {
            double prevcolat = 0;
            double prevaz = 0;
            double colat0 = 0;
            double az0 = 0;
            for (int i = 0; i < lats.size(); i++) {
                double colat = 2 * Math.atan2(Math.sqrt(Math.pow(Math.sin(lats.get(i) * Math.PI / 180 / 2), 2) + Math.cos(lats.get(i) * Math.PI / 180) * Math.pow(Math.sin(lons.get(i) * Math.PI / 180 / 2), 2)), Math.sqrt(1 - Math.pow(Math.sin(lats.get(i) * Math.PI / 180 / 2), 2) - Math.cos(lats.get(i) * Math.PI / 180) * Math.pow(Math.sin(lons.get(i) * Math.PI / 180 / 2), 2)));
                double az = 0;
                if (lats.get(i) >= 90) {
                    az = 0;
                } else if (lats.get(i) <= -90) {
                    az = Math.PI;
                } else {
                    az = Math.atan2(Math.cos(lats.get(i) * Math.PI / 180) * Math.sin(lons.get(i) * Math.PI / 180), Math.sin(lats.get(i) * Math.PI / 180)) % (2 * Math.PI);
                }
                if (i == 0) {
                    colat0 = colat;
                    az0 = az;
                }
                if (i > 0 && i < lats.size()) {
                    sum = sum + (1 - Math.cos(prevcolat + (colat - prevcolat) / 2)) * Math.PI * ((Math.abs(az - prevaz) / Math.PI) - 2 * Math.ceil(((Math.abs(az - prevaz) / Math.PI) - 1) / 2)) * Math.signum(az - prevaz);
                }
                prevcolat = colat;
                prevaz = az;
            }
            sum = sum + (1 - Math.cos(prevcolat + (colat0 - prevcolat) / 2)) * (az0 - prevaz);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return 5.10072E14 * Math.min(Math.abs(sum) / 4 / Math.PI, 1 - Math.abs(sum) / 4 / Math.PI);
    }
}