package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class SetTrackTimeDialog extends Dialog {
    public EditText timeinseconds;
    //    public TextView timeinminutes;
    public Button btn_submit;
    //    private TextWatcher ipAddressWatcher;
    private Context context;

    public SetTrackTimeDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_set_track_time);
            init();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            timeinseconds = (EditText) findViewById(R.id.timeinseconds);
            timeinseconds.setText("");
            btn_submit = (Button) findViewById(R.id.btn_submit_ip);
            timeinseconds.addTextChangedListener(new TextWatcher() {
                @Override
                public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

                }

                @Override
                public void onTextChanged(CharSequence charSequence, int i, int i1, int i2) {
                    String time = timeinseconds.getText().toString();
                    if (!time.equals("")) {
                        int input = Integer.parseInt(time);

                        int hours = input / 3600;
                        int minutes = (input % 3600) / 60;
                        int seconds = (input % 3600) % 60;
//                    timeinminutes.setText("Hr:- " + hours + " min:- " + minutes + " sec:- " + seconds);
                    }
                }
                @Override
                public void afterTextChanged(Editable editable) {
                }
            });
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
