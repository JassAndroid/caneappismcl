package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 22-06-2017.
 */

public class TrackingResultModel {
    private String code;

    public String getCode() {
        return code;
    }

    public void setCode(String code) {
        this.code = code;
    }
}
