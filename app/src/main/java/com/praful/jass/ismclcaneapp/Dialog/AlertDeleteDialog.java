package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class AlertDeleteDialog extends Dialog {
    public TextView message_txt;
    //    public TextView timeinminutes;
    public Button btn_submit, btn_no;
    //    private TextWatcher ipAddressWatcher;
    private Context context;

    public AlertDeleteDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_alert_delete);
            init();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            message_txt = (TextView) findViewById(R.id.message_txt);
            message_txt.setText("");
//        timeinminutes = (TextView) findViewById(R.id.timeinminutes);
            btn_submit = (Button) findViewById(R.id.btn_ok);
            btn_no = (Button) findViewById(R.id.btn_no);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
