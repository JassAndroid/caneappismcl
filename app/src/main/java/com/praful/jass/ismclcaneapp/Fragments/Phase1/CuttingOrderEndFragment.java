package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.Models.CuttingOrderStartModel;
import com.praful.jass.ismclcaneapp.Models.CuttingOrderSubmitModel;
import com.praful.jass.ismclcaneapp.Models.HarvestingForCuttingOrderModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import org.json.JSONArray;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;

public class CuttingOrderEndFragment extends Fragment {

    Button btn_cutting_order_submit;
    Context context;
    public ProgressDialog pdLoading;
    LinearLayout ll_plantation_id, ll_harvestor_code;
    TextView txt_plantation_id, txt_village, txt_gut, txt_survey, txt_irre_name, txt_cane_verity, txt_plantation_date_tc, txt_plantation_type_tc, txt_farmer_name, txt_farmer_village, txt_shivar_gut, txt_shivar_subgut, txt_taluka, txt_harvestor_code, txt_start_date;
    EditText txt_end_date;
    int selectListType = 0;
    private OptionListFragment fragment;
    private FragmentManager fragmentManager;
    ArrayList<String> growarCode = new ArrayList();
    ArrayList<String> growarNameList = new ArrayList();
    ArrayList<CuttingOrderStartModel> cuttingOrderStartModel;
    ArrayList<HarvestingForCuttingOrderModel> harvestingForCuttingOrderModel;
    TelephonyManager telephonyManager;
    String harvesterID = "", device = "", orderID = "";
    SystemDateTime dateTime = new SystemDateTime();
    FragmentTransaction fragmentTransaction;
    ArrayList<String> plantationListList = new ArrayList();
    ArrayList<String> docNoList = new ArrayList();

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_cutting_order_end, container, false);
        telephonyManager = (TelephonyManager) getActivity().getSystemService(getActivity().getBaseContext().TELEPHONY_SERVICE);
        init(view);
        return view;
    }

    private void init(View view) {
        context = getActivity();
        fragmentManager = getActivity().getSupportFragmentManager();
        btn_cutting_order_submit = view.findViewById(R.id.btn_cutting_order_submit);
        pdLoading = new ProgressDialog(getActivity());
        pdLoading.setCancelable(false);
        txt_plantation_id = view.findViewById(R.id.txt_plantation_id);
        txt_village = view.findViewById(R.id.txt_village);
        txt_gut = view.findViewById(R.id.txt_gut);
        txt_survey = view.findViewById(R.id.txt_survey);
        txt_irre_name = view.findViewById(R.id.txt_irre_name);
        txt_cane_verity = view.findViewById(R.id.txt_cane_verity);
        txt_plantation_date_tc = view.findViewById(R.id.txt_plantation_date_tc);
        txt_plantation_type_tc = view.findViewById(R.id.txt_plantation_type_tc);
        txt_farmer_name = view.findViewById(R.id.txt_farmer_name);
        txt_farmer_village = view.findViewById(R.id.txt_farmer_village);
        txt_shivar_gut = view.findViewById(R.id.txt_shivar_gut);
        txt_shivar_subgut = view.findViewById(R.id.txt_shivar_subgut);
        txt_taluka = view.findViewById(R.id.txt_taluka);
        txt_harvestor_code = view.findViewById(R.id.txt_harvestor_code);
        txt_start_date = view.findViewById(R.id.txt_start_date);
        txt_end_date = view.findViewById(R.id.txt_end_date);
        ll_plantation_id = view.findViewById(R.id.ll_plantation_id);
        ll_harvestor_code = view.findViewById(R.id.ll_harvestor_code);

        txt_end_date.addTextChangedListener(endDate);

        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            device = telephonyManager.getDeviceId(2);
        } else {
            device = telephonyManager.getDeviceId();//getDeviceId(1);
        }

        //Method calling
        setListeners();
    }

    private void setListeners() {

        btn_cutting_order_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    if (validateData()) {
                        CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
                        JSONObject jsonObject = new JSONObject();
                        JSONArray jsonArray = new JSONArray();
                        jsonObject.put("p_season", Preferences.getSeasonID());
                        jsonObject.put("p_date", changeDateFormate(txt_end_date.getText() + ""));
                        jsonObject.put("p_order_id", orderID);
                        jsonObject.put("p_user_id", Preferences.getCurrentUserId());
                        jsonObject.put("p_device", device);
                        jsonObject.put("p_lat", locationModel.getLatitude());
                        jsonObject.put("p_long", locationModel.getLongitude());
                        jsonArray.put(jsonObject);
                        JSONObject jsonObject1 = new JSONObject();
                        jsonObject1.put("CuttingOrder", jsonArray);
                        selectListType = AppConstants.CUTTING_SUBMIT_SELECT;
                        callAPI(AppConstants.URL_CUTTING_ORDER_END_SUBMIT, CuttingOrderSubmitModel.class, jsonObject1);
                    }
                } catch (Exception ex) {

                }
            }

        });

        ll_plantation_id.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                pdLoading.setMessage("\tLoading...");
                pdLoading.show();
                selectListType = AppConstants.PLANTATION_CUTTING_ORDER;
                callAPI(AppConstants.URL_CUTTING_ORDER_END, CuttingOrderStartModel.class);
            }
        });

//        ll_harvestor_code.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View v) {
//                pdLoading.setMessage("\tLoading...");
//                pdLoading.show();
//                selectListType = AppConstants.HARVESTING_CUTTING_ORDER;
//                callAPI(AppConstants.URL_CUTTING_ORDER_HARVESTING, HarvestingForCuttingOrderModel.class);
//            }
//        });
    }

    private String changeDateFormate(String startDateString) {
//        startDateString = "27/06/2007";
        String newDateString = "";
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date startDate;
            try {
                startDate = df.parse(startDateString);
                newDateString = df1.format(startDate);
                System.out.println(newDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return newDateString;
    }

    TextWatcher endDate = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int end, int before, int count) {
            try {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8) {
                        clean = clean + ddmmyyyy.substring(clean.length());
                    } else {
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int day = Integer.parseInt(clean.substring(0, 2));
                        int mon = Integer.parseInt(clean.substring(2, 4));
                        int year = Integer.parseInt(clean.substring(4, 8));

                        if (mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 2000) ? 2000 : (year > 2200) ? 2200 : year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012


                        day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                        clean = String.format("%02d%02d%02d", day, mon, year);
                    }

                    clean = String.format("%s/%s/%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    txt_end_date.setText(current);
                    txt_end_date.setSelection(sel < current.length() ? sel : current.length());
                }
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private void callAPI(String apiURI, Class modelType) {
        try {
            JSONObject jsonObject = new JSONObject();
            if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
                jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                jsonObject.put("season", Preferences.getSeasonID() + "");
//            } else if (selectListType == AppConstants.HARVESTING_CUTTING_ORDER) {
//                jsonObject.put("season", Preferences.getSeasonID() + "");
            }

            callAPI(apiURI, modelType, jsonObject);
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void callAPI(String apiURI, Class modelType, JSONObject inputJsone) {
        try {
            new APIRestManager().postAPI(apiURI, inputJsone, context, modelType, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {
                    try {
                        if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
                            if (resultObj instanceof ArrayList) {
                                cuttingOrderStartModel = (ArrayList<CuttingOrderStartModel>) resultObj;
                                ArrayList<String> arrayList = new ArrayList<>();
                                for (int i = 0; i < cuttingOrderStartModel.size(); i++) {
                                    arrayList.add(cuttingOrderStartModel.get(i).getHRV_CODE());
                                    plantationListList.add(cuttingOrderStartModel.get(i).getPLANTATION_CODE());
                                    docNoList.add(cuttingOrderStartModel.get(i).getCUTTING_OBJECT_ID());
                                    growarNameList.add(cuttingOrderStartModel.get(i).getFARMER_NAME());
                                }
                                showOptionList(arrayList, txt_plantation_id);
//                                new AsyncCaller().execute();
                            }
//                        } else if (selectListType == AppConstants.HARVESTING_CUTTING_ORDER) {
//                            if (resultObj instanceof ArrayList) {
//                                harvestingForCuttingOrderModel = (ArrayList<HarvestingForCuttingOrderModel>) resultObj;
//                                new AsyncCaller().execute();
//                            }
                        } else if (selectListType == AppConstants.CUTTING_SUBMIT_SELECT) {
                            Toast.makeText(getContext(), "Current Order Successfully Submitted", Toast.LENGTH_SHORT).show();
                            MenuFragment menuFragment = new MenuFragment();
                            fragmentTransaction = fragmentManager.beginTransaction();
                            fragmentTransaction.replace(R.id.frame, menuFragment);
                            fragmentTransaction.commit();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(String strError) {
                    try {
                        pdLoading.dismiss();
                        if (strError == null) {
                            Snackbar.make(getView().findViewById(R.id.cutting_order_end), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();

                        } else {
                            Snackbar.make(getView().findViewById(R.id.cutting_order_end), strError, Snackbar.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private class AsyncCaller extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
//            pdLoading.setMessage("\tLoading...");
//                pdLoading.show();

        }

        @Override
        protected Void doInBackground(Void... params) {

            if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
                ArrayList<String> arrayList = new ArrayList<>();
                for (int i = 0; i < cuttingOrderStartModel.size(); i++) {
                    arrayList.add(cuttingOrderStartModel.get(i).getPLANTATION_CODE());
                    plantationListList.add(cuttingOrderStartModel.get(i).getPLANTATION_CODE());
                    docNoList.add(cuttingOrderStartModel.get(i).getCUTTING_OBJECT_ID());
                    growarNameList.add(cuttingOrderStartModel.get(i).getFARMER_NAME());
                }
                showOptionList(arrayList, txt_plantation_id);
//            } else if (selectListType == AppConstants.HARVESTING_CUTTING_ORDER) {
//                ArrayList<String> arrayList = new ArrayList<>();
//                for (int i = 0; i < harvestingForCuttingOrderModel.size(); i++) {
//                    arrayList.add(harvestingForCuttingOrderModel.get(i).getH_NAME());
//                }
//                showOptionList(arrayList, txt_harvestor_code);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    private void showOptionList(ArrayList<String> listArray, final TextView textView) {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(listArray, textView.getText().toString(), growarCode);
        if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
            fragment.getFarmerNameList(growarNameList);
        }
        if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
            fragment.getplantationIDList(plantationListList, docNoList);
        }
        fragment.setListTitle("kljklkllk");

        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                removeOptionList();
            }

            @Override
            public void onListItemSelected(Object model) {

                if (selectListType == AppConstants.PLANTATION_CUTTING_ORDER) {
                    String value = (String) model;
                    txt_plantation_id.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getPLANTATION_CODE());
                    txt_plantation_id.setTextColor(getResources().getColor(R.color.colorSelectedValueInPlantationAndRujvat));
                    txt_village.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getSHIVAR_VILLAGE_NAME());
                    txt_gut.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getFARMER_GUT());
                    txt_survey.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getSARVE_NO());
                    txt_irre_name.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getIRRE_NAME());
                    txt_cane_verity.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getVARITY());
                    txt_plantation_date_tc.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getPLANTATION_DATE());
                    txt_plantation_type_tc.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getPLANTATION_TYPE());
                    txt_farmer_name.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getFARMER_NAME());
                    txt_farmer_village.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getFARMER_VILLAGE());
                    txt_shivar_gut.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getSHIVAR_GUT_NAME());
                    txt_shivar_subgut.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getFARMER_SUBGUT());
                    txt_taluka.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getFARMER_TALUKA());
                    orderID = cuttingOrderStartModel.get(AppConstants.selectItemIndex).getCUTTING_OBJECT_ID();
                    txt_harvestor_code.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getHRV_NAME());
                    txt_start_date.setText(cuttingOrderStartModel.get(AppConstants.selectItemIndex).getPLOT_START_DATE());
//                } else if (selectListType == AppConstants.HARVESTING_CUTTING_ORDER) {
//                    String value = (String) model;
//                    txt_harvestor_code.setText(value);
//                    txt_harvestor_code.setTextColor(getResources().getColor(R.color.colorSelectedValueInPlantationAndRujvat));
//                    harvesterID = harvestingForCuttingOrderModel.get(AppConstants.selectItemIndex).getH_ID();
                }
                AppConstants.selectItemIndex = 0;
                removeOptionList();
            }
        };
        ft.add(R.id.cutting_order_end, fragment);
        ft.commit();
        pdLoading.dismiss();
    }

    void removeOptionList() {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        ft.remove(fragment).commit();
        growarNameList.clear();
        plantationListList.clear();
        docNoList.clear();
    }

    private boolean validateData() {

        if (txt_plantation_id.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Plantation", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (txt_end_date.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Enter End Date", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (checkDateFormat(txt_end_date.getText().toString())) {
            Toast.makeText(getContext(), "Date Format Wrong", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (validateDate(txt_end_date.getText().toString())) {
            Toast.makeText(getContext(), "Enter Valid End Date", Toast.LENGTH_SHORT).show();
            return false;
        }

        if (validateDate(txt_end_date.getText().toString(), txt_start_date.getText().toString())) {
            Toast.makeText(getContext(), "End Date Not Valid", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    private boolean validateDate(String myDate) {
        boolean returnVal = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Date dLastUpdateDate = sdf.parse(myDate);
            Date dCurrentDate = dateFormat.parse(dateFormat.format(new Date()));
            if (dCurrentDate.after(dLastUpdateDate)) {
                returnVal = true;
            } else {
                returnVal = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnVal;
    }
    private boolean validateDate(String myDate, String starDate) {
        boolean returnVal = false;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MMM-yy");
            final SimpleDateFormat dateFormat = new SimpleDateFormat("yyyyMMdd");
            Date dLastUpdateDate = sdf.parse(myDate);
            Date dCurrentDate = dateFormat.parse(starDate);
            if (dCurrentDate.after(dLastUpdateDate)) {
                returnVal = true;
            } else {
                returnVal = false;
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnVal;
    }

    private boolean checkDateFormat(String value) {
        Date date = null;
        boolean rerurnVal = true;
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd/MM/yyyy");
            date = sdf.parse(value);
            if (!value.equals(sdf.format(date))) {
                date = null;
            }
        } catch (ParseException ex) {
            rerurnVal = true;
            ex.printStackTrace();
        }
        if (date == null) {
            rerurnVal = true;
        } else {
            rerurnVal = false;
        }
        return rerurnVal;
    }
}
//AGRICSB
//AGRIMHK