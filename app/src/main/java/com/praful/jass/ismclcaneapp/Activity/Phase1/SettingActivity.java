package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.SwitchCompat;
import android.view.View;
import android.widget.CompoundButton;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.ChangeLocaleLanguage;
import com.praful.jass.ismclcaneapp.Utils.Preferences;


public class SettingActivity extends AppCompatActivity {

    SwitchCompat languageSwitch;
    ChangeLocaleLanguage changeLocaleLanguage;
    TextView heading;
    ImageView settingGoBack;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_setting);
        init();
        clickListener();
    }

    public void init() {
        Preferences.appContext = getApplicationContext();
        changeLocaleLanguage = new ChangeLocaleLanguage();
        languageSwitch = findViewById(R.id.languageSwitch);
        settingGoBack = findViewById(R.id.settingGoBack);
        heading = findViewById(R.id.heading);
        if (Preferences.getLocalLanguage().equals(AppConstants.ENGLISH)) {
            languageSwitch.setChecked(false);
            updateUI();
        } else {
            languageSwitch.setChecked(true);
            updateUI();
        }
    }

    public void clickListener() {
        languageSwitch.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    Preferences.setLocalLanguage(AppConstants.MARATHI);
                    changeLocaleLanguage.changeLocale(Preferences.getLocalLanguage(), getApplicationContext());
                    updateUI();

                } else {
                    Preferences.setLocalLanguage(AppConstants.ENGLISH);
                    changeLocaleLanguage.changeLocale(Preferences.getLocalLanguage(), getApplicationContext());
                    updateUI();
                }
            }
        });
        settingGoBack.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callBackButton();
            }
        });
    }

    public void updateUI() {
        languageSwitch.setText(getText(R.string.language) + " " + getString(R.string.language_name));
        heading.setText(getText(R.string.setting_activity));
    }

    public void callBackButton() {
        Intent intent = new Intent(getApplicationContext(), MenuActivityPhase1.class);
        intent.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(intent);
    }

    @Override
    public void onBackPressed() {
        try {
            callBackButton();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


}
