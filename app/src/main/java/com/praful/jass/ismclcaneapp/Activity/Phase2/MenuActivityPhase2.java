package com.praful.jass.ismclcaneapp.Activity.Phase2;

import android.os.Bundle;
import android.os.Handler;
import android.support.annotation.NonNull;
import android.support.design.widget.NavigationView;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.view.MenuItem;
import android.view.View;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Fragments.Phase1.EmployeeTripFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.OptionListFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase2.Phase2GenerateTokenFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase2.Phase2MenuFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase2.Phase2TransferTokenFragment;
import com.praful.jass.ismclcaneapp.R;

public class MenuActivityPhase2 extends AppCompatActivity {

    FragmentTransaction fragmentTransaction;
    boolean doubleBackToExitPressedOnce = false;
    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;
    public static int navItemIndex = 0;
    private String[] activityTitles;
    private static final String TAG_MENU = "menu";
    public static String CURRENT_TAG = TAG_MENU;
    public static String TAG_EMPLOYEE_TRIP = "employee_trip";
    public static String TAG_GENERATE_TOKEN = "generate_token";
    public static String TAG_TANSFER_TOKEN = "transfer_token";
    private Handler mHandler;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.drwaer_layout_menu_phase2);
            init();
            // load toolbar titles from string resources
            activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles_phase2);
            toolbar = (Toolbar) findViewById(R.id.toolbar);
            setSupportActionBar(toolbar);
            mHandler = new Handler();
            loadNavHeader();
            setUpNavigationView();
            if (savedInstanceState == null) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_MENU;
                loadHomeFragment();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        try {
            navigationView = (NavigationView) findViewById(R.id.nav_view_phase_2);
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout_phase2);
            navHeader = navigationView.getHeaderView(0);
            txtName = (TextView) navHeader.findViewById(R.id.name);
            txtWebsite = (TextView) navHeader.findViewById(R.id.website);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpNavigationView() {
        try {
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                @Override
                public boolean onNavigationItemSelected(@NonNull MenuItem item) {
                    switch (item.getItemId()) {
                        case R.id.nav_menu_phase2:
                            navItemIndex = 0;
                            CURRENT_TAG = TAG_MENU;
                            break;
                        case R.id.nav_employee_trip:
                            navItemIndex = 1;
                            CURRENT_TAG = TAG_EMPLOYEE_TRIP;
                            break;
                        case R.id.nav_generate_token:
                            navItemIndex = 2;
                            CURRENT_TAG = TAG_GENERATE_TOKEN;
                            break;
                        case R.id.nav_transfer_token:
                            navItemIndex = 3;
                            CURRENT_TAG = TAG_TANSFER_TOKEN;
                            break;
                        default:
                            navItemIndex = 0;
                    }
                    if (item.isChecked()) {
                        item.setChecked(false);
                    } else {
                        item.setChecked(true);
                    }
                    item.setChecked(true);

                    loadHomeFragment();
                    return true;
                }
            });

            ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

                @Override
                public void onDrawerClosed(View drawerView) {
                    // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                    super.onDrawerClosed(drawerView);
                }

                @Override
                public void onDrawerOpened(View drawerView) {
                    // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                    super.onDrawerOpened(drawerView);
                }
            };

            //Setting the actionbarToggle to drawer layout
            drawer.setDrawerListener(actionBarDrawerToggle);

            //calling sync state is necessary or else your hamburger icon wont show up
            actionBarDrawerToggle.syncState();

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadNavHeader() {
        // name, website
        try {
            txtName.setText(" ");
            txtWebsite.setText("  ");
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setToolbarTitle() {
        try {
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void loadHomeFragment() {
        try {
            selectNavMenu();
            setToolbarTitle();
            if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
                drawer.closeDrawers();
                return;
            }
            Runnable mPendingRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        // update the main content by replacing fragments
                        Fragment fragment = getHomeFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(android.R.anim.fade_in,
                                android.R.anim.fade_out);
                        fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                        fragmentTransaction.commitAllowingStateLoss();
                    } catch (Exception ex) {
                        Toast.makeText(MenuActivityPhase2.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };
            if (mPendingRunnable != null) {
                mHandler.post(mPendingRunnable);
            }
            drawer.closeDrawers();
            invalidateOptionsMenu();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void selectNavMenu() {
        try {
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                Phase2MenuFragment fragment = new Phase2MenuFragment();
                return fragment;
            case 1:
                EmployeeTripFragment fragment1 = new EmployeeTripFragment();
                return fragment1;
            case 2:
                Phase2GenerateTokenFragment fragment2 = new Phase2GenerateTokenFragment();
                return fragment2;
            case 3:
                Phase2TransferTokenFragment fragment3 = new Phase2TransferTokenFragment();
                return fragment3;
            default:
                return new OptionListFragment();
        }
    }

    @Override
    public void onBackPressed() {

        try {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame_phase2);
            if (f instanceof Phase2MenuFragment) {
                // do something with f

                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, "Please click BACK again to exit", Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else {
                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                Phase2MenuFragment menuFragment = new Phase2MenuFragment();
                fragmentTransaction.replace(R.id.frame_phase2, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle("Menu");
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}

