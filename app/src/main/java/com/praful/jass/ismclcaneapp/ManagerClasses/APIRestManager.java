package com.praful.jass.ismclcaneapp.ManagerClasses;

import android.content.Context;
import android.net.ConnectivityManager;
import android.net.NetworkInfo;
import android.util.Log;
import android.widget.Toast;

import com.android.volley.DefaultRetryPolicy;
import com.android.volley.Request;
import com.android.volley.RequestQueue;
import com.android.volley.Response;
import com.android.volley.VolleyError;
import com.android.volley.toolbox.JsonObjectRequest;
import com.android.volley.toolbox.Volley;
import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;


public class APIRestManager {

    private RequestQueue requestQueue;
    //public  APIManagerInterface listner;


    public interface APIManagerInterface {
        public void onSuccess(Object resultObj);

        public void onError(String strError);
    }

    public void postAPI(String url, JSONObject params, final Context context, final Class classType, final APIManagerInterface listner) {
//        AppConstants.CHECK_INTERNET_CONNECTION = true;
//        if(haveNetworkConnection(context)) {
        try {
            if (url.contains("null")) {
                url = url.replace("null", Preferences.getServerIP());
            }
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest jsonData = new JsonObjectRequest(Request.Method.POST, url, params,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            try {
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    try {
                                        if (response.has("IPList")) {
                                            if (listner != null) {
                                                listner.onSuccess(response);
                                            }
                                        }
                                        Object json = response.get("response");
                                        //if multiple objects
                                        if (json instanceof JSONArray) {
                                            ArrayList resultArray = new ArrayList();
                                            JSONArray list = response.getJSONArray("response");
                                            for (int i = 0; i < list.length(); i++) {
                                                JSONObject object = list.getJSONObject(i);
                                                Object model = gson.fromJson(object.toString(), classType);
                                                resultArray.add(model);
                                            }
                                            if (listner != null) {
                                                listner.onSuccess(resultArray);
                                            }
                                        } else {
                                            String jsonStr = "response";
                                            Object result = null;
                                            if (response.toString().contains("No record found")) {
                                                result = "No record found";
                                            } else {
                                                JSONObject jsonObject = response.getJSONObject(jsonStr);
                                                result = gson.fromJson(jsonObject.toString(), classType);
                                            }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                            if (listner != null) {
                                                listner.onSuccess(result);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        listner.onError(e.getLocalizedMessage());
                                    }
//                                {"responce":[{"value":"1"},{"value":"1"}]}
                                } else if (response.getBoolean("responce")) {
                                    Object json = response.get("response");
                                    if (json instanceof JSONArray) {
                                        ArrayList resultArray = new ArrayList();

                                        JSONArray list = response.getJSONArray("response");
                                        for (int i = 0; i < list.length(); i++) {
                                            JSONObject object = list.getJSONObject(i);
                                            Object model = gson.fromJson(object.toString(), classType);
                                            resultArray.add(model);
                                        }
                                        if (listner != null) {
                                            listner.onSuccess(resultArray);
                                        }
                                    } else {
                                        String jsonStr = "response";
                                        Object result = null;
                                        if (response.toString().contains("No record found")) {
                                            result = "No record found";
                                        } else {
                                            JSONObject jsonObject = response.getJSONObject(jsonStr);
                                            result = gson.fromJson(jsonObject.toString(), classType);
                                        }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                        if (listner != null) {
                                            listner.onSuccess(result);
                                        }
                                    }
                                    if (listner != null) {
                                        listner.onError("Fail ");
                                    }
                                } else if (response.getString("status").equalsIgnoreCase("Error")) {
                                    String msg = response.getString("message");
                                    if (listner != null) {
                                        listner.onError(msg);
                                    }
                                } else if (response.getString("status").equalsIgnoreCase("Fail")) {
                                    if (!response.getBoolean("response")) {
                                        String msg = response.getString("message");
                                        if (listner != null) {
                                            listner.onError(msg);
                                        }
                                    } else {
                                        //response
                                        if (listner != null) {
                                            listner.onError("no user found");
                                        }
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listner != null) {
                                    listner.onError("in catch");
                                }
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //AppUtils.showAlert(error.getLocalizedMessage(),context);
                    if (listner != null) {
                        listner.onError(error.getLocalizedMessage());
                    }
                }
            });
            requestQueue.add(jsonData);
            jsonData.setShouldCache(false);
            jsonData.setRetryPolicy(new DefaultRetryPolicy(10000,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void postRujvatData(String url, JSONObject params, final Context context, final Class classType, final APIManagerInterface listner) {

//        AppConstants.CHECK_INTERNET_CONNECTION = true;
//        if(haveNetworkConnection(context)) {

        try {
            if (url.contains("null")) {
                url = url.replace("null", Preferences.getServerIP());
            }

            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest jsonData = new JsonObjectRequest(Request.Method.POST, url, params,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            try {
                                Object json = response.get("responce");
                                //if multiple objects
                                if (json instanceof JSONArray) {
                                    ArrayList resultArray = new ArrayList();

                                    JSONArray list = response.getJSONArray("responce");
                                    for (int i = 0; i < list.length(); i++) {
                                        JSONObject object = list.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
//                                            model = ModelParser.parseModel(object, model);
                                        resultArray.add(model);
                                    }
                                    if (listner != null) {
                                        listner.onSuccess(resultArray);
                                    }

                                } else {
                                    String jsonStr = "response";
                                    Object result = null;
                                    if (response.toString().contains("No record found")) {
                                        result = "No record found";
                                    } else {
                                        JSONObject jsonObject = response.getJSONObject(jsonStr);
                                        result = gson.fromJson(jsonObject.toString(), classType);
                                    }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                    if (listner != null) {
                                        listner.onSuccess(result);
                                    }
                                }
                            } catch (Exception e) {
                                e.printStackTrace();
                                listner.onError(e.getLocalizedMessage());
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    //AppUtils.showAlert(error.getLocalizedMessage(),context);
                    if (listner != null) {
                        listner.onError(error.getLocalizedMessage());
                    }
                }
            });
            requestQueue.add(jsonData);
            jsonData.setShouldCache(false);
//    }
//        else{
            // Toast.makeText(context, "Please Check Your Internet Connectivity", Toast.LENGTH_SHORT).show();
//            if (listner != null) {
//                listner.onError("Please Check Your Internet Connectivity");
//            }
//        }
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void postAPICommon(String url, JSONObject params, final Context context, final Class classType, final APIManagerInterface listner) {

//        AppConstants.CHECK_INTERNET_CONNECTION = true;
//        if(haveNetworkConnection(context)) {

        try {

            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest jsonData = new JsonObjectRequest(Request.Method.POST, url, params,

                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Gson gson = new Gson();
                            try {
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    try {
                                        Object json = response.get("response");
                                        //if multiple objects
                                        if (json instanceof JSONArray) {
                                            ArrayList resultArray = new ArrayList();

                                            JSONArray list = response.getJSONArray("response");
                                            for (int i = 0; i < list.length(); i++) {
                                                JSONObject object = list.getJSONObject(i);
                                                Object model = gson.fromJson(object.toString(), classType);
                                                resultArray.add(model);
                                            }

                                            if (listner != null) {
                                                listner.onSuccess(resultArray);
                                            }
                                        } else {
                                            String jsonStr = "response";
                                            Object result = null;
                                            if (response.toString().contains("No record found")) {
                                                result = "No record found";
                                            } else {
                                                JSONObject jsonObject = response.getJSONObject(jsonStr);
                                                result = gson.fromJson(jsonObject.toString(), classType);
                                            }
//                                        result = ModelParser.parseModel((JSONObject) json, result);

                                            if (listner != null) {
                                                listner.onSuccess(result);
                                            }
                                        }
                                    } catch (Exception e) {
                                        e.printStackTrace();
                                        listner.onError(e.getLocalizedMessage());
                                    }
                                } else if (response.getString("status").equalsIgnoreCase("Error")) {
                                    String msg = response.getString("message");
                                    if (listner != null) {
                                        listner.onError(msg);
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                            }
                        }
                    }, new Response.ErrorListener() {

                @Override
                public void onErrorResponse(VolleyError error) {
                    //AppUtils.showAlert(error.getLocalizedMessage(),context);
                    if (listner != null) {
                        listner.onError(error.getLocalizedMessage());
                    }
                }

            });
            requestQueue.add(jsonData);
            jsonData.setShouldCache(false);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void postJSONArrayAPI(String url, JSONObject params, final Class classType, final Context context, final APIManagerInterface listener) {
        if (Connection(context)) {
            if (url.contains("null")) {
                url = url.replace("null", Preferences.getServerIP());
            }
            requestQueue = Volley.newRequestQueue(context);

            JsonObjectRequest mJsonObjectRequest = new JsonObjectRequest(Request.Method.POST, url, params,
                    new Response.Listener<JSONObject>() {
                        @Override
                        public void onResponse(JSONObject response) {
                            Log.d("TAG", "RESPONSE  = " + response);
                            try {
                                if (response.getString("status").equalsIgnoreCase("SUCCESS")) {
                                    Gson gson = new Gson();
                                    Object json = null;

                                    String jsonString = response.getString("response");
                                    //String successMsg = response.getString("message");

                                    //TO GET AN ARRAY OF OBJECT
                                    JSONArray jsonArray = new JSONArray(jsonString);
                                    ArrayList arrList = new ArrayList();

                                    for (int i = 0; i < jsonArray.length(); i++) {
                                        JSONObject object = jsonArray.getJSONObject(i);
                                        Object model = gson.fromJson(object.toString(), classType);
                                        arrList.add(model);
                                    }

                                    if (listener != null) {
                                        listener.onSuccess(arrList);
                                    }
                                } else {
                                    if (listener != null) {
                                        listener.onError(response.getString("message"));
                                    }
                                }
                            } catch (JSONException e) {
                                e.printStackTrace();
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("message"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
                            } catch (Exception e) {
                                if (listener != null) {
                                    try {
                                        listener.onError(response.getString("message"));
                                    } catch (JSONException e1) {
                                        e1.printStackTrace();
                                    }
                                }
//                                Toast.makeText(context, e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    }, new Response.ErrorListener() {
                @Override
                public void onErrorResponse(VolleyError error) {
                    if (listener != null) {
                        listener.onError(error.toString());
                    }
                }
            });

            requestQueue.add(mJsonObjectRequest);
            mJsonObjectRequest.setShouldCache(false);
            mJsonObjectRequest.setRetryPolicy(new DefaultRetryPolicy(
                    0,
                    DefaultRetryPolicy.DEFAULT_MAX_RETRIES,
                    DefaultRetryPolicy.DEFAULT_BACKOFF_MULT));
        } else {

            if (listener != null && context != null) {
                listener.onError("No internet");
                //Toast.makeText(context, "Please check your internet connection", Toast.LENGTH_SHORT).show();
            }
        }
    }

    // CHECK INTERNET CONNECTION
    public static boolean Connection(Context context) {
        if (context != null) {
            ConnectivityManager conMgr = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
            NetworkInfo netInfo = conMgr.getActiveNetworkInfo();
            return !(netInfo == null || !netInfo.isConnected() || !netInfo.isAvailable());
        }

        return false;
    }
//    private boolean haveNetworkConnection(Context context) {
//        boolean haveConnectedWifi = false;
//        boolean haveConnectedMobile = false;
//
//        ConnectivityManager cm = (ConnectivityManager) context.getSystemService(Context.CONNECTIVITY_SERVICE);
//        NetworkInfo[] netInfo = cm.getAllNetworkInfo();
//        for (NetworkInfo ni : netInfo) {
//            if (ni.getTypeName().equalsIgnoreCase("WIFI"))
//                if (ni.isConnected())
//                    haveConnectedWifi = true;
//            if (ni.getTypeName().equalsIgnoreCase("MOBILE"))
//                if (ni.isConnected())
//                    haveConnectedMobile = true;
//        }
//        boolean status = haveConnectedMobile || haveConnectedWifi;
//        AppConstants.CHECK_INTERNET_CONNECTION = status;
//        return haveConnectedWifi || haveConnectedMobile;
//    }

}
