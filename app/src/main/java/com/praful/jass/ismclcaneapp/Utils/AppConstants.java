package com.praful.jass.ismclcaneapp.Utils;

import com.praful.jass.ismclcaneapp.Models.LoginResponceModel;

/**
 * Created by Praful on 28-03-2017.
 */

public class AppConstants {

    //    Web Services URLS
    public static final String WSDL_TARGET_NAMESPACE = "http://tempuri.org/";
    //        public static final String SOAP_ADDRESS = "http://59.90.136.142:3838//AppService/CaneApp.asmx";
    public static final String SOAP_ADDRESS = "http://" + Preferences.getServerIP() + ":3838//AppService/CaneApp.asmx";
//            public static final String BASE_URL_PHP_SERVICE = "http://59.90.136.142:80/store_procedure_image/";
//    public static String BASE_URL_PHP_SERVICE = "http://" + Preferences.getServerIP() + ":80/WebServices/CaneAPPT/";
    public static String BASE_URL_PHP_SERVICE = "http://" + Preferences.getServerIP() + ":80/store_procedure_image/";
//    public static String BASE_URL_PHP_SERVICE = "http://" + Preferences.getServerIP() + ":80/store_procedure_image(07-07-2018)/";

    // Upload Images on Server
    public static final String UPLOAD_FOUR_IMAGE_PLANTATION = BASE_URL_PHP_SERVICE + "plantation_with_image.php";
    public static final String UPLOAD_FOUR_IMAGE_RUJVAT = BASE_URL_PHP_SERVICE + "rujvat_with_image.php";
    public static final String UPLOAD_FOUR_IMAGE_PLANTATION_WITH_ARRAY = BASE_URL_PHP_SERVICE + "plantation_with_image_array_webservice.php";
    public static final String UPLOAD_FOUR_IMAGE_RUJVAT_WITH_ARRAY = BASE_URL_PHP_SERVICE + "rujvat_with_image_array_webservice.php";
    public static final String URL_UPLOAD_KYC_IMAGE_ARRAY = BASE_URL_PHP_SERVICE + "KYC_submit_image.php";

    //    Other Webservice URLS
//    public static final String URL_TRACKING = BASE_URL_PHP_SERVICE + "pr_emp_track_entry.php";
    public static final String URL_TRACKING = BASE_URL_PHP_SERVICE + "pr_emp_track.php";
    public static final String URL_PLANTATION_DATA_FOR_RUJVAT = BASE_URL_PHP_SERVICE + "rujvat_query.php";
    public static final String URL_ADD_RUJVAT_REASON = BASE_URL_PHP_SERVICE + "plantation_reason_master.php";
    public static final String URL_GET_ALL_PLANTATION_FOR_RUJVAT = BASE_URL_PHP_SERVICE + "Get_All_Plantation_for_rujvat.php";
    public static final String URL_UPDATE_RUJVAT_REASON = BASE_URL_PHP_SERVICE + "pr_rujvat_data.php";
    public static final String URL_TRIP_DATA_UPDATE = BASE_URL_PHP_SERVICE + "employee_trip_data_with_image.php";
    public static final String URL_USER_LOGIN = BASE_URL_PHP_SERVICE + "login_function_for_app.php";
    public static final String URL_VILLAGE_LIST = BASE_URL_PHP_SERVICE + "village_list_webservice.php";
    public static final String URL_FARMER_LIST = BASE_URL_PHP_SERVICE + "farmer_list_webservice.php";
    public static final String URL_IRRIGATION_LIST = BASE_URL_PHP_SERVICE + "irrigation_list_webservice.php";
    public static final String URL_CANE_VARITY_LIST = BASE_URL_PHP_SERVICE + "cane_varity_list_webservice.php";
    public static final String URL_CANE_TYPE_LIST = BASE_URL_PHP_SERVICE + "cane_type_list_webservice.php";
    public static final String URL_PLANTATION_TYPE_LIST = BASE_URL_PHP_SERVICE + "plantation_type_webservice.php";
    //    public static final String URL_EMPLOYEE_TRIP_DATA_ARRAY = BASE_URL_PHP_SERVICE + "employee_data_image.php";
    public static final String URL_EMPLOYEE_TRIP_DATA_ARRAY = BASE_URL_PHP_SERVICE + "employee_trip_data_with_latlong.php";//"employee_trip_data.php";
    public static final String URL_EMPLOYEE_TRIP_IMG_UPLOAD_IN_ARRY = BASE_URL_PHP_SERVICE + "employee_trip_images_upload.php";
    public static final String URL_SEASON_ID = BASE_URL_PHP_SERVICE + "season_id.php";
//    public static final String URL_PLANTATION_DATA_SUBMIT = BASE_URL_PHP_SERVICE + "plantation_data_submit.php";
    public static final String URL_PLANTATION_DATA_SUBMIT = BASE_URL_PHP_SERVICE + "plantation_data_submit_seasion.php";
    public static final String URL_AllPLANTATION_AND_RUJVAT_IMAGE_VERIFY = BASE_URL_PHP_SERVICE + "CheckAllImageBefourDelete.php";
    public static final String URL_GET_APP_VERSION = BASE_URL_PHP_SERVICE + "version_check.php";
    public static final String URL_GET_WORK_STATUS_OF_SLIP_BOY = BASE_URL_PHP_SERVICE + "work_status_datewise_slip_boy.php";
    public static final String URL_GET_ALL_SEASON_LIST = BASE_URL_PHP_SERVICE + "season_list_with_name.php";
    public static final String URL_GET_ALL_VILLAGE_LIST_DETAILS = BASE_URL_PHP_SERVICE + "VillageListWithDetals.php";
    public static final String URL_GET_FACTORY_NAME_LIST = BASE_URL_PHP_SERVICE + "FactoryNameList.php";
    public static final String URL_KYC_SUBMIT = BASE_URL_PHP_SERVICE + "KYC_submit.php";
    public static final String URL_GET_WORK_TYPE_LIST = BASE_URL_PHP_SERVICE + "WorkTypeList.php";
    public static final String URL_GET_WORK_CANDIDATE_LIST = BASE_URL_PHP_SERVICE + "slip_boy_list_parameter_manager.php";
    public static final String URL_GET_WORK_TYPE_DATA_LIST = BASE_URL_PHP_SERVICE + "work_type_data_list.php";
    public static final String URL_CUTTING_ORDER_START = BASE_URL_PHP_SERVICE + "CuttingOrderStartDate.php";
    public static final String URL_CUTTING_ORDER_END = BASE_URL_PHP_SERVICE + "CuttingOrderEndDate.php";
    public static final String URL_CUTTING_ORDER_HARVESTING = BASE_URL_PHP_SERVICE + "HarvesterListCuttingOrder.php";
    public static final String URL_CUTTING_ORDER_START_SUBMIT = BASE_URL_PHP_SERVICE + "CuttingOrderSubmit.php";
    public static final String URL_CUTTING_ORDER_END_SUBMIT = BASE_URL_PHP_SERVICE + "CuttingOrderEndSubmit.php";
    public static final String URL_CUTTING_ORDER_APPROVE = BASE_URL_PHP_SERVICE + "cutting_order_approve.php";
    public static final String URL_CUTTING_ORDER_DENY = BASE_URL_PHP_SERVICE + "cutting_order_deny.php";
    public static final String URL_PLANTATION_APPROVE = BASE_URL_PHP_SERVICE + "plantation_approve.php";
    public static final String URL_CANE_QUALITY = BASE_URL_PHP_SERVICE + "CaneQualityList.php";
    public static final String URL_UPDATE_VERSION_AND_GET_USER_FLAG = BASE_URL_PHP_SERVICE + "updateVersionAndGetUserFlag.php";
    public static final String URL_CHITBOY_WORK_HISTORY = BASE_URL_PHP_SERVICE + "ChitBoyWorkList.php";
    public static final String URL_IP_LIST = BASE_URL_PHP_SERVICE + "IPListMaster.php";

    // Models Static Objects

    public static LoginResponceModel loginResponceModel = new LoginResponceModel();

    public static boolean IN_TESTING = false;
    public static boolean IMAGE_ONE = false;
    public static boolean IMAGE_TWO = false;
    public static boolean IMAGE_THREE = false;
    public static final int GROWER_SELECT = 1;
    public static final int SHIVAR_SELECT = 2;
    public static final int IRRIGATION_SELECT = 3;
    public static final int CAN_VERIETY_SELECT = 4;
    public static final int PLANTATION_TYPE_SELECT = 5;
    public static final int RUJVAT_REASON_SELECT = 6;
    public static final int CAN_TYPE_SELECT = 7;
    public static final int RUJVAT_ALL_DATA_SELECT = 8;
    public static final int LANGUAGE_SELECT = 9;
    public static final int VILLAGE_ALL_DETAILS_SELECT = 10;
    public static final int FACTORY_NAME_LIST = 11;
    public static final int PLANTATION_CUTTING_ORDER = 12;
    public static final int HARVESTING_CUTTING_ORDER = 13;
    public static final int CUTTING_SUBMIT_SELECT = 14;
    public static final int CANE_QUALITY_SELECT = 15;
    public static final int UPDATE_VERSION_SELECT = 16;
    public static final int IP_LIST_SELECT = 17;
    public static final int SEASON_SELECT = 18;

    public static final int UPDATE_PLANTATION_SELECT = 5225;
    public static final int RENAME_FILES_SELECT = 5226;
    public static final int UPDATE_RUJVAT_SELECT = 5227;
    public static final int UPLOAD_IMAGE_SELECT = 5228;
    public static final int DELETE_ALL_DATA_SELECT = 5229;
    public static final int UPLOAD_TRIP_INFORMATION = 5230;
    public static final int UPLOAD_KYC_INFORMATION = 5231;
    public static final int SEASON_ID_SELECT = 10101010;


    public static final int SERVER_NOT_RESPONDING_ERROR = 8080;

    public static final int PLANTATIONID_ADDER_PL = 1;

    // INDEX
    public static int selectItemIndex = 0;


    //=====================================Trip Status========================================

    public static final String START_TRIP = "START";
    public static final String STOPT_TRIP = "STOP";

    //=====================================Image Number========================================

    public static final String FIRST_IMAGE = "IMG_FIRST_";
    public static final String SECOND_IMAGE = "IMG_SECOND_";
    public static final String THREE_IMAGE = "IMG_THIRD_";
    public static final String FOUR_IMAGE = "IMG_FOURTH_";

    //    public static int SESSION_ID = 0;
    public static int USER_ID = 0;
    public static String API_TOKEN = "ISMCL_CANE_APP";

    public static String PLANTATION_CONSTANT = "P";
    public static String RUJVAT_CONSTANT = "R";
    public static String KYC_CONSTANT = "K";

    public static String DATA_BASE_NOT_AVAILABLE = "NODB";
    public static String TABLE_NOT_AVAILABLE = "NOTBL";

    public static String SEND_BY = "Send by:-";
    public static String SEND_BY_NFC = " NFC";
    public static String SEND_BY_INTERNET = " Internet";

    public static String ENGLISH = "EN";
    public static String MARATHI = "MR";

    //    public static Location location;
    public static double latitude = 0.0;
    public static double longitude = 0.0;

    public static String FLAG_WORK_TYPE_SELECTED = "";

    public static String SELECTED_USER_ID = "";
}


// App Seasion in plantation time
// DB Internet Network.
