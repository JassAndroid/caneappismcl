package com.praful.jass.ismclcaneapp.Adapter;

import android.content.Context;
import android.content.Intent;
import android.support.annotation.NonNull;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.EditText;
import android.widget.ImageView;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.Activity.Phase1.ApprovalCandidateWorkDetailActivity;
import com.praful.jass.ismclcaneapp.Activity.Phase1.ApprovalPlantationActivity;
import com.praful.jass.ismclcaneapp.Models.CandidateWorkDataModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import java.util.ArrayList;

public class SlipBoyOptionDataAdapter extends RecyclerView.Adapter<SlipBoyOptionDataAdapter.MyViewHolder> {

    Context context;
    ArrayList<CandidateWorkDataModel> mList;

    public SlipBoyOptionDataAdapter(Context context, ArrayList<CandidateWorkDataModel> mList) {
        this.context = context;
        this.mList = mList;
    }

    public void updateAdapter(ArrayList<CandidateWorkDataModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View view = LayoutInflater.from(context).inflate(R.layout.list_item_work, parent, false);
        return new MyViewHolder(view);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, int position) {
        final CandidateWorkDataModel candidateWorkDataModel = mList.get(position);
        holder.txt_farmer_name.setText(candidateWorkDataModel.getFARMER_NAME());
        holder.txt_plantation_code.setText(candidateWorkDataModel.getPLANTATION_CODE());
        holder.img_next.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppConstants.FLAG_WORK_TYPE_SELECTED.equalsIgnoreCase("CO"))
                    context.startActivity(new Intent(context, ApprovalCandidateWorkDetailActivity.class).putExtra("model", new Gson().toJson(candidateWorkDataModel)));
                else if (AppConstants.FLAG_WORK_TYPE_SELECTED.equalsIgnoreCase("PL")) {
                    context.startActivity(new Intent(context, ApprovalPlantationActivity.class).putExtra("model", new Gson().toJson(candidateWorkDataModel)));
                }
            }
        });
    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        EditText txt_farmer_name, txt_plantation_code;
        ImageView img_next;

        public MyViewHolder(View itemView) {
            super(itemView);
            txt_farmer_name = itemView.findViewById(R.id.txt_farmer_name);
            txt_plantation_code = itemView.findViewById(R.id.txt_plantation_code);
            img_next = itemView.findViewById(R.id.img_next);
        }
    }
}
