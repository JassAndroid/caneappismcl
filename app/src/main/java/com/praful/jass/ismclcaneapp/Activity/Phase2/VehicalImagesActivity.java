package com.praful.jass.ismclcaneapp.Activity.Phase2;

import android.content.Intent;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.provider.MediaStore;
import android.support.v7.app.AppCompatActivity;
import android.util.Log;
import android.view.View;
import android.widget.ImageView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

public class VehicalImagesActivity extends AppCompatActivity {
    ImageView imgBack;
    ImageView imgOne;
    ImageView imgTwo;
    ImageView imgThree;
    private Uri file;
    String imgFileName = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_vehical_image);
            init();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void init() {
        try {
            imgBack = (ImageView) findViewById(R.id.backActivity);
            imgOne = (ImageView) findViewById(R.id.img_one);
            imgTwo = (ImageView) findViewById(R.id.img_two);
            imgThree = (ImageView) findViewById(R.id.img_three);
            setOnClickListner();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setOnClickListner() {
        try {
            imgBack.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    finish();
                }
            });
            imgOne.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstants.IMAGE_ONE = true;
                    AppConstants.IMAGE_TWO = false;
                    AppConstants.IMAGE_THREE = false;
                    takePicture();
                }
            });
            imgTwo.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstants.IMAGE_ONE = false;
                    AppConstants.IMAGE_TWO = true;
                    AppConstants.IMAGE_THREE = false;
                    takePicture();
                }
            });
            imgThree.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    AppConstants.IMAGE_ONE = false;
                    AppConstants.IMAGE_TWO = false;
                    AppConstants.IMAGE_THREE = true;
                    takePicture();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void takePicture() {
        try {
            Intent intent = new Intent(MediaStore.ACTION_IMAGE_CAPTURE);
            file = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
            startActivityForResult(intent, 100);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = null;
        try {
            mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CameraDemo", "failed to create directory");
                    return null;
                }
            }

            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            imgFileName = "IMG_TRIP_" + timeStamp + ".jpg";
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return new File(mediaStorageDir.getPath() + File.separator + imgFileName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 100) {
                if (resultCode == RESULT_OK) {
                    if (AppConstants.IMAGE_ONE) {
                        imgOne.setImageURI(file);
                    } else if (AppConstants.IMAGE_TWO) {
                        imgTwo.setImageURI(file);
                    } else if (AppConstants.IMAGE_THREE) {
                        imgThree.setImageURI(file);
                    }

                    Bitmap photo = null;
                    try {
                        photo = MediaStore.Images.Media.getBitmap(
                                this.getContentResolver(), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    saveImage(getResizedBitmap(photo, 800));
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void saveImage(Bitmap finalBitmap) {
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CameraDemo", "failed to create directory");
                }
            }
            File file = new File(mediaStorageDir.getPath() + File.separator + imgFileName);
            if (file.exists()) file.delete();
            try {
                FileOutputStream out = new FileOutputStream(file);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
                out.flush();
                out.close();

            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = 0;
        int height = 0;
        try {
            width = image.getWidth();
            height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

}
