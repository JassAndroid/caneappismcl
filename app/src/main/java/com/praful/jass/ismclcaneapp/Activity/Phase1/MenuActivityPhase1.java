package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.ActivityManager;
import android.app.AlertDialog;
import android.app.PendingIntent;
import android.content.Context;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.IntentFilter;
import android.content.pm.PackageManager;
import android.nfc.NfcAdapter;
import android.nfc.Tag;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.os.Handler;
import android.support.design.widget.NavigationView;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.ActionBarDrawerToggle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.Toolbar;
import android.util.Log;
import android.view.Menu;
import android.view.MenuItem;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Dialog.IPUpdateDialog;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.DailyHistoryFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.EmployeeTripFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.IPUpdateFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.MenuFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.OptionListFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.PlantationRegistrationFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.RujvatFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.SlipBoyListFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.TodaysWorkoutFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase2.NFCWriteFragment;
import com.praful.jass.ismclcaneapp.Interface.Listener;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.services.SchedulerService;

public class MenuActivityPhase1 extends AppCompatActivity implements Listener {

    FragmentTransaction fragmentTransaction;
    boolean doubleBackToExitPressedOnce = false;

    SchedulerService schedulerService;
    Intent mServiceIntent;
    Context ctx;

    public Context getCtx() {
        return ctx;
    }


    private NavigationView navigationView;
    private DrawerLayout drawer;
    private View navHeader;
    private ImageView imgNavHeaderBg, imgProfile;
    private TextView txtName, txtWebsite;
    private Toolbar toolbar;

    // index to identify current nav menu item
    public static int navItemIndex = 0;

    // tags used to attach the fragments

    private static final String TAG_MENU = "menu";
    private static final String TAG_PLANTATION = "plantation";
    private static final String TAG_RUJVAT = "rujvat";
    private static final String TAG_EMPLOYEE_TRIP = "employee trip";
    private static final String TAG_DAILY_HISTORY = "daily history";
    private static final String TAG_SLIP_BOY_WORK_STATUS = "slip boy work status";
    private static final String TAG_TODAYS_WORKOUT = "todays workout";
    public static String CURRENT_TAG = TAG_MENU;

    // toolbar titles respected to selected nav menu item
    private String[] activityTitles;
    private String flashNFC = "";
    private Handler mHandler;

    /**
     * Drawer Initialization of All Variables End
     */

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_drawer_layout_menu);
            init();
            startServices();
            initNFC();
            try {
                if (Preferences.getSchedulerStatus()) {
                    startServices();
                    Preferences.setSchedulerStatus(false);
                }
            } catch (Exception e) {
                Snackbar.make(findViewById(R.id.drawer_layout), getString(R.string.login_app), Snackbar.LENGTH_LONG).show();
            }

            /**  Drawer Create Data Start */
            toolbar = findViewById(R.id.toolbar);

            setSupportActionBar(toolbar);

            mHandler = new Handler();

            // load toolbar titles from string resources
            activityTitles = getResources().getStringArray(R.array.nav_item_activity_titles);

            // load nav menu header data
            loadNavHeader();

            // initializing navigation menu
            setUpNavigationView();

            if (savedInstanceState == null) {
                navItemIndex = 0;
                CURRENT_TAG = TAG_MENU;
                loadHomeFragment();
            }
            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
            /**  Drawer Create Data End */

            if (ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(this, new String[]{Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.READ_PHONE_STATE}, 0);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            /**  Drawer   Start * */
            drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
            navigationView = (NavigationView) findViewById(R.id.nav_view);

            // Navigation view header
            navHeader = navigationView.getHeaderView(0);
            txtName = (TextView) navHeader.findViewById(R.id.name);
            txtWebsite = (TextView) navHeader.findViewById(R.id.website);
            imgNavHeaderBg = (ImageView) navHeader.findViewById(R.id.img_header_bg);
            imgProfile = (ImageView) navHeader.findViewById(R.id.img_profile);
            schedulerService = new SchedulerService();
            /**  Drawer   End * */
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    ==============================================Service Functions==================================================
    private boolean isMyServiceRunning(Class<?> serviceClass) {
        try {
            ActivityManager manager = (ActivityManager) this.getSystemService(Context.ACTIVITY_SERVICE);
            for (ActivityManager.RunningServiceInfo service : manager.getRunningServices(Integer.MAX_VALUE)) {
                if (serviceClass.getName().equals(service.service.getClassName())) {
                    Log.i("isMyServiceRunning?", true + "");
                    return true;
                }
            }
            Log.i("isMyServiceRunning?", false + "");
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return false;
    }

    public void startServices() {
        try {
            ctx = this;
            schedulerService = new SchedulerService(getCtx());
            mServiceIntent = new Intent(getCtx(), schedulerService.getClass());
            if (!isMyServiceRunning(schedulerService.getClass())) {
                getApplication().startService(mServiceIntent);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Load navigation menu header information
     * like background image, profile image
     * name, website, notifications action view (dot)
     */
    private void loadNavHeader() {
        try {
            txtName.setText("  ");
            txtWebsite.setText("  ");
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /***
     * Returns respected fragment that user
     * selected from navigation menu
     */
    private void loadHomeFragment() {
        try {
            // selecting appropriate nav menu item
            selectNavMenu();
            // set toolbar title
            setToolbarTitle();
            // if user select the current navigation menu again, don't do anything
            // just close the navigation drawer
            if (getSupportFragmentManager().findFragmentByTag(CURRENT_TAG) != null) {
                drawer.closeDrawers();
                // show or hide the fab button
                return;
            }

            // Sometimes, when fragment has huge data, screen seems hanging
            // when switching between navigation menus
            // So using runnable, the fragment is loaded with cross fade effect
            // This effect can be seen in GMail app
            Runnable mPendingRunnable = new Runnable() {
                @Override
                public void run() {
                    try {
                        // update the main content by replacing fragments
                        Fragment fragment = getHomeFragment();
                        FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                        fragmentTransaction.replace(R.id.frame, fragment, CURRENT_TAG);
                        fragmentTransaction.commitAllowingStateLoss();
                    } catch (Exception ex) {
                        Toast.makeText(MenuActivityPhase1.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };

            // If mPendingRunnable is not null, then add to the message queue
            if (mPendingRunnable != null) {
                try {
                    mHandler.post(mPendingRunnable);
                } catch (Exception ex) {
                    Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }

            //Closing drawer on item click
            drawer.closeDrawers();

            // refresh toolbar menu
            invalidateOptionsMenu();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Fragment getHomeFragment() {
        switch (navItemIndex) {
            case 0:
                // menu
                MenuFragment menuFragment = new MenuFragment();
                return menuFragment;
            case 1:
                // plantaton
                PlantationRegistrationFragment plantationRegistrationFragment = new PlantationRegistrationFragment();
                return plantationRegistrationFragment;

            case 2:
                // Rujvat fragment
                RujvatFragment rujvatFragment = new RujvatFragment();
                return rujvatFragment;
            case 3:
                // Employee trip fragment
                EmployeeTripFragment employeeTripFragment = new EmployeeTripFragment();
                return employeeTripFragment;
            case 4:
                // Daily History fragment
                DailyHistoryFragment dailyHistoryFragment = new DailyHistoryFragment();
                return dailyHistoryFragment;
            case 5:
                // Todays Workout fragment
                TodaysWorkoutFragment todaysWorkoutFragment = new TodaysWorkoutFragment();
                return todaysWorkoutFragment;
            case 6:
                SlipBoyListFragment slipBoyWorkStatusFragment = new SlipBoyListFragment();
                return slipBoyWorkStatusFragment;
            default:
                return new OptionListFragment();
        }
    }

    private void setToolbarTitle() {
        try {
            getSupportActionBar().setTitle(activityTitles[navItemIndex]);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void selectNavMenu() {
        try {
            navigationView.getMenu().getItem(navItemIndex).setChecked(true);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setUpNavigationView() {
        try {
            //Setting Navigation View Item Selected Listener to handle the item click of the navigation menu
            navigationView.setNavigationItemSelectedListener(new NavigationView.OnNavigationItemSelectedListener() {
                // This method will trigger on item Click of navigation menu
                @Override
                public boolean onNavigationItemSelected(MenuItem menuItem) {

                    //Check to see which item was being clicked and perform appropriate action
                    switch (menuItem.getItemId()) {
                        //Replacing the main content with ContentFragment Which is our Inbox View;
                        case R.id.nav_menu:
                            navItemIndex = 0;
                            CURRENT_TAG = TAG_MENU;
                            break;
                        case R.id.nav_plantation:
                            navItemIndex = 1;
                            CURRENT_TAG = TAG_PLANTATION;
                            break;
                        case R.id.nav_rujvat:
                            navItemIndex = 2;
                            CURRENT_TAG = TAG_RUJVAT;
                            break;
                        case R.id.nav_employee_trip:
                            navItemIndex = 3;
                            CURRENT_TAG = TAG_EMPLOYEE_TRIP;
                            break;
                        case R.id.nav_daily_history:
                            navItemIndex = 4;
                            CURRENT_TAG = TAG_DAILY_HISTORY;
                            break;
                        case R.id.nav_todays_workout:
                            navItemIndex = 5;
                            CURRENT_TAG = TAG_TODAYS_WORKOUT;
                            break;
                        case R.id.nav_slip_boy_work_status:
                            navItemIndex = 6;
                            CURRENT_TAG = TAG_SLIP_BOY_WORK_STATUS;
                            break;
                        case R.id.nav_update_database:
                            // launch new intent instead of loading fragment
                            startActivity(new Intent(MenuActivityPhase1.this, UpdateDatabase.class));
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_left);
                            drawer.closeDrawers();
                            return true;
                        case R.id.nav_update_ip:
//                            updateIPAddress();

                            IPUpdateFragment ipUpdateFragment = new IPUpdateFragment();
                            FragmentTransaction fragmentTransaction = getSupportFragmentManager().beginTransaction();
                            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                            fragmentTransaction.replace(R.id.frame, ipUpdateFragment);
                            fragmentTransaction.commit();

                            drawer.closeDrawers();
                            return true;
                        case R.id.nav_flash_nfc_card:
                            mNfcWriteFragment = (NFCWriteFragment) getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);

                            if (mNfcWriteFragment == null) {
                                flashNFC = "Y";
                                mNfcWriteFragment = NFCWriteFragment.newInstance();
                            }
                            mNfcWriteFragment.show(getFragmentManager(), NFCWriteFragment.TAG);
                            return true;
                        case R.id.nav_emergency_backup:
                            startActivity(new Intent(getApplicationContext(), EmergencyBackupActivity.class));
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_left);
                            drawer.closeDrawers();
                            return true;
                        case R.id.nav_privacy_policy:
                            //   fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);

                            // launch new intent instead of loading fragment
                            //  Intent myIntent = new Intent(getApplicationContext(), PrivacyPolicy.class);
                            //  ActivityOptions options = ActivityOptions.makeCustomAnimation(getApplicationContext(), R.anim.slide_left, R.anim.slide_right);
                            //
                            startActivity(new Intent(getApplicationContext(), PrivacyPolicy.class));
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_left);
                            //R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left

                            //   startActivity(new Intent(MenuActivityPhase1.this, PrivacyPolicy.class));

                            drawer.closeDrawers();
                            return true;

                        case R.id.nav_help_desk:
                            startActivity(new Intent(getApplicationContext(), HelpDeskActivity.class));
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_left);
                            drawer.closeDrawers();
                            return true;

                        case R.id.nav_setting:
                            startActivity(new Intent(getApplicationContext(), SettingActivity.class));
                            overridePendingTransition(R.anim.enter_from_left, R.anim.exit_to_left);
                            drawer.closeDrawers();
                            return true;
                        default:
                            navItemIndex = 0;
                    }

                    try {
                        //Checking if the item is in checked state or not, if not make it in checked state
                        if (menuItem.isChecked()) {
                            menuItem.setChecked(false);
                        } else {
                            menuItem.setChecked(true);
                        }
                        menuItem.setChecked(true);

                        loadHomeFragment();
                    } catch (Exception ex) {
                        Toast.makeText(MenuActivityPhase1.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                    return true;
                }
            });

            try {
                ActionBarDrawerToggle actionBarDrawerToggle = new ActionBarDrawerToggle(this, drawer, toolbar, R.string.openDrawer, R.string.closeDrawer) {

                    @Override
                    public void onDrawerClosed(View drawerView) {
                        // Code here will be triggered once the drawer closes as we dont want anything to happen so we leave this blank
                        super.onDrawerClosed(drawerView);
                    }

                    @Override
                    public void onDrawerOpened(View drawerView) {
                        // Code here will be triggered once the drawer open as we dont want anything to happen so we leave this blank
                        super.onDrawerOpened(drawerView);
                    }
                };

                //Setting the actionbarToggle to drawer layout
                drawer.setDrawerListener(actionBarDrawerToggle);

                //calling sync state is necessary or else your hamburger icon wont show up
                actionBarDrawerToggle.syncState();
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public boolean onCreateOptionsMenu(Menu menu) {
        // Inflate the menu; this adds items to the action bar if it is present.

        // show menu only when home fragment is selected
        // if (navItemIndex == 0) {
        try {
            getMenuInflater().inflate(R.menu.main, menu);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        // }

        // when fragment is notifications, load the menu created for notifications
       /* if (navItemIndex == 4) {
            getMenuInflater().inflate(R.menu.notifications, menu);
        }*/
        return true;
    }

    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        try {
            // Handle action bar item clicks here. The action bar will
            // automatically handle clicks on the Home/Up button, so long
            // as you specify a parent activity in AndroidManifest.xml.

            int id = item.getItemId();

            //noinspection SimplifiableIfStatement
            if (id == R.id.action_logout) {
                AlertDialog.Builder alertDialogBuilder = new AlertDialog.Builder(MenuActivityPhase1.this);
                alertDialogBuilder.setMessage(R.string.AlertMsg);
                alertDialogBuilder.setPositiveButton(R.string.Yes, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        Intent i = new Intent(getApplicationContext(), LoginActivityTemp.class);
                        i.setFlags(i.FLAG_ACTIVITY_NEW_TASK | i.FLAG_ACTIVITY_CLEAR_TASK | i.FLAG_ACTIVITY_CLEAR_TOP);
                        startActivity(i); // Launch the HomescreenActivity
                        finish();
                        Toast.makeText(getApplicationContext(), getString(R.string.logout_user), Toast.LENGTH_LONG).show();
                    }

                });
                alertDialogBuilder.setNegativeButton(R.string.No, new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialog, int which) {
                        //finish();

                    }
                });
                AlertDialog dialog = alertDialogBuilder.create();
                dialog.show();
                // Close down the SettingsActivity
                return true;
            }

            // user is in notifications fragment
            // and selected 'Mark all as Read'
            if (id == R.id.action_mark_all_read) {
                Toast.makeText(getApplicationContext(), getString(R.string.notifications_marked_read), Toast.LENGTH_LONG).show();
            }

            // user is in notifications fragment
            // and selected 'Clear All'
            if (id == R.id.action_clear_notifications) {
                Toast.makeText(getApplicationContext(), getString(R.string.clear_all_notifications), Toast.LENGTH_LONG).show();
            }

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        return super.onOptionsItemSelected(item);
    }

    private void updateIPAddress() {
        try {
            final IPUpdateDialog ipUpdateDialog = new IPUpdateDialog(MenuActivityPhase1.this);
            ipUpdateDialog.show();
            ipUpdateDialog.setCancelable(false);

            ipUpdateDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                             @Override
                                                             public void onClick(final View view) {
                                                                 try {
                                                                     if (ipUpdateDialog.changeIP.getText().toString().equals("")) {
                                                                         Snackbar.make(view, getString(R.string.update_server_IP), Snackbar.LENGTH_LONG).show();

                                                                     } else {
//                                                                 if (CheckInternet.isNetworkAvailable(this)) {
                                                                         if (ipUpdateDialog.changeIP.getText().toString().matches("([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9])")) {
                                                                             Preferences.setServerIP(ipUpdateDialog.changeIP.getText().toString());
                                                                             ipUpdateDialog.dismiss();
                                                                         } else
                                                                             Snackbar.make(view, getString(R.string.check_IP_format), Snackbar.LENGTH_LONG).show();
//                                                                 } else {
//                                                                     Snackbar.make(view, "Check Internet Connection", Snackbar.LENGTH_LONG).show();
//                                                                 }
                                                                     }
                                                                 } catch (Exception ex) {
                                                                     Toast.makeText(MenuActivityPhase1.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                                                 }
                                                             }
                                                         }

            );
            ipUpdateDialog.btn_cancle.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    ipUpdateDialog.dismiss();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onBackPressed() {
        try {
            Fragment f = getSupportFragmentManager().findFragmentById(R.id.frame);
            if (f instanceof MenuFragment) {
                // do something with f
                if (doubleBackToExitPressedOnce) {
                    super.onBackPressed();
                    return;
                }
                this.doubleBackToExitPressedOnce = true;
                Toast.makeText(this, getString(R.string.click_back_again), Toast.LENGTH_SHORT).show();
                new Handler().postDelayed(new Runnable() {

                    @Override
                    public void run() {
                        doubleBackToExitPressedOnce = false;
                    }
                }, 2000);
            } else if (f instanceof PlantationRegistrationFragment) {
                final AlertDialog.Builder builder = new AlertDialog.Builder(this);
                builder.setTitle(getString(R.string.confirm));
                builder.setMessage(getString(R.string.want_exit_plantation));
                builder.setNegativeButton(getString(R.string.no), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        dialogInterface.dismiss();
                    }
                });

                builder.setPositiveButton(getString(R.string.yes), new DialogInterface.OnClickListener() {
                    @Override
                    public void onClick(DialogInterface dialogInterface, int i) {
                        FragmentManager fragmentManager = getSupportFragmentManager();
                        fragmentTransaction = fragmentManager.beginTransaction();
                        MenuFragment menuFragment = new MenuFragment();
                        fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                        fragmentTransaction.replace(R.id.frame, menuFragment);
                        fragmentTransaction.commit();
                        toolbar.setTitle(getString(R.string.nav_menu));
                        dialogInterface.dismiss();
                    }
                });
                builder.show();
            } else {

                FragmentManager fragmentManager = getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    protected void onResume() {
        try {
            super.onResume();
            IntentFilter tagDetected = new IntentFilter(NfcAdapter.ACTION_TAG_DISCOVERED);
            IntentFilter ndefDetected = new IntentFilter(NfcAdapter.ACTION_NDEF_DISCOVERED);
            IntentFilter techDetected = new IntentFilter(NfcAdapter.ACTION_TECH_DISCOVERED);
            IntentFilter[] nfcIntentFilter = new IntentFilter[]{techDetected, tagDetected, ndefDetected};
            PendingIntent pendingIntent = PendingIntent.getActivity(
                    this, 0, new Intent(this, getClass()).addFlags(Intent.FLAG_ACTIVITY_SINGLE_TOP), 0);
            if (mNfcAdapter != null)
                mNfcAdapter.enableForegroundDispatch(this, pendingIntent, nfcIntentFilter, null);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    protected void onPause() {
        try {
            super.onPause();
            if (mNfcAdapter != null)
                mNfcAdapter.disableForegroundDispatch(this);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void writeData(Ndef ndef) {
        String messageToWrite = "FlashBYSB";
        mNfcWriteFragment = (NFCWriteFragment) getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
        boolean returnVAl = mNfcWriteFragment.onNfcDetected(ndef, messageToWrite);
    }

    @Override
    protected void onNewIntent(Intent intent) {
        try {
            Tag tag = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
            Log.d(TAG, "onNewIntent: " + intent.getAction());
            if (NfcAdapter.ACTION_TAG_DISCOVERED.equals(getIntent().getAction())) {
                Tag tag1 = intent.getParcelableExtra(NfcAdapter.EXTRA_TAG);
                Log.d(TAG, "tag ID = " + tag.getId().toString());
                Toast.makeText(this, tag.getId().toString(), Toast.LENGTH_SHORT).show();
            }
            if (tag != null) {
//                byte[] idk = tag.getId();
//            Toast.makeText(this, getString(R.string.message_tag_detected) + " " + tag.getId().toString(), Toast.LENGTH_SHORT).show();
                Ndef ndef = Ndef.get(tag);
                if (write) {
                    if (flashNFC.equals("Y")) {
                        writeData(ndef);
                        flashNFC = "";
                    } else
                        MenuFragment.transferTokenFragment.writeData(ndef);
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void initNFC() {
        try {
            mNfcAdapter = NfcAdapter.getDefaultAdapter(this);
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private NfcAdapter mNfcAdapter;
    public static final String TAG = MenuActivityPhase1.class.getSimpleName();

    @Override
    public void onDialogDisplayed() {
    }

    @Override
    public void onDialogDismissed() {
    }

    public static boolean write = false;
    private NFCWriteFragment mNfcWriteFragment;
}

//7775870772 pankaj