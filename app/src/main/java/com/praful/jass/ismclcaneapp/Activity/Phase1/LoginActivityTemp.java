package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.ActivityNotFoundException;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.text.method.PasswordTransformationMethod;
import android.util.Log;
import android.view.View;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.DataSet.DataSet;
import com.praful.jass.ismclcaneapp.Dialog.IPEnterDialog;
import com.praful.jass.ismclcaneapp.Dialog.SetTrackTimeDialog;
import com.praful.jass.ismclcaneapp.Dialog.ShowDeviceIDDialog;
import com.praful.jass.ismclcaneapp.Dialog.UpdateAppDialog;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.OptionListFragment;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.XMLManager;
import com.praful.jass.ismclcaneapp.Models.CaneQualityModel;
import com.praful.jass.ismclcaneapp.Models.CurrentSlipNumberModel;
import com.praful.jass.ismclcaneapp.Models.FactoryNameListModel;
import com.praful.jass.ismclcaneapp.Models.GrowerDetailListModel;
import com.praful.jass.ismclcaneapp.Models.HarvestorListModel;
import com.praful.jass.ismclcaneapp.Models.IPListMasterModel;
import com.praful.jass.ismclcaneapp.Models.Irrigation_Cane_Type_and_Cane_Variety_list_model;
import com.praful.jass.ismclcaneapp.Models.LoginResponceModel;
import com.praful.jass.ismclcaneapp.Models.PlantationDataForRujvatModel;
import com.praful.jass.ismclcaneapp.Models.RujvatPlantatonDataModel;
import com.praful.jass.ismclcaneapp.Models.RujvatReasonModel;
import com.praful.jass.ismclcaneapp.Models.SeasonIdModel;
import com.praful.jass.ismclcaneapp.Models.ShivarVillageListModel;
import com.praful.jass.ismclcaneapp.Models.UserTypeFlageModel;
import com.praful.jass.ismclcaneapp.Models.VehicalListModel;
import com.praful.jass.ismclcaneapp.Models.VersionUpdateModel;
import com.praful.jass.ismclcaneapp.Models.VillageListAllDetailsModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.AppConstantsPhase_II;
import com.praful.jass.ismclcaneapp.Utils.ChangeLocaleLanguage;
import com.praful.jass.ismclcaneapp.Utils.CheckInternet;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.Validator;
import com.praful.jass.ismclcaneapp.services.CallSoap;
import com.praful.jass.ismclcaneapp.services.RestServiceCall;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.IOException;
import java.util.ArrayList;
import java.util.concurrent.TimeUnit;

/**
 * Created by Praful on 03-07-2017.
 */

public class LoginActivityTemp extends AppCompatActivity {
    public boolean APISTATUS = false;
    public ProgressDialog pdLoading;
    private TextView txt_login;
    private CallSoap cs;
    private RestServiceCall rsc;
    private XMLManager xmlManager;
    private SQLiteHelper sqLiteHelper;
    private EditText emailET, passET;
    private TelephonyManager telephonyManager;
    private String email, pass;
    private String deviceID = null;
    private Object output;
    private String flag = "online";
    private int app_ver = 0;

    private LinearLayout llLanguage, layoutLanguage, ll_season_list;
    private EditText edtLanguage, edtSeason;
    private OptionListFragment fragment;
    Context context;
    CheckBox chk_show_hide;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_login);
            Preferences.appContext = getApplicationContext();

            try {
                app_ver = this.getPackageManager().getPackageInfo(this.getPackageName(), 0).versionCode;
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }

            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                if (ContextCompat.checkSelfPermission(this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                    ActivityCompat.requestPermissions(LoginActivityTemp.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                } else {
                    callInitialFn();
                }
            } else {
                callInitialFn();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onRequestPermissionsResult(int requestCode,
                                           String permissions[], int[] grantResults) {
        try {
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {

                switch (requestCode) {
                    case 0: {
                        // If request is cancelled, the result arrays are empty.
                        if (grantResults.length > 0
                                && grantResults[0] == PackageManager.PERMISSION_GRANTED) {
                            callInitialFn();
                        } else {
                            String message = getString(R.string.allow_permission);
                            final ShowDeviceIDDialog showDeviceIDDialog = new ShowDeviceIDDialog(LoginActivityTemp.this);
                            showDeviceIDDialog.show();
                            showDeviceIDDialog.setCancelable(false);
                            showDeviceIDDialog.message_txt.setText(message);
                            showDeviceIDDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                                                 @Override
                                                                                 public void onClick(final View view) {
                                                                                     showDeviceIDDialog.dismiss();
                                                                                     if (ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED) {
                                                                                         ActivityCompat.requestPermissions(LoginActivityTemp.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                                                                                     }
                                                                                 }
                                                                             }
                            );
                        }
                        return;
                    }
                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void callInitialFn() {
        try {
            init();
            loginButtonListner();
            checkForUpdate(AppConstants.UPDATE_VERSION_SELECT, AppConstants.URL_GET_APP_VERSION);
            if (Preferences.getFirstTimeStatus()) {
                getDeviceID();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    @SuppressLint("HardwareIds")
    public void init() {
        try {
            pdLoading = new ProgressDialog(LoginActivityTemp.this);
            pdLoading.setIndeterminate(true);
            pdLoading.setCancelable(false);
            context = this;
            txt_login = (TextView) findViewById(R.id.txt_login);
            emailET = (EditText) findViewById(R.id.txt_username);
            passET = (EditText) findViewById(R.id.txt_password);
            llLanguage = (LinearLayout) findViewById(R.id.ll_language);
            ll_season_list = (LinearLayout) findViewById(R.id.ll_season_list);
            layoutLanguage = (LinearLayout) findViewById(R.id.layout_language);
            edtLanguage = (EditText) findViewById(R.id.edt_language);
            edtSeason = (EditText) findViewById(R.id.edt_season);
            chk_show_hide = (CheckBox) findViewById(R.id.chk_show_hide);
            cs = new CallSoap();
            rsc = new RestServiceCall();
            telephonyManager = (TelephonyManager) this.getSystemService(TELEPHONY_SERVICE);
            xmlManager = new XMLManager();
            sqLiteHelper = new SQLiteHelper(this);

            if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling

                return;
            }
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                deviceID = telephonyManager.getDeviceId(2);
            } else {
                deviceID = telephonyManager.getDeviceId();//getDeviceId(1);
            }

            Preferences.setDeviceId(deviceID);

        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }

        beforeLogin();
    }

    private void beforeLogin() {
        if (Preferences.getFirstTimeStatus()) {
            ll_season_list.setVisibility(View.GONE);
            llLanguage.setVisibility(View.GONE);
        } else {
            ll_season_list.setVisibility(View.VISIBLE);
            llLanguage.setVisibility(View.VISIBLE);
            if (Preferences.getLocalLanguage().equals(AppConstants.ENGLISH)) {
                edtLanguage.setText(R.string.txt_english);
            } else if (Preferences.getLocalLanguage().equals(AppConstants.MARATHI)) {
                edtLanguage.setText(R.string.txt_marathi);
            }
            emailET.setText(Preferences.getUserName());
            emailET.setEnabled(false);
        }
    }

    public void loginButtonListner() {
        try {
            txt_login.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
                        if (ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(LoginActivityTemp.this, Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED && Build.VERSION.SDK_INT >= Build.VERSION_CODES.LOLLIPOP) {
                            ActivityCompat.requestPermissions(LoginActivityTemp.this, new String[]{Manifest.permission.CAMERA, Manifest.permission.READ_PHONE_STATE, Manifest.permission.ACCESS_FINE_LOCATION, Manifest.permission.WRITE_EXTERNAL_STORAGE}, 0);
                        } else {
                            email = emailET.getText().toString();
                            pass = passET.getText().toString();
                            pdLoading.setMessage(getString(R.string.login_for) + email);
                            pdLoading.show();
                            if (AppConstants.IN_TESTING) {
                                Intent intent = new Intent(LoginActivityTemp.this, MenuActivityPhase1.class);
                                startActivity(intent);
                            } else if (Validator.isValidString(email, pass)) {
                                pdLoading.show();
                                new AsyncCaller().execute(v);
//                        login(v);
                            } else {
                                if (!Validator.isValidString(email)) {
                                    pdLoading.dismiss();
//                emailET.setError("UserName is mandatory");
                                    Toast.makeText(LoginActivityTemp.this, getString(R.string.username_mandatory), Toast.LENGTH_SHORT).show();

                                }
                                if (!Validator.isValidString(pass)) {
                                    pdLoading.dismiss();
//                passET.setError("Password is mandatory");
                                    Toast.makeText(LoginActivityTemp.this, getString(R.string.password_mandatory), Toast.LENGTH_SHORT).show();

                                }
                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }


        edtLanguage.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionList(AppConstants.LANGUAGE_SELECT);
            }
        });

        chk_show_hide.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (isChecked) {
                    passET.setTransformationMethod(null);
                } else {
                    passET.setTransformationMethod(new PasswordTransformationMethod());
                }
            }
        });


        edtSeason.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showOptionList(AppConstants.SEASON_ID_SELECT);
            }
        });
    }


    public void showOptionList(final int SELECT) {
        ArrayList<String> mList = new ArrayList<>();
        JSONArray jsonArray = new JSONArray();
        DataSet dataSet = new DataSet();
        String title = "Nothing";

        try {

            if (SELECT == AppConstants.LANGUAGE_SELECT) {
                mList = dataSet.getListLanguage(context);
                title = getString(R.string.hint_language);
            } else if (SELECT == AppConstants.SEASON_ID_SELECT) {
//                mList = dataSet.getListSeason(context);
                JSONObject jsonObject = new JSONObject(Preferences.getSeasonList());
                jsonArray = jsonObject.getJSONArray("seasonList");
                for (int i = 0; i < jsonArray.length(); i++) {
                    mList.add(((JSONObject) jsonArray.get(i)).get("seasonName").toString());
                }
                title = getString(R.string.hint_season);
            }

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        final FragmentTransaction ft = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(mList, title);
        fragment.setListTitle("Languages");

        final JSONArray finalJsonArray = jsonArray;
        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                removeOptionList();
            }

            @Override
            public void onListItemSelected(Object model) {
                if (SELECT == AppConstants.LANGUAGE_SELECT) {
                    String lCode;
                    if (model.toString().equalsIgnoreCase(getString(R.string.txt_english))) {
                        lCode = AppConstants.ENGLISH;
                    } else {
                        lCode = AppConstants.MARATHI;
                    }
                    new ChangeLocaleLanguage().changeLocale(lCode, getApplicationContext());
                    Preferences.setLocalLanguage(lCode);
                    removeOptionList();
                    updateUI();
                } else if (SELECT == AppConstants.SEASON_ID_SELECT) {
                    removeOptionList();
                    edtSeason.setText((CharSequence) model);
                    try {
                        Preferences.setSeasonIDSelected(((JSONObject) finalJsonArray.get(AppConstants.selectItemIndex)).get("seasonID").toString());
                        Preferences.setSeasonName(((JSONObject) finalJsonArray.get(AppConstants.selectItemIndex)).get("seasonName").toString());
                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                }
                //updateUI();
            }
        };
        ft.add(R.id.activity_login, fragment);
        ft.commit();
        pdLoading.dismiss();
    }

    void removeOptionList() {
        try {
            final FragmentTransaction ft = (FragmentTransaction) getSupportFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            ft.remove(fragment).commit();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void updateUI() {
        Intent i = new Intent(context, LoginActivityTemp.class);
        i.addFlags(Intent.FLAG_ACTIVITY_CLEAR_TOP);
        startActivity(i);
    }

    public void login(View v) {
        try {
            final String email = emailET.getText().toString();
            String pass = passET.getText().toString();

            if (AppConstants.URL_USER_LOGIN.contains("null")) {
                AppConstants.BASE_URL_PHP_SERVICE = "http://" + Preferences.getServerIP() + ":80/store_procedure_image/";
//                AppConstants.BASE_URL_PHP_SERVICE = "http://" + Preferences.getServerIP() + ":80/store_procedure_image(07-07-2018)/";
            }
            if (CheckInternet.isNetworkAvailable(this)) {
                JSONObject jsonObject = new JSONObject();

                try {
                    jsonObject.put("user_name", email);
                    jsonObject.put("password", pass);
                    jsonObject.put("device", deviceID);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
//            Object loginOutput = cs.CallLogin(email, pass, deviceID); //deviceID  //  Use to call admin login service

                checkLogin(AppConstants.URL_USER_LOGIN, jsonObject, LoginResponceModel.class, this, v); //deviceID  //  Use to call admin login service
            } else {

                flag = "offline";
                loginAndGetListData(sqLiteHelper.getLoginData(email, pass), v);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void checkForUpdate(final int URLType, String URL) {
        try {
            if (CheckInternet.isNetworkAvailable(this)) {
                JSONObject jsonObject = new JSONObject();
                if (URLType == AppConstants.UPDATE_VERSION_SELECT) {
                    try {
                        jsonObject.put("api_token", AppConstants.API_TOKEN);
                        jsonObject.put("app_version", getPackageManager().getPackageInfo(getPackageName(), 0).versionCode);

                    } catch (JSONException e) {
                        e.printStackTrace();
                    }
                } else if (URLType == AppConstants.IP_LIST_SELECT) {
                }
                if (!URL.contains("null")) {
                    try {
                        new APIRestManager().postAPI(URL, jsonObject, this, VersionUpdateModel.class, new APIRestManager.APIManagerInterface() {
                            @Override
                            public void onSuccess(Object resultObj) {
                                if (URLType == AppConstants.UPDATE_VERSION_SELECT) {
                                    VersionUpdateModel model = (VersionUpdateModel) resultObj;
                                    try {
                                        if (model.getVersion() > getPackageManager().getPackageInfo(getPackageName(), 0).versionCode) {
                                            showUpdateDialog(model);
                                        }
                                    } catch (PackageManager.NameNotFoundException e) {
                                        e.printStackTrace();
                                    }
                                    checkForUpdate(AppConstants.IP_LIST_SELECT, AppConstants.URL_IP_LIST);
                                } else if (URLType == AppConstants.IP_LIST_SELECT) {
                                    try {
                                        JSONObject object = (JSONObject) resultObj;
                                        Preferences.setIPList(object.toString());
//                                        Object json = object.get("response");
//                                        Gson gson = new Gson();
//                                        if (json instanceof JSONArray) {
//                                            ArrayList resultArray = new ArrayList();
//                                            JSONArray list = object.getJSONArray("response");
//                                            for (int i = 0; i < list.length(); i++) {
//                                                JSONObject object1 = list.getJSONObject(i);
//                                                Object model = gson.fromJson(object1.toString(), IPListMasterModel.class);
//                                                resultArray.add(model);
//                                            }
//                                        }
//                                    } catch (JSONException jex) {
//                                        Toast.makeText(LoginActivityTemp.this, jex.getMessage(), Toast.LENGTH_LONG).show();
                                    } catch (Exception ex) {
                                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_LONG).show();
                                    }
                                }
                            }
                            @Override
                            public void onError(String error) {
                                Toast.makeText(LoginActivityTemp.this, error, Toast.LENGTH_LONG).show();
//                            finish();
                            }
                        });
                    } catch (Exception ex) {
                        Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            } else {
//                Snackbar.make(R.id., "Check Internet Connection", Snackbar.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
    }


    private void showUpdateDialog(VersionUpdateModel model) {
        try {
            UpdateAppDialog dialog = new UpdateAppDialog(this);
            dialog.setCancelable(false);
            dialog.show();

            dialog.txtUpdateDesc.setText(getString(R.string.whats_new) + " : \n" + model.getDescription());
            dialog.btnOkay.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    try {
//                Toast.makeText(SplashActivity.this, "Navigates to Play store", Toast.LENGTH_SHORT).show();
//                Intent i = new Intent(this, LoginActivity.class);
//                startActivity(i);
//                finish();
                        launchMarket();
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void launchMarket() {
        try {
            Uri uri = Uri.parse("market://details?id=" + getPackageName());
            Intent myAppLinkToMarket = new Intent(Intent.ACTION_VIEW, uri);
            try {
                startActivity(myAppLinkToMarket);
            } catch (ActivityNotFoundException e) {
                Toast.makeText(this, getString(R.string.unable_market_app), Toast.LENGTH_LONG).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getDeviceID() {
        try {
            String message = getString(R.string.register_device) + " " + deviceID;
//            Log.w("masg", message);
            final ShowDeviceIDDialog showDeviceIDDialog = new ShowDeviceIDDialog(LoginActivityTemp.this);
            showDeviceIDDialog.show();
            showDeviceIDDialog.setCancelable(false);
            showDeviceIDDialog.message_txt.setText(message);
            showDeviceIDDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                                 @Override
                                                                 public void onClick(final View view) {
                                                                     try {
                                                                         showDeviceIDDialog.dismiss();
                                                                         setIPAddress();
                                                                     } catch (Exception ex) {
                                                                         Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                                                     }
                                                                 }
                                                             }
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setIPAddress() {
        try {
            final IPEnterDialog ipEnterDialog = new IPEnterDialog(LoginActivityTemp.this);
            ipEnterDialog.show();
            ipEnterDialog.setCancelable(false);

            ipEnterDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                            @Override
                                                            public void onClick(final View view) {
                                                                try {
                                                                    if (ipEnterDialog.changeIP.getText().toString().equals("")) {
                                                                        Snackbar.make(view, getString(R.string.server_ip), Snackbar.LENGTH_LONG).show();

                                                                    } else {
                                                                        if (CheckInternet.isNetworkAvailable(LoginActivityTemp.this)) {
                                                                            if (ipEnterDialog.changeIP.getText().toString().matches("([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9]).([0-9]{3}|[0-9]{2}|[0-9])")) {
                                                                                Preferences.setServerIP(ipEnterDialog.changeIP.getText().toString());
                                                                                ipEnterDialog.dismiss();
                                                                                setTrackingTime();
                                                                            } else
                                                                                Snackbar.make(view, getString(R.string.IP_format), Snackbar.LENGTH_LONG).show();
                                                                        } else {
                                                                            Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                                                                        }
                                                                    }
                                                                } catch (Exception ex) {
                                                                    Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                                                }
                                                            }
                                                        }
            );
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void setTrackingTime() {
        try {
            final SetTrackTimeDialog setTrackTimeDialog = new SetTrackTimeDialog(LoginActivityTemp.this);
            setTrackTimeDialog.show();
            setTrackTimeDialog.setCancelable(false);

            setTrackTimeDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(final View view) {
                    try {
                        if (setTrackTimeDialog.timeinseconds.getText().toString().equals("")) {
                            Snackbar.make(view, getString(R.string.tracking_time_login), Snackbar.LENGTH_LONG).show();

                        } else {
                            if (CheckInternet.isNetworkAvailable(LoginActivityTemp.this)) {
                                long l = TimeUnit.SECONDS.toMillis(Long.parseLong(setTrackTimeDialog.timeinseconds.getText().toString()));
                                Preferences.setTrackingtime(l + "");
                                setTrackTimeDialog.dismiss();
                            } else {
                                Snackbar.make(view, getString(R.string.internet_connection), Snackbar.LENGTH_LONG).show();
                            }
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void checkLogin(String apiUrl, final JSONObject jsonObject, Class modelType, Context context, final View v) {
        try {
            output = "";
            new APIRestManager().postAPI(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {
                    try {
                        if (resultObj instanceof LoginResponceModel) {
                            LoginResponceModel loginResponceModel = (LoginResponceModel) resultObj;
                            JSONObject param = new JSONObject();
                            JSONObject jsonObject = new JSONObject();
                            param.put("p_user", loginResponceModel.getUser_id());
                            param.put("p_device", deviceID);
                            param.put("p_ver", getPackageManager().getPackageInfo(getPackageName(), 0).versionName);
                            JSONArray jsonArray = new JSONArray();
                            jsonArray.put(param);
                            jsonObject.put("UserInfo", jsonArray);
                            checkUserFlag(AppConstants.URL_UPDATE_VERSION_AND_GET_USER_FLAG, jsonObject, UserTypeFlageModel.class, LoginActivityTemp.this, v, loginResponceModel);
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onError(String strError) {
                    try {
                        pdLoading.dismiss();
                        if (strError == null) {
                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();

                        } else if (strError.equals(getString(R.string.user_not_found))) {
                            Snackbar.make(findViewById(R.id.activity_login), strError, Snackbar.LENGTH_LONG).show();

                        } else {
                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void checkUserFlag(String apiUrl, JSONObject jsonObject, Class modelType, Context context, final View v, final LoginResponceModel loginResponceModel) {

        try {
            output = "";
            new APIRestManager().postAPI(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {
                    try {
                        if (resultObj instanceof ArrayList) {
                            ArrayList<UserTypeFlageModel> userTypeFlageModels = (ArrayList<UserTypeFlageModel>) resultObj;
                            Preferences.setUserTypeFlag(userTypeFlageModels.get(0).UserType.trim());
                            loginAndGetListData(loginResponceModel.getUser_id(), v);
                        } else {
                            Toast.makeText(LoginActivityTemp.this, "Check User Flag Fail", Toast.LENGTH_SHORT).show();
                            pdLoading.dismiss();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                        pdLoading.dismiss();
                    }
                }

                @Override
                public void onError(String strError) {
                    try {
                        pdLoading.dismiss();
                        if (strError == null) {
                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        } else if (strError.equals(getString(R.string.user_not_found))) {
                            Snackbar.make(findViewById(R.id.activity_login), strError, Snackbar.LENGTH_LONG).show();
                        } else {
                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        }
                    } catch (Exception ex) {
                        Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                        pdLoading.dismiss();
                    }
                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            pdLoading.dismiss();
        }
    }


    public void loginAndGetListData(String loginOutput, View v) {
        try {
            if (loginOutput.toString().contains(AppConstants.SERVER_NOT_RESPONDING_ERROR + "")) {
                pdLoading.dismiss();
                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
            } else if (loginOutput.equals(AppConstants.DATA_BASE_NOT_AVAILABLE)) {
                pdLoading.dismiss();
                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.login_first_time), Snackbar.LENGTH_LONG).show();
            } else {
                if (loginOutput.equalsIgnoreCase("0")) {
                    pdLoading.dismiss();
                    Snackbar.make(findViewById(R.id.activity_login), getString(R.string.invalid_username_password), Snackbar.LENGTH_LONG).show();
                } else {
                    Snackbar.make(getCurrentFocus(), getString(R.string.login_successfully), Snackbar.LENGTH_LONG).show();
                    if (flag.equals("offline"))
                        AppConstants.USER_ID = Integer.parseInt(Preferences.getCurrentUserId());
                    APISTATUS = true;
                    sqLiteHelper.insertLoginData(email, pass, loginOutput);
                    Preferences.setUserName(email);
                    Preferences.setUserPass(pass);
                    Preferences.setCurrentUserId(loginOutput);
                    AppConstants.USER_ID = Integer.parseInt(loginOutput);

                    if (Preferences.getUpdationFirstTimeStatus()) {
                        Preferences.setTripImgStatus(true);
                        Preferences.setRujvatImgStatus(true);
                        Preferences.setUpdationFirstTimeStatus(false);
                        Preferences.setKycInfoStatus(true);
                        Preferences.setKycImgStatus(true);
                    }

                    if (Preferences.getFirstTimeStatus()) {

                        Preferences.setPlantationUploadStatus(true);
                        Preferences.setImageRenameStatus(true);
                        Preferences.setRujvatUploadStatus(true);
                        Preferences.setAllImageUploadStatus(true);
                        Preferences.setKycInfoStatus(true);
                        Preferences.setKycImgStatus(true);
                        java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(this);
                        Preferences.setTripCounter("23.23.23");

                        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                                Environment.DIRECTORY_PICTURES), "CaneApp");

                        if (!mediaStorageDir.exists()) {
                            if (!mediaStorageDir.mkdirs()) {
                                Log.d("Camera Check", "failed to create directory");
                            }
                        }
                        File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                        if (!file.exists()) {
                            try {
                                file.createNewFile();
                            } catch (IOException e) {
                                e.printStackTrace();
                            }
                        }
                        pdLoading.setMessage(getString(R.string.fetching_data));

                        try {

                            Preferences.setTripStatus(AppConstants.STOPT_TRIP);
                            callRespAPIForList(AppConstants.URL_SEASON_ID, SeasonIdModel.class, AppConstants.SEASON_ID_SELECT, LoginActivityTemp.this, v);

                        } catch (Exception e) {
                            pdLoading.dismiss();
                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                            Preferences.setFirstTimeStatus(true);
                        }
                    } else {
                        allServicesSucsessFull();
                    }

                }
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void callRespAPIForList(String apiUrl, Class modelType, final int listType, Context context, final View v) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                if (listType == AppConstants.GROWER_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
//                jsonObject.put("season", Preferences.getSeasonID() + "");
                } else if (listType == AppConstants.SHIVAR_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
//                jsonObject.put("season", Preferences.getSeasonID() + "");
                } else if (listType == AppConstants.RUJVAT_ALL_DATA_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
//                    jsonObject.put("admin_flag", Preferences.getUserTypeFlag());
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT) {
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("device_id", deviceID + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else {
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                }
            } catch (JSONException ex) {
                ex.printStackTrace();
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }

            try {
                if (APISTATUS) {
                    new APIRestManager().postAPI(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {

                        @Override
                        public void onSuccess(Object resultObj) {
                            try {
                                if (listType == AppConstants.RUJVAT_REASON_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String rujvatReasonList = null;
                                            rujvatReasonList = xmlManager.updatedRujvatReasonList(v, (ArrayList<RujvatReasonModel>) resultObj);
                                            if (!rujvatReasonList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertReasonList(rujvatReasonList);
                                                callRespAPIForList(AppConstantsPhase_II.URL_PLANTATION_LIST_TC, PlantationDataForRujvatModel.class, AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.GROWER_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String growerDetailList = null;
                                            growerDetailList = xmlManager.updatedFarmerList(v, (ArrayList<GrowerDetailListModel>) resultObj);
                                            if (!growerDetailList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertFarmerList(growerDetailList);
                                                callRespAPIForList(AppConstants.URL_VILLAGE_LIST, ShivarVillageListModel.class, AppConstants.SHIVAR_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstants.SHIVAR_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String shivarVillageList = null;
                                            shivarVillageList = xmlManager.updatedVillageList(v, (ArrayList<ShivarVillageListModel>) resultObj);
                                            if (!shivarVillageList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertVillageList(shivarVillageList);
                                                callRespAPIForList(AppConstants.URL_IRRIGATION_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.IRRIGATION_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstants.IRRIGATION_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String irrigationList = null;
                                            irrigationList = xmlManager.updatedSugarIrrigationMaster(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                            if (!irrigationList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertIrrigationMaster(irrigationList);
                                                callRespAPIForList(AppConstants.URL_CANE_VARITY_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.CAN_VERIETY_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.CAN_VERIETY_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String caneVeriety = null;
                                            caneVeriety = xmlManager.updatedCaneVerityList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                            if (!caneVeriety.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertCaneVerityList(caneVeriety);
                                                callRespAPIForList(AppConstants.URL_CANE_TYPE_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.CAN_TYPE_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.CAN_TYPE_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String caneType = null;
                                            caneType = xmlManager.updatedCaneTypeList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                            if (!caneType.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertCaneTypeList(caneType);
                                                callRespAPIForList(AppConstants.URL_PLANTATION_TYPE_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.PLANTATION_TYPE_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.PLANTATION_TYPE_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String plantationType = null;
                                            plantationType = xmlManager.updatedPlantationList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                            if (!plantationType.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertCanePlantationList(plantationType);
                                                callRespAPIForList(AppConstants.URL_ADD_RUJVAT_REASON, RujvatReasonModel.class, AppConstants.RUJVAT_REASON_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.SEASON_ID_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            ArrayList<SeasonIdModel> seasonIdList = new ArrayList();
                                            seasonIdList = (ArrayList<SeasonIdModel>) resultObj;
                                            if (!seasonIdList.get(0).getSEASON_ID().equalsIgnoreCase("0")) {
                                                Preferences.setSeasonID(seasonIdList.get(0).getSEASON_ID() + "");
                                                callRespAPIForList(AppConstants.URL_FARMER_LIST, GrowerDetailListModel.class, AppConstants.GROWER_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.RUJVAT_ALL_DATA_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            ArrayList<RujvatPlantatonDataModel> seasonIdList = (ArrayList<RujvatPlantatonDataModel>) resultObj;
                                            if (seasonIdList.size() > 0) {
                                                for (int i = 0; i < seasonIdList.size(); i++) {
                                                    sqLiteHelper.insertRjvatDataRJ(seasonIdList.get(i).getPLANTATION_CODE(), seasonIdList.get(i).getFARMER_NAME(), seasonIdList.get(i).getFARMER_CODE(), seasonIdList.get(i).getVILLAGE_NAME(), seasonIdList.get(i).getGUT_NAME(), seasonIdList.get(i).getSUB_GUT_NAME(), seasonIdList.get(i).getTALUKA_NAME(), seasonIdList.get(i).getSHIVAR_VILLAGE_NAME(), seasonIdList.get(i).getSHIVAR_GUT_NAME(), seasonIdList.get(i).getSARVE_NO(), seasonIdList.get(i).getIRRE_NAME(), seasonIdList.get(i).getVARITY(), seasonIdList.get(i).getPLANTATION_DATE(), seasonIdList.get(i).getPLANTATION_TYPE(), seasonIdList.get(i).getPLANTATION_AREA(), seasonIdList.get(i).getOBJECT_ID());
                                                }
                                                APISTATUS = true;
                                                allServicesSucsessFull();

                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.rujvat_not_found), Snackbar.LENGTH_LONG).show();
                                                allServicesSucsessFull();
                                                APISTATUS = true;
                                                pdLoading.dismiss();
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String planationDetailList = null;
                                            planationDetailList = xmlManager.jsonPlantationDataFORTC(v, (ArrayList<PlantationDataForRujvatModel>) resultObj);
                                            if (!planationDetailList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertPlantationList(planationDetailList);
                                                callRespAPIForList(AppConstantsPhase_II.URL_VEHICAL_LIST_TC, VehicalListModel.class, AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String vehicalList = null;
                                            vehicalList = xmlManager.jsonVehicalDataFORTC(v, (ArrayList<VehicalListModel>) resultObj);
                                            if (!vehicalList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertVehicleList(vehicalList);
                                                callRespAPIForList(AppConstantsPhase_II.URL_HARVESTOR_LIST_TC, HarvestorListModel.class, AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String harvesterList = null;
                                            harvesterList = xmlManager.jsonHarvesterDataFORTC(v, (ArrayList<HarvestorListModel>) resultObj);
                                            if (!harvesterList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertHarvesterList(harvesterList);
                                                callRespAPIForList(AppConstantsPhase_II.URL_CURRENT_SLIP_ID_LIST_TC, CurrentSlipNumberModel.class, AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            e.printStackTrace();
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            ArrayList<CurrentSlipNumberModel> currentSlipNumber = (ArrayList<CurrentSlipNumberModel>) resultObj;
                                            String slipNumber = currentSlipNumber.get(0).getNVL();
                                            Preferences.setCurrentSlipNumber(slipNumber);
                                            callRespAPIForList(AppConstants.URL_GET_ALL_VILLAGE_LIST_DETAILS, VillageListAllDetailsModel.class, AppConstants.VILLAGE_ALL_DETAILS_SELECT, LoginActivityTemp.this, v);
                                            APISTATUS = true;
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                            pdLoading.dismiss();
                                            allServicesSucsessFull();
                                        }
                                    }
                                } else if (listType == AppConstants.VILLAGE_ALL_DETAILS_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String villageDetailList = null;
                                            villageDetailList = xmlManager.jsonvillageListDetails(v, (ArrayList<VillageListAllDetailsModel>) resultObj);
                                            if (!villageDetailList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertAllVillageList(villageDetailList);
                                                callRespAPIForList(AppConstants.URL_CANE_QUALITY, CaneQualityModel.class, AppConstants.CANE_QUALITY_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.CANE_QUALITY_SELECT) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String caneQualityList = null;
                                            caneQualityList = xmlManager.jsonCaneQualityList(v, (ArrayList<CaneQualityModel>) resultObj);
                                            if (!caneQualityList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertAllCaneQuality(caneQualityList);
                                                callRespAPIForList(AppConstants.URL_GET_FACTORY_NAME_LIST, FactoryNameListModel.class, AppConstants.FACTORY_NAME_LIST, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                            pdLoading.dismiss();
                                        }
                                    }
                                } else if (listType == AppConstants.FACTORY_NAME_LIST) {
                                    if (resultObj instanceof ArrayList) {
                                        try {
                                            String factoryNameList = null;
                                            factoryNameList = xmlManager.jsonFactoryNameList(v, (ArrayList<FactoryNameListModel>) resultObj);
                                            if (!factoryNameList.equalsIgnoreCase("0")) {
                                                sqLiteHelper.insertFactoryNameList(factoryNameList);
                                                callRespAPIForList(AppConstants.URL_GET_ALL_PLANTATION_FOR_RUJVAT, RujvatPlantatonDataModel.class, AppConstants.RUJVAT_ALL_DATA_SELECT, LoginActivityTemp.this, v);
                                                APISTATUS = true;
                                            } else {
                                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                                APISTATUS = false;
                                            }
                                        } catch (Exception e) {
                                            APISTATUS = false;
                                            Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                            e.printStackTrace();
                                            pdLoading.dismiss();
                                        }
                                    }
                                }
                            } catch (Exception ex) {
                                Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }

                        @Override
                        public void onError(String strError) {
                            try {
                                output = AppConstants.SERVER_NOT_RESPONDING_ERROR;
                                APISTATUS = false;
                                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                pdLoading.dismiss();
                            } catch (Exception ex) {
                                Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } catch (Exception ex) {
                Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void allServicesSucsessFull() {
        try {
            if (APISTATUS) {
                Preferences.setFirstTimeStatus(false);
                startActivity(new Intent(this, MenuActivityPhase1.class));
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                pdLoading.dismiss();
                finish();
            } else {
                pdLoading.dismiss();
                Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                Preferences.setFirstTimeStatus(true);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private class AsyncCaller extends AsyncTask<View, Void, Void> {

        @Override
        protected void onPreExecute() {
            try {
                super.onPreExecute();
            } catch (Exception ex) {
                Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Void doInBackground(View... params) {
            try {
                login(params[0]);
            } catch (Exception ex) {
                Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                super.onPostExecute(result);
            } catch (Exception ex) {
                Toast.makeText(LoginActivityTemp.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}//117.232.126.133

