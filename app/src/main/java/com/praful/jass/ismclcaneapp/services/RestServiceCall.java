package com.praful.jass.ismclcaneapp.services;

import android.content.Context;

import org.json.JSONObject;

import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

public class RestServiceCall {
    Object output;

    public RestServiceCall() {

    }


    public Object callREASON_MASTER(String apiUrl, Class modelType, Context context) {
        JSONObject jsonObject = new JSONObject();

        new APIRestManager().postAPI(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {

            @Override
            public void onSuccess(Object resultObj) {
                output = resultObj;
            }

            @Override
            public void onError(String strError) {
                output = AppConstants.SERVER_NOT_RESPONDING_ERROR;
            }
        });
        return output;
    }

//    public Object checkLogin(String apiUrl, JSONObject jsonObject, Class modelType, Context context) {
//        new APIRestManager().postAPICommon(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {
//
//            @Override
//            public void onSuccess(Object resultObj) {
//                output = resultObj;
//            }
//
//            @Override
//            public void onError(String strError) {
//                output = AppConstants.SERVER_NOT_RESPONDING_ERROR;
//            }
//        });
//        return output;
//    }
}
