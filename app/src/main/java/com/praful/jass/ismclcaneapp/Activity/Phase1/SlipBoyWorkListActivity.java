package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.support.v7.app.AppCompatActivity;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Adapter.SlipBoyOptionDataAdapter;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.CandidateWorkDataModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class SlipBoyWorkListActivity extends AppCompatActivity {

    private ArrayList<CandidateWorkDataModel> mList;
    private RecyclerView recycler_view;
    private SlipBoyOptionDataAdapter mAdapter;
    TextView slip_data_msg;
    ImageView go_back;
    Context context;
    private String URL = "";

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_slip_option_data);
        Intent intent = getIntent();
        if (intent.getStringExtra("Purpose").equalsIgnoreCase("history"))
            URL = AppConstants.URL_CHITBOY_WORK_HISTORY;
        else
            URL = AppConstants.URL_GET_WORK_TYPE_DATA_LIST;
        init();
    }

    @Override
    protected void onResume() {
        super.onResume();
        getCandidateWorkDataList();
        setAdapter();
        setListeners();
    }

    private void init() {
        context = this;
        mList = new ArrayList<>();
        recycler_view = findViewById(R.id.recycler_view);
        slip_data_msg = findViewById(R.id.slip_data_msg);
        go_back = findViewById(R.id.go_back);
        // mList = new DataSet().getTripBoyOptionsData(context);
        //Method Calling
        getCandidateWorkDataList();
        setAdapter();
        setListeners();
    }

    private void setListeners() {
        go_back.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });
    }

    private void setAdapter() {
        recycler_view.setVisibility(View.VISIBLE);
        slip_data_msg.setVisibility(View.GONE);
        mAdapter = new SlipBoyOptionDataAdapter(context, mList);
        recycler_view.setLayoutManager(new LinearLayoutManager(context));
        recycler_view.setAdapter(mAdapter);
    }

    /**
     * API CALLING
     **/

    private void getCandidateWorkDataList() {

        final ProgressDialog progressDialog = new ProgressDialog(context);
        progressDialog.show();
        JSONObject object = new JSONObject();
        try {
            object.put("user_id", "112");
//            object.put("user_id", AppConstants.SELECTED_USER_ID);
            object.put("season", Preferences.getSeasonID());
            object.put("work_type", AppConstants.FLAG_WORK_TYPE_SELECTED);
            //object.put("work_type",AppConstants.FLAG_WORK_TYPE_SELECTED);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        //String url = AppConstants.URL_GET_WORK_TYPE_DATA_LIST+"?season="+Preferences.getSeasonID()+"&user_id="+AppConstants.USER_ID;

        new APIRestManager().postJSONArrayAPI(URL, object, CandidateWorkDataModel.class, context, new APIRestManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj) {
                progressDialog.dismiss();
                mList = (ArrayList<CandidateWorkDataModel>) resultObj;
                mAdapter.updateAdapter(mList);
            }

            @Override
            public void onError(String strError) {
                progressDialog.dismiss();
                Toast.makeText(context, strError, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
