package com.praful.jass.ismclcaneapp.Models;

public class KYCModel {

    String p_first_name, p_middel_name, p_last_name, p_mob, p_village_id, p_aadhar_name, p_pan_name, p_bank_name, p_vote_name, p_aadharno, p_panno, p_voterid, p_accountno,p_user_id,p_date,p_lat,p_lng,p_device;

    public String getP_first_name() {
        return p_first_name;
    }

    public void setP_first_name(String p_first_name) {
        this.p_first_name = p_first_name;
    }

    public String getP_middel_name() {
        return p_middel_name;
    }

    public void setP_middel_name(String p_middel_name) {
        this.p_middel_name = p_middel_name;
    }

    public String getP_last_name() {
        return p_last_name;
    }

    public void setP_last_name(String p_last_name) {
        this.p_last_name = p_last_name;
    }

    public String getP_mob() {
        return p_mob;
    }

    public void setP_mob(String p_mob) {
        this.p_mob = p_mob;
    }

    public String getP_village_id() {
        return p_village_id;
    }

    public void setP_village_id(String p_village_id) {
        this.p_village_id = p_village_id;
    }

    public String getP_aadhar_name() {
        return p_aadhar_name;
    }

    public void setP_aadhar_name(String p_aadhar_name) {
        this.p_aadhar_name = p_aadhar_name;
    }

    public String getP_pan_name() {
        return p_pan_name;
    }

    public void setP_pan_name(String p_pan_name) {
        this.p_pan_name = p_pan_name;
    }

    public String getP_bank_name() {
        return p_bank_name;
    }

    public void setP_bank_name(String p_bank_name) {
        this.p_bank_name = p_bank_name;
    }

    public String getP_vote_name() {
        return p_vote_name;
    }

    public void setP_vote_name(String p_vote_name) {
        this.p_vote_name = p_vote_name;
    }

    public String getP_aadharno() {
        return p_aadharno;
    }

    public void setP_aadharno(String p_aadharno) {
        this.p_aadharno = p_aadharno;
    }

    public String getP_panno() {
        return p_panno;
    }

    public void setP_panno(String p_panno) {
        this.p_panno = p_panno;
    }

    public String getP_voterid() {
        return p_voterid;
    }

    public void setP_voterid(String p_voterid) {
        this.p_voterid = p_voterid;
    }

    public String getP_accountno() {
        return p_accountno;
    }

    public void setP_accountno(String p_accountno) {
        this.p_accountno = p_accountno;
    }

    public String getP_user_id() {
        return p_user_id;
    }

    public void setP_user_id(String p_user_id) {
        this.p_user_id = p_user_id;
    }

    public String getP_date() {
        return p_date;
    }

    public void setP_date(String p_date) {
        this.p_date = p_date;
    }

    public String getP_lat() {
        return p_lat;
    }

    public void setP_lat(String p_lat) {
        this.p_lat = p_lat;
    }

    public String getP_lng() {
        return p_lng;
    }

    public void setP_lng(String p_lng) {
        this.p_lng = p_lng;
    }

    public String getP_device() {
        return p_device;
    }

    public void setP_device(String p_device) {
        this.p_device = p_device;
    }
}
