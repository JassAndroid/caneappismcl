package com.praful.jass.ismclcaneapp.Models;

public class RopTypeModel {

    String r_type_name,r_type_code;

    public String getR_type_name() {
        return r_type_name;
    }

    public void setR_type_name(String r_type_name) {
        this.r_type_name = r_type_name;
    }

    public String getR_type_code() {
        return r_type_code;
    }

    public void setR_type_code(String r_type_code) {
        this.r_type_code = r_type_code;
    }
}
