package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.content.Context;
import android.net.Uri;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.praful.jass.ismclcaneapp.Adapter.ListViewAdapter;
import com.praful.jass.ismclcaneapp.DataSet.DataSet;
import com.praful.jass.ismclcaneapp.Models.DailyHistoryModel;
import com.praful.jass.ismclcaneapp.R;

import java.util.ArrayList;
import java.util.List;

public class SlipBoyWorkListFragment extends Fragment {

    private List<DailyHistoryModel> listItem = new ArrayList<>();
    private RecyclerView recycler_view;
    private ListViewAdapter mAdapter;
    TextView slip_data_msg;
    View view;
    Context context;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_slip_boy_work_list, container, false);
        init(view);
        setAdapter();
        listItem = new DataSet().getTripBoyOptionsData(context);
        // Inflate the layout for this fragment
        return view;
    }

    private void setAdapter() {
        mAdapter = new ListViewAdapter(getActivity(),listItem);
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
        recycler_view.setLayoutManager(mLayoutManager);
        recycler_view.setItemAnimator(new DefaultItemAnimator());
        recycler_view.setAdapter(mAdapter);
    }

    private void init(View view) {
        context=getContext();
        slip_data_msg = view.findViewById(R.id.slip_data_msg);
        recycler_view = view.findViewById(R.id.recycler_view);
    }

    public interface OnFragmentInteractionListener {
        // TODO: Update argument type and name
        void onFragmentInteraction(Uri uri);
    }
}
