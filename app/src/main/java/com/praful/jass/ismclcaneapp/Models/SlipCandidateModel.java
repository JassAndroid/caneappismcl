package com.praful.jass.ismclcaneapp.Models;

public class SlipCandidateModel
{
    private String SLIP_BOY_NAME;

    private String SLIP_BOY_ID;

    public String getSLIP_BOY_NAME ()
    {
        return SLIP_BOY_NAME;
    }

    public void setSLIP_BOY_NAME (String SLIP_BOY_NAME)
    {
        this.SLIP_BOY_NAME = SLIP_BOY_NAME;
    }

    public String getSLIP_BOY_ID ()
    {
        return SLIP_BOY_ID;
    }

    public void setSLIP_BOY_ID (String SLIP_BOY_ID)
    {
        this.SLIP_BOY_ID = SLIP_BOY_ID;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [SLIP_BOY_NAME = "+SLIP_BOY_NAME+", SLIP_BOY_ID = "+SLIP_BOY_ID+"]";
    }
}
