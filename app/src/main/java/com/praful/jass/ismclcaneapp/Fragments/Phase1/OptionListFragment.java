package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.AdapterView;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.ListView;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Adapter.OptionListAdapter;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import java.util.ArrayList;


public class OptionListFragment extends Fragment {


    private static final String TAG = OptionListFragment.class.getSimpleName();
    private View optionView;
    private ListView listView;
    public OptionListInterface listner;
    TextView optionListTitle;
    OptionListAdapter listAdapter;
    String listTitle;
    EditText edtSearch;
    private Button btn_cancel;
    private ArrayList<String> arrOptions;
    private ArrayList<String> growarCode;
    private FragmentManager fragmentManager;
    LinearLayout main_background;
    OptionListFragment fragment;
    ArrayList<String> growarNameList = new ArrayList<>();
    ArrayList<String> plantationID = new ArrayList<>();
    ArrayList<String> docNo = new ArrayList<>();

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        try {
            if (optionView == null) {
                optionView = inflater.inflate(R.layout.fragment_option_list, container, false);
                init();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return optionView;
    }


    @Override
    public void onActivityCreated(@Nullable Bundle savedInstanceState) {
        try {
            super.onActivityCreated(savedInstanceState);
            init();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /*****************************************************************
     * Initializing
     ****************************************************************/

    private void init() {
        try {
            main_background = (LinearLayout) optionView.findViewById(R.id.main_background);
            listView = (ListView) optionView.findViewById(R.id.listview_common_option);
            edtSearch = (EditText) optionView.findViewById(R.id.inputSearch);
            btn_cancel = (Button) optionView.findViewById(R.id.btn_cancel);

            if (plantationID.size() > 0 && growarNameList.size() > 0)
                listAdapter = new OptionListAdapter(arrOptions, plantationID, getContext(), growarNameList, growarCode,docNo);
            else if (growarNameList.size() > 0)
                listAdapter = new OptionListAdapter(getContext(), arrOptions, growarCode, growarNameList);
            else
                listAdapter = new OptionListAdapter(getContext(), arrOptions, growarCode);
            listView.setAdapter(listAdapter);
            fragmentManager = getActivity().getSupportFragmentManager();
            main_background.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                }
            });
            edtSearch.addTextChangedListener(new TextWatcher() {
                @Override
                public void onTextChanged(CharSequence s, int start, int before, int count) {
                    listAdapter.getFilter().filter(s);
                }

                @Override
                public void beforeTextChanged(CharSequence s, int start, int count, int after) {
                }

                @Override
                public void afterTextChanged(Editable s) {
                }
            });
            listView.setOnItemClickListener(new AdapterView.OnItemClickListener() {
                @Override
                public void onItemClick(AdapterView<?> parent, View view, int position,
                                        long id) {
                    if (listner != null) {
                        AppConstants.selectItemIndex = arrOptions.indexOf(listAdapter.filteredData.get(position));
                        if (position < listAdapter.filteredData.size()) {
                            listner.onListItemSelected(listAdapter.filteredData.get(position));
                        } else {
                            listner.onListItemSelected(null);
                        }
                    }
                }
            });
            btn_cancel.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    removeCurrentFragment();
                }
            });
//            optionListTitle.setOnClickListener(new View.OnClickListener() {
//                @Override
//                public void onClick(View v) {
//                }
//            });
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void removeCurrentFragment() {
        try {
            getActivity().getSupportFragmentManager().beginTransaction().setCustomAnimations(R.anim.slide_up, R.anim.slide_down).remove(OptionListFragment.this).commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void updateList(ArrayList<String> list, String title, ArrayList<String> growarCode) {
        try {
            this.arrOptions = list;
            this.growarCode = growarCode;
//        optionListTitle = (TextView) optionView.findViewById(R.id.optionlistTitle);
//        optionListTitle.setText(title);

            if (list.size() > 0) {
                Object model = list.get(0);
//            if (model instanceof CountryModel) {
//                setListTitle("Select Your Country");
//            } else if (model instanceof StateModel) {
//                setListTitle("Select Your State");
//            } else if (model instanceof CityModel) {
//                setListTitle("Select Your City");
//            } else if (model instanceof MotherTongueModel) {
//                setListTitle("Select Your MotherTounge");
//            } else if (model instanceof DegreeModel) {
//                setListTitle("Select Your Education");
//            } else if (model instanceof OccupationModel) {
//                setListTitle("Select Your WorkArea");
//            } else if (model instanceof ReligionModel) {
//                setListTitle("Select Your Religion");
//            } else if (model instanceof CasteModel) {
//                setListTitle("Select Your Caste");
//            } else if (model instanceof SubcasteModel) {
//                setListTitle("Select Your Sub-Caste");
//            }
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void getFarmerNameList(ArrayList<String> growarNameList) {
        try {
            this.growarNameList = growarNameList;
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void getplantationIDList(ArrayList<String> plantationID, ArrayList<String> docNo) {
        try {
            this.plantationID = plantationID;
            this.docNo = docNo;
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public void updateList(ArrayList<String> list, String title) {
        try {
            this.arrOptions = list;
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void setListTitle(String title) {
        try {
            this.listTitle = title;
            if (optionListTitle != null) {
                optionListTitle.setText(title);
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    @Override
    public void onDestroy() {
        super.onDestroy();
    }


    public interface OptionListInterface {
        public void onListDismiss();

        public void onListItemSelected(Object model);
    }
}
