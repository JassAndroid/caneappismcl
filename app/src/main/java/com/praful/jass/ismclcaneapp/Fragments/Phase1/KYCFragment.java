package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.Manifest;
import android.app.AlertDialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.graphics.Typeface;
import android.net.Uri;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.provider.MediaStore;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.Models.CaneTypeListModel;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;

import org.json.JSONException;
import org.json.JSONObject;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;

import static android.app.Activity.RESULT_OK;

;


public class KYCFragment extends Fragment {

    EditText growerNameEditText, middleNameEditText, lastNameEditText, mobileNoEditText, kycAadharNotxt, kycBankNotxt, kycVotingNotxt, kycPanNotxt;
    TextView aadharCardTextView, bankPassbookTextView, votingCardTextView, panCardTextView, villageListTextView, talukaEditText, districtEditText, gutEditText, subgutEditText;
    Button submitButton;
    LinearLayout villageListRL;

    private Toolbar toolbar;
    private FragmentManager fragmentManager;
    OptionListFragment fragment;
    ArrayList<String> namLlist = new ArrayList();
    ArrayList<String> growarCode = new ArrayList();
    ArrayList<CaneTypeListModel> deatilsList;
    SQLiteHelper sqLiteHelper;
    TelephonyManager telephonyManager;
    private Uri file;
    String imgFileName = "";
    boolean successCapture = false;
    int myRequestCode = 0;
    int countDocs = 0;
    FragmentTransaction fragmentTransaction;
    SystemDateTime dateTime = new SystemDateTime();
    String firstName = "", middleName = "", lastName = "", villageName = "", taluka = "", districtName = "", gutName = "", subGutName = "", mobileNumber = "", kycAadharNo = "", kycBankNo = "", kycVotingNo = "", kycPanNo = "";
    String device = "";
    String aadharCardFileName = "", panCardFileName = "", bankPassbookFileName = "", votingCardFileName = "";

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_kyc, container, false);
        try {
            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.KYC_form));
            telephonyManager = (TelephonyManager) getActivity().getSystemService(getActivity().getBaseContext().TELEPHONY_SERVICE);
            fragmentManager = getActivity().getSupportFragmentManager();
            sqLiteHelper = new SQLiteHelper(getContext());
            init(view);
            setOnClickListener();
            //To avoid URI exposure problem in API 24+ devices. :)
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
        } catch (Exception e) {
            e.printStackTrace();
        }

        return view;
    }

    private void init(View view) {

        growerNameEditText = (EditText) view.findViewById(R.id.kyc_grower_name_edit_text);
        middleNameEditText = (EditText) view.findViewById(R.id.kyc_middle_name_edit_text);
        lastNameEditText = (EditText) view.findViewById(R.id.kyc_last_name_edit_text);
        mobileNoEditText = (EditText) view.findViewById(R.id.kyc_mobile_no_edit_text);
        districtEditText = (TextView) view.findViewById(R.id.kyc_district_edit_text);
        gutEditText = (TextView) view.findViewById(R.id.kyc_gut_edit_text);
        subgutEditText = (TextView) view.findViewById(R.id.kyc_sub_gut_edit_text);
        talukaEditText = (TextView) view.findViewById(R.id.kyc_taluka_edit_text);
        villageListRL = view.findViewById(R.id.ll_kyc_village_list);
        aadharCardTextView = (TextView) view.findViewById(R.id.kyc_aadhar_card_text_view);
        bankPassbookTextView = (TextView) view.findViewById(R.id.kyc_bank_passbook_text_view);
        votingCardTextView = (TextView) view.findViewById(R.id.kyc_voting_card_text_view);
        villageListTextView = (TextView) view.findViewById(R.id.kyc_village_list);
        panCardTextView = (TextView) view.findViewById(R.id.kyc_pan_card_text_view);
        submitButton = (Button) view.findViewById(R.id.kyc_btn_submit);
        kycAadharNotxt = view.findViewById(R.id.kyc_aadhar_no);
        kycBankNotxt = view.findViewById(R.id.kyc_passbook_no);
        kycVotingNotxt = view.findViewById(R.id.kyc_voting_no);
        kycPanNotxt = view.findViewById(R.id.kyc_pan_no);
        if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
            // TODO: Consider calling

            return;
        }
        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
            device = telephonyManager.getDeviceId(2);
        } else {
            device = telephonyManager.getDeviceId();//getDeviceId(1);
        }
    }

    public boolean saveData() {
        try {

            CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);

            firstName = growerNameEditText.getText().toString();
            middleName = middleNameEditText.getText().toString();
            lastName = lastNameEditText.getText().toString();
//            villageName = villageListTextView.getText().toString();
            districtName = districtEditText.getText().toString();
            taluka = talukaEditText.getText().toString();
            gutName = gutEditText.getText().toString();
            mobileNumber = mobileNoEditText.getText().toString();
            subGutName = subgutEditText.getText().toString();
            kycAadharNo = kycAadharNotxt.getText().toString();
            kycBankNo = kycBankNotxt.getText().toString();
            kycVotingNo = kycVotingNotxt.getText().toString();
            kycPanNo = kycPanNotxt.getText().toString();
            sqLiteHelper.insertKycForm(firstName, middleName, lastName, mobileNumber, villageName, districtName, gutName, subGutName, aadharCardFileName, bankPassbookFileName, votingCardFileName, panCardFileName, taluka, kycAadharNo, kycBankNo, kycVotingNo, kycPanNo, Preferences.getCurrentUserId() + "", dateTime.systemDateTime("dd-MMM-yyyy"), locationModel.getLatitude() + "", locationModel.getLongitude() + "", device);
        } catch (Exception e) {
            e.printStackTrace();
            return false;
        }
        return true;
    }

    private void setOnClickListener() {
        submitButton.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (validateData()) {
                    boolean b = saveData();
                    if (b) {
                        Preferences.setKycInfoStatus(false);
                        Preferences.setKycImgStatus(false);
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(getString(R.string.grower_added_kyc));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                FragmentManager fragmentManager = getFragmentManager();
                                fragmentTransaction = fragmentManager.beginTransaction();
                                MenuFragment menuFragment = new MenuFragment();
                                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                fragmentTransaction.replace(R.id.frame, menuFragment);
                                fragmentTransaction.commit();
                                toolbar.setTitle("Menu");
                            }
                        });
                        builder.show();
                    } else {
                        final AlertDialog.Builder builder = new AlertDialog.Builder(getActivity());
                        builder.setMessage(getString(R.string.grower_not_created));
                        builder.setCancelable(false);
                        builder.setPositiveButton(getString(R.string.ok_button), new DialogInterface.OnClickListener() {
                            @Override
                            public void onClick(DialogInterface dialogInterface, int i) {
                                dialogInterface.dismiss();
                            }
                        });
                        builder.show();
                    }
                }
//                else{
//                    Toast.makeText(getContext(), "Validation Fail", Toast.LENGTH_SHORT).show();
//                }

            }
        });

        villageListRL.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {

                new AsyncCaller().execute();

            }
        });

        aadharCardTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRequestCode = 1888;
                takePicture();
//                if(successCapture){
//                    aadharCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp),null,null);
//                }
//                successCapture =false;
            }
        });

        bankPassbookTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRequestCode = 1887;
                takePicture();
//                if(successCapture){
//                    bankPassbookTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp),null,null);
//                }
//                successCapture =false;
            }
        });

        votingCardTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRequestCode = 1886;
                takePicture();
//                if(successCapture){
//                    votingCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp),null,null);
//                }
//                successCapture =false;
            }
        });

        panCardTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                myRequestCode = 1885;
                takePicture();
//                if(successCapture){
//                    panCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp),null,null);
//                }
//                successCapture =false;
            }
        });
    }

    public void takePicture() {
        try {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            file = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
            startActivityForResult(intent, myRequestCode);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateData() {

        if (growerNameEditText.getText().toString().length() == 0 || growerNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_grower_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (middleNameEditText.getText().toString().length() == 0 || middleNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_middle_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (lastNameEditText.getText().toString().length() == 0 || lastNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (villageListTextView.getText().toString().length() == 0) {
            Toast.makeText(getContext(), getString(R.string.select_village_list), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (mobileNoEditText.getText().toString().length() == 0) {
            Toast.makeText(getContext(), getString(R.string.enter_mobile_number), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (kycAadharNotxt.getText().toString().length() == 0 || lastNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (kycBankNotxt.getText().toString().length() == 0 || lastNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (kycVotingNotxt.getText().toString().length() == 0 || lastNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (kycPanNotxt.getText().toString().length() == 0 || lastNameEditText.getText().toString().equals(" ")) {
            Toast.makeText(getContext(), getString(R.string.enter_last_name), Toast.LENGTH_SHORT).show();
            return false;
        }

        if (countDocs < 1) {
            Toast.makeText(getContext(), getString(R.string.choose_documents), Toast.LENGTH_SHORT).show();
            return false;
        }

        return true;

    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            try {
                super.onPreExecute();
//                //this method will be running on UI thread
//                pdLoading.setMessage("\tLoading...");
//                pdLoading.show();
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                String villageList = sqLiteHelper.getAllVillageListData();
                if (villageList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage(getString(R.string.village_not_found));
                else
                    showOptionList(getJsoneManager(villageList), villageListTextView);
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);
        }
    }

    public void showMessage(final String message) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void showOptionList(ArrayList<String> listArray, final TextView textView) {
        try {
            final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

            fragment = new OptionListFragment();
            fragment.updateList(listArray, textView.getText().toString(), namLlist);
            fragment.setListTitle("kljklkllk");

            fragment.listner = new OptionListFragment.OptionListInterface() {
                @Override
                public void onListDismiss() {
                    try {
                        removeOptionList();
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onListItemSelected(Object model) {

                    try {
                        // you can use this model to show selected value in the text
                        if (model instanceof String) {

                            String value = (String) model;
                            textView.setText(value);
                            villageName = deatilsList.get(AppConstants.selectItemIndex).getVILLAGE_ID();
                            districtEditText.setText(deatilsList.get(AppConstants.selectItemIndex).getDISTRICT());
                            gutEditText.setText(deatilsList.get(AppConstants.selectItemIndex).getGUT());
                            subgutEditText.setText(deatilsList.get(AppConstants.selectItemIndex).getSUB_GUT());
                            talukaEditText.setText(deatilsList.get(AppConstants.selectItemIndex).getTALUKA());


                            textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                            // txt_shivar_gut.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
                            textView.setTypeface(null, Typeface.BOLD);
                            gutEditText.setTypeface(null, Typeface.BOLD);
                            subgutEditText.setTypeface(null, Typeface.BOLD);
                            talukaEditText.setTypeface(null, Typeface.BOLD);
                            districtEditText.setTypeface(null, Typeface.BOLD);
                            // txt_shivar_gut.setTypeface(null, Typeface.BOLD);

                        }
                        AppConstants.selectItemIndex = 0;
                        removeOptionList();
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };
            ft.add(R.id.KYC_form, fragment);
            ft.commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private ArrayList<String> getJsoneManager(String jsone) {
        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), CaneTypeListModel.class, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    deatilsList = new ArrayList();

                    namLlist = new ArrayList();
                    growarCode = new ArrayList();
                    deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                    for (int i = 0; i < deatilsList.size(); i++) {
                        namLlist.add(i, deatilsList.get(i).getVILLAGE());
                    }
                }

                @Override
                public void onError(String strError) {
                    Toast.makeText(getContext(), strError, Toast.LENGTH_SHORT).show();
                }
            });
        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return namLlist;
    }

    void removeOptionList() {
        try {
            final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            ft.remove(fragment).commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CaneApp");
        try {
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CameraDemo", "failed to create directory");
                    return null;
                }
            } else {
                File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                try {
                    if (file.exists())
                        Log.d("Trp Camera File", "File Already Exists");
                    else
                        file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());

            if (myRequestCode == 1888) {

                imgFileName = "AACA_" + Preferences.getCurrentUserId() + "_recordId" + timeStamp + ".jpg";
                aadharCardFileName = imgFileName;

            } else if (myRequestCode == 1887) {

                imgFileName = "BAPA_" + Preferences.getCurrentUserId() + "_recordId" + timeStamp + ".jpg";
                bankPassbookFileName = imgFileName;

            } else if (myRequestCode == 1886) {

                imgFileName = "VOCA_" + Preferences.getCurrentUserId() + "_recordId" + timeStamp + ".jpg";
                votingCardFileName = imgFileName;

            } else if (myRequestCode == 1885) {

                imgFileName = "PACA_" + Preferences.getCurrentUserId() + "_recordId" + timeStamp + ".jpg";
                panCardFileName = imgFileName;

            }

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return new File(mediaStorageDir.getPath() + File.separator + imgFileName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == myRequestCode) {
                if (resultCode == RESULT_OK) {
//                Bitmap tempPhoto = (Bitmap) data.getExtras().get("data");
//                img_meeter_image.setImageBitmap(tempPhoto);
                    Bitmap photo = null;
                    try {
                        photo = MediaStore.Images.Media.getBitmap(
                                getActivity().getContentResolver(), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    // img_meeter_image.setImageBitmap(photo);
                    saveImage(getResizedBitmap(photo, 800));
                    countDocs++;
                    if (myRequestCode == 1888) {

                        aadharCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp), null, null);
                    } else if (myRequestCode == 1887) {

                        bankPassbookTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp), null, null);
                    } else if (myRequestCode == 1886) {

                        votingCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp), null, null);
                    } else if (myRequestCode == 1885) {

                        panCardTextView.setCompoundDrawablesWithIntrinsicBounds(null, getResources().getDrawable(R.drawable.ic_done_24dp), null, null);
                    }
                }
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = 0;
        int height = 0;
        try {
            width = image.getWidth();
            height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void saveImage(Bitmap finalBitmap) {
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CaneApp", "failed to create directory");
                }
            }
            File file1 = new File(mediaStorageDir.getPath() + File.separator + imgFileName);
            if (file1.exists()) file1.delete();
            try {
                FileOutputStream out = new FileOutputStream(file1);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
                out.flush();
                out.close();
                //img_meeter_image.setImageURI(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
