package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 01-08-2017.
 */

public class SeasonIdModel {
    private String SEASON_ID;

    public String getSEASON_ID() {
        return SEASON_ID;
    }

    public void setSEASON_ID(String SEASON_ID) {
        this.SEASON_ID = SEASON_ID;
    }
}
