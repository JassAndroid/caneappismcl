package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.EditText;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class PlantationCodeDialog extends Dialog {
    public EditText plantationCode;
    public Button btn_cancel, btn_submit;
    private Context context;

    public PlantationCodeDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_plantation_code);
            init();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            plantationCode = (EditText) findViewById(R.id.edtext_plantation_code);
            plantationCode.setText("");
            btn_cancel = (Button) findViewById(R.id.btn_cancel);
            btn_submit = (Button) findViewById(R.id.btn_submit);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
