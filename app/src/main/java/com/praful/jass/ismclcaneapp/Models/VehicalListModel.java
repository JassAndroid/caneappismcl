package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-09-2017.
 */

public class VehicalListModel {
    String VEHICAL_NO, VNO_OBJECT_ID, TRANSPORTER_ID, TR_NAME, CODE, VEHICAL_TYPE, V_NAME;

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getTR_NAME() {
        return TR_NAME;
    }

    public void setTR_NAME(String TR_NAME) {
        this.TR_NAME = TR_NAME;
    }

    public String getTRANSPORTER_ID() {
        return TRANSPORTER_ID;
    }

    public void setTRANSPORTER_ID(String TRANSPORTER_ID) {
        this.TRANSPORTER_ID = TRANSPORTER_ID;
    }

    public String getV_NAME() {
        return V_NAME;
    }

    public void setV_NAME(String v_NAME) {
        V_NAME = v_NAME;
    }

    public String getVEHICAL_NO() {
        return VEHICAL_NO;
    }

    public void setVEHICAL_NO(String VEHICAL_NO) {
        this.VEHICAL_NO = VEHICAL_NO;
    }

    public String getVEHICAL_TYPE() {
        return VEHICAL_TYPE;
    }

    public void setVEHICAL_TYPE(String VEHICAL_TYPE) {
        this.VEHICAL_TYPE = VEHICAL_TYPE;
    }

    public String getVNO_OBJECT_ID() {
        return VNO_OBJECT_ID;
    }

    public void setVNO_OBJECT_ID(String VNO_OBJECT_ID) {
        this.VNO_OBJECT_ID = VNO_OBJECT_ID;
    }
}
