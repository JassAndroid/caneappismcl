package com.praful.jass.ismclcaneapp.Models;

public class WorkTypeModel
{
    private String NAME;

    private String FLAG;

    public String getNAME ()
    {
        return NAME;
    }

    public void setNAME (String NAME)
    {
        this.NAME = NAME;
    }

    public String getFLAG ()
    {
        return FLAG;
    }

    public void setFLAG (String FLAG)
    {
        this.FLAG = FLAG;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [NAME = "+NAME+", FLAG = "+FLAG+"]";
    }
}
