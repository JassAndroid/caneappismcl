package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.content.Intent;
import android.net.Uri;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.ImageView;
import android.widget.TextView;

import com.praful.jass.ismclcaneapp.R;

public class HelpDeskActivity extends AppCompatActivity {

    TextView latakeTextView, jathavTextView;
    ImageView goBackImageView;

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_help_desk);
        init();
    }

    private void init() {
        latakeTextView = (TextView)findViewById(R.id.cl_latake_sir);
        jathavTextView = (TextView)findViewById(R.id.cl_jadhav_sir);

        goBackImageView = (ImageView) findViewById(R.id.help_desk_go_back);

        onClickMethods();
    }

    private void onClickMethods() {
        latakeTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:7774076285"));
                startActivity(callIntent);
            }
        });

        jathavTextView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Intent callIntent = new Intent(Intent.ACTION_DIAL);
                callIntent.setData(Uri.parse("tel:7709325599"));
                startActivity(callIntent);
            }
        });

        goBackImageView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                finish();
            }
        });


    }
}
