package com.praful.jass.ismclcaneapp.services;

import android.os.StrictMode;

import org.ksoap2.SoapEnvelope;
import org.ksoap2.serialization.PropertyInfo;
import org.ksoap2.serialization.SoapObject;
import org.ksoap2.serialization.SoapSerializationEnvelope;
import org.ksoap2.transport.HttpTransportSE;

import com.praful.jass.ismclcaneapp.Utils.AppConstants;

public class CallSoap {
    Object output;

    public CallSoap() {
    }

    public SoapObject callGetCount(String UserID) {
        final String SOAP_ACTION = "http://tempuri.org/GET_COUNT_DASHBOARD";
        final String OPERATION_NAME = "GET_COUNT_DASHBOARD";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("UserID");
        pi.setValue(UserID);
        pi.setType(Integer.class);
        Request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 1000);
        SoapObject Response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            Response = (SoapObject) envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
//            Response = e.getMessage();
        }
        return Response;
    }


    public Object CallLogin(String a, String b, String c) {
        final String SOAP_ACTION = "http://tempuri.org/LoginUser";
        final String OPERATION_NAME = "LoginUser";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("UserName");
        pi.setValue(a);
        pi.setType(Integer.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("Password");
        pi.setValue(b);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("DeviceID");
        pi.setValue(c);
        pi.setType(String.class);
        Request.addProperty(pi);

        System.setProperty("http.keepAlive", "false");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }


    public Object callGet_SUGAR_IRRIGATION_MASTER() {
        final String SOAP_ACTION = "http://tempuri.org/Get_SUGAR_IRRIGATION_MASTER";
        final String OPERATION_NAME = "Get_SUGAR_IRRIGATION_MASTER";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        System.setProperty("http.keepAlive", "false");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callGet_CANE_VERITY_LIST() {
        final String SOAP_ACTION = "http://tempuri.org/Get_CANE_VERITY_LIST";
        final String OPERATION_NAME = "Get_CANE_VERITY_LIST";

        System.setProperty("http.keepAlive", "false");

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callGet_CANE_TYPE_LIST() {
        final String SOAP_ACTION = "http://tempuri.org/Get_CANE_TYPE_LIST";
        final String OPERATION_NAME = "Get_CANE_TYPE_LIST";

        System.setProperty("http.keepAlive", "false");
        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callGet_PLANTATION_TYPE() {
        final String SOAP_ACTION = "http://tempuri.org/Get_PLANTATION_TYPE";
        final String OPERATION_NAME = "Get_PLANTATION_TYPE";

        System.setProperty("http.keepAlive", "false");
        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callGet_Village_List(String UserID) {
        final String SOAP_ACTION = "http://tempuri.org/Get_Village_List";
        final String OPERATION_NAME = "Get_Village_List";

        System.setProperty("http.keepAlive", "false");
        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("UserID");
        pi.setValue(UserID);
        pi.setType(Integer.class);
        Request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callGet_Farmer_List(String UserID) {
        final String SOAP_ACTION = "http://tempuri.org/Get_Farmer_List";
        final String OPERATION_NAME = "Get_Farmer_List";

        System.setProperty("http.keepAlive", "false");
        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("UserID");
        pi.setValue(UserID);
        pi.setType(Integer.class);
        Request.addProperty(pi);

        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 1200000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

//    public Object callRespAPIForList() {
//        final String SOAP_ACTION = "http://tempuri.org/REASON_MASTER";
//        final String OPERATION_NAME = "REASON_MASTER";
//        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
//        System.setProperty("http.keepAlive", "false");
//        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
//        envelope.dotNet = true;
//        envelope.setOutputSoapObject(Request);
//        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
//        StrictMode.setThreadPolicy(policy);
//        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
//        Object response = null;
//        try {
//            httpTransport.call(SOAP_ACTION, envelope);
//            response = envelope.getResponse();
//        } catch (Exception e) {
//            // TODO: handle exception
//            e.printStackTrace();
//            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
//        }
//        return response;
//    }


    public Object callGET_SEASON() {
        final String SOAP_ACTION = "http://tempuri.org/GET_SEASON";
        final String OPERATION_NAME = "GET_SEASON";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);

        System.setProperty("http.keepAlive", "false");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callPLANTATION_DATA_SUBMIT(String enter_date, int grower_id, int form_no, String form_date, int sarve_no, String sarve_date, int agrement_no, String agrement_date, String plantation_date, int varity_id, int cane_type, int irrigation, String plantation_area, int plantation_method, int total_area, int shivar_village_id, int shivar_gut_id, int shivar_sub_gut_id, long device, int user_id, String lat, String longi) {
        final String SOAP_ACTION = "http://tempuri.org/PLANTATION_DATA_SUBMIT";
        final String OPERATION_NAME = "PLANTATION_DATA_SUBMIT";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("enter_date");
        pi.setValue(enter_date);
        pi.setType(Integer.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("grower_id");
        pi.setValue(grower_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("form_no");
        pi.setValue(form_no);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("form_date");
        pi.setValue(form_date);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("sarve_no");
        pi.setValue(sarve_no);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("sarve_date");
        pi.setValue(sarve_date);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("agrement_no");
        pi.setValue(agrement_no);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("agrement_date");
        pi.setValue(agrement_date);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("plantation_date");
        pi.setValue(plantation_date);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("varity_id");
        pi.setValue(varity_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("cane_type");
        pi.setValue(cane_type);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("irrigation");
        pi.setValue(irrigation);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("plantation_area");
        pi.setValue(plantation_area);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("plantation_method");
        pi.setValue(plantation_method);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("total_area");
        pi.setValue(total_area);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("shivar_village_id");
        pi.setValue(shivar_village_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("shivar_gut_id");
        pi.setValue(shivar_gut_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("shivar_sub_gut_id");
        pi.setValue(shivar_sub_gut_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("device");
        pi.setValue(device);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("user_id");
        pi.setValue(user_id);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("lat");
        pi.setValue(lat);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("longi");
        pi.setValue(longi);
        pi.setType(String.class);
        Request.addProperty(pi);
        System.setProperty("http.keepAlive", "false");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }

    public Object callEMPLOYEE_TRIP_DATA(String device, String startTrip, String endTrip, String startPic, String endPic, String tripFlag) {
        final String SOAP_ACTION = "http://tempuri.org/EMPLOYEE_TRIP_DATA";
        final String OPERATION_NAME = "EMPLOYEE_TRIP_DATA";

        SoapObject Request = new SoapObject(AppConstants.WSDL_TARGET_NAMESPACE, OPERATION_NAME);
        PropertyInfo pi = new PropertyInfo();
        pi.setName("device");
        pi.setValue(device);
        pi.setType(Integer.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("startTrip");
        pi.setValue(startTrip);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("endTrip");
        pi.setValue(endTrip);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("startPic");
        pi.setValue(startPic);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("endPic");
        pi.setValue(endPic);
        pi.setType(String.class);
        Request.addProperty(pi);
        pi = new PropertyInfo();
        pi.setName("tripFlag");
        pi.setValue(tripFlag);
        pi.setType(String.class);
        Request.addProperty(pi);

        System.setProperty("http.keepAlive", "false");
        SoapSerializationEnvelope envelope = new SoapSerializationEnvelope(SoapEnvelope.VER11);
        envelope.dotNet = true;
        envelope.setOutputSoapObject(Request);
        StrictMode.ThreadPolicy policy = new StrictMode.ThreadPolicy.Builder().permitAll().build();
        StrictMode.setThreadPolicy(policy);
        HttpTransportSE httpTransport = new HttpTransportSE(AppConstants.SOAP_ADDRESS, 60000);
        Object response = null;
        try {
            httpTransport.call(SOAP_ACTION, envelope);
            response = envelope.getResponse();
        } catch (Exception e) {
            // TODO: handle exception
            e.printStackTrace();
            response = AppConstants.SERVER_NOT_RESPONDING_ERROR;
        }
        return response;
    }
}
