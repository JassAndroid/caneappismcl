package com.praful.jass.ismclcaneapp.Interface;

public interface Listener {

    void onDialogDisplayed();

    void onDialogDismissed();
}
