package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 27-08-2017.
 */

public class RujvatPlantatonDataMode {

    private String PLANTATION_CODE;

    private String FARMER_NAME;

    private String FARMER_CODE;

    private String VILLAGE_NAME;

    private String GUT_NAME;

    private String SUB_GUT_NAME;

    private String TALUKA_NAME;

    private String SHIVAR_VILLAGE_NAME;

    private String SHIVAR_GUT_NAME;

    private String SARVE_NO;

    private String IRRE_NAME;

    private String VARITY;

    private String PLANTATION_DATE;

    private String PLANTATION_TYPE;

    private String PLANTATION_AREA;

    private String OBJECT_ID;



    public String getSARVE_NO() {
        return SARVE_NO;
    }

    public void setSARVE_NO(String SARVE_NO) {
        this.SARVE_NO = SARVE_NO;
    }

    public String getPLANTATION_DATE() {
        return PLANTATION_DATE;
    }

    public void setPLANTATION_DATE(String PLANTATION_DATE) {
        this.PLANTATION_DATE = PLANTATION_DATE;
    }

    public String getPLANTATION_AREA() {
        return PLANTATION_AREA;
    }

    public void setPLANTATION_AREA(String PLANTATION_AREA) {
        this.PLANTATION_AREA = PLANTATION_AREA;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getVARITY ()
    {
        return VARITY;
    }

    public void setVARITY (String VARITY)
    {
        this.VARITY = VARITY;
    }

    public String getFARMER_CODE ()
    {
        return FARMER_CODE;
    }

    public void setFARMER_CODE (String FARMER_CODE)
    {
        this.FARMER_CODE = FARMER_CODE;
    }

    public String getPLANTATION_TYPE ()
    {
        return PLANTATION_TYPE;
    }

    public void setPLANTATION_TYPE (String PLANTATION_TYPE)
    {
        this.PLANTATION_TYPE = PLANTATION_TYPE;
    }

    public String getSHIVAR_VILLAGE_NAME ()
    {
        return SHIVAR_VILLAGE_NAME;
    }

    public void setSHIVAR_VILLAGE_NAME (String SHIVAR_VILLAGE_NAME)
    {
        this.SHIVAR_VILLAGE_NAME = SHIVAR_VILLAGE_NAME;
    }

    public String getTALUKA_NAME ()
    {
        return TALUKA_NAME;
    }

    public void setTALUKA_NAME (String TALUKA_NAME)
    {
        this.TALUKA_NAME = TALUKA_NAME;
    }

    public String getGUT_NAME ()
    {
        return GUT_NAME;
    }

    public void setGUT_NAME (String GUT_NAME)
    {
        this.GUT_NAME = GUT_NAME;
    }

    public String getVILLAGE_NAME ()
    {
        return VILLAGE_NAME;
    }

    public void setVILLAGE_NAME (String VILLAGE_NAME)
    {
        this.VILLAGE_NAME = VILLAGE_NAME;
    }

    public String getPLANTATION_CODE ()
    {
        return PLANTATION_CODE;
    }

    public void setPLANTATION_CODE (String PLANTATION_CODE)
    {
        this.PLANTATION_CODE = PLANTATION_CODE;
    }

    public String getIRRE_NAME ()
    {
        return IRRE_NAME;
    }

    public void setIRRE_NAME (String IRRE_NAME)
    {
        this.IRRE_NAME = IRRE_NAME;
    }

    public String getSHIVAR_GUT_NAME ()
    {
        return SHIVAR_GUT_NAME;
    }

    public void setSHIVAR_GUT_NAME (String SHIVAR_GUT_NAME)
    {
        this.SHIVAR_GUT_NAME = SHIVAR_GUT_NAME;
    }

    public String getFARMER_NAME ()
    {
        return FARMER_NAME;
    }

    public void setFARMER_NAME (String FARMER_NAME)
    {
        this.FARMER_NAME = FARMER_NAME;
    }

    public String getSUB_GUT_NAME ()
    {
        return SUB_GUT_NAME;
    }

    public void setSUB_GUT_NAME (String SUB_GUT_NAME)
    {
        this.SUB_GUT_NAME = SUB_GUT_NAME;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [VARITY = "+VARITY+", FARMER_CODE = "+FARMER_CODE+", PLANTATION_TYPE = "+PLANTATION_TYPE+", SHIVAR_VILLAGE_NAME = "+SHIVAR_VILLAGE_NAME+", TALUKA_NAME = "+TALUKA_NAME+", GUT_NAME = "+GUT_NAME+", VILLAGE_NAME = "+VILLAGE_NAME+", PLANTATION_CODE = "+PLANTATION_CODE+", IRRE_NAME = "+IRRE_NAME+", SHIVAR_GUT_NAME = "+SHIVAR_GUT_NAME+", FARMER_NAME = "+FARMER_NAME+", SUB_GUT_NAME = "+SUB_GUT_NAME+"]";
    }
}
