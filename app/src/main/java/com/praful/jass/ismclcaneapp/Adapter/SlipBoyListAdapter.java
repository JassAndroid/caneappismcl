package com.praful.jass.ismclcaneapp.Adapter;

import android.content.Context;
import android.support.annotation.NonNull;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.praful.jass.ismclcaneapp.Models.SlipCandidateModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import java.util.ArrayList;

/**
 * Created by Jass-Services on 7/20/2018.
 */

public class SlipBoyListAdapter extends RecyclerView.Adapter<SlipBoyListAdapter.MyViewHolder> {

    private Context context;
    FragmentTransaction fragmentTransaction;

    private SlipBoyWorkStatusListener slipBoyWorkStatusListener;
    ArrayList<SlipCandidateModel> mList;

    public interface SlipBoyWorkStatusListener {
        void itemClick(int id);
    }

    public SlipBoyListAdapter(Context context, SlipBoyWorkStatusListener slipBoyWorkStatusListener, ArrayList<SlipCandidateModel> mList) {
        this.context = context;
        this.slipBoyWorkStatusListener = slipBoyWorkStatusListener;
        this.mList = mList;
    }

    public void updateAdapter(ArrayList<SlipCandidateModel> mList) {
        this.mList = mList;
        notifyDataSetChanged();
    }

    @NonNull
    @Override
    public MyViewHolder onCreateViewHolder(@NonNull ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_slip_boy, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(@NonNull MyViewHolder holder, final int position) {
        SlipCandidateModel slipCandidateModel = mList.get(position);

        holder.id.setText(slipCandidateModel.getSLIP_BOY_ID());

        holder.name.setText(slipCandidateModel.getSLIP_BOY_NAME());

        holder.id.setText(String.valueOf(position + 1));
        holder.itemView.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                slipBoyWorkStatusListener.itemClick(position);
                AppConstants.SELECTED_USER_ID = mList.get(position).getSLIP_BOY_ID();
            }
        });


    }

    @Override
    public int getItemCount() {
        return mList.size();
    }

    public class MyViewHolder extends RecyclerView.ViewHolder {

        TextView id, name;

        public MyViewHolder(View itemView) {
            super(itemView);

            id = itemView.findViewById(R.id.slip_boy_work_status_item_emp_id);
            name = itemView.findViewById(R.id.slip_boy_work_status_item_emp_name);
        }
    }
}
