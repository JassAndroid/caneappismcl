package com.praful.jass.ismclcaneapp.Utils;

import android.content.Context;
import android.content.res.Configuration;

import java.util.Locale;

public class ChangeLocaleLanguage {
    private Locale myLocale;


    public void changeLocale(String lang, Context context) {
        if (lang.equalsIgnoreCase(""))
            return;
        myLocale = new Locale(lang);//Set Selected Locale
//        saveLocale(lang);//Save the selected locale
        Locale.setDefault(myLocale);//set new locale as default
        Configuration config = new Configuration();//get Configuration
        config.locale = myLocale;//set config locale as selected locale
        context.getResources().updateConfiguration(config, context.getResources().getDisplayMetrics());//Update the config
    }
}
