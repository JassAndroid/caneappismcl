package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.View;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class FieldSlipDetailsDialog extends Dialog {
    public Button btn_submit;
    private Context context;
    public TextView txtTransportCode;
    public TextView txtHarvestorCode;
    public TextView txtGrowarFactoryName;
    public TextView txtPlantationID;
    public TextView txtVehicalType;
    public TextView txtVehicalNumber;
    public TextView txttrly1;
    public TextView txttrly2;
    public TextView grower_name, txt_village, txt_gut, txt_subgut, txt_taluka;
    public TextView txt_shivarVillage, txt_shivarGut, txt_surveyNumber, txt_surveyDate, txt_irrigationSource, txt_caneVerity, txt_caneType, txt_plantationDate, txt_plantationType, txt_plantationArea, txt_rop_type, txt_driver_name, txt_cane_quality;


    public FieldSlipDetailsDialog(Context context) {
        super(context);
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_field_slip_details);
            init();
//            clickListener();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            btn_submit = findViewById(R.id.btn_ok_ok);
            grower_name = findViewById(R.id.grower_name);
            txt_village = findViewById(R.id.txt_village);
            txt_gut = findViewById(R.id.txt_gut);
            txt_subgut = findViewById(R.id.txt_subgut);
            txt_taluka = findViewById(R.id.txt_taluka);
            txtTransportCode = findViewById(R.id.txt_transport_code);
            txtHarvestorCode = findViewById(R.id.txt_harvestor_code);
            txtPlantationID = findViewById(R.id.txt_plantation_id);
            txtVehicalType = findViewById(R.id.txt_vehical_type);
            txtVehicalNumber = findViewById(R.id.txt_vehical_number);
            txt_shivarVillage = findViewById(R.id.txt_shivar_village);
            txt_shivarGut = findViewById(R.id.txt_shivar_gut);
            txt_surveyNumber = findViewById(R.id.txt_survey_number_22);
            txt_irrigationSource = findViewById(R.id.txt_irrigation_resource);
            txt_caneVerity = findViewById(R.id.txt_cane_verity);
            txt_plantationDate = findViewById(R.id.txt_plantation_date_tc);
            txt_plantationType = findViewById(R.id.txt_plantation_type_tc);
            txtGrowarFactoryName = findViewById(R.id.txt_growar_factory_name);
            txttrly1 = findViewById(R.id.txt_trly1);
            txttrly2 = findViewById(R.id.txt_trly2);
            txt_rop_type = findViewById(R.id.txt_rop_type);
            txt_driver_name = findViewById(R.id.txt_driver_name);
            txt_cane_quality = findViewById(R.id.txt_cane_quality);

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void clickListener() {
        btn_submit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dismiss();
            }
        });
    }
}
