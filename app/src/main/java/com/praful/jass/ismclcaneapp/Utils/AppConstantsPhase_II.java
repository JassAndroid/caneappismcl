package com.praful.jass.ismclcaneapp.Utils;

public class AppConstantsPhase_II {

    public static final String URL_FARMER_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "P-II_getFarmerListForTodeChitti.php";
    //    public static final String URL_ADMIN_PLANTATION_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "admin_P-II_getPlanatationDataForTodeChitti.php";
    public static final String URL_PLANTATION_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "P-II_getPlanatationDataForTodeChitti.php";
    public static final String URL_VEHICAL_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "P-II_getTransporterListForTodeChitti.php";
    public static final String URL_HARVESTOR_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "P-II_getHarvestorListForTodeChitti.php";
    public static final String URL_CURRENT_SLIP_ID_LIST_TC = AppConstants.BASE_URL_PHP_SERVICE + "P-II_GetCurrentSlipID.php";
    public static final String URL_SUBMIT_SLIP_DATA_TO_SERVER = AppConstants.BASE_URL_PHP_SERVICE + "P-II_SubmitSlipDataToServer.php";

    public static final int FARMER_LIST_TODA_CHITTI_SELECT = 5230;
    public static final int PLANTATION_LIST_TODA_CHITTI_SELECT = 5231;
    public static final int VEHICAL_LIST_TODA_CHITTI_SELECT = 5232;
    public static final int HARVESTOR_LIST_TODA_CHITTI_SELECT = 5233;
    public static final int TRAILOR_I_LIST_TODA_CHITTI_SELECT = 5234;
    public static final int TRAILOR_II_LIST_TODA_CHITTI_SELECT = 5235;
    public static final int CURRENT_SLIP_ID_SELECT = 5236;
    public static final int GROWER_FACTORY_SELECT = 5237;
    public static final int ROP_TYPE_SELECT = 5238;
    public static final int CANE_QUALITY_SELECT = 5239;


}
