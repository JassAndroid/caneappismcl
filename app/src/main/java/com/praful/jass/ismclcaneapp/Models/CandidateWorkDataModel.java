package com.praful.jass.ismclcaneapp.Models;

public class CandidateWorkDataModel {
    private String FARMER_VILLAGE;

    private String VARITY;

    private String FARMER_CODE;

    private String FARMER_GUT;

    private String PLANTATION_DATE;

    private String SHIVAR_VILLAGE_NAME;

    private String PLANTATION_TYPE;

    private String PLANTATION_CODE;

    private String HRV_NAME;

    private String CUTTING_OBJECT_ID;

    private String OBJECT_ID;

    private String FARMER_TALUKA;

    private String HRV_CODE;

    private String FARMER_NAME;

    private String IRRE_NAME;

    private String SHIVAR_GUT_NAME;

    private String SARVE_NO;

    private String PLOT_START_DATE;

    private String FARMER_SUBGUT;

    private String FARMER_ID;

    private String CODE;
    private String NAME;
    private String LOCAL_NAME;
    private String VG_NAME;
    private String GUT_NAME;
    private String PLANTATION_ID;
    private String PLANTATION_AREA;
    private String SHIVAR_VG_NAME;
    private String PLANT_REG_TYPE;

    public String getFARMER_VILLAGE() {
        return FARMER_VILLAGE;
    }

    public void setFARMER_VILLAGE(String FARMER_VILLAGE) {
        this.FARMER_VILLAGE = FARMER_VILLAGE;
    }

    public String getVARITY() {
        return VARITY;
    }

    public void setVARITY(String VARITY) {
        this.VARITY = VARITY;
    }

    public String getFARMER_CODE() {
        return FARMER_CODE;
    }

    public void setFARMER_CODE(String FARMER_CODE) {
        this.FARMER_CODE = FARMER_CODE;
    }

    public String getFARMER_GUT() {
        return FARMER_GUT;
    }

    public void setFARMER_GUT(String FARMER_GUT) {
        this.FARMER_GUT = FARMER_GUT;
    }

    public String getPLANTATION_DATE() {
        return PLANTATION_DATE;
    }

    public void setPLANTATION_DATE(String PLANTATION_DATE) {
        this.PLANTATION_DATE = PLANTATION_DATE;
    }

    public String getSHIVAR_VILLAGE_NAME() {
        return SHIVAR_VILLAGE_NAME;
    }

    public void setSHIVAR_VILLAGE_NAME(String SHIVAR_VILLAGE_NAME) {
        this.SHIVAR_VILLAGE_NAME = SHIVAR_VILLAGE_NAME;
    }

    public String getPLANTATION_TYPE() {
        return PLANTATION_TYPE;
    }

    public void setPLANTATION_TYPE(String PLANTATION_TYPE) {
        this.PLANTATION_TYPE = PLANTATION_TYPE;
    }

    public String getPLANTATION_CODE() {
        return PLANTATION_CODE;
    }

    public void setPLANTATION_CODE(String PLANTATION_CODE) {
        this.PLANTATION_CODE = PLANTATION_CODE;
    }

    public String getHRV_NAME() {
        return HRV_NAME;
    }

    public void setHRV_NAME(String HRV_NAME) {
        this.HRV_NAME = HRV_NAME;
    }

    public String getCUTTING_OBJECT_ID() {
        return CUTTING_OBJECT_ID;
    }

    public void setCUTTING_OBJECT_ID(String CUTTING_OBJECT_ID) {
        this.CUTTING_OBJECT_ID = CUTTING_OBJECT_ID;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getFARMER_TALUKA() {
        return FARMER_TALUKA;
    }

    public void setFARMER_TALUKA(String FARMER_TALUKA) {
        this.FARMER_TALUKA = FARMER_TALUKA;
    }

    public String getHRV_CODE() {
        return HRV_CODE;
    }

    public void setHRV_CODE(String HRV_CODE) {
        this.HRV_CODE = HRV_CODE;
    }

    public String getFARMER_NAME() {
        return FARMER_NAME;
    }

    public void setFARMER_NAME(String FARMER_NAME) {
        this.FARMER_NAME = FARMER_NAME;
    }

    public String getIRRE_NAME() {
        return IRRE_NAME;
    }

    public void setIRRE_NAME(String IRRE_NAME) {
        this.IRRE_NAME = IRRE_NAME;
    }

    public String getSHIVAR_GUT_NAME() {
        return SHIVAR_GUT_NAME;
    }

    public void setSHIVAR_GUT_NAME(String SHIVAR_GUT_NAME) {
        this.SHIVAR_GUT_NAME = SHIVAR_GUT_NAME;
    }

    public String getSARVE_NO() {
        return SARVE_NO;
    }

    public void setSARVE_NO(String SARVE_NO) {
        this.SARVE_NO = SARVE_NO;
    }

    public String getPLOT_START_DATE() {
        return PLOT_START_DATE;
    }

    public void setPLOT_START_DATE(String PLOT_START_DATE) {
        this.PLOT_START_DATE = PLOT_START_DATE;
    }

    public String getFARMER_SUBGUT() {
        return FARMER_SUBGUT;
    }

    public void setFARMER_SUBGUT(String FARMER_SUBGUT) {
        this.FARMER_SUBGUT = FARMER_SUBGUT;
    }

    public String getFARMER_ID() {
        return FARMER_ID;
    }

    public void setFARMER_ID(String FARMER_ID) {
        this.FARMER_ID = FARMER_ID;
    }

    public String getCODE() {
        return CODE;
    }

    public void setCODE(String CODE) {
        this.CODE = CODE;
    }

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getLOCAL_NAME() {
        return LOCAL_NAME;
    }

    public void setLOCAL_NAME(String LOCAL_NAME) {
        this.LOCAL_NAME = LOCAL_NAME;
    }

    public String getVG_NAME() {
        return VG_NAME;
    }

    public void setVG_NAME(String VG_NAME) {
        this.VG_NAME = VG_NAME;
    }

    public String getGUT_NAME() {
        return GUT_NAME;
    }

    public void setGUT_NAME(String GUT_NAME) {
        this.GUT_NAME = GUT_NAME;
    }

    public String getPLANTATION_ID() {
        return PLANTATION_ID;
    }

    public void setPLANTATION_ID(String PLANTATION_ID) {
        this.PLANTATION_ID = PLANTATION_ID;
    }

    public String getPLANTATION_AREA() {
        return PLANTATION_AREA;
    }

    public void setPLANTATION_AREA(String PLANTATION_AREA) {
        this.PLANTATION_AREA = PLANTATION_AREA;
    }

    public String getSHIVAR_VG_NAME() {
        return SHIVAR_VG_NAME;
    }

    public void setSHIVAR_VG_NAME(String SHIVAR_VG_NAME) {
        this.SHIVAR_VG_NAME = SHIVAR_VG_NAME;
    }

    public String getPLANT_REG_TYPE() {
        return PLANT_REG_TYPE;
    }

    public void setPLANT_REG_TYPE(String PLANT_REG_TYPE) {
        this.PLANT_REG_TYPE = PLANT_REG_TYPE;
    }


    @Override
    public String toString() {
        return "ClassPojo [FARMER_VILLAGE = " + FARMER_VILLAGE + ", VARITY = " + VARITY + ", FARMER_CODE = " + FARMER_CODE + ", FARMER_GUT = " + FARMER_GUT + ", PLANTATION_DATE = " + PLANTATION_DATE + ", SHIVAR_VILLAGE_NAME = " + SHIVAR_VILLAGE_NAME + ", PLANTATION_TYPE = " + PLANTATION_TYPE + ", PLANTATION_CODE = " + PLANTATION_CODE + ", HRV_NAME = " + HRV_NAME + ", CUTTING_OBJECT_ID = " + CUTTING_OBJECT_ID + ", OBJECT_ID = " + OBJECT_ID + ", FARMER_TALUKA = " + FARMER_TALUKA + ", HRV_CODE = " + HRV_CODE + ", FARMER_NAME = " + FARMER_NAME + ", IRRE_NAME = " + IRRE_NAME + ", SHIVAR_GUT_NAME = " + SHIVAR_GUT_NAME + ", SARVE_NO = " + SARVE_NO + ", PLOT_START_DATE = " + PLOT_START_DATE + ", FARMER_SUBGUT = " + FARMER_SUBGUT + ", FARMER_ID = " + FARMER_ID + "]";
    }
}

