package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 04-09-2017.
 */

public class PlantationKeyAndIDModel {
    String key, id;

    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public String getKey() {
        return key;
    }

    public void setKey(String key) {
        this.key = key;
    }
}
