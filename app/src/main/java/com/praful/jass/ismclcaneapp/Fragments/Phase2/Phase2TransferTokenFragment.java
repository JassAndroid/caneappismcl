package com.praful.jass.ismclcaneapp.Fragments.Phase2;

import android.Manifest;
import android.content.pm.PackageManager;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Adapter.TodChittiListViewAdapter;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Dialog.AlertDeleteDialog;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.MenuFragment;
import com.praful.jass.ismclcaneapp.Models.PlantationDataOfTCModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import java.util.ArrayList;

public class Phase2TransferTokenFragment extends android.support.v4.app.Fragment {
    private Toolbar toolbar;
    Button writeToken, btnDelete;
    RecyclerView todChittiList;
    private TodChittiListViewAdapter mAdapter;
    SQLiteHelper sqlLiteHelperPhase_ii;
    private TelephonyManager telephonyManager;
    private String deviceID = null;
    FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_phase2_transfer_token, container, false);
        // Inflate the layout for this fragment
        Preferences.appContext = getContext();
        init(view);
        loadData();
        return view;
    }


    @Override
    public void onResume() {
        super.onResume();


    }

    public void loadData() {
        TodChittiListViewAdapter.selectedTodChitti.clear();
        ArrayList<PlantationDataOfTCModel> todChittiList = sqlLiteHelperPhase_ii.getTodChittiList();
        mAdapter = new TodChittiListViewAdapter(todChittiList, getContext());
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Transfer Token");
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getContext());
        this.todChittiList.setLayoutManager(mLayoutManager);
        this.todChittiList.setItemAnimator(new DefaultItemAnimator());
        this.todChittiList.setAdapter(mAdapter);
    }

    public void init(View view) {
        writeToken = (Button) view.findViewById(R.id.btn_Write_token_next);
        btnDelete = (Button) view.findViewById(R.id.btn_delete);
        todChittiList = (RecyclerView) view.findViewById(R.id.todChittiList);
        telephonyManager = (TelephonyManager) getContext().getSystemService(getContext().TELEPHONY_SERVICE);
        sqlLiteHelperPhase_ii = new SQLiteHelper(getContext());
        setOnClickListner();

        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                // here to request the missing permissions, and then overriding
                //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                //                                          int[] grantResults)
                // to handle the case where the user grants the permission. See the documentation
                // for ActivityCompat#requestPermissions for more details.
                return;
            }
            deviceID = telephonyManager.getDeviceId(2);
        } else {
            deviceID = telephonyManager.getDeviceId();//getDeviceId(1);
        }

    }

    private void setOnClickListner() {
        writeToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (TodChittiListViewAdapter.selectedTodChitti.size() > 0) {

                    mNfcWriteFragment = (NFCWriteFragment) getActivity().getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);

                    if (mNfcWriteFragment == null) {

                        mNfcWriteFragment = NFCWriteFragment.newInstance();
                    }
                    mNfcWriteFragment.show(getActivity().getFragmentManager(), NFCWriteFragment.TAG);
                } else {
                    Toast.makeText(getActivity().getBaseContext(), "Please Select plantation ", Toast.LENGTH_SHORT).show();
                }
            }
        });

        btnDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (TodChittiListViewAdapter.selectedTodChitti.size() > 0) {
                    callDeleteAlertDialog("You really want to delete " + TodChittiListViewAdapter.selectedTodChitti.size() + " Field Slip");
                } else {
                    Toast.makeText(getActivity().getBaseContext(), "Please Select plantation ", Toast.LENGTH_SHORT).show();
                }
            }
        });
    }

    private void callDeleteAlertDialog(String message) {
        try {
            final AlertDeleteDialog alertDeleteDialog = new AlertDeleteDialog(getContext());
            alertDeleteDialog.show();
            alertDeleteDialog.setCancelable(false);
            alertDeleteDialog.message_txt.setText(message);
            alertDeleteDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    for (int i = 0; i < TodChittiListViewAdapter.selectedTodChitti.size(); i++) {
                        sqlLiteHelperPhase_ii.updateTodChittiData(TodChittiListViewAdapter.selectedTodChitti.get(i).getTOD_CHITTI_ID(), "Y");
                    }
                    Toast.makeText(getActivity().getBaseContext(), "Field slip Successfully Deleted", Toast.LENGTH_SHORT).show();
                    loadData();
                    alertDeleteDialog.dismiss();
                }
            });
            alertDeleteDialog.btn_no.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    alertDeleteDialog.dismiss();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void writeData(Ndef ndef) {
        String messageToWrite = "";
        if (TodChittiListViewAdapter.selectedTodChitti.size() > 0) {
            for (int i = 0; i < TodChittiListViewAdapter.selectedTodChitti.size(); i++) {
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getDATE_TIME_TC() + ",";
                messageToWrite = messageToWrite + deviceID + ",";
                messageToWrite = messageToWrite + Preferences.getCurrentUserId() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getFARMER_ID() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getOBJECT_ID() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getTRANSPORTER_ID() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getHARVEStER_CODE_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getVNO_OBJECT_ID() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getTRAILER_1() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getTRAILER_2() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getTOD_CHITTI_ID() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getFACTORY_NAME_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getLAT_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getLONG_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getDRIVER_NAME_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getROP_TYPE_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getCANE_QUALITY_TC() + ",";
                messageToWrite = messageToWrite + TodChittiListViewAdapter.selectedTodChitti.get(i).getCUTTING_OBJECT_ID_TC();
                messageToWrite = messageToWrite + "#";
            }
//            messageToWrite = "Ok Baby";//new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "," + farmerCode + "," + txtPlantationID.getText().toString() + "," + txtVehicalNumber.getText().toString() + "," + harvesterCode + "," + txttrly1.getText().toString() + "," + txttrly2.getText().toString();
            mNfcWriteFragment = (NFCWriteFragment) getActivity().getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
            boolean returnVAl = mNfcWriteFragment.onNfcDetected(ndef, messageToWrite);
            if (returnVAl) {
                for (int i = 0; i < TodChittiListViewAdapter.selectedTodChitti.size(); i++) {
                    sqlLiteHelperPhase_ii.updateTodChittiData(TodChittiListViewAdapter.selectedTodChitti.get(i).getTOD_CHITTI_ID(), "Y");
                }
            }
            TodChittiListViewAdapter.selectedTodChitti.clear();
            FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            MenuFragment menuFragment = new MenuFragment();
            fragmentTransaction.replace(R.id.frame, menuFragment);
            fragmentTransaction.commit();
        } else {
            Toast.makeText(getActivity().getBaseContext(), "Please Select plantation ", Toast.LENGTH_SHORT).show();
        }
    }

    private NFCWriteFragment mNfcWriteFragment;
}
