package com.praful.jass.ismclcaneapp.Fragments.Phase2;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.nfc.tech.Ndef;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.DataSet.DataSet;
import com.praful.jass.ismclcaneapp.Dialog.MessageDidplayDialog;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.MenuFragment;
import com.praful.jass.ismclcaneapp.Fragments.Phase1.OptionListFragment;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.Models.CaneQualityModel;
import com.praful.jass.ismclcaneapp.Models.CaneTypeListModel;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.Models.FactoryNameListModel;
import com.praful.jass.ismclcaneapp.Models.HarvestorListModel;
import com.praful.jass.ismclcaneapp.Models.PlantationDataForRujvatModel;
import com.praful.jass.ismclcaneapp.Models.RopTypeModel;
import com.praful.jass.ismclcaneapp.Models.VehicalListModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.AppConstantsPhase_II;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Date;


public class Phase2GenerateTokenFragment extends android.support.v4.app.Fragment {
    private Toolbar toolbar;
    Button btnNext;
    View view;
    private String farmerCode = "", harvesterCode = "", OBJECT_ID = "", FARMER_ID = "", VNO_OBJECT_ID = "", TRANSPORTER_ID = "", VEHICAL_TYPE = "", FACTORY_CODE = "", ROP_TYPE = "", caneQuality = "";
    private OptionListFragment fragment;
    private FragmentManager fragmentManager;
    public ProgressDialog pdLoading;
    //    LinearLayout llTransporterCode;
    LinearLayout llHarvestorCode;
    LinearLayout llPlantationID;
    //    LinearLayout llVehicalType;
    LinearLayout llVehicalNumber;
    LinearLayout ll_trly1;
    LinearLayout ll_trly2;
    LinearLayout llGrowarFactoryName;
    LinearLayout llRopType;
    LinearLayout llCaneQuality;
    //    TextView txtFarmerCode;
    TextView txtTransportCode;
    TextView txtHarvestorCode;
    TextView txtGrowarFactoryName;
    TextView txtPlantationID;
    TextView txtVehicalType;
    TextView txtVehicalNumber;
    TextView txttrly1;
    TextView txttrly2;
    TextView grower_name, txt_village, txt_gut, txt_subgut, txt_taluka;
    TextView txt_shivarVillage, txt_shivarGut, txt_surveyNumber, txt_surveyDate, txt_irrigationSource, txt_caneVerity, txt_caneType, txt_plantationDate, txt_plantationType, txt_plantationArea, txt_rop_type, txt_driver_name, txt_cane_quality;
    int selectListType = 0;
    SQLiteHelper sqLiteHelper;
    //    SQLLiteHelperPhase_II sqlLiteHelperPhase_ii;
    ArrayList<String> namLlist = new ArrayList();
    ArrayList<String> growarCode = new ArrayList();
    ArrayList<String> growarNameList = new ArrayList();
    ArrayList<String> plantationListList = new ArrayList();
    ArrayList<String> docNoList = new ArrayList();
    //    ArrayList deatilsList;
    ArrayList<RopTypeModel> ropList;
    ArrayList<HarvestorListModel> harvesterList;
    ArrayList<FactoryNameListModel> growerFactoryList;
    ArrayList<CaneQualityModel> caneQualityList;
    ArrayList<VehicalListModel> vehicalList;
    ArrayList<PlantationDataForRujvatModel> plantationList;
    ArrayList<CaneTypeListModel> farmerList = new ArrayList<CaneTypeListModel>();
    private String deviceID = null;
    private TelephonyManager telephonyManager;
    FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        view = inflater.inflate(R.layout.fragment_phase2_generate_token, container, false);
        // Inflate the layout for this fragment
        Preferences.appContext = getContext();
        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle("Generate Token");
        init();
        return view;

    }

    private void init() {
        fragmentManager = getActivity().getSupportFragmentManager();
        pdLoading = new ProgressDialog(getActivity());
        pdLoading.setCancelable(false);
        btnNext = (Button) view.findViewById(R.id.btn_generate_token_next);
        grower_name = (TextView) view.findViewById(R.id.grower_name);
        txt_village = (TextView) view.findViewById(R.id.txt_village);
        txt_gut = (TextView) view.findViewById(R.id.txt_gut);
        txt_subgut = (TextView) view.findViewById(R.id.txt_subgut);
        txt_taluka = (TextView) view.findViewById(R.id.txt_taluka);
//        llTransporterCode = (LinearLayout) view.findViewById(R.id.ll_transporter_code);
        llHarvestorCode = (LinearLayout) view.findViewById(R.id.ll_harvestor_code);
        llPlantationID = (LinearLayout) view.findViewById(R.id.ll_plantation_id);
//        llVehicalType = (LinearLayout) view.findViewById(R.id.ll_vehical_type);
        llVehicalNumber = (LinearLayout) view.findViewById(R.id.ll_vehical_number);
//        layout_grower_name = (LinearLayout) view.findViewById(R.id.layout_grower_name);
        ll_trly1 = (LinearLayout) view.findViewById(R.id.ll_trly1);
        ll_trly2 = (LinearLayout) view.findViewById(R.id.ll_trly2);
        llGrowarFactoryName = (LinearLayout) view.findViewById(R.id.ll_growar_factory_name);
        llRopType = (LinearLayout) view.findViewById(R.id.ll_rop_type);
        llCaneQuality = view.findViewById(R.id.ll_cane_quality);

//        txtFarmerCode = (TextView) view.findViewById(R.id.txt_farmer_code);
        txtTransportCode = (TextView) view.findViewById(R.id.txt_transport_code);
        txtHarvestorCode = (TextView) view.findViewById(R.id.txt_harvestor_code);
        txtPlantationID = (TextView) view.findViewById(R.id.txt_plantation_id);
        txtVehicalType = (TextView) view.findViewById(R.id.txt_vehical_type);
        txtVehicalNumber = (TextView) view.findViewById(R.id.txt_vehical_number);
        txt_shivarVillage = (TextView) view.findViewById(R.id.txt_shivar_village);
        txt_shivarGut = (TextView) view.findViewById(R.id.txt_shivar_gut);
        txt_surveyNumber = (TextView) view.findViewById(R.id.txt_survey_number_22);
//        txt_surveyDate = (TextView) view.findViewById(R.id.txt_surveyDate);
        txt_irrigationSource = (TextView) view.findViewById(R.id.txt_irrigation_resource);
        txt_caneVerity = (TextView) view.findViewById(R.id.txt_cane_verity);
//        txt_caneType = (TextView) view.findViewById(R.id.txt_cane_type);
        txt_plantationDate = (TextView) view.findViewById(R.id.txt_plantation_date_tc);
        txt_plantationType = (TextView) view.findViewById(R.id.txt_plantation_type_tc);
        txtGrowarFactoryName = (TextView) view.findViewById(R.id.txt_growar_factory_name);
//        txt_plantationArea = (TextView) view.findViewById(R.id.txt_plantationArea);
        txttrly1 = (TextView) view.findViewById(R.id.txt_trly1);
        txttrly2 = (TextView) view.findViewById(R.id.txt_trly2);
        txt_rop_type = (TextView) view.findViewById(R.id.txt_rop_type);
        txt_driver_name = (TextView) view.findViewById(R.id.txt_driver_name);
        txt_cane_quality = view.findViewById(R.id.txt_cane_quality);
        telephonyManager = (TelephonyManager) getActivity().getBaseContext().getSystemService(getContext().TELEPHONY_SERVICE);


        if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                return;
            }
            deviceID = telephonyManager.getDeviceId(2);
        } else {
            deviceID = telephonyManager.getDeviceId();//getDeviceId(1);
        }

        sqLiteHelper = new SQLiteHelper(getContext());
        setOnClickListner();
    }

    //1800266/3030/1515
    private void setOnClickListner() {
        btnNext.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                try {
                    if (validateData()) {
//                    startActivity(new Intent(getContext(), VehicalImagesActivity.class));
//                    mNfcWriteFragment = (NFCWriteFragment) getActivity().getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
//
//                    if (mNfcWriteFragment == null) {
//
//                        mNfcWriteFragment = NFCWriteFragment.newInstance();
//                    }
//                    mNfcWriteFragment.show(getActivity().getFragmentManager(), NFCWriteFragment.TAG);

//                        String currentSlipNumber = Preferences.getCurrentSlipNumber();
//                        if (currentSlipNumber.equals("0")) {
//                            currentSlipNumber = deviceID + "1";
//                        } else {
//                            currentSlipNumber = currentSlipNumber.substring(0, 15) + (Integer.parseInt(currentSlipNumber.substring(15)) + 1);
//                        }
//                        Preferences.setCurrentSlipNumber(currentSlipNumber);
                        CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
                        String slipNumber = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date());
                        String datetime = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss").format(new Date());
                        boolean result = sqLiteHelper
                                .insertTodChittiData(slipNumber, txtPlantationID.getText().toString(), farmerCode, grower_name.getText().toString(), txt_village.getText().toString(), txt_gut.getText().toString(), txt_subgut.getText().toString(), txt_taluka.getText().toString(), txt_shivarVillage.getText().toString(), txt_shivarGut.getText().toString(), txt_surveyNumber.getText().toString(), txt_irrigationSource.getText().toString(), txt_caneVerity.getText().toString(), txt_plantationDate.getText().toString(), txt_plantationType.getText().toString(), txtVehicalNumber.getText().toString(), txtVehicalType.getText().toString(), txtTransportCode.getText().toString(), harvesterCode, txttrly1.getText().toString(), txttrly2.getText().toString(), OBJECT_ID, FARMER_ID, VNO_OBJECT_ID, TRANSPORTER_ID, VEHICAL_TYPE, datetime, txtGrowarFactoryName.getText().toString().trim(), locationModel.getLatitude() + "", locationModel.getLongitude() + "", "", "", txtHarvestorCode.getText().toString(), "", "", caneQuality, txt_driver_name.getText().toString(), ROP_TYPE);
                        if (result) {
                            callMeaasgeDialog("Token Slip No:- " + txtPlantationID.getText().toString());
                        }
                    }
                } catch (Exception ex) {
                    Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
        });

//        layout_grower_name.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectListType = AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT;
//                new AsyncCaller().execute();
//            }
//        });

//        llHarvestorCode.setOnClickListener(new View.OnClickListener() {
//            @Override
//            public void onClick(View view) {
//                selectListType = AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT;
//                new AsyncCaller().execute();
//            }
//        });

        llPlantationID.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectListType = AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT;
                new AsyncCaller().execute();
            }
        });

        llVehicalNumber.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                selectListType = AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT;
                new AsyncCaller().execute();
            }
        });
        ll_trly1.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ArrayList<String> list = new DataSet().gettroly();
//                showOptionList(list, txttrly1);
                selectListType = AppConstantsPhase_II.TRAILOR_I_LIST_TODA_CHITTI_SELECT;
                new AsyncCaller().execute();
            }
        });
        ll_trly2.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
//                ArrayList<String> list = new DataSet().gettroly();
//                showOptionList(list, txttrly2);
                selectListType = AppConstantsPhase_II.TRAILOR_II_LIST_TODA_CHITTI_SELECT;
                new AsyncCaller().execute();
            }
        });

        llGrowarFactoryName.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectListType = AppConstantsPhase_II.GROWER_FACTORY_SELECT;
                new AsyncCaller().execute();
            }
        });

        llRopType.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectListType = AppConstantsPhase_II.ROP_TYPE_SELECT;
                new AsyncCaller().execute();
            }
        });

        llCaneQuality.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                selectListType = AppConstantsPhase_II.CANE_QUALITY_SELECT;
                new AsyncCaller().execute();
            }
        });
    }

    private void callMeaasgeDialog(String message) {
        final MessageDidplayDialog messageDidplayDialog = new MessageDidplayDialog(getContext());
        messageDidplayDialog.show();
        messageDidplayDialog.setCancelable(false);
        messageDidplayDialog.message_txt.setText(message);
        messageDidplayDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                               @Override
                                                               public void onClick(final View view) {
                                                                   messageDidplayDialog.dismiss();
                                                                   FragmentManager fragmentManager = getFragmentManager();
                                                                   fragmentTransaction = fragmentManager.beginTransaction();
                                                                   MenuFragment menuFragment = new MenuFragment();
                                                                   fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                                                                   fragmentTransaction.replace(R.id.frame, menuFragment);
                                                                   fragmentTransaction.commit();
                                                                   toolbar.setTitle("Menu");
                                                               }
                                                           }
        );
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void> {


//        pdLoading=new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            super.onPreExecute();

            //this method will be running on UI thread
            pdLoading.setMessage("\tLoading...");
            pdLoading.show();
        }

        @Override
        protected Void doInBackground(Void... params) {

            //this method will be running on background thread so don't update UI frome here
            //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
//            if (selectListType == AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT) {
//                String farmerList = sqlLiteHelperPhase_ii.getFarmerList();
//                if (farmerList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
//                    showMessage("Farmer List Not Found");
//                else
//                    showOptionList(getJsoneManager(farmerList, CaneTypeListModel.class), grower_name);
//            } else
            if (selectListType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                String plantationList = sqLiteHelper.getPlanatationList();
                if (plantationList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage("Plantation List Not Found");
                else
                    showOptionList(getJsoneManager(plantationList, PlantationDataForRujvatModel.class), txtPlantationID);
            } else if (selectListType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                String vehicleList = sqLiteHelper.getVehicleList();
                if (vehicleList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage("Vehicle List Not Found");
                else
                    showOptionList(getJsoneManager(vehicleList, VehicalListModel.class), txtVehicalNumber);
            } else if (selectListType == AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT) {
                String harvesterList = sqLiteHelper.getHarvesterList();
                if (harvesterList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage("Harvester List Not Found");
                else
                    showOptionList(getJsoneManager(harvesterList, HarvestorListModel.class), txtHarvestorCode);
            } else if (selectListType == AppConstantsPhase_II.TRAILOR_I_LIST_TODA_CHITTI_SELECT) {
                ArrayList<String> list = new DataSet().gettroly(getContext());
                showOptionList(list, txttrly1);
            } else if (selectListType == AppConstantsPhase_II.TRAILOR_II_LIST_TODA_CHITTI_SELECT) {
                ArrayList<String> list = new DataSet().gettroly(getContext());
                showOptionList(list, txttrly2);
            } else if (selectListType == AppConstantsPhase_II.ROP_TYPE_SELECT) {
                ropList = new DataSet().getRopType(getContext());
                ArrayList<String> list = new ArrayList<>();
                list.add(ropList.get(0).getR_type_name());
                list.add(ropList.get(1).getR_type_name());
                showOptionList(list, txt_rop_type);
            } else if (selectListType == AppConstantsPhase_II.GROWER_FACTORY_SELECT) {
                String growerFactoryList = sqLiteHelper.getFactoryNameList();
                if (growerFactoryList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage("Factory List Not Found");
                else
                    showOptionList(getJsoneManager(growerFactoryList, FactoryNameListModel.class), txtGrowarFactoryName);
            } else if (selectListType == AppConstantsPhase_II.CANE_QUALITY_SELECT) {
                String caneQualityList = sqLiteHelper.getAllCaneQuality();
                if (caneQualityList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                    showMessage("Cane Quality Not Found");
                else
                    showOptionList(getJsoneManager(caneQualityList, CaneQualityModel.class), txt_cane_quality);
            }
            return null;
        }


        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

            //this method will be running on UI thread

//            pdLoading.dismiss();
        }
    }


    private boolean validateData() {
//        if (grower_name.getText().toString().equals("Grower Name")) {
//            Toast.makeText(getContext(), "Please Select Grower Name", Toast.LENGTH_SHORT).show();
//            return false;
//        }
        if (txtPlantationID.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Plantation ID", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txtVehicalNumber.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Vehicle Number", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txtHarvestorCode.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Harvester Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txttrly1.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Trailer 1", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txttrly2.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Trailer 2", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txtGrowarFactoryName.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Factory Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txt_driver_name.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Enter Driver Name", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txt_rop_type.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Rop Type", Toast.LENGTH_SHORT).show();
            return false;
        }
        if (txt_cane_quality.getText().toString().equals("")) {
            Toast.makeText(getContext(), "Please Select Cane Quality", Toast.LENGTH_SHORT).show();
            return false;
        }
        return true;
    }

    public void showMessage(final String message) {
        getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                pdLoading.dismiss();
                Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
            }
        });
    }

    private ArrayList<String> getJsoneManager(String jsone, Class model) {
        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), model, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    if (resultObj instanceof ArrayList) {
//                        if (selectListType == AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT) {
//                            namLlist = new ArrayList();
//                            growarCode = new ArrayList();
//                            farmerList = (ArrayList<CaneTypeListModel>) resultObj;
//                            for (int i = 0; i < farmerList.size(); i++) {
//                                namLlist.add(i, farmerList.get(i).getFarmerName());
//                                growarCode.add(i, farmerList.get(i).getFarmerCode());
//                            }
//                        } else
                        if (selectListType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            growarNameList = new ArrayList();
                            plantationList = (ArrayList<PlantationDataForRujvatModel>) resultObj;
                            for (int i = 0; i < plantationList.size(); i++) {
                                namLlist.add(i, plantationList.get(i).getHRV_CODE());
                                plantationListList.add(i, plantationList.get(i).getPLANTATION_CODE());
                                docNoList.add(i, plantationList.get(i).getCUTTING_OBJECT_ID());
                                growarNameList.add(i, plantationList.get(i).getFARMER_NAME());
                            }
                        } else if (selectListType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            vehicalList = (ArrayList<VehicalListModel>) resultObj;
                            for (int i = 0; i < vehicalList.size(); i++) {
                                namLlist.add(i, vehicalList.get(i).getVEHICAL_NO());
                                growarNameList.add(vehicalList.get(i).getCODE());
                            }
                        } else if (selectListType == AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            harvesterList = (ArrayList<HarvestorListModel>) resultObj;
                            for (int i = 0; i < harvesterList.size(); i++) {
                                namLlist.add(i, harvesterList.get(i).getH_NAME());
                                growarCode.add(i, harvesterList.get(i).getCODE());
                            }
                        } else if (selectListType == AppConstantsPhase_II.GROWER_FACTORY_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            growerFactoryList = (ArrayList<FactoryNameListModel>) resultObj;
                            for (int i = 0; i < growerFactoryList.size(); i++) {
                                namLlist.add(i, growerFactoryList.get(i).getSHORT_NAME());
//                                growarCode.add(i, growerFactoryList.get(i).getID());
                            }
                        } else if (selectListType == AppConstantsPhase_II.CANE_QUALITY_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            caneQualityList = (ArrayList<CaneQualityModel>) resultObj;
                            for (int i = 0; i < caneQualityList.size(); i++) {
                                namLlist.add(i, caneQualityList.get(i).getNAME());
                            }
                        }
                    }
                }

                @Override
                public void onError(String strError) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return namLlist;
    }


    private void showOptionList(ArrayList<String> listArray, final TextView textView) {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(listArray, textView.getText().toString(), growarCode);
        if (selectListType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT || selectListType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
            fragment.getFarmerNameList(growarNameList);
        }
        if (selectListType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
            fragment.getplantationIDList(plantationListList, docNoList);
        }
        fragment.setListTitle("kljklkllk");

        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                removeOptionList();
            }

            @Override
            public void onListItemSelected(Object model) {


                // you can use this model to show selected value in the text
                if (model instanceof String) {
//                    if (selectListType == AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT) {
//                        String value = (String) model;
//                        textView.setText(value);
//                        farmerCode = farmerList.get(AppConstants.selectItemIndex).getFarmerCode();
//                        txt_village.setText(farmerList.get(AppConstants.selectItemIndex).getFarmerVillageName());
//                        txt_gut.setText(farmerList.get(AppConstants.selectItemIndex).getFarmerGutName());
//                        txt_subgut.setText(farmerList.get(AppConstants.selectItemIndex).getFarmerSubgutName());
//                        txt_taluka.setText(farmerList.get(AppConstants.selectItemIndex).getFarmerTalukaName());
////                        grower_id = deatilsList.get(AppConstants.selectItemIndex).getFarmerID();
////                        varity_id = Integer.parseInt(deatilsList.get(AppConstants.selectItemIndex).getFarmerObjectID());
//                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_village.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_gut.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_subgut.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_taluka.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        textView.setTypeface(null, Typeface.BOLD);
//                        txt_village.setTypeface(null, Typeface.BOLD);
//                        txt_gut.setTypeface(null, Typeface.BOLD);
//                        txt_subgut.setTypeface(null, Typeface.BOLD);
//                        txt_taluka.setTypeface(null, Typeface.BOLD);
//                    } else
                    if (selectListType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                        txtVehicalType.setText(vehicalList.get(AppConstants.selectItemIndex).getV_NAME());
                        txtVehicalType.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txtVehicalType.setTypeface(null, Typeface.BOLD);
                        txtTransportCode.setText(vehicalList.get(AppConstants.selectItemIndex).getTR_NAME());
                        txtTransportCode.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txtTransportCode.setTypeface(null, Typeface.BOLD);
                        VNO_OBJECT_ID = vehicalList.get(AppConstants.selectItemIndex).getVNO_OBJECT_ID();
                        TRANSPORTER_ID = vehicalList.get(AppConstants.selectItemIndex).getTRANSPORTER_ID();
                        VEHICAL_TYPE = vehicalList.get(AppConstants.selectItemIndex).getVEHICAL_TYPE();
                    } else if (selectListType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                        String value = (String) model;
                        textView.setText(plantationList.get(AppConstants.selectItemIndex).getPLANTATION_CODE());
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                        txt_shivarVillage.setText(plantationList.get(AppConstants.selectItemIndex).getSHIVAR_VILLAGE_NAME());
                        txt_shivarVillage.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_shivarVillage.setTypeface(null, Typeface.BOLD);
                        txt_shivarGut.setText(plantationList.get(AppConstants.selectItemIndex).getSHIVAR_GUT_NAME());
                        txt_shivarGut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_shivarGut.setTypeface(null, Typeface.BOLD);
                        OBJECT_ID = plantationList.get(AppConstants.selectItemIndex).getOBJECT_ID();
                        FARMER_ID = plantationList.get(AppConstants.selectItemIndex).getFARMER_ID();
                        txt_surveyNumber.setText(plantationList.get(AppConstants.selectItemIndex).getSARVE_NO());
                        txt_surveyNumber.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_surveyNumber.setTypeface(null, Typeface.BOLD);
//                        txt_surveyDate.setText(new DataSet().getsurveyDate().get(AppConstants.selectItemIndex));
//                        txt_surveyDate.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_surveyDate.setTypeface(null, Typeface.BOLD);
                        txt_irrigationSource.setText(plantationList.get(AppConstants.selectItemIndex).getIRRE_NAME());
                        txt_irrigationSource.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_irrigationSource.setTypeface(null, Typeface.BOLD);
                        txt_caneVerity.setText(plantationList.get(AppConstants.selectItemIndex).getVARITY());
                        txt_caneVerity.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_caneVerity.setTypeface(null, Typeface.BOLD);
//                        txt_caneType.setText(new DataSet().getcaneType().get(AppConstants.selectItemIndex));
//                        txt_caneType.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_caneType.setTypeface(null, Typeface.BOLD);
                        txt_plantationDate.setText(plantationList.get(AppConstants.selectItemIndex).getPLANTATION_DATE());
                        txt_plantationDate.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_plantationDate.setTypeface(null, Typeface.BOLD);
                        txt_plantationType.setText(plantationList.get(AppConstants.selectItemIndex).getPLANTATION_TYPE());
                        txt_plantationType.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_plantationType.setTypeface(null, Typeface.BOLD);
//                        txt_plantationArea.setText(new DataSet().getplantationArea().get(AppConstants.selectItemIndex));
//                        txt_plantationArea.setTextColor(ContextCompat.getColor(getContext(), R.color.selectedValueInPlantationAndRujvat));
//                        txt_plantationArea.setTypeface(null, Typeface.BOLD);
                        farmerCode = plantationList.get(AppConstants.selectItemIndex).getFARMER_CODE();
                        grower_name.setText(plantationList.get(AppConstants.selectItemIndex).getFARMER_NAME());
                        txt_village.setText(plantationList.get(AppConstants.selectItemIndex).getFARMER_VILLAGE());
                        txt_gut.setText(plantationList.get(AppConstants.selectItemIndex).getFARMER_GUT());
                        txt_subgut.setText(plantationList.get(AppConstants.selectItemIndex).getFARMER_SUBGUT());
                        txt_taluka.setText(plantationList.get(AppConstants.selectItemIndex).getFARMER_TALUKA());
                        harvesterCode = plantationList.get(AppConstants.selectItemIndex).getHRV_CODE();
                        txtHarvestorCode.setText(plantationList.get(AppConstants.selectItemIndex).getHRV_NAME());
//                        grower_id = deatilsList.get(AppConstants.selectItemIndex).getFarmerID();
//                        varity_id = Integer.parseInt(deatilsList.get(AppConstants.selectItemIndex).getFarmerObjectID());
                        grower_name.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_village.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_gut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_subgut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        txt_taluka.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        grower_name.setTypeface(null, Typeface.BOLD);
                        txt_village.setTypeface(null, Typeface.BOLD);
                        txt_gut.setTypeface(null, Typeface.BOLD);
                        txt_subgut.setTypeface(null, Typeface.BOLD);
                        txt_taluka.setTypeface(null, Typeface.BOLD);

                    } else if (selectListType == AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT) {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                        harvesterCode = harvesterList.get(AppConstants.selectItemIndex).getOBJECT_ID();
                    } else if (selectListType == AppConstantsPhase_II.GROWER_FACTORY_SELECT) {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    } else if (selectListType == AppConstantsPhase_II.ROP_TYPE_SELECT) {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                        ROP_TYPE = ropList.get(AppConstants.selectItemIndex).getR_type_code();
                    } else if (selectListType == AppConstantsPhase_II.CANE_QUALITY_SELECT) {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                        caneQuality = caneQualityList.get(AppConstants.selectItemIndex).getQUALITY_ID();
                    } else {
                        String value = (String) model;
                        textView.setText(value);
                        textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                        textView.setTypeface(null, Typeface.BOLD);
                    }
                }
                AppConstants.selectItemIndex = 0;
                removeOptionList();
            }
        };
        ft.add(R.id.frame_generate_token, fragment);
        ft.commit();
        pdLoading.dismiss();

    }

    void removeOptionList() {
        final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
        ft.remove(fragment).commit();
        growarNameList.clear();
        plantationListList.clear();
        docNoList.clear();
        growarCode.clear();
    }

    public void writeData(Ndef ndef) {
        String messageToWrite = new SimpleDateFormat("yyyyMMddHHmmss").format(new Date()) + "," + farmerCode + "," + txtPlantationID.getText().toString() + "," + txtVehicalNumber.getText().toString() + "," + harvesterCode + "," + txttrly1.getText().toString() + "," + txttrly2.getText().toString();
        mNfcWriteFragment = (NFCWriteFragment) getActivity().getFragmentManager().findFragmentByTag(NFCWriteFragment.TAG);
        mNfcWriteFragment.onNfcDetected(ndef, messageToWrite);
    }

    private NFCWriteFragment mNfcWriteFragment;
}
