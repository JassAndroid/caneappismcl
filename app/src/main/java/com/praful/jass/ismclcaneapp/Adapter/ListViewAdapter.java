package com.praful.jass.ismclcaneapp.Adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import com.praful.jass.ismclcaneapp.Models.DailyHistoryModel;
import com.praful.jass.ismclcaneapp.R;

import java.util.List;

/**
 * Created by Jass on 4/21/2017.
 */

public class ListViewAdapter extends RecyclerView.Adapter<ListViewAdapter.MyViewHolder>{

    private Context context;
    private List<DailyHistoryModel> listItems;

    public class MyViewHolder extends RecyclerView.ViewHolder {

        public TextView growerName, workType, time;

        public MyViewHolder(View view) {
            super(view);

            growerName = (TextView)itemView.findViewById(R.id.dailly_histry_item_grower_name);
            workType = (TextView)itemView.findViewById(R.id.dailly_histry_item_work_type);
            time = (TextView)itemView.findViewById(R.id.dailly_histry_item_time);
        }
    }

    public void updateAdapter(List<DailyHistoryModel> listItems) {
        this.listItems.clear();
        this.listItems = listItems;
         notifyDataSetChanged();
    }
    public ListViewAdapter(Context context, List<DailyHistoryModel> listItems) {
        this.listItems = listItems;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.list_item_daily_history, parent, false);

        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(MyViewHolder holder, int position) {

       final DailyHistoryModel model = listItems.get(position);
       holder.growerName.setText(model.getNAME());
       holder.workType.setText(model.getWORK_TYPE());
       holder.time.setText(model.getENTER_DATE());

    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }


}
