package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.SharedPreferences;
import android.content.pm.PackageManager;
import android.graphics.Bitmap;
import android.net.Uri;
import android.os.Bundle;
import android.os.Environment;
import android.os.StrictMode;
import android.preference.PreferenceManager;
import android.provider.MediaStore;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;
import com.praful.jass.ismclcaneapp.services.GoogleService;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import java.io.File;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;
import java.util.UUID;

import static android.app.Activity.RESULT_OK;


public class EmployeeTripFragment extends Fragment {

    Button btnStart, btnStop;
    String getButtonText;
    TextView btn_take_pick;//tripStartStop
    EditText reading;
    ImageView img_meeter_image;
    boolean boolean_permission;
    private Uri file;
    private Toolbar toolbar;
    public ProgressDialog pdLoading;
    String imgFileName = "";
    TelephonyManager telephonyManager;
    SystemDateTime dateTime = new SystemDateTime();
    FragmentTransaction fragmentTransaction;
    private static final int REQUEST_PERMISSIONS = 100;
    private SQLiteHelper sqLiteHelper;
    Context ctx;
    SharedPreferences mPref;
    SharedPreferences.Editor medit;


    public Context getCtx() {
        return ctx;
    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        final View view = inflater.inflate(R.layout.fragment_employee_trip, container, false);
        try {
            init(view);
            fn_permission();
            mPref = PreferenceManager.getDefaultSharedPreferences(getContext());
            medit = mPref.edit();
            stopButton();
            startButton();
            if (ContextCompat.checkSelfPermission(getContext(), Manifest.permission.CAMERA) != PackageManager.PERMISSION_GRANTED || ContextCompat.checkSelfPermission(getContext(), Manifest.permission.WRITE_EXTERNAL_STORAGE) != PackageManager.PERMISSION_GRANTED) {
                ActivityCompat.requestPermissions(getActivity(), new String[]{Manifest.permission.CAMERA, Manifest.permission.WRITE_EXTERNAL_STORAGE, Manifest.permission.ACCESS_FINE_LOCATION}, 0);
            }
            Preferences.appContext = getContext();
            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.nav_employee_trip));


            btnStart.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (checkTripInOneDay()) {
                        if (validateData()) {
                            pdLoading.show();
                            getButtonText = btnStart.getText().toString();
                            setPreferences();
                            startButton();
                            UploadDataTripData("S");
                            reading.setText("");
                            imgFileName = "";
                            java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
                            Preferences.setTripCounter(dateFormat.format(new java.util.Date()));
                        }
                    } else {
                        Snackbar.make(view.findViewById(R.id.activity_employee_trip), getString(R.string.already_started_trip), Snackbar.LENGTH_LONG).show();
                    }
                    pdLoading.dismiss();
                }
            });
            btnStop.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (validateData()) {
                        pdLoading.show();
                        getButtonText = btnStop.getText().toString();
                        setPreferences();
                        stopButton();
                        UploadDataTripData("E");
                        reading.setText("");
                        imgFileName = "";
                    }
                    pdLoading.dismiss();
                }
            });
            btn_take_pick.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    takePicture();
                }
            });

            //To avoid URI exposure problem in API 24+ devices. :)
            StrictMode.VmPolicy.Builder builder = new StrictMode.VmPolicy.Builder();
            StrictMode.setVmPolicy(builder.build());
            // Inflate the layout for this fragment
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    private void fn_permission() {
        try {
            if ((ContextCompat.checkSelfPermission(getContext(), android.Manifest.permission.ACCESS_FINE_LOCATION) != PackageManager.PERMISSION_GRANTED)) {

                if ((ActivityCompat.shouldShowRequestPermissionRationale(getActivity(), android.Manifest.permission.ACCESS_FINE_LOCATION))) {

                } else {
                    ActivityCompat.requestPermissions(getActivity(), new String[]{android.Manifest.permission.ACCESS_FINE_LOCATION
                            },
                            REQUEST_PERMISSIONS);
                }
            } else {
                boolean_permission = true;
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean checkTripInOneDay() {
        boolean status = false;
        try {
            java.text.DateFormat dateFormat = android.text.format.DateFormat.getDateFormat(getContext());
            if (Preferences.getTripCounter().equals(dateFormat.format(new java.util.Date())))
                status = false;
            else
                status = true;
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return status;
    }

    public void init(View view) {
        try {
            btnStart = (Button) view.findViewById(R.id.btn_start);
            btnStop = (Button) view.findViewById(R.id.btn_stop);
            btn_take_pick = (TextView) view.findViewById(R.id.btn_take_pick);
            img_meeter_image = (ImageView) view.findViewById(R.id.img_meeter_image);
            reading = (EditText) view.findViewById(R.id.reading);
            reading.setText("");
            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);
            sqLiteHelper = new SQLiteHelper(getActivity());
            telephonyManager = (TelephonyManager) getContext().getSystemService(getContext().TELEPHONY_SERVICE);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void startButton() {
        try {
            if (Preferences.getTripStatus().equals(AppConstants.START_TRIP) && !Preferences.getTripStatus().equals(null)) {

                ctx = getActivity();
                if (boolean_permission) {
                    Intent intent = new Intent(getContext(), GoogleService.class);
                    getActivity().startService(intent);
                } else {
                    Toast.makeText(getContext(), getString(R.string.enable_the_gps), Toast.LENGTH_SHORT).show();
                }

                btnStart.setVisibility(View.GONE);
                btnStop.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), getString(R.string.no_preference_available), Toast.LENGTH_SHORT).show();
        }
    }

    public void stopButton() {
        try {
            if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP) || Preferences.getTripStatus().equals(null)) {
                ctx = getActivity();
                if (boolean_permission) {
                    Intent intent = new Intent(getContext(), GoogleService.class);
                    getActivity().startService(intent);
                } else {
                    Toast.makeText(getContext(), getString(R.string.enable_the_gps), Toast.LENGTH_SHORT).show();
                }
                btnStart.setVisibility(View.VISIBLE);
                btnStop.setVisibility(View.GONE);
            }
        } catch (Exception e) {
            Toast.makeText(getContext(), getString(R.string.no_preference_available), Toast.LENGTH_SHORT).show();
        }
    }

    public void setPreferences() {
        try {
            if (Preferences.getTripStatus().equals(AppConstants.START_TRIP) && !Preferences.getTripStatus().equals(null)) {
                Preferences.setTripStatus(AppConstants.STOPT_TRIP);
                Preferences.setTripImgStatus(false);
            } else if (Preferences.getTripStatus().equals(AppConstants.STOPT_TRIP) || Preferences.getTripStatus().equals(null)) {
                Preferences.setTripStatus(AppConstants.START_TRIP);
                Preferences.setTripImgStatus(false);
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private boolean validateData() {
        try {
            if (reading.getText().toString().equals("")) {
                Toast.makeText(getContext(), getString(R.string.enter_reading_T), Toast.LENGTH_SHORT).show();
                return false;
            } else if (reading.getText().toString().length() > 6) {
                Toast.makeText(getContext(), getString(R.string.reading_digits), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (imgFileName.equals("")) {
                Toast.makeText(getContext(), getString(R.string.take_picture), Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    public void takePicture() {
        try {
            Intent intent = new Intent(android.provider.MediaStore.ACTION_IMAGE_CAPTURE);
            file = Uri.fromFile(getOutputMediaFile());
            intent.putExtra(MediaStore.EXTRA_OUTPUT, file);
            startActivityForResult(intent, 1888);
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    public Bitmap getResizedBitmap(Bitmap image, int maxSize) {
        int width = 0;
        int height = 0;
        try {
            width = image.getWidth();
            height = image.getHeight();

            float bitmapRatio = (float) width / (float) height;
            if (bitmapRatio > 1) {
                width = maxSize;
                height = (int) (width / bitmapRatio);
            } else {
                height = maxSize;
                width = (int) (height * bitmapRatio);
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return Bitmap.createScaledBitmap(image, width, height, true);
    }

    private void saveImage(Bitmap finalBitmap) {
        try {
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");

            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CaneApp", "failed to create directory");
                }
            }
            File file1 = new File(mediaStorageDir.getPath() + File.separator + imgFileName);
            if (file1.exists()) file1.delete();
            try {
                FileOutputStream out = new FileOutputStream(file1);
                finalBitmap.compress(Bitmap.CompressFormat.JPEG, 70, out);
                out.flush();
                out.close();
                img_meeter_image.setImageURI(file);
            } catch (Exception e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private File getOutputMediaFile() {
        File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                Environment.DIRECTORY_PICTURES), "CaneApp");
        try {
            if (!mediaStorageDir.exists()) {
                if (!mediaStorageDir.mkdirs()) {
                    Log.d("CameraDemo", "failed to create directory");
                    return null;
                }
            } else {
                File file = new File(mediaStorageDir.getPath() + File.separator + ".nomedia");
                try {
                    if (file.exists())
                        Log.d("Trp Camera File", "File Already Exists");
                    else
                        file.createNewFile();
                } catch (IOException e) {
                    e.printStackTrace();
                }
            }
            String timeStamp = new SimpleDateFormat("yyyyMMdd_HHmmss").format(new Date());
            imgFileName = "IMG_TRIP_" + Preferences.getCurrentUserId() + "_" + timeStamp + ".jpg";
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return new File(mediaStorageDir.getPath() + File.separator + imgFileName);
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        try {
            if (requestCode == 1888) {
                if (resultCode == RESULT_OK) {
//                Bitmap tempPhoto = (Bitmap) data.getExtras().get("data");
//                img_meeter_image.setImageBitmap(tempPhoto);
                    Bitmap photo = null;
                    try {
                        photo = MediaStore.Images.Media.getBitmap(
                                getActivity().getContentResolver(), file);
                    } catch (IOException e) {
                        e.printStackTrace();
                    }
                    img_meeter_image.setImageBitmap(photo);
                    saveImage(getResizedBitmap(photo, 800));
                } else {
                    imgFileName = "";
                }
            } else {
                Toast.makeText(getContext(), getString(R.string.request_not_match_trip), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void UploadDataTripData(String flag) {
        try {
            CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
            if (flag.equals("S")) {
                insertEmployeeTripDataIntoDatabase(reading.getText().toString(), imgFileName, flag, locationModel.getLatitude() + "", locationModel.getLongitude() + "", "0", "0");
//            uploadMultipart(reading.getText().toString(), imgFileName, flag);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
            } else if (flag.equals("E")) {
                insertEmployeeTripDataIntoDatabase(reading.getText().toString(), imgFileName, flag, "0", "0", locationModel.getLatitude() + "", locationModel.getLongitude() + "");
//            uploadMultipart(reading.getText().toString(), imgFileName, flag);
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void uploadMultipart(String reading, String imgName, String flag) {
        try {
            String startReading = "", startTime = "", startImg = "", endReading = "", endTime = "", endImg = "";
            if (flag.equals("S")) {
                startReading = reading;
                startTime = dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss");
                startImg = imgName;
                endReading = "";
                endTime = "";
                endImg = "";
            } else if (flag.equals("E")) {
                startReading = "";
                startTime = "";
                startImg = "";
                endReading = reading;
                endTime = dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss");
                endImg = imgName;
            }
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(
                    Environment.DIRECTORY_PICTURES), "CaneApp");
            String deviceId = null;
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    // here to request the missing permissions, and then overriding
                    //   public void onRequestPermissionsResult(int requestCode, String[] permissions,
                    //                                          int[] grantResults)
                    // to handle the case where the user grants the permission. See the documentation
                    // for ActivityCompat#requestPermissions for more details.
                    return;
                }
                deviceId = telephonyManager.getDeviceId(2);
            } else {
                deviceId = telephonyManager.getDeviceId();

            }
            //Uploading code
            try {
                String uploadId = UUID.randomUUID().toString();

                //Creating a multi part request
                new MultipartUploadRequest(getContext(), uploadId, AppConstants.URL_TRIP_DATA_UPDATE)
                        .addFileToUpload(mediaStorageDir.getPath() + File.separator + imgFileName, "img_1") //Adding file
                        .addParameter("p_device", deviceId) //Adding text parameter to the request
                        .addParameter("p_date", dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss")) //Adding text parameter to the request
                        .addParameter("p_start", startReading) //Adding text parameter to the request
                        .addParameter("p_start_time", startTime) //Adding text parameter to the request
                        .addParameter("p_end", endReading) //Adding text parameter to the request
                        .addParameter("p_end_time", endTime) //Adding text parameter to the request
                        .addParameter("p_start_pic", startImg) //Adding text parameter to the request
                        .addParameter("p_end_pic", endImg) //Adding text parameter to the request
                        .addParameter("p_flag", flag) //Adding text parameter to the request
                        .setNotificationConfig(new UploadNotificationConfig())
                        .setMaxRetries(2)
                        .startUpload(); //Starting the upload

            } catch (Exception exc) {
                Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void insertEmployeeTripDataIntoDatabase(String reading, String imgName, String flag, String startLat, String startLong, String endLat, String endLong) {
        try {
            String startReading = "", startTime = "", startImg = "", endReading = "", endTime = "", endImg = "";
            if (flag.equals("S")) {
                startReading = reading;
                startTime = dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss");
                startImg = imgName;
                endReading = "";
                endTime = "";
                endImg = "";
            } else if (flag.equals("E")) {
                startReading = "";
                startTime = "";
                startImg = "";
                endReading = reading;
                endTime = dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss");
                endImg = imgName;
            }
            String deviceId = null;
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling
                //    ActivityCompat#requestPermissions
                return;
            }
            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                deviceId = telephonyManager.getDeviceId(2);
            } else {
                deviceId = telephonyManager.getDeviceId();
            }

            try {
                sqLiteHelper.insertEmployeeTripData(deviceId, dateTime.systemDateTime("dd-MMM-yyyy HH:mm:ss"), startReading, startTime, endReading, endTime, startImg, endImg, flag, imgFileName, startLat, startLong, endLat, endLong);
            } catch (Exception exc) {
                Toast.makeText(getContext(), exc.getMessage(), Toast.LENGTH_SHORT).show();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
