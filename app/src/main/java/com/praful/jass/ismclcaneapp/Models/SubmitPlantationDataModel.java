package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 27-08-2017.
 */

public class SubmitPlantationDataModel {
    public String plantation_key, Code;

    public String getCode() {
        return Code;
    }

    public void setCode(String code) {
        Code = code;
    }

    public String getPlantation_key() {
        return plantation_key;
    }

    public void setPlantation_key(String plantation_key) {
        this.plantation_key = plantation_key;
    }
}
