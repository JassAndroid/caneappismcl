package com.praful.jass.ismclcaneapp.Models;

public class IPListMasterModel {
    public String NET_NAME, NET_IP, NET_FLAG;

    public String getNET_NAME() {
        return NET_NAME;
    }

    public void setNET_NAME(String NET_NAME) {
        this.NET_NAME = NET_NAME;
    }

    public String getNET_IP() {
        return NET_IP;
    }

    public void setNET_IP(String NET_IP) {
        this.NET_IP = NET_IP;
    }

    public String getNET_FLAG() {
        return NET_FLAG;
    }

    public void setNET_FLAG(String NET_FLAG) {
        this.NET_FLAG = NET_FLAG;
    }
}
