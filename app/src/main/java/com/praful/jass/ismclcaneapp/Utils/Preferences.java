package com.praful.jass.ismclcaneapp.Utils;

import android.content.Context;
import android.content.SharedPreferences;

import com.praful.jass.ismclcaneapp.Models.IPListMasterModel;

import java.util.ArrayList;

import static android.content.Context.MODE_PRIVATE;


public class Preferences {

    private final static String preferencesName = "PAMPASADMIN";//CANEAPP_PRAFUL
    public static Context appContext;

    public static void setCurrentUserId(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getCurrentUserId", value);
        editor.commit();
    }

    public static String getCurrentUserId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getCurrentUserId", null);
        return value;
    }


    public static void setUserName(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getUserName", value);
        editor.commit();
    }

    public static String getUserName() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getUserName", null);
        return value;
    }

    public static void setUserPass(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getUserPass", value);
        editor.commit();
    }

    public static String getUserPass() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getUserPass", null);
        return value;
    }


    public static void setFirstTimeStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getFirstTimeStatus", value);
        editor.commit();
    }

    public static boolean getFirstTimeStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getFirstTimeStatus", true);
        return value;
    }

    public static void setUpdationFirstTimeStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getUpdationFirstTimeStatus", value);
        editor.commit();
    }

    public static boolean getUpdationFirstTimeStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getUpdationFirstTimeStatus", true);
        return value;
    }


    public static void setTripStatus(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getTripStatus", value);
        editor.commit();
    }

    public static String getTripStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getTripStatus", null);
        return value;
    }

    public static void setServerIP(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getServerIP", value);
        editor.commit();
    }

    public static String getServerIP() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getServerIP", null);
        return value;
    }

    public static void setTrackingtime(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getTrackTime", value);
        editor.commit();
    }

    public static String getTrackingTime() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getTrackTime", null);
        return value;
    }

    public static void setSchedulerStatus(Boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getSchedulerStatus", value);
        editor.commit();
    }

    public static boolean getSchedulerStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getSchedulerStatus", true);
        return value;
    }


    public static void setSeasonID(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getSeasonID", value);
        editor.commit();
    }

    public static String getSeasonID() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getSeasonID", null);
        return value;
    }

    public static void setSeasonIDSelected(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getSeasonIDSelected", value);
        editor.commit();
    }

    public static String getSeasonIDSelected() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getSeasonIDSelected", null);
        return value;
    }

    public static void setSeasonName(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getSeasonName", value);
        editor.commit();
    }

    public static String getSeasonName() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getSeasonName", null);
        return value;
    }

    public static void setPlantationIDValueKey(int value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putInt("getplantationIDValueKey", value);
        editor.commit();
    }

    public static int getPlantationIDValueKey() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        int value = prefs.getInt("getplantationIDValueKey", 0);
        return value;
    }


    public static void setPlantationUploadStatus(Boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getPlantationUploadStatus", value);
        editor.commit();
    }

    public static boolean getPlantationUploadStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getPlantationUploadStatus", false);
        return value;
    }

    public static void setImageRenameStatus(Boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getImageRenameStatus", value);
        editor.commit();
    }

    public static boolean getImageRenameStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getImageRenameStatus", false);
        return value;
    }

    public static void setRujvatUploadStatus(Boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getRujvatUploadStatus", value);
        editor.commit();
    }

    public static boolean getRujvatUploadStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getRujvatUploadStatus", false);
        return value;
    }

    public static void setAllImageUploadStatus(Boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getAllImageUploadStatus", value);
        editor.commit();
    }

    public static boolean getAllImageUploadStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getAllImageUploadStatus", false);
        return value;
    }

    public static void setTripCounter(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getTripCounter", value);
        editor.commit();
    }

    public static String getTripCounter() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getTripCounter", null);
        return value;
    }

    public static void setCurrentSlipNumber(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getCurrentSlipNumber", value);
        editor.commit();
    }

    public static String getCurrentSlipNumber() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getCurrentSlipNumber", null);
        return value;
    }


    public static void setUserLocation(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getUserLocation", value);
        editor.commit();
    }

    public static String getUserLocation() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getUserLocation", null);
        return value;
    }

    public static void setTripImgStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getTripImgStatus", value);
        editor.commit();
    }

    public static boolean getTripImgStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getTripImgStatus", false);
        return value;
    }

    public static void setRujvatImgStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getRujvatImgStatus", value);
        editor.commit();
    }

    public static boolean getRujvatImgStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getRujvatImgStatus", false);
        return value;
    }

    public static void setKycInfoStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getKycInfoStatus", value);
        editor.commit();
    }

    public static boolean getKycInfoStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getKycInfoStatus", false);
        return value;
    }

    public static void setKycImgStatus(boolean value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putBoolean("getKycImgStatus", value);
        editor.commit();
    }

    public static boolean getKycImgStatus() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        boolean value = prefs.getBoolean("getKycImgStatus", false);
        return value;
    }

    public static void setLocalLanguage(String value) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getLocalLanguage", value);
        editor.commit();
    }

    public static String getLocalLanguage() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getLocalLanguage", AppConstants.ENGLISH);
        return value;
    }

    public static void setDeviceId(String deviceId) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getDeviceId", deviceId);
        editor.commit();
    }

    public static String getDeviceId() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getDeviceId", null);
        return value;
    }

    public static void setSeasonList(String seasonList) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getSeasonList", seasonList);
        editor.commit();
    }

    public static String getSeasonList() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getSeasonList", null);
        return value;
    }

    public static void setUserTypeFlag(String seasonList) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getUserTypeFlag", seasonList);
        editor.commit();
    }

    public static String getUserTypeFlag() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getUserTypeFlag", null);
        return value;
    }

    public static void setIPList(String ipList) {
        SharedPreferences.Editor editor = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE).edit();
        editor.putString("getIPList", ipList);
        editor.commit();
    }

    public static String getIPList() {
        SharedPreferences prefs = appContext.getSharedPreferences(preferencesName, MODE_PRIVATE);
        String value = prefs.getString("getIPList", null);
        return value;
    }

}
