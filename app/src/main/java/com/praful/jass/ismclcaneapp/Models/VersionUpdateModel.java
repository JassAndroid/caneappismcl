package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by DEBASHISH on 1/13/2018.
 */

public class VersionUpdateModel {

    String id;
    Integer version;
    String description;


    public String getId() {
        return id;
    }

    public void setId(String id) {
        this.id = id;
    }

    public Integer getVersion() {
        return version;
    }

    public void setVersion(Integer version) {
        this.version = version;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }
}
