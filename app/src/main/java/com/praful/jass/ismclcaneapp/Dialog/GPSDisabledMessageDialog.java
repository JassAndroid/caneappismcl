package com.praful.jass.ismclcaneapp.Dialog;

import android.app.Dialog;
import android.content.Context;
import android.os.Bundle;
import android.view.Window;
import android.widget.Button;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;


/**
 * Created by Dawnster on 5/22/2017.
 */

public class GPSDisabledMessageDialog extends Dialog {
    public TextView message_txt;
    public TextView message_title;
    public Button btn_accept;
    String title, message;
    private Context context;

    public GPSDisabledMessageDialog(Context context, String title, String massage) {
        super(context);
        this.title = title;
        this.message = massage;
        this.context = context;
    }


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            requestWindowFeature(Window.FEATURE_NO_TITLE);
            setContentView(R.layout.dialog_gps_disabled_message);
            init();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            message_txt = (TextView) findViewById(R.id.message_txt);
            message_txt.setText(message);
            message_title = (TextView) findViewById(R.id.message_title);
            message_title.setText(title);
//        timeinminutes = (TextView) findViewById(R.id.timeinminutes);
            btn_accept = (Button) findViewById(R.id.btn_ok);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}
