package com.praful.jass.ismclcaneapp.services;

import android.Manifest;
import android.app.Service;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.Environment;
import android.os.IBinder;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.telephony.TelephonyManager;
import android.util.Log;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.RujvatUpdationModel;
import com.praful.jass.ismclcaneapp.Models.TodaChittiSubmitModel;
import com.praful.jass.ismclcaneapp.Models.TrackingResultModel;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.AppConstantsPhase_II;
import com.praful.jass.ismclcaneapp.Utils.CheckInternet;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import net.gotev.uploadservice.MultipartUploadRequest;
import net.gotev.uploadservice.UploadNotificationConfig;

import org.json.JSONObject;

import java.io.File;
import java.util.ArrayList;
import java.util.Timer;
import java.util.TimerTask;
import java.util.UUID;

//import org.apache.http.HttpResponse;
//import org.apache.http.client.HttpClient;
//import org.apache.http.client.methods.HttpPost;
//import org.apache.http.entity.mime.MultipartEntity;
//import org.apache.http.impl.client.DefaultHttpClient;

/**
 * Created by fabio on 30/01/2016.
 */
public class SchedulerService extends Service {
    public int counter = 0;
    SQLiteHelper sqLiteHelper = new SQLiteHelper(this);
//    SQLLiteHelperPhase_II sqlLiteHelperPhase_ii = new SQLLiteHelperPhase_II(this);

    private String deviceID = null;
    private TelephonyManager telephonyManager;

    public SchedulerService(Context applicationContext) {
        super();
        Log.i("HERE", "here I am!");
    }

    public SchedulerService() {
    }

    @Override
    public int onStartCommand(Intent intent, int flags, int startId) {
        try {
            super.onStartCommand(intent, flags, startId);
            Preferences.appContext = this;
            telephonyManager = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);


            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions

//                return;
                }
                deviceID = telephonyManager.getDeviceId(2);
            } else {
                deviceID = telephonyManager.getDeviceId();//getDeviceId(1);
            }
            startTimer();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return START_STICKY;
    }

    @Override
    public void onDestroy() {
        try {
            super.onDestroy();
            Log.i("EXIT", "ondestroy!");
            Intent broadcastIntent = new Intent("link");
            sendBroadcast(broadcastIntent);
            stoptimertask();
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private Timer timer;
    private TimerTask timerTask;

    public void startTimer() {
        try {
            //set a new Timer
            timer = new Timer();

            //initialize the TimerTask's job
            initializeTimerTask();

            //schedule the timer, to wake up every 1 second
            timer.schedule(timerTask, 500, 30000); //300000  // Integer.parseInt(Preferences.getTrackingTime())
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * it sets the timer to print the counter every x seconds
     */
    public void initializeTimerTask() {
        try {
            timerTask = new TimerTask() {
                public void run() {
                    Log.i("in timer", "GPS:- " + " , " + " :-" + (counter++));
                    try {
                        String tackingData = sqLiteHelper.getTackingTop300Row();
                        if (tackingData.equalsIgnoreCase(AppConstants.TABLE_NOT_AVAILABLE)) {
//                            Toast.makeText(getApplicationContext(), "", Toast.LENGTH_SHORT).show();
                        } else {
                            if (!tackingData.equalsIgnoreCase("{\"TrackEntry\":[]}")) {
                                JSONObject jsonObject = new JSONObject(tackingData);
                                sendDataTOServer(AppConstants.URL_TRACKING, TrackingResultModel.class, jsonObject);
                            } else {
                                submitTodeChitiDataOnline();
                            }
                        }
                    } catch (Exception e) {
                        e.printStackTrace();
//                    timer.schedule(timerTask, 500, 60000); //00
                    }
                }
            };
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * not needed
     */
    public void stoptimertask() {
        try {
            //stop the timer, if it's not already null
            if (timer != null) {
                timer.cancel();
                timer = null;
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Nullable
    @Override
    public IBinder onBind(Intent intent) {
        return null;
    }

    void sendDataTOServer(String apiUrl, Class modelType, final JSONObject jsonObject) {
        try {
            if (CheckInternet.isNetworkAvailable(this)) {
                new APIRestManager().postAPI(apiUrl, jsonObject, this, modelType, new APIRestManager.APIManagerInterface() {

                    @Override
                    public void onSuccess(Object resultObj) {
                        try {

                            if (resultObj instanceof TrackingResultModel) {
                                TrackingResultModel trackingResultModel = (TrackingResultModel) resultObj;
                                if (trackingResultModel.getCode().equals("1")) {
//                            submitEmployeeTripDataOnServer();
                                    submitTodeChitiDataOnline();
                                    sqLiteHelper.deleteTraceEmployeeTop300(Integer.parseInt(((jsonObject.getJSONArray("TrackEntry").getJSONObject(jsonObject.getJSONArray("TrackEntry").length() - 1).getString("p_trace_id")))));
                                }
                            }
                        } catch (Exception e) {
                            Toast.makeText(SchedulerService.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                        }
                    }

                    @Override
                    public void onError(String strError) {

                        //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
                        String str = strError;
                    }
                });
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void submitRujvatData(String apiUrl, Class modelType, JSONObject jsonObject) {

        try {
            new APIRestManager().postRujvatData(apiUrl, jsonObject, this, modelType, new APIRestManager.APIManagerInterface() {

                @Override
                public void onSuccess(Object resultObj) {

                    if (resultObj instanceof ArrayList) {
                        ArrayList<RujvatUpdationModel> arrayList = (ArrayList<RujvatUpdationModel>) resultObj;
                        if (arrayList.get(0).getValue().equals("1")) {

                        }
                    }
                }

                @Override
                public void onError(String strError) {

                }
            });
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void submitTodeChitiDataOnline() {
        try {
            String todChittiList = sqLiteHelper.getTodChittiListSubmit(deviceID);
            if (!todChittiList.equalsIgnoreCase("{\"SlipSubmit\":[]}")) {
                new APIRestManager().postAPI(AppConstantsPhase_II.URL_SUBMIT_SLIP_DATA_TO_SERVER, new JSONObject(todChittiList), this, TodaChittiSubmitModel.class, new APIRestManager.APIManagerInterface() {

                    @Override
                    public void onSuccess(Object resultObj) {
                        ArrayList<TodaChittiSubmitModel> todaChittiFlagCodeList = (ArrayList<TodaChittiSubmitModel>) resultObj;
                        for (int i = 0; i < todaChittiFlagCodeList.size(); i++) {
                            sqLiteHelper.updateTodChittiOnlineFlag(todaChittiFlagCodeList.get(i).getSlip_id(), todaChittiFlagCodeList.get(i).getSlip_key());
                        }
                    }

                    @Override
                    public void onError(String strError) {
                        //Snackbar.make(viewForSnackBar, strError, Snackbar.LENGTH_LONG).show();
//                    Snackbar.make(view.findViewById(R.id.activity_update_server), "Something Wrong with Data", Snackbar.LENGTH_LONG).show();
                    }
                });
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    void submitEmployeeTripDataOnServer() {
        try {
            String uploadId = UUID.randomUUID().toString();
            File mediaStorageDir = new File(Environment.getExternalStoragePublicDirectory(Environment.DIRECTORY_PICTURES), "CaneApp");
            ArrayList<String[]> employeeTripData = sqLiteHelper.getEmployeeTripData();
            //Creating a multi part request
            if (employeeTripData.get(0).length > 0) {
                MultipartUploadRequest multipartUploadRequest = new MultipartUploadRequest(this, uploadId, AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY);
//            new MultipartUploadRequest(this, uploadId, AppConstants.URL_EMPLOYEE_TRIP_DATA_ARRAY)
//                    .addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9), "img_1") //Adding file

                for (int i = 0; i < employeeTripData.get(9).length; i++) { //
                    multipartUploadRequest.addParameter("p_device[]", employeeTripData.get(0)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_date[]", employeeTripData.get(1)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_start[]", employeeTripData.get(2)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_start_time[]", employeeTripData.get(3)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_end[]", employeeTripData.get(4)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_end_time[]", employeeTripData.get(5)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_start_pic[]", employeeTripData.get(6)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_end_pic[]", employeeTripData.get(7)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addParameter("p_flag[]", employeeTripData.get(8)[i]); //Adding text parameter to the request
                    multipartUploadRequest.addFileToUpload(mediaStorageDir.getPath() + File.separator + employeeTripData.get(9)[i], "userfile[]");
                }
                multipartUploadRequest.setNotificationConfig(new UploadNotificationConfig());
                multipartUploadRequest.setMaxRetries(2);
                String s = multipartUploadRequest.startUpload();//Starting the upload
//            Toast.makeText(this, s + " Trip Upload", Toast.LENGTH_SHORT).show();
                sqLiteHelper.deleteEmployeeTripData();
            }
        } catch (Exception exc) {
            Toast.makeText(this, exc.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }
}