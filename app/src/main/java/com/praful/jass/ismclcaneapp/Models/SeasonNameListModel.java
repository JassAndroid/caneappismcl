package com.praful.jass.ismclcaneapp.Models;

public class SeasonNameListModel {

    private String NAME;
    private String OBJECT_ID;
    private String seasonID;
    private String seasonName;

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }

    public String getSeasonID() {
        return seasonID;
    }

    public void setSeasonID(String seasonID) {
        this.seasonID = seasonID;
    }

    public String getSeasonName() {
        return seasonName;
    }

    public void setSeasonName(String seasonName) {
        this.seasonName = seasonName;
    }

    @Override
    public String toString() {
        return "ClassPojo [NAME = " + NAME + ", OBJECT_ID = " + OBJECT_ID + "]";
    }
}

