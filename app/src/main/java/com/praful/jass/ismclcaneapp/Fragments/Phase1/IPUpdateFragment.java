package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.Activity.Phase1.SlipBoyWorkListActivity;
import com.praful.jass.ismclcaneapp.Adapter.SlipBoyListAdapter;
import com.praful.jass.ismclcaneapp.DataSet.DataSet;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.IPListMasterModel;
import com.praful.jass.ismclcaneapp.Models.SlipBoyWorkStatusModel;
import com.praful.jass.ismclcaneapp.Models.SlipCandidateModel;
import com.praful.jass.ismclcaneapp.Models.WorkTypeModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class IPUpdateFragment extends Fragment {

    private SlipBoyListAdapter mAdapter;
    private Toolbar toolbar;
    View view;
    TextView dailyHistoryMsg;
    SlipBoyListAdapter.SlipBoyWorkStatusListener slipBoyWorkStatusListener;
    private OptionListFragment fragment;
    ArrayList<SlipCandidateModel> mList = new ArrayList<>();
    ArrayList<IPListMasterModel> ipListModelArray = new ArrayList<>();
    ArrayList<String> ipNameList = new ArrayList<>();
    private String purpose = "";
    FragmentTransaction fragmentTransaction;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.fragment_ipupdate, container, false);
            dailyHistoryMsg = (TextView) view.findViewById(R.id.daily_history_msg);

            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.slip_boy_work_status));

            getIPList();

//            FragmentManager fragmentManager = getFragmentManager();
//            fragmentTransaction = fragmentManager.beginTransaction();
//            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
//            slipBoyWorkStatusListener = new SlipBoyListAdapter.SlipBoyWorkStatusListener() {
//                @Override
//                public void itemClick(int id) {
//                    startActivity(new Intent(getActivity(), SlipBoyWorkListActivity.class));
//                    /*DailyHistoryFragment dailyHistoryFragment = new DailyHistoryFragment();
//                    fragmentTransaction.replace(R.id.frame, dailyHistoryFragment);
//                    fragmentTransaction.commit();*/
//                }
//            };
//
//            mAdapter = new SlipBoyListAdapter(getActivity(), slipBoyWorkStatusListener, mList);

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            purpose = bundle.getString("Purpose");
        }
    }

    public void showOptionList() {
        final DataSet dataSet = new DataSet();
        String title = "Nothing";

        //mList1 = dataSet.getTripBoyOptions(getContext());
        final FragmentTransaction ft = (FragmentTransaction) getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(ipNameList, title);
        fragment.setListTitle("Slip Boy Options");

        //final JSONArray finalJsonArray = jsonArray;
        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
            }

            @Override
            public void onListItemSelected(Object model) {
                //String flag;

                Preferences.setServerIP(ipListModelArray.get(AppConstants.selectItemIndex).getNET_IP());
                removeOptionList();
                FragmentManager fragmentManager = getFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
                fragmentTransaction.replace(R.id.frame, new MenuFragment());
                fragmentTransaction.commit();
            }
        };
        ft.add(R.id.layout_ip_list, fragment);
        ft.commit();
        //pdLoading.dismiss();
    }

    void removeOptionList() {
        try {
            final FragmentTransaction ft = (FragmentTransaction) getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            ft.remove(fragment).commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * API CALLING
     **/

    private void getIPList() {

        try {
            JSONObject jsonObject = new JSONObject(Preferences.getIPList());
            ArrayList resultArray = new ArrayList();
            Object json = jsonObject.get("response");
            Gson gson = new Gson();
            if (json instanceof JSONArray) {
                JSONArray list = jsonObject.getJSONArray("response");
                for (int i = 0; i < list.length(); i++) {
                    JSONObject object1 = list.getJSONObject(i);
                    Object model = gson.fromJson(object1.toString(), IPListMasterModel.class);
                    resultArray.add(model);
                }
            }
            ipListModelArray = (ArrayList<IPListMasterModel>) resultArray;

            for (IPListMasterModel obj : ipListModelArray) {
                ipNameList.add(obj.getNET_NAME());
            }
            showOptionList();
        } catch (Exception ex) {
            Toast.makeText(getActivity(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//    private void getWorkCandidateList() {
//
//        final ProgressDialog progressDialog = new ProgressDialog(getContext());
//        progressDialog.show();
//        JSONObject object = new JSONObject();
//        try {
//            object.put("user_id", Preferences.getCurrentUserId());
//            object.put("season", Preferences.getSeasonID());
//            //object.put("work_type",AppConstants.FLAG_WORK_TYPE_SELECTED);
//        } catch (JSONException e) {
//            e.printStackTrace();
//        }
//
//        String url = AppConstants.URL_GET_WORK_CANDIDATE_LIST;//+"?season="+Preferences.getSeasonID()+"&user_id="+AppConstants.USER_ID;
//
//        new APIRestManager().postJSONArrayAPI(url, object, SlipCandidateModel.class, getActivity(), new APIRestManager.APIManagerInterface() {
//            @Override
//            public void onSuccess(Object resultObj) {
//                progressDialog.dismiss();
//                mList = (ArrayList<SlipCandidateModel>) resultObj;
//                mAdapter.updateAdapter(mList);
//            }
//
//            @Override
//            public void onError(String strError) {
//                progressDialog.dismiss();
//                Toast.makeText(getActivity(), strError, Toast.LENGTH_SHORT).show();
//            }
//        });
//    }
}
