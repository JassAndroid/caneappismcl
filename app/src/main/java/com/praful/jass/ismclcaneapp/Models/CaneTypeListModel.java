package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 18-05-2017.
 */

public class CaneTypeListModel {

    //==============================================CANE TYPE================================================
    String caneTypeID, caneTypeName;

    public String getCaneTypeID() {
        return caneTypeID;
    }

    public void setCaneTypeID(String caneTypeID) {
        this.caneTypeID = caneTypeID;
    }

    public String getCaneTypeName() {
        return caneTypeName;
    }

    public void setCaneTypeName(String caneTypeName) {
        this.caneTypeName = caneTypeName;
    }


    //==============================================CANE VERITY================================================

    String caneVerityID, caneVerityName;

    public String getCaneVerityID() {
        return caneVerityID;
    }

    public void setCaneVerityID(String caneVerityID) {
        this.caneVerityID = caneVerityID;
    }

    public String getCaneVerityName() {
        return caneVerityName;
    }

    public void setCaneVerityName(String caneVerityName) {
        this.caneVerityName = caneVerityName;
    }


    //==============================================CANE PLANTATION TYPE================================================

    String plantationID, plantationName;

    public String getPlantationID() {
        return plantationID;
    }

    public void setPlantationID(String plantationID) {
        this.plantationID = plantationID;
    }

    public String getPlantationName() {
        return plantationName;
    }

    public void setPlantationName(String plantationName) {
        this.plantationName = plantationName;
    }

//==============================================IRRIGATION MASTER================================================

    String irrigationMasterID, irrigationMasterName;

    public String getIrrigationMasterID() {
        return irrigationMasterID;
    }

    public void setIrrigationMasterID(String irrigationMasterID) {
        this.irrigationMasterID = irrigationMasterID;
    }

    public String getIrrigationMasterName() {
        return irrigationMasterName;
    }

    public void setIrrigationMasterName(String irrigationMasterName) {
        this.irrigationMasterName = irrigationMasterName;
    }


    //==============================================FARMER/GROWER LIST================================================

    String farmerName, farmerCode, farmerID, farmerVillageName, farmerObjectID, farmerGutID, farmerGutName, farmerSubgutID, farmerSubgutName, farmerTalukaID, farmerTalukaName;

    public String getFarmerCode() {
        return farmerCode;
    }

    public void setFarmerCode(String farmerCode) {
        this.farmerCode = farmerCode;
    }

    public String getFarmerGutID() {
        return farmerGutID;
    }

    public void setFarmerGutID(String farmerGutID) {
        this.farmerGutID = farmerGutID;
    }

    public String getFarmerGutName() {
        return farmerGutName;
    }

    public void setFarmerGutName(String farmerGutName) {
        this.farmerGutName = farmerGutName;
    }

    public String getFarmerID() {
        return farmerID;
    }

    public void setFarmerID(String farmerID) {
        this.farmerID = farmerID;
    }

    public String getFarmerName() {
        return farmerName;
    }

    public void setFarmerName(String farmerName) {
        this.farmerName = farmerName;
    }

    public String getFarmerObjectID() {
        return farmerObjectID;
    }

    public void setFarmerObjectID(String farmerObjectID) {
        this.farmerObjectID = farmerObjectID;
    }

    public String getFarmerSubgutID() {
        return farmerSubgutID;
    }

    public void setFarmerSubgutID(String farmerSubgutID) {
        this.farmerSubgutID = farmerSubgutID;
    }

    public String getFarmerSubgutName() {
        return farmerSubgutName;
    }

    public void setFarmerSubgutName(String farmerSubgutName) {
        this.farmerSubgutName = farmerSubgutName;
    }

    public String getFarmerTalukaID() {
        return farmerTalukaID;
    }

    public void setFarmerTalukaID(String farmerTalukaID) {
        this.farmerTalukaID = farmerTalukaID;
    }

    public String getFarmerTalukaName() {
        return farmerTalukaName;
    }

    public void setFarmerTalukaName(String farmerTalukaName) {
        this.farmerTalukaName = farmerTalukaName;
    }

    public String getFarmerVillageName() {
        return farmerVillageName;
    }

    public void setFarmerVillageName(String farmerVillageName) {
        this.farmerVillageName = farmerVillageName;
    }


    //==============================================VILLAGE LIST================================================


    String villageName, objectID, gutID, gutName;

    public String getGutID() {
        return gutID;
    }

    public void setGutID(String gutID) {
        this.gutID = gutID;
    }

    public String getGutName() {
        return gutName;
    }

    public void setGutName(String gutName) {
        this.gutName = gutName;
    }

    public String getObjectID() {
        return objectID;
    }

    public void setObjectID(String objectID) {
        this.objectID = objectID;
    }

    public String getVillageName() {
        return villageName;
    }

    public void setVillageName(String villageName) {
        this.villageName = villageName;
    }


    //==============================================Rujvat Reason================================================

    String reason, reasonID;

    public String getReason() {
        return reason;
    }

    public void setReason(String reason) {
        this.reason = reason;
    }

    public String getReasonID() {
        return reasonID;
    }

    public void setReasonID(String reasonID) {
        this.reasonID = reasonID;
    }


    //==============================================ALL VILLAGE LIST DETAILS================================================

    private String VILLAGE, DISTRICT, TALUKA, VILLAGE_ID, SUB_GUT, GUT;

    public String getVILLAGE() {
        return VILLAGE;
    }

    public void setVILLAGE(String VILLAGE) {
        this.VILLAGE = VILLAGE;
    }

    public String getDISTRICT() {
        return DISTRICT;
    }

    public void setDISTRICT(String DISTRICT) {
        this.DISTRICT = DISTRICT;
    }

    public String getTALUKA() {
        return TALUKA;
    }

    public void setTALUKA(String TALUKA) {
        this.TALUKA = TALUKA;
    }

    public String getVILLAGE_ID() {
        return VILLAGE_ID;
    }

    public void setVILLAGE_ID(String VILLAGE_ID) {
        this.VILLAGE_ID = VILLAGE_ID;
    }

    public String getSUB_GUT() {
        return SUB_GUT;
    }

    public void setSUB_GUT(String SUB_GUT) {
        this.SUB_GUT = SUB_GUT;
    }

    public String getGUT() {
        return GUT;
    }

    public void setGUT(String GUT) {
        this.GUT = GUT;
    }
}
