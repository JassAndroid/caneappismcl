package com.praful.jass.ismclcaneapp.Models;
/**
 * Created by Jass-Services on 7/5/2018.
 */

public    class SlipBoyWorkStatusModel {

    private String EMP_ID;

    public String getEMP_ID() {
        return EMP_ID;
    }

    public void setEMP_ID(String EMP_ID) {
        this.EMP_ID=EMP_ID;
    }

    private String NAME;

    private String PLANTATION_CODE;

    private String ENTER_DATE;

    private String WORK_TYPE;

    public String getNAME ()
    {
        return NAME;
    }

    public void setNAME (String NAME)
    {
        this.NAME = NAME;
    }

    public String getPLANTATION_CODE ()
    {
        return PLANTATION_CODE;
    }

    public void setPLANTATION_CODE (String PLANTATION_CODE)
    {
        this.PLANTATION_CODE = PLANTATION_CODE;
    }

    public String getENTER_DATE ()
    {
        return ENTER_DATE;
    }

    public void setENTER_DATE (String ENTER_DATE)
    {
        this.ENTER_DATE = ENTER_DATE;
    }

    public String getWORK_TYPE ()
    {
        return WORK_TYPE;
    }

    public void setWORK_TYPE (String WORK_TYPE)
    {
        this.WORK_TYPE = WORK_TYPE;
    }

}
