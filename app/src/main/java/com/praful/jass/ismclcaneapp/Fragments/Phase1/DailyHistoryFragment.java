package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.app.ProgressDialog;
import android.icu.text.SimpleDateFormat;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Adapter.ListViewAdapter;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.DailyHistoryModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.List;


public class DailyHistoryFragment extends Fragment {
    private List<DailyHistoryModel> listItem = new ArrayList<>();
    private RecyclerView recyclerView;
    private ListViewAdapter mAdapter;
    private Toolbar toolbar;
    View view;
    TextView dailyHistoryMsg;

    @RequiresApi(api=Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.fragment_daily_history, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            dailyHistoryMsg = (TextView)view.findViewById(R.id.daily_history_msg);

            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.daily_history));

            mAdapter = new ListViewAdapter(getActivity(),listItem);
            getWorkHistory();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        // Inflate the layout for this fragment
        return view;
    }

    @RequiresApi(api=Build.VERSION_CODES.N)
    private void getWorkHistory() {

        Date c = Calendar.getInstance().getTime();
        SimpleDateFormat df = new SimpleDateFormat("dd-MMM-yyyy");
        String today = df.format(c);

        Calendar cal = Calendar.getInstance();
        cal.add(Calendar.DATE, -1);
        String yesterday = df.format(cal.getTime());

        final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();
        JSONObject object = new JSONObject();

        try {
            object.put("season", Preferences.getSeasonID());
            object.put("user_id", Preferences.getCurrentUserId());
            object.put("device", Preferences.getDeviceId());
            object.put("from_date", yesterday);
            object.put("to_date", today);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        new APIRestManager().postAPI(AppConstants.URL_GET_WORK_STATUS_OF_SLIP_BOY, object, getActivity(), DailyHistoryModel.class, new APIRestManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj) {

                listItem = (ArrayList<DailyHistoryModel>)resultObj;

                if(listItem.size()!=0){
                    
                    recyclerView.setVisibility(View.VISIBLE);
                    dailyHistoryMsg.setVisibility(View.GONE);
                    mAdapter.updateAdapter(listItem);

                }else{
                    
                    dailyHistoryMsg.setVisibility(View.VISIBLE);
                    recyclerView.setVisibility(View.GONE);

                }
                progressDialog.dismiss();
            }

            @Override
            public void onError(String strError) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), strError, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
