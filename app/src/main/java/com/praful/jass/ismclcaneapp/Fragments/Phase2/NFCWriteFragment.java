package com.praful.jass.ismclcaneapp.Fragments.Phase2;

import android.app.DialogFragment;
import android.content.Context;
import android.nfc.FormatException;
import android.nfc.NdefMessage;
import android.nfc.NdefRecord;
import android.nfc.tech.Ndef;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ProgressBar;
import android.widget.TextView;

import com.praful.jass.ismclcaneapp.Activity.Phase1.MenuActivityPhase1;
import com.praful.jass.ismclcaneapp.Interface.Listener;
import com.praful.jass.ismclcaneapp.R;

import java.io.IOException;
import java.io.UnsupportedEncodingException;

public class NFCWriteFragment extends DialogFragment {

    public static final String TAG = NFCWriteFragment.class.getSimpleName();

    public static NFCWriteFragment newInstance() {

        return new NFCWriteFragment();
    }

    private TextView mTvMessage, title;
    private ProgressBar mProgress;
    private Listener mListener;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
        View view = inflater.inflate(R.layout.fragment_write, container, false);
        initViews(view);
        MenuActivityPhase1.write = true;
        return view;
    }

    private void initViews(View view) {

        mTvMessage = (TextView) view.findViewById(R.id.tv_message);
        title = (TextView) view.findViewById(R.id.title);
        mProgress = (ProgressBar) view.findViewById(R.id.progress);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        mListener = (MenuActivityPhase1) context;
        mListener.onDialogDisplayed();
    }

    @Override
    public void onDetach() {
        super.onDetach();
        MenuActivityPhase1.write = false;
        mListener.onDialogDismissed();
    }

    public boolean onNfcDetected(Ndef ndef, String messageToWrite) {
        mProgress.setVisibility(View.VISIBLE);
        return writeToNfc(ndef, messageToWrite);
    }

    private boolean writeToNfc(Ndef ndef, String message) {
        boolean returnVal = false;
        mTvMessage.setText(getString(R.string.message_write_progress));
        if (message.equalsIgnoreCase("FlashBYSB")) {
            title.setText(getString(R.string.flash_nfc_tag));
        }
        if (ndef != null) {

            try {
                ndef.connect();
//                NdefRecord mimeRecord = NdefRecord.createMime("text/plain", message.getBytes(Charset.forName("US-ASCII")));
                NdefRecord mimeRecord = createRecord(message);
                ndef.writeNdefMessage(new NdefMessage(mimeRecord));
                ndef.close();
                //Write Successful
                if (message.equalsIgnoreCase("FlashBYSB")) {
                    mTvMessage.setText(getString(R.string.message_flash_success));
                } else {
                    mTvMessage.setText(getString(R.string.message_write_success));
                }
                returnVal = true;

            } catch (IOException | FormatException e) {
                e.printStackTrace();
                returnVal = false;
                mTvMessage.setText(getString(R.string.message_write_error));

            } finally {
                mProgress.setVisibility(View.GONE);
            }

        }
        return returnVal;
    }

    private NdefRecord createRecord(String text) throws UnsupportedEncodingException {
        String lang = "";
        byte[] textBytes = text.getBytes();
        byte[] langBytes = lang.getBytes("US-ASCII");
        int langLength = langBytes.length;
        int textLength = textBytes.length;
        byte[] payload = new byte[1 + langLength + textLength];

        // set status byte (see NDEF spec for actual bits)
        payload[0] = (byte) langLength;

        // copy langbytes and textbytes into payload
        System.arraycopy(langBytes, 0, payload, 1, langLength);
        System.arraycopy(textBytes, 0, payload, 1 + langLength, textLength);

        NdefRecord recordNFC = new NdefRecord(NdefRecord.TNF_WELL_KNOWN, NdefRecord.RTD_TEXT, new byte[0], payload);

        return recordNFC;
    }
}
