package com.praful.jass.ismclcaneapp.Activity.Phase1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.design.widget.Snackbar;
import android.support.v4.app.ActivityCompat;
import android.support.v7.app.AppCompatActivity;
import android.telephony.TelephonyManager;
import android.view.View;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.XMLManager;
import com.praful.jass.ismclcaneapp.Models.CaneQualityModel;
import com.praful.jass.ismclcaneapp.Models.CurrentSlipNumberModel;
import com.praful.jass.ismclcaneapp.Models.FactoryNameListModel;
import com.praful.jass.ismclcaneapp.Models.GrowerDetailListModel;
import com.praful.jass.ismclcaneapp.Models.HarvestorListModel;
import com.praful.jass.ismclcaneapp.Models.Irrigation_Cane_Type_and_Cane_Variety_list_model;
import com.praful.jass.ismclcaneapp.Models.PlantationDataForRujvatModel;
import com.praful.jass.ismclcaneapp.Models.RujvatPlantatonDataModel;
import com.praful.jass.ismclcaneapp.Models.RujvatReasonModel;
import com.praful.jass.ismclcaneapp.Models.SeasonIdModel;
import com.praful.jass.ismclcaneapp.Models.ShivarVillageListModel;
import com.praful.jass.ismclcaneapp.Models.VehicalListModel;
import com.praful.jass.ismclcaneapp.Models.VillageListAllDetailsModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.AppConstantsPhase_II;
import com.praful.jass.ismclcaneapp.Utils.CheckInternet;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.services.CallSoap;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

public class UpdateDatabase extends AppCompatActivity {

    CallSoap cs;
    public boolean APISTATUS = true;
    XMLManager xmlManager;
    SQLiteHelper sqLiteHelper;
    public ProgressDialog pdLoading;
    private String deviceID = null;
    private TelephonyManager telephonyManager;


    @Override
    protected void onCreate(Bundle savedInstanceState) {
        try {
            super.onCreate(savedInstanceState);
            setContentView(R.layout.activity_update_database);
            init();
            if (CheckInternet.isNetworkAvailable(this)) {
                new AsyncCaller().execute();
            } else {
                Snackbar bar = Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.internet_connection), Snackbar.LENGTH_INDEFINITE)
                        .setAction(getString(R.string.click_try_again), new View.OnClickListener() {
                            @Override
                            public void onClick(View v) {
                                Intent intent = new Intent(UpdateDatabase.this, UpdateDatabase.class);
                                startActivity(intent);
                                finish();
                            }
                        });
                bar.show();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void init() {
        try {
            cs = new CallSoap();
            xmlManager = new XMLManager();
            sqLiteHelper = new SQLiteHelper(this);
            pdLoading = new ProgressDialog(UpdateDatabase.this);
            pdLoading.setCancelable(false);
            telephonyManager = (TelephonyManager) this.getSystemService(this.TELEPHONY_SERVICE);

            if (android.os.Build.VERSION.SDK_INT >= android.os.Build.VERSION_CODES.M) {
                if (ActivityCompat.checkSelfPermission(this, Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                    // TODO: Consider calling
                    //    ActivityCompat#requestPermissions
                    return;
                }
                deviceID = telephonyManager.getDeviceId(2);
            } else {
                deviceID = telephonyManager.getDeviceId();//getDeviceId(1);
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private void getUpdateDatabase() {
        try {
            callServices();
        } catch (Exception e) {
            Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
            Preferences.setFirstTimeStatus(true);
        }
    }

    private void allServicesSucsessFull() {
        try {
            if (APISTATUS) {
                Preferences.setFirstTimeStatus(false);
                startActivity(new Intent(this, MenuActivityPhase1.class));
                overridePendingTransition(R.anim.slide_up, R.anim.slide_down);
                finish();
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void callServices() {
        try {
            callRespAPIForList(AppConstants.URL_SEASON_ID, SeasonIdModel.class, AppConstants.SEASON_ID_SELECT, UpdateDatabase.this, this.getWindow().getDecorView().findViewById(android.R.id.content));
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public void callRespAPIForList(String apiUrl, Class modelType, final int listType, Context context, final View v) {
        try {
            JSONObject jsonObject = new JSONObject();
            try {
                if (listType == AppConstants.GROWER_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
//                jsonObject.put("season", Preferences.getSeasonID() + "");
                } else if (listType == AppConstants.SHIVAR_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
//                jsonObject.put("season", Preferences.getSeasonID() + "");
                } else if (listType == AppConstants.RUJVAT_ALL_DATA_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.FARMER_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("user_id", Preferences.getCurrentUserId() + "");
                    jsonObject.put("season", Preferences.getSeasonID() + "");
//                    jsonObject.put("admin_flag", Preferences.getUserTypeFlag());
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else if (listType == AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT) {
                    jsonObject.put("season", Preferences.getSeasonID() + "");
                    jsonObject.put("device_id", deviceID + "");
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                } else {
                    jsonObject.put("P_LANG", Preferences.getLocalLanguage());
                }
            } catch (JSONException e) {
                e.printStackTrace();
            }

            if (APISTATUS) {
                new APIRestManager().postAPI(apiUrl, jsonObject, context, modelType, new APIRestManager.APIManagerInterface() {

                    @Override
                    public void onSuccess(Object resultObj) {

                        if (listType == AppConstants.RUJVAT_REASON_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String rujvatReasonList = null;
                                    rujvatReasonList = xmlManager.updatedRujvatReasonList(v, (ArrayList<RujvatReasonModel>) resultObj);
                                    if (!rujvatReasonList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertReasonList(rujvatReasonList);
                                        callRespAPIForList(AppConstantsPhase_II.URL_PLANTATION_LIST_TC, PlantationDataForRujvatModel.class, AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.GROWER_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String growerDetailList = null;
                                    growerDetailList = xmlManager.updatedFarmerList(v, (ArrayList<GrowerDetailListModel>) resultObj);
                                    if (!growerDetailList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertFarmerList(growerDetailList);
                                        callRespAPIForList(AppConstants.URL_VILLAGE_LIST, ShivarVillageListModel.class, AppConstants.SHIVAR_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.SHIVAR_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String shivarVillageList = null;
                                    shivarVillageList = xmlManager.updatedVillageList(v, (ArrayList<ShivarVillageListModel>) resultObj);
                                    if (!shivarVillageList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertVillageList(shivarVillageList);
                                        callRespAPIForList(AppConstants.URL_IRRIGATION_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.IRRIGATION_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.IRRIGATION_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String irrigationList = null;
                                    irrigationList = xmlManager.updatedSugarIrrigationMaster(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                    if (!irrigationList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertIrrigationMaster(irrigationList);
                                        callRespAPIForList(AppConstants.URL_CANE_VARITY_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.CAN_VERIETY_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.CAN_VERIETY_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String caneVeriety = null;
                                    caneVeriety = xmlManager.updatedCaneVerityList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                    if (!caneVeriety.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertCaneVerityList(caneVeriety);
                                        callRespAPIForList(AppConstants.URL_CANE_TYPE_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.CAN_TYPE_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.CAN_TYPE_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String caneType = null;
                                    caneType = xmlManager.updatedCaneTypeList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                    if (!caneType.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertCaneTypeList(caneType);
                                        callRespAPIForList(AppConstants.URL_PLANTATION_TYPE_LIST, Irrigation_Cane_Type_and_Cane_Variety_list_model.class, AppConstants.PLANTATION_TYPE_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.PLANTATION_TYPE_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String plantationType = null;
                                    plantationType = xmlManager.updatedPlantationList(v, (ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model>) resultObj);
                                    if (!plantationType.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertCanePlantationList(plantationType);
                                        callRespAPIForList(AppConstants.URL_ADD_RUJVAT_REASON, RujvatReasonModel.class, AppConstants.RUJVAT_REASON_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.SEASON_ID_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    ArrayList<SeasonIdModel> seasonIdList = new ArrayList();
                                    seasonIdList = (ArrayList<SeasonIdModel>) resultObj;
                                    if (!seasonIdList.get(0).getSEASON_ID().equalsIgnoreCase("0")) {
                                        Preferences.setSeasonID(seasonIdList.get(0).getSEASON_ID() + "");
                                        callRespAPIForList(AppConstants.URL_FARMER_LIST, GrowerDetailListModel.class, AppConstants.GROWER_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.RUJVAT_ALL_DATA_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    ArrayList<RujvatPlantatonDataModel> seasonIdList = (ArrayList<RujvatPlantatonDataModel>) resultObj;
                                    if (seasonIdList.size() > 0) {
                                        sqLiteHelper.deleteRujvatDataRJ();
                                        for (int i = 0; i < seasonIdList.size(); i++) {
                                            sqLiteHelper.insertRjvatDataRJ(seasonIdList.get(i).getPLANTATION_CODE(), seasonIdList.get(i).getFARMER_NAME(), seasonIdList.get(i).getFARMER_CODE(), seasonIdList.get(i).getVILLAGE_NAME(), seasonIdList.get(i).getGUT_NAME(), seasonIdList.get(i).getSUB_GUT_NAME(), seasonIdList.get(i).getTALUKA_NAME(), seasonIdList.get(i).getSHIVAR_VILLAGE_NAME(), seasonIdList.get(i).getSHIVAR_GUT_NAME(), seasonIdList.get(i).getSARVE_NO(), seasonIdList.get(i).getIRRE_NAME(), seasonIdList.get(i).getVARITY(), seasonIdList.get(i).getPLANTATION_DATE(), seasonIdList.get(i).getPLANTATION_TYPE(), seasonIdList.get(i).getPLANTATION_AREA(), seasonIdList.get(i).getOBJECT_ID());
                                        }
                                        APISTATUS = true;
                                        allServicesSucsessFull();

                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.rujvat_not_found), Snackbar.LENGTH_LONG).show();
                                        allServicesSucsessFull();
                                        APISTATUS = true;
                                        pdLoading.dismiss();
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstantsPhase_II.PLANTATION_LIST_TODA_CHITTI_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String planationDetailList = null;
                                    planationDetailList = xmlManager.jsonPlantationDataFORTC(v, (ArrayList<PlantationDataForRujvatModel>) resultObj);
                                    if (!planationDetailList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertPlantationList(planationDetailList);
                                        callRespAPIForList(AppConstantsPhase_II.URL_VEHICAL_LIST_TC, VehicalListModel.class, AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String vehicalList = null;
                                    vehicalList = xmlManager.jsonVehicalDataFORTC(v, (ArrayList<VehicalListModel>) resultObj);
                                    if (!vehicalList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertVehicleList(vehicalList);
                                        callRespAPIForList(AppConstantsPhase_II.URL_HARVESTOR_LIST_TC, HarvestorListModel.class, AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstantsPhase_II.HARVESTOR_LIST_TODA_CHITTI_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String harvesterList = null;
                                    harvesterList = xmlManager.jsonHarvesterDataFORTC(v, (ArrayList<HarvestorListModel>) resultObj);
                                    if (!harvesterList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertHarvesterList(harvesterList);
                                        callRespAPIForList(AppConstantsPhase_II.URL_CURRENT_SLIP_ID_LIST_TC, CurrentSlipNumberModel.class, AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    e.printStackTrace();
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstantsPhase_II.CURRENT_SLIP_ID_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    ArrayList<CurrentSlipNumberModel> currentSlipNumber = (ArrayList<CurrentSlipNumberModel>) resultObj;
                                    String slipNumber = currentSlipNumber.get(0).getNVL();
                                    Preferences.setCurrentSlipNumber(slipNumber);
                                    callRespAPIForList(AppConstants.URL_GET_ALL_VILLAGE_LIST_DETAILS, VillageListAllDetailsModel.class, AppConstants.VILLAGE_ALL_DETAILS_SELECT, UpdateDatabase.this, v);
                                    APISTATUS = true;
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                    pdLoading.dismiss();
                                    Toast.makeText(UpdateDatabase.this, e.getMessage(), Toast.LENGTH_SHORT).show();
                                }
                            }
                        } else if (listType == AppConstants.VILLAGE_ALL_DETAILS_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String villageDetailList = null;
                                    villageDetailList = xmlManager.jsonvillageListDetails(v, (ArrayList<VillageListAllDetailsModel>) resultObj);
                                    if (!villageDetailList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertAllVillageList(villageDetailList);
                                        callRespAPIForList(AppConstants.URL_CANE_QUALITY, CaneQualityModel.class, AppConstants.CANE_QUALITY_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.CANE_QUALITY_SELECT) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String caneQualityList = null;
                                    caneQualityList = xmlManager.jsonCaneQualityList(v, (ArrayList<CaneQualityModel>) resultObj);
                                    if (!caneQualityList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertAllCaneQuality(caneQualityList);
                                        callRespAPIForList(AppConstants.URL_GET_FACTORY_NAME_LIST, FactoryNameListModel.class, AppConstants.FACTORY_NAME_LIST, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                    pdLoading.dismiss();
                                }
                            }
                        } else if (listType == AppConstants.FACTORY_NAME_LIST) {
                            if (resultObj instanceof ArrayList) {
                                try {
                                    String factoryNameList = null;
                                    factoryNameList = xmlManager.jsonFactoryNameList(v, (ArrayList<FactoryNameListModel>) resultObj);
                                    if (!factoryNameList.equalsIgnoreCase("0")) {
                                        sqLiteHelper.insertFactoryNameList(factoryNameList);
                                        callRespAPIForList(AppConstants.URL_GET_ALL_PLANTATION_FOR_RUJVAT, RujvatPlantatonDataModel.class, AppConstants.RUJVAT_ALL_DATA_SELECT, UpdateDatabase.this, v);
                                        APISTATUS = true;
                                    } else {
                                        Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                        APISTATUS = false;
                                    }
                                } catch (Exception e) {
                                    APISTATUS = false;
                                    Snackbar.make(findViewById(R.id.activity_login), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                                    e.printStackTrace();
                                    pdLoading.dismiss();
                                }
                            }
                        }

                    }

                    @Override
                    public void onError(String strError) {
//                    output = AppConstants.SERVER_NOT_RESPONDING_ERROR;
                        APISTATUS = false;
                        Snackbar.make(findViewById(R.id.activity_update_database), getString(R.string.server_not_responding), Snackbar.LENGTH_LONG).show();
                        pdLoading.dismiss();
                    }
                });
            }
        } catch (Exception ex) {
            Toast.makeText(this, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    private class AsyncCaller extends AsyncTask<Void, Void, Void> {

        @Override
        protected void onPreExecute() {
            try {
                super.onPreExecute();

                //this method will be running on UI thread
                runOnUiThread(new Runnable() {
                    @Override
                    public void run() {
                        //stuff that updates ui
                        pdLoading.setMessage("\t" + getString(R.string.fetching_data));
                        pdLoading.show();
                    }
                });
            } catch (Exception ex) {
                Toast.makeText(UpdateDatabase.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //this method will be running on background thread so don't update UI frome here
                //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
                getUpdateDatabase();
            } catch (Exception ex) {
                Toast.makeText(UpdateDatabase.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            try {
                super.onPostExecute(result);
                //this method will be running on UI thread
//            pdLoading.dismiss();
            } catch (Exception ex) {
                Toast.makeText(UpdateDatabase.this, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
