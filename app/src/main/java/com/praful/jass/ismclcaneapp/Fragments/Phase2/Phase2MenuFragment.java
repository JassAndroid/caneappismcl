package com.praful.jass.ismclcaneapp.Fragments.Phase2;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.RelativeLayout;

import com.praful.jass.ismclcaneapp.Fragments.Phase1.EmployeeTripFragment;

import com.praful.jass.ismclcaneapp.R;

public class Phase2MenuFragment extends Fragment {
    RelativeLayout rlEmployeeTrip;
    RelativeLayout rlGenerateToken;
    RelativeLayout rlTransferToken;
    View view;
    FragmentTransaction fragmentTransaction;

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
         view = inflater.inflate(R.layout.fragment_phase2_menu, container, false);
        // Inflate the layout for this fragment
        init();
        return view;
    }

    private void init(){
        rlEmployeeTrip = (RelativeLayout) view.findViewById(R.id.menu_employee_trip);
        rlGenerateToken = (RelativeLayout) view.findViewById(R.id.menu_generate_token);
        rlTransferToken = (RelativeLayout) view.findViewById(R.id.menu_transfer_token);

        FragmentManager fragmentManager = getFragmentManager();
        fragmentTransaction = fragmentManager.beginTransaction();
        setOnClickListner();
    }

    private void setOnClickListner(){
        rlEmployeeTrip.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                EmployeeTripFragment employeeTripFragment = new EmployeeTripFragment();
                fragmentTransaction.replace(R.id.frame_phase2, employeeTripFragment);
                fragmentTransaction.commit();
            }
        });

        rlGenerateToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Phase2GenerateTokenFragment generateTokenFragment = new Phase2GenerateTokenFragment();
                fragmentTransaction.replace(R.id.frame_phase2,generateTokenFragment);
                fragmentTransaction.commit();
            }
        });

        rlTransferToken.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                Phase2TransferTokenFragment transferTokenFragment = new Phase2TransferTokenFragment();
                fragmentTransaction.replace(R.id.frame_phase2,transferTokenFragment);
                fragmentTransaction.commit();
            }
        });
    }


}
