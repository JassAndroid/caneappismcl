package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.app.ProgressDialog;
import android.content.Intent;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.RequiresApi;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Activity.Phase1.SlipBoyWorkListActivity;
import com.praful.jass.ismclcaneapp.Adapter.SlipBoyListAdapter;
import com.praful.jass.ismclcaneapp.DataSet.DataSet;
import com.praful.jass.ismclcaneapp.ManagerClasses.APIRestManager;
import com.praful.jass.ismclcaneapp.Models.SlipBoyWorkStatusModel;
import com.praful.jass.ismclcaneapp.Models.SlipCandidateModel;
import com.praful.jass.ismclcaneapp.Models.WorkTypeModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.Preferences;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;
import java.util.List;


public class SlipBoyListFragment extends Fragment {

    private List<SlipBoyWorkStatusModel> listItem = new ArrayList<>();
    private RecyclerView recyclerView;
    private SlipBoyListAdapter mAdapter;
    private Toolbar toolbar;
    View view;
    TextView dailyHistoryMsg;
    SlipBoyListAdapter.SlipBoyWorkStatusListener slipBoyWorkStatusListener;
    private OptionListFragment fragment;
    ArrayList<SlipCandidateModel> mList = new ArrayList<>();
    ArrayList<WorkTypeModel> mListWorkType = new ArrayList<>();
    ArrayList<String> newListWorkType = new ArrayList<>();
    private String purpose = "";
    FragmentTransaction fragmentTransaction;

    @RequiresApi(api = Build.VERSION_CODES.N)
    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        try {
            view = inflater.inflate(R.layout.fragment_slip_boy_list, container, false);
            recyclerView = (RecyclerView) view.findViewById(R.id.recycler_view);
            dailyHistoryMsg = (TextView) view.findViewById(R.id.daily_history_msg);

            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.slip_boy_work_status));

            getWorkTypeList();

            FragmentManager fragmentManager = getFragmentManager();
            fragmentTransaction = fragmentManager.beginTransaction();
            fragmentTransaction.setCustomAnimations(R.anim.enter_from_left, R.anim.exit_to_right, R.anim.enter_from_right, R.anim.exit_to_left);
            slipBoyWorkStatusListener = new SlipBoyListAdapter.SlipBoyWorkStatusListener() {
                @Override
                public void itemClick(int id) {
                    startActivity(new Intent(getActivity(), SlipBoyWorkListActivity.class));
                    /*DailyHistoryFragment dailyHistoryFragment = new DailyHistoryFragment();
                    fragmentTransaction.replace(R.id.frame, dailyHistoryFragment);
                    fragmentTransaction.commit();*/
                }
            };

            mAdapter = new SlipBoyListAdapter(getActivity(), slipBoyWorkStatusListener, mList);
            // getWorkHistory();
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(getActivity().getApplicationContext());
            recyclerView.setLayoutManager(mLayoutManager);
            recyclerView.setItemAnimator(new DefaultItemAnimator());
            recyclerView.setAdapter(mAdapter);

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        // Inflate the layout for this fragment
        return view;
    }

    @Override
    public void onActivityCreated(Bundle savedInstanceState) {
        super.onActivityCreated(savedInstanceState);

        Bundle bundle = this.getArguments();
        if (bundle != null) {
            purpose = bundle.getString("Purpose");
        }
    }

    public void showOptionList() {
        final DataSet dataSet = new DataSet();
        String title = "Nothing";

        //mList1 = dataSet.getTripBoyOptions(getContext());
        final FragmentTransaction ft = (FragmentTransaction) getFragmentManager().beginTransaction();
        ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

        fragment = new OptionListFragment();
        fragment.updateList(newListWorkType, title);
        fragment.setListTitle("Slip Boy Options");

        //final JSONArray finalJsonArray = jsonArray;
        fragment.listner = new OptionListFragment.OptionListInterface() {
            @Override
            public void onListDismiss() {
                FragmentManager fragmentManager = getActivity().getSupportFragmentManager();
                fragmentTransaction = fragmentManager.beginTransaction();
                MenuFragment menuFragment = new MenuFragment();
                fragmentTransaction.setCustomAnimations(R.anim.enter_from_right, R.anim.exit_to_left, R.anim.enter_from_left, R.anim.exit_to_right);
                fragmentTransaction.replace(R.id.frame, menuFragment);
                fragmentTransaction.commit();
                toolbar.setTitle(getString(R.string.nav_menu));
            }

            @Override
            public void onListItemSelected(Object model) {
                //String flag;
                removeOptionList();
                if (model.equals("PLANTATION")) {
                    AppConstants.FLAG_WORK_TYPE_SELECTED = "PL";
                } else if (model.equals("CUTTING ORDER")) {
                    AppConstants.FLAG_WORK_TYPE_SELECTED = "CO";
                } else if (model.equals("KYC")) {
                    AppConstants.FLAG_WORK_TYPE_SELECTED = "KY";
                } else if (model.equals("RUJVAT")) {
                    AppConstants.FLAG_WORK_TYPE_SELECTED = "RJ";
                }
                if (purpose.equalsIgnoreCase("history"))
                    startActivity(new Intent(getActivity(), SlipBoyWorkListActivity.class).putExtra("Purpose", "history"));
                else
                    getWorkCandidateList();
                //startActivity(new Intent(getActivity(), SlipOptionDataActivity.class));
            }
        };
        ft.add(R.id.fragment_slip_boy, fragment);
        ft.commit();
        //pdLoading.dismiss();
    }

    void removeOptionList() {
        try {
            final FragmentTransaction ft = (FragmentTransaction) getFragmentManager().beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            ft.remove(fragment).commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * API CALLING
     **/

    private void getWorkTypeList() {

        /*final ProgressDialog progressDialog = new ProgressDialog(getActivity());
        progressDialog.show();*/
        JSONObject object = new JSONObject();

        new APIRestManager().postJSONArrayAPI(AppConstants.URL_GET_WORK_TYPE_LIST, object, WorkTypeModel.class, getActivity(), new APIRestManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj) {

                mListWorkType = (ArrayList<WorkTypeModel>) resultObj;

                for (WorkTypeModel obj :
                        mListWorkType) {
                    newListWorkType.add(obj.getNAME());
                }
                showOptionList();
                // progressDialog.dismiss();
            }

            @Override
            public void onError(String strError) {
                // progressDialog.dismiss();
                Toast.makeText(getContext(), strError, Toast.LENGTH_SHORT).show();
            }
        });
    }

    private void getWorkCandidateList() {

        final ProgressDialog progressDialog = new ProgressDialog(getContext());
        progressDialog.show();
        JSONObject object = new JSONObject();
        try {
            object.put("user_id", Preferences.getCurrentUserId());
            object.put("season", Preferences.getSeasonID());
            //object.put("work_type",AppConstants.FLAG_WORK_TYPE_SELECTED);
        } catch (JSONException e) {
            e.printStackTrace();
        }

        String url = AppConstants.URL_GET_WORK_CANDIDATE_LIST;//+"?season="+Preferences.getSeasonID()+"&user_id="+AppConstants.USER_ID;

        new APIRestManager().postJSONArrayAPI(url, object, SlipCandidateModel.class, getActivity(), new APIRestManager.APIManagerInterface() {
            @Override
            public void onSuccess(Object resultObj) {
                progressDialog.dismiss();
                mList = (ArrayList<SlipCandidateModel>) resultObj;
                mAdapter.updateAdapter(mList);
            }

            @Override
            public void onError(String strError) {
                progressDialog.dismiss();
                Toast.makeText(getActivity(), strError, Toast.LENGTH_SHORT).show();
            }
        });
    }
}
