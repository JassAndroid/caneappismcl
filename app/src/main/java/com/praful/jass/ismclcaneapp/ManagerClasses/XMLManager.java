package com.praful.jass.ismclcaneapp.ManagerClasses;

import android.support.design.widget.Snackbar;
import android.view.View;

import com.praful.jass.ismclcaneapp.Models.CaneQualityModel;
import com.praful.jass.ismclcaneapp.Models.FactoryNameListModel;
import com.praful.jass.ismclcaneapp.Models.GrowerDetailListModel;
import com.praful.jass.ismclcaneapp.Models.HarvestorListModel;
import com.praful.jass.ismclcaneapp.Models.Irrigation_Cane_Type_and_Cane_Variety_list_model;
import com.praful.jass.ismclcaneapp.Models.PlantationDataForRujvatModel;
import com.praful.jass.ismclcaneapp.Models.RujvatReasonModel;
import com.praful.jass.ismclcaneapp.Models.ShivarVillageListModel;
import com.praful.jass.ismclcaneapp.Models.VehicalListModel;
import com.praful.jass.ismclcaneapp.Models.VillageListAllDetailsModel;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;
import org.ksoap2.serialization.SoapObject;

import java.util.ArrayList;

/**
 * Created by Praful on 30-03-2017.
 */

public class XMLManager {


    public String getLoginData(SoapObject response, View view) {
        String responseString = response.toString();
        System.out.println(responseString);
        if (response.toString().contains("MsgResult=No Records")) {//anyType{MobileServicesParam=anyType{MsgResult=No Records.; }; }

            Snackbar.make(view, "NO Record Found", Snackbar.LENGTH_LONG).show();
            return "0";
        } else {
            int outerPropertyCount = response.getPropertyCount();

            for (int i = 0; i < outerPropertyCount; i++) {
                SoapObject obj = (SoapObject) response.getProperty(i);
                AppConstants.loginResponceModel.setUserId(obj.getPrimitivePropertyAsString("UserId"));
                return AppConstants.loginResponceModel.getUserId();
            }
        }
        return "0";
    }

    public String updatedCaneTypeList(View view, ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("caneTypeID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("caneTypeName", arrayList.get(i).getNAME());
                jsonArray.put(jsonObject);

            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String updatedCaneVerityList(View view, ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("caneVerityID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("caneVerityName", arrayList.get(i).getNAME());
                jsonArray.put(jsonObject);

            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }

        return returnData;
    }


    public String updatedPlantationList(View view, ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("plantationID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("plantationName", arrayList.get(i).getNAME());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }

        return returnData;
    }


    public String updatedSugarIrrigationMaster(View view, ArrayList<Irrigation_Cane_Type_and_Cane_Variety_list_model> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("irrigationMasterID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("irrigationMasterName", arrayList.get(i).getNAME());
                jsonArray.put(jsonObject);

            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }

        return returnData;
    }

    public String updatedFarmerList(View view, ArrayList<GrowerDetailListModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("farmerName", arrayList.get(i).getFARMER_NAME());
                jsonObject.put("farmerCode", arrayList.get(i).getFARMER_CODE());
                jsonObject.put("farmerID", arrayList.get(i).getFARMER_ID());
                jsonObject.put("farmerVillageName", arrayList.get(i).getVILLAGE_NAME());
                jsonObject.put("farmerObjectID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("farmerGutID", arrayList.get(i).getGUT_ID());
                jsonObject.put("farmerGutName", arrayList.get(i).getGUT_NAME());
                jsonObject.put("farmerSubgutID", arrayList.get(i).getSUBGUT_ID());
                jsonObject.put("farmerSubgutName", arrayList.get(i).getSUB_GUT_NAME());
                jsonObject.put("farmerTalukaID", arrayList.get(i).getTALUKA_ID());
                jsonObject.put("farmerTalukaName", arrayList.get(i).getTALUKA_NAME());
                jsonArray.put(jsonObject);

            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonPlantationDataFORTC(View view, ArrayList<PlantationDataForRujvatModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("PLANTATION_CODE", arrayList.get(i).getPLANTATION_CODE());
                jsonObject.put("SHIVAR_VILLAGE_NAME", arrayList.get(i).getSHIVAR_VILLAGE_NAME());
                jsonObject.put("SHIVAR_GUT_NAME", arrayList.get(i).getSHIVAR_GUT_NAME());
                jsonObject.put("SARVE_NO", arrayList.get(i).getSARVE_NO());
                jsonObject.put("IRRE_NAME", arrayList.get(i).getIRRE_NAME());
                jsonObject.put("VARITY", arrayList.get(i).getVARITY());
                jsonObject.put("PLANTATION_DATE", arrayList.get(i).getPLANTATION_DATE());
                jsonObject.put("PLANTATION_TYPE", arrayList.get(i).getPLANTATION_TYPE());
                jsonObject.put("OBJECT_ID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("FARMER_NAME", arrayList.get(i).getFARMER_NAME());
                jsonObject.put("FARMER_CODE", arrayList.get(i).getFARMER_CODE());
                jsonObject.put("FARMER_ID", arrayList.get(i).getFARMER_ID());
                jsonObject.put("FARMER_VILLAGE", arrayList.get(i).getFARMER_VILLAGE());
                jsonObject.put("FARMER_GUT", arrayList.get(i).getFARMER_GUT());
                jsonObject.put("FARMER_SUBGUT", arrayList.get(i).getFARMER_SUBGUT());
                jsonObject.put("FARMER_TALUKA", arrayList.get(i).getFARMER_TALUKA());
                jsonObject.put("CUTTING_OBJECT_ID", arrayList.get(i).getCUTTING_OBJECT_ID());
                jsonObject.put("HRV_CODE", arrayList.get(i).getHRV_CODE());
                jsonObject.put("HRV_NAME", arrayList.get(i).getHRV_NAME());
                jsonObject.put("PLOT_START_DATE", arrayList.get(i).getPLOT_START_DATE());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonVehicalDataFORTC(View view, ArrayList<VehicalListModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("VEHICAL_NO", arrayList.get(i).getVEHICAL_NO());
                jsonObject.put("VNO_OBJECT_ID", arrayList.get(i).getVNO_OBJECT_ID());
                jsonObject.put("TRANSPORTER_ID", arrayList.get(i).getTRANSPORTER_ID());
                jsonObject.put("TR_NAME", arrayList.get(i).getTR_NAME());
                jsonObject.put("CODE", arrayList.get(i).getCODE());
                jsonObject.put("VEHICAL_TYPE", arrayList.get(i).getVEHICAL_TYPE());
                jsonObject.put("V_NAME", arrayList.get(i).getV_NAME());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonHarvesterDataFORTC(View view, ArrayList<HarvestorListModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("H_NAME", arrayList.get(i).getH_NAME());
                jsonObject.put("CODE", arrayList.get(i).getCODE());
                jsonObject.put("OBJECT_ID", arrayList.get(i).getOBJECT_ID());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonvillageListDetails(View view, ArrayList<VillageListAllDetailsModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("VILLAGE_ID", arrayList.get(i).getVILLAGE_ID());
                jsonObject.put("VILLAGE", arrayList.get(i).getVILLAGE());
                jsonObject.put("TALUKA", arrayList.get(i).getTALUKA());
                jsonObject.put("DISTRICT", arrayList.get(i).getDISTRICT());
                jsonObject.put("GUT", arrayList.get(i).getGUT());
                jsonObject.put("SUB_GUT", arrayList.get(i).getSUB_GUT());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonCaneQualityList(View view, ArrayList<CaneQualityModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("NAME", arrayList.get(i).getNAME());
                jsonObject.put("QUALITY_ID", arrayList.get(i).getQUALITY_ID());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String jsonFactoryNameList(View view, ArrayList<FactoryNameListModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("ID", arrayList.get(i).getID());
                jsonObject.put("SHORT_NAME", arrayList.get(i).getSHORT_NAME());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

    public String updatedVillageList(View view, ArrayList<ShivarVillageListModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject = new JSONObject();
                jsonObject.put("villageName", arrayList.get(i).getVILLAGE_NAME());
                jsonObject.put("objectID", arrayList.get(i).getOBJECT_ID());
                jsonObject.put("gutID", arrayList.get(i).getGUT_ID());
                jsonObject.put("gutName", arrayList.get(i).getGUT_NAME());
                jsonArray.put(jsonObject);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();

        } catch (JSONException e) {
            e.printStackTrace();
//            Snackbar.make(view, "NO Record Found", Snackbar.LENGTH_LONG).show();
            returnData = "0";
        }

        return returnData;
    }

    public String updatedRujvatReasonList(View view, ArrayList<RujvatReasonModel> arrayList) {
        String returnData = "";
        JSONObject returnJsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            for (int i = 0; i < arrayList.size(); i++) {
                JSONObject jsonObject1 = new JSONObject();
                jsonObject1.put("reasonID", arrayList.get(i).getOBJECT_ID());
                jsonObject1.put("reason", arrayList.get(i).getNAME());
                jsonArray.put(jsonObject1);
            }
            returnJsonObject.put("status", "SUCCESS");
            returnJsonObject.put("response", jsonArray);
            returnData = returnJsonObject.toString();
        } catch (JSONException e) {
            e.printStackTrace();
            returnData = "0";
        }
        return returnData;
    }

//    public int getUserSession(SoapObject response, View view) {
//        String responseString = response.toString();
//        System.out.println(responseString);
//        if (response.toString().contains("MsgResult=Server Error")) {//anyType{MobileServicesParam=anyType{MsgResult=No Records.; }; }
//
//            Snackbar.make(view, "NO Record Found", Snackbar.LENGTH_LONG).show();
//            return 0;
//        } else {
//            int outerPropertyCount = response.getPropertyCount();
//
//            for (int i = 0; i < outerPropertyCount; i++) {
//                SoapObject obj = (SoapObject) response.getProperty(i);
//                AppConstants.SESSION_ID = Integer.parseInt(obj.getPrimitivePropertyAsString("NAME"));
//            }
//        }
//        return AppConstants.SESSION_ID;
//    }

    public int getPlantationIDFromPlantation(SoapObject response, View view) {
        int plantationID = 0;
        String responseString = response.toString();
        System.out.println(responseString);
        try {
            if (response.toString().contains("MsgResult=Server Error")) {//anyType{MobileServicesParam=anyType{MsgResult=No Records.; }; }

                Snackbar.make(view, "NO Record Found", Snackbar.LENGTH_LONG).show();
                return 0;
            } else {
                int outerPropertyCount = response.getPropertyCount();

                for (int i = 0; i < outerPropertyCount; i++) {
                    SoapObject obj = (SoapObject) response.getProperty(i);
                    plantationID = Integer.parseInt(obj.getPrimitivePropertyAsString("p_out"));
                }
            }
        } catch (Exception e) {
            plantationID = 0;
        }
        return plantationID;
    }
}
