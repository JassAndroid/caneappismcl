package com.praful.jass.ismclcaneapp.Adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CheckBox;
import android.widget.CompoundButton;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.Dialog.FieldSlipDetailsDialog;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.Models.CaneQualityModel;
import com.praful.jass.ismclcaneapp.Models.PlantationDataOfTCModel;
import com.praful.jass.ismclcaneapp.Models.VehicalListModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.AppConstantsPhase_II;

import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Praful on 10/08/2017.
 */

public class TodChittiListViewAdapter extends RecyclerView.Adapter<TodChittiListViewAdapter.MyViewHolder> {
    private ArrayList<PlantationDataOfTCModel> listItems;
    public static ArrayList<PlantationDataOfTCModel> selectedTodChitti = new ArrayList<>();
    Context context;

    public class MyViewHolder extends RecyclerView.ViewHolder {
        public TextView plantationId, farmerName, sendby, txt_nfc_internet, ropTypeTC;
        public CheckBox selectTodChitti;
        public CardView todChittiItem;

        public MyViewHolder(View view) {
            super(view);
            plantationId = (TextView) view.findViewById(R.id.plantationIDTC);
            farmerName = (TextView) view.findViewById(R.id.farmerNameTC);
            ropTypeTC = (TextView) view.findViewById(R.id.ropTypeTC);
            sendby = (TextView) view.findViewById(R.id.sendby);
            txt_nfc_internet = (TextView) view.findViewById(R.id.txt_nfc_internet);
            selectTodChitti = (CheckBox) view.findViewById(R.id.selectTodChitti);
            todChittiItem = (CardView) view.findViewById(R.id.stripColor);
        }
    }

    public TodChittiListViewAdapter(ArrayList<PlantationDataOfTCModel> listItems, Context context) {
        this.listItems = listItems;
        this.context = context;
    }


    @Override
    public MyViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View itemView = LayoutInflater.from(parent.getContext())
                .inflate(R.layout.tod_chitti_list_item, parent, false);
        return new MyViewHolder(itemView);
    }

    @Override
    public void onBindViewHolder(final MyViewHolder holder, final int position) {
        try {
            for (int j = 0; j < listItems.size(); j++) {
                PlantationDataOfTCModel model = listItems.get(position);
                holder.plantationId.setText(model.getPLANTATION_CODE_TC());
                holder.farmerName.setText(model.getFARMER_NAME_TC());
                holder.ropTypeTC.setText(model.getROP_TYPE_TC());
                holder.itemView.setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View v) {
                        showDetailsPopup(context, position, listItems);
                    }
                });
                holder.selectTodChitti.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
                    @Override
                    public void onCheckedChanged(CompoundButton compoundButton, boolean b) {
                        if (b) {
                            if (selectedTodChitti.size() < 4) {
                                if (selectedTodChitti.size() < 1) {
                                    selectedTodChitti.add(listItems.get(position));
                                } else if (selectedTodChitti.size() > 1) {
                                    if (listItems.get(position).getROP_TYPE_TC().equals("D")) {
                                        selectedTodChitti.add(listItems.get(position));
                                    } else {
                                        holder.selectTodChitti.setChecked(false);
                                        Toast.makeText(compoundButton.getContext(), "Rop Type Double Can Write Only multipaltimes", Toast.LENGTH_SHORT).show();
                                    }
                                } else if (selectedTodChitti.size() == 1 && selectedTodChitti.get(0).getROP_TYPE_TC().equals("S") && listItems.get(position).getROP_TYPE_TC().equals("D")) {
                                    holder.selectTodChitti.setChecked(false);
                                    Toast.makeText(compoundButton.getContext(), "Rop Type Single Can Write Only 1", Toast.LENGTH_SHORT).show();
                                } else if (selectedTodChitti.size() == 1 && selectedTodChitti.get(0).getROP_TYPE_TC().equals("D") && listItems.get(position).getROP_TYPE_TC().equals("S")) {
                                    holder.selectTodChitti.setChecked(false);
                                    Toast.makeText(compoundButton.getContext(), "Rop Type Single Can Write Only 1", Toast.LENGTH_SHORT).show();
                                } else if (selectedTodChitti.size() == 1 && selectedTodChitti.get(0).getROP_TYPE_TC().equals("D") && listItems.get(position).getROP_TYPE_TC().equals("D")) {
                                    selectedTodChitti.add(listItems.get(position));
                                } else {
                                    holder.selectTodChitti.setChecked(false);
                                    Toast.makeText(compoundButton.getContext(), "Rop Type Single Can Write Only 1", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                holder.selectTodChitti.setChecked(false);
                                Toast.makeText(compoundButton.getContext(), "You can select only 4 Field slip", Toast.LENGTH_SHORT).show();
                            }
                        } else {
                            for (int i = 0; i < selectedTodChitti.size(); i++) {
                                if (selectedTodChitti.get(i).getPLANTATION_CODE_TC().equals(listItems.get(position).getPLANTATION_CODE_TC())) {
                                    selectedTodChitti.remove(i);
                                }
                            }
//                        Toast.makeText(compoundButton.getContext(), "Unchecked", Toast.LENGTH_SHORT).show();
                        }
                    }
                });
                if (model.getFLAG().equals("Y") && model.getFLAG_ONLINE_SUBMIT_TC().equals("N")) {
                    holder.todChittiItem.setCardBackgroundColor(Color.rgb(243, 107, 107));
                    holder.txt_nfc_internet.setText(AppConstants.SEND_BY_NFC);
                }
                if (model.getFLAG().equals("N") && !model.getFLAG_ONLINE_SUBMIT_TC().equals("N")) {
                    holder.todChittiItem.setCardBackgroundColor(Color.rgb(255, 140, 0));
                    holder.txt_nfc_internet.setText(AppConstants.SEND_BY_NFC + " And" + AppConstants.SEND_BY_INTERNET);
                }
                if (model.getFLAG().equals("Y") && !model.getFLAG_ONLINE_SUBMIT_TC().equals("N")) {
                    holder.todChittiItem.setCardBackgroundColor(Color.rgb(229, 204, 255));
                    holder.txt_nfc_internet.setText(AppConstants.SEND_BY_INTERNET);
                }
            }
        } catch (Exception ex) {
            Toast.makeText(holder.plantationId.getContext(), "Unchecked", Toast.LENGTH_SHORT).show();
        }
    }

    private void showDetailsPopup(final Context context, int position, ArrayList<PlantationDataOfTCModel> selectedSlip) {
        try {
            final FieldSlipDetailsDialog fieldSlipDetailsDialog = new FieldSlipDetailsDialog(context);
            fieldSlipDetailsDialog.show();
            SQLiteHelper sqLiteHelper = new SQLiteHelper(context);
//            fieldSlipDetailsDialog.setCancelable(false);
            fieldSlipDetailsDialog.txtPlantationID.setText(selectedSlip.get(position).getPLANTATION_CODE_TC());
            fieldSlipDetailsDialog.grower_name.setText(selectedSlip.get(position).getFARMER_NAME_TC());
            fieldSlipDetailsDialog.txt_village.setText(selectedSlip.get(position).getFARMER_VILLAGE_TC());
            fieldSlipDetailsDialog.txt_shivarVillage.setText(selectedSlip.get(position).getSHIVAR_VILLAGE_TC());
            fieldSlipDetailsDialog.txt_shivarGut.setText(selectedSlip.get(position).getSHIVAR_GUT_TC());
            fieldSlipDetailsDialog.txt_surveyNumber.setText(selectedSlip.get(position).getSURVEY_NUMBER_TC());
            fieldSlipDetailsDialog.txt_irrigationSource.setText(selectedSlip.get(position).getIRRIGATION_TC());
            fieldSlipDetailsDialog.txt_caneVerity.setText(selectedSlip.get(position).getCANE_VERITY_TC());
            fieldSlipDetailsDialog.txt_plantationDate.setText(selectedSlip.get(position).getPLANTATION_DATE_TC());
            fieldSlipDetailsDialog.txt_plantationType.setText(selectedSlip.get(position).getPLANTATION_TYPE_TC());
            fieldSlipDetailsDialog.txt_village.setText(selectedSlip.get(position).getFARMER_VILLAGE_TC());
            fieldSlipDetailsDialog.txt_gut.setText(selectedSlip.get(position).getFARMER_GUT_TC());
            fieldSlipDetailsDialog.txt_subgut.setText(selectedSlip.get(position).getFARMER_SUBGUT_TC());
            fieldSlipDetailsDialog.txt_taluka.setText(selectedSlip.get(position).getFARMER_TALUKA_TC());
            fieldSlipDetailsDialog.txtHarvestorCode.setText(selectedSlip.get(position).getHRV_NAME_TC());
            fieldSlipDetailsDialog.txtVehicalNumber.setText(selectedSlip.get(position).getVEHICAL_NUMBER_TC());
            fieldSlipDetailsDialog.txtVehicalType.setText(getJsoneManager(sqLiteHelper.getVehicleList(), VehicalListModel.class, AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT, selectedSlip.get(position).getVEHICAL_TYPE()));
            fieldSlipDetailsDialog.txtTransportCode.setText(selectedSlip.get(position).getTRANSPORTER_NAME_TC());
            fieldSlipDetailsDialog.txtGrowarFactoryName.setText(selectedSlip.get(position).getFACTORY_NAME_TC());
            fieldSlipDetailsDialog.txt_driver_name.setText(selectedSlip.get(position).getDRIVER_NAME_TC());
            fieldSlipDetailsDialog.txt_rop_type.setText(getropType(selectedSlip.get(position).getROP_TYPE_TC()));
            fieldSlipDetailsDialog.txttrly1.setText(selectedSlip.get(position).getTRAILER_1());
            fieldSlipDetailsDialog.txttrly2.setText(selectedSlip.get(position).getTRAILER_2());
            fieldSlipDetailsDialog.txt_cane_quality.setText(getJsoneManager(sqLiteHelper.getAllCaneQuality(), CaneQualityModel.class, AppConstantsPhase_II.CANE_QUALITY_SELECT, selectedSlip.get(position).getCANE_QUALITY_TC()));

            fieldSlipDetailsDialog.btn_submit.setOnClickListener(new View.OnClickListener() {
                                                                     @Override
                                                                     public void onClick(final View view) {
                                                                         try {
                                                                             fieldSlipDetailsDialog.dismiss();
                                                                         } catch (Exception ex) {
                                                                             Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
                                                                         }
                                                                     }
                                                                 }
            );
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String getJsoneManager(String jsone, Class model, final int selectListType, final String valueID) {
        final String[] returnVal = new String[1];
        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), model, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    if (resultObj instanceof ArrayList) {
                        if (selectListType == AppConstantsPhase_II.CANE_QUALITY_SELECT) {
                            ArrayList<CaneQualityModel> caneQualityList = (ArrayList<CaneQualityModel>) resultObj;
                            for (int i = 0; i < caneQualityList.size(); i++) {
                                if (caneQualityList.get(i).getQUALITY_ID().equalsIgnoreCase(valueID)) {
                                    returnVal[0] = caneQualityList.get(i).getNAME();
                                }
                            }
                        } else if (resultObj instanceof ArrayList) {
                            if (selectListType == AppConstantsPhase_II.VEHICAL_LIST_TODA_CHITTI_SELECT) {
                                ArrayList<VehicalListModel> vehicalListModels = (ArrayList<VehicalListModel>) resultObj;
                                for (int i = 0; i < vehicalListModels.size(); i++) {
                                    if (vehicalListModels.get(i).getVEHICAL_TYPE().equalsIgnoreCase(valueID)) {
                                        returnVal[0] = vehicalListModels.get(i).getV_NAME();
                                    }
                                }
                            }
                        }
                    }
                }

                @Override
                public void onError(String strError) {

                }
            });
        } catch (JSONException e) {
            e.printStackTrace();
        }
        return returnVal[0];
    }


    private String getropType(String ropId) {
        String returnVal = "";
        if (ropId.equalsIgnoreCase("D"))
            returnVal = "Double";
        else if (ropId.equalsIgnoreCase("S"))
            returnVal = "Single";
        return returnVal;
    }

    @Override
    public int getItemCount() {
        return listItems.size();
    }

}
