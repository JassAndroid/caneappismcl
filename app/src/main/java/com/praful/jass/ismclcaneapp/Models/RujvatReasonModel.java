package com.praful.jass.ismclcaneapp.Models;

/**
 * Created by Praful on 24-06-2017.
 */

public class RujvatReasonModel {
    String NAME, OBJECT_ID;

    public String getNAME() {
        return NAME;
    }

    public void setNAME(String NAME) {
        this.NAME = NAME;
    }

    public String getOBJECT_ID() {
        return OBJECT_ID;
    }

    public void setOBJECT_ID(String OBJECT_ID) {
        this.OBJECT_ID = OBJECT_ID;
    }
}
