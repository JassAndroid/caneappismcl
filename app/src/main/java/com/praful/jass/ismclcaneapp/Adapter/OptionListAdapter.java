package com.praful.jass.ismclcaneapp.Adapter;

import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.Filter;
import android.widget.Filterable;
import android.widget.TextView;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.R;

import java.util.ArrayList;
import java.util.List;

public class OptionListAdapter extends BaseAdapter implements View.OnClickListener, Filterable {
    private LayoutInflater mInflater;
    private Context context;
    private List<String> listArray;
    private List<String> growerList;
    private List<String> plantationID = new ArrayList<>();
    private List<String> docNo = new ArrayList<>();
    public List<String> filteredData = null;
    ArrayList<String> growarNameList = new ArrayList<>();
    ArrayList<String> filtaredGrowarNameList = new ArrayList<>();
    ArrayList<String> filtaredplantationIDList = new ArrayList<>();
    ArrayList<String> filtareddocNoList = new ArrayList<>();
    private ItemFilter mFilter = new ItemFilter();

    public OptionListAdapter(Context context, List<String> listArray) {
        this.context = context;
        this.listArray = listArray;
        this.filteredData = listArray;
        mInflater = LayoutInflater.from(context);
    }

    public OptionListAdapter(Context context, List<String> listArray, List<String> growerList) {
        try {
            this.context = context;
            this.listArray = listArray;
            this.filteredData = listArray;
            this.growerList = growerList;
            mInflater = LayoutInflater.from(context);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public OptionListAdapter(List<String> listArray, List<String> plantationID, Context context, ArrayList<String> growarNameList, List<String> growerList, List<String> docNo) {
        try {
            this.context = context;
            this.listArray = listArray;
            this.filteredData = listArray;
            this.plantationID = plantationID;
            this.growarNameList = growarNameList;
            this.growerList = growerList;
            this.docNo = docNo;
            filtaredGrowarNameList.clear();
            filtaredplantationIDList.clear();
            filtareddocNoList.clear();
            mInflater = LayoutInflater.from(context);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    public OptionListAdapter(Context context, List<String> listArray, List<String> growerList, ArrayList<String> growarNameList) {
        try {
            this.context = context;
            this.listArray = listArray;
            this.filteredData = listArray;
            this.growerList = growerList;
            this.growarNameList = growarNameList;
            filtaredGrowarNameList.clear();
            mInflater = LayoutInflater.from(context);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public Filter getFilter() {
        return mFilter;
    }

    public void updateData(List<String> listArray) {
        this.listArray = listArray;
    }

    @Override
    public int getCount() {
        return filteredData.size();
    }

    @Override
    public Object getItem(int position) {
        return filteredData.get(position);
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    @Override
    public View getView(int position, View convertView, ViewGroup parent) {
        try {
            EventsViewHolder holder;

            Object model = filteredData.get(position);

            //EventModel.Response events = (EventModel.Response)this.getItem(position);
            if (convertView == null) {
                holder = new EventsViewHolder();
                convertView = mInflater.inflate(R.layout.list_item_option_list, null);
                holder.txtTitle = (TextView) convertView.findViewById(R.id.txtOptionTitle);
                // holder.txtTitle.setMovementMethod(new ScrollingMovementMethod());

                convertView.setTag(holder);
            }

            holder = (EventsViewHolder) convertView.getTag();
            if (model instanceof String) {
                if (filtaredplantationIDList.size() > 0)
                    holder.txtTitle.setText(filtaredplantationIDList.get(position) + "      " + filtaredGrowarNameList.get(position) + "   " + (String) model + "    " + filtareddocNoList.get(position));
                else if (plantationID.size() > 0)
                    holder.txtTitle.setText(plantationID.get(position) + "      " + growarNameList.get(position) + "   " + (String) model + "   " + docNo.get(position));
                else if (filtaredGrowarNameList.size() > 0)
                    holder.txtTitle.setText((String) model + "      " + filtaredGrowarNameList.get(position));
                else if (growarNameList.size() > 0)
                    holder.txtTitle.setText((String) model + "      " + growarNameList.get(position));
                else
                    holder.txtTitle.setText((String) model);
            }

        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return convertView;
    }


    /*****************************************************************
     * Start Alarm Activity
     ****************************************************************/


    @Override
    public void onClick(View v) {
//        switch (v.getId())
//        {
//            case R.id.rl_event:
//                 onStartEventDetailsActivity(v);
//                 break;
//
//            case R.id.img_event_alarm:
//                 onStartAlarmActivity(v.getTag().toString());
//                 break;
//        }
    }

    /*****************************************************************
     * View Holder
     ****************************************************************/

    public class EventsViewHolder {
        TextView txtTitle;
    }


    private class ItemFilter extends Filter {
        @Override
        protected FilterResults performFiltering(CharSequence constraint) {
            FilterResults results = new FilterResults();
            try {
                String filterString = constraint.toString().toLowerCase();
                final List<String> sizeof = listArray;
                filtaredGrowarNameList.clear();
                filtaredplantationIDList.clear();
                filtareddocNoList.clear();
                int count = sizeof.size();
                final ArrayList<Object> nlist = new ArrayList<Object>(count);
                String filterableString = "";
                String filterableStringPID = "";
                String filterableStringGNL = "";
                String filterableStringdocNo = "";
                if (plantationID.size() > 0) {
                    for (int i = 0; i < count; i++) {
                        Object model = listArray.get(i);
                        Object modelPID = plantationID.get(i);
                        Object modelGNL = growarNameList.get(i);
                        Object modeldocNo = docNo.get(i);
                        boolean addlist = true;
                        boolean addPIDlist = true;
                        boolean addGNLlist = true;
                        boolean adddocNolist = true;

                        if (model instanceof String) {
                            if (addlist) {
                                String tempModel = (String) model;
                                filterableString = tempModel;
                                if (filterableString.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    filtaredplantationIDList.add(plantationID.get(i));
                                    filtareddocNoList.add(docNo.get(i));
                                    addlist = false;
                                    addPIDlist = false;
                                    addGNLlist = false;
                                    adddocNolist = false;
                                }
                            }

                        }
                        if (modelPID instanceof String) {
                            if (addPIDlist) {
                                String tempModel = (String) modelPID;
                                filterableStringPID = tempModel;
                                if (filterableStringPID.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    filtaredplantationIDList.add(plantationID.get(i));
                                    filtareddocNoList.add(docNo.get(i));
                                    addlist = false;
                                    addPIDlist = false;
                                    addGNLlist = false;
                                    adddocNolist = false;
                                }
                            }

                        }
                        if (modelGNL instanceof String) {
                            if (addGNLlist) {
                                String tempModel = (String) modelGNL;
                                filterableStringGNL = tempModel;
                                if (filterableStringGNL.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    filtaredplantationIDList.add(plantationID.get(i));
                                    filtareddocNoList.add(docNo.get(i));
                                    addlist = false;
                                    addPIDlist = false;
                                    addGNLlist = false;
                                    adddocNolist = false;
                                }
                            }
                        }
                        if (modeldocNo instanceof String) {
                            if (adddocNolist) {
                                String tempModel = (String) modeldocNo;
                                filterableStringdocNo = tempModel;
                                if (filterableStringdocNo.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    filtaredplantationIDList.add(plantationID.get(i));
                                    filtareddocNoList.add(docNo.get(i));
                                    addlist = false;
                                    addPIDlist = false;
                                    addGNLlist = false;
                                    adddocNolist = false;
                                }
                            }
                        }
                    }
                } else if (growerList.size() > 0) {
                    for (int i = 0; i < count; i++) {
                        Object model = listArray.get(i);
                        Object modelGNL = growerList.get(i);
                        boolean addlist = true;
                        boolean addGNLlist = true;

                        if (model instanceof String) {
                            if (addlist) {
                                String tempModel = (String) model;
                                filterableString = tempModel;
                                if (filterableString.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growerList.get(i));
                                    addlist = false;
                                    addGNLlist = false;
                                }
                            }

                        }
                        if (modelGNL instanceof String) {
                            if (addGNLlist) {
                                String tempModel = (String) modelGNL;
                                filterableStringGNL = tempModel;
                                if (filterableStringGNL.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growerList.get(i));
                                    addlist = false;
                                    addGNLlist = false;
                                }
                            }
                        }
//                        if (model instanceof String) {
//                            String tempModel = (String) model;
//                            filterableString = tempModel;
//                        }
//                        if (filterableString.toLowerCase().contains(filterString)) {
//                            nlist.add(listArray.get(i));
//                        }
                    }
                } else if (growarNameList.size() > 0) {
                    for (int i = 0; i < count; i++) {
                        Object model = listArray.get(i);
                        Object modelGNL = growarNameList.get(i);
                        boolean addlist = true;
                        boolean addGNLlist = true;

                        if (model instanceof String) {
                            if (addlist) {
                                String tempModel = (String) model;
                                filterableString = tempModel;
                                if (filterableString.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    addlist = false;
                                    addGNLlist = false;
                                }
                            }

                        }
                        if (modelGNL instanceof String) {
                            if (addGNLlist) {
                                String tempModel = (String) modelGNL;
                                filterableStringGNL = tempModel;
                                if (filterableStringGNL.toLowerCase().contains(filterString)) {
                                    nlist.add(listArray.get(i));
                                    filtaredGrowarNameList.add(growarNameList.get(i));
                                    addlist = false;
                                    addGNLlist = false;
                                }
                            }
                        }
                    }
                } else {
                    for (int i = 0; i < count; i++) {
                        Object model = listArray.get(i);
                        if (model instanceof String) {
                            String tempModel = (String) model;
                            filterableString = tempModel;
                        }
                        if (filterableString.toLowerCase().contains(filterString)) {
                            nlist.add(model);

                            if (growarNameList.size() > 0)
                                filtaredGrowarNameList.add(growarNameList.get(i));
                        }
                    }
                }

                results.values = nlist;
                results.count = nlist.size();

            } catch (Exception ex) {
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return results;
        }

        @SuppressWarnings("unchecked")
        @Override
        protected void publishResults(CharSequence constraint, FilterResults results) {
            try {
                filteredData = (ArrayList<String>) results.values;
                notifyDataSetChanged();
            } catch (Exception ex) {
                Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }
    }
}
