package com.praful.jass.ismclcaneapp.DBHelper;

import android.content.ContentValues;
import android.content.Context;
import android.content.pm.PackageManager;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.widget.Toast;

import com.praful.jass.ismclcaneapp.Models.KYCModel;
import com.praful.jass.ismclcaneapp.Models.PlantationDataOfTCModel;
import com.praful.jass.ismclcaneapp.Models.PlantationKeyAndIDModel;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.util.ArrayList;

/**
 * Created by Praful on 15-05-2017.
 */

public class SQLiteHelper extends SQLiteOpenHelper {

    /**
     * Initialization of variables Start
     */

    //    =============================================Database Name and Version===================================================
    private final static String DATABASE_NAME = "CANE_BUDDY_APP";
    //    private final static int DATABASE_VERSION_OLD = 1; // getPackageManager().getPackageInfo(getPackageName(), 0).versionName
    private final static int DATABASE_VERSION = 17;
    SQLiteDatabase dbRead;
    SQLiteDatabase dbWrite;

    //    =============================================Create Database for Get_Village_List===================================================
    private final String TBL_VILLAGE_LIST = "TBL_VILLAGE_LIST";
    private final String ID_VILLAGE = "ID";
    private final String VILLAGE_LIST = "VILLAGE_LIST";


    //    =============================================Create Database for get_SUGAR_IRRIGATION_MASTER===================================================
    private final String TBL_SUGAR_IRRIGATION_MASTER = "TBL_SUGAR_IRRIGATION_MASTER";
    private final String ID_IRRIGATION = "ID";
    private final String SUGAR_IRRIGATION_MASTER = "SUGAR_IRRIGATION_MASTER";


    //    =============================================Create Database for Get_Farmer_List===================================================
    private final String TBL_FARMER_LIST = "TBL_FARMER_LIST";
    private final String ID_FARMER = "ID";
    private final String FARMER_LIST = "FARMER_LIST";


    //    =============================================Create Database for Get_CANE_VERITY_LIST===================================================
    private final String TBL_CANE_VERITY_LIST = "TBL_CANE_VERITY_LIST";
    private final String ID_CANE_VERITY = "ID";
    private final String CANE_VERITY_LIST = "CANE_VERITY_LIST";


    //    =============================================Create Database for Get_CANE_TYPE_LIST===================================================
    private final String TBL_CANE_TYPE_LIST = "TBL_CANE_TYPE_LIST";
    private final String ID_CANE_TYPE = "ID";
    private final String CANE_TYPE_LIST = "CANE_TYPE_LIST";

    //    =============================================Create Database for Get_PLANTATION_TYPE===================================================
    private final String TBL_PLANTATION_TYPE = "TBL_PLANTATION_TYPE";
    private final String ID_PLANTATION_TYPE = "ID";
    private final String PLANTATION_TYPE_LIST = "CANE_TYPE_LIST";


    //    =============================================Create Database for REASON_MASTER===================================================
    private final String TBL_REASON_MASTERT = "TBL_REASON_MASTER";
    private final String ID_REASON = "ID";
    private final String RUJVAT_REASON_LIST = "RUJVAT_REASON_LIST";

    //    =============================================Create Database for LOGIN_DATA===================================================
    private final String TBL_LOGIN_MASTERT = "TBL_LOGIN_MASTER";
    private final String USER_NAME = "USER_NAME";
    private final String USER_PASS = "USER_PASS";
    private final String USER_ID = "USER_ID";

    //    =============================================Create Database for Trace Employee===================================================
    private final String TBL_TRACE_LAT_LONG = "TBL_TRACE_LAT_LONG";
    private final String TRACE_ID = "TRACE_ID";
    private final String DEVICE_ID = "DEVICE_ID";
    private final String DATE_TIME = "DATE_TIME";
    private final String LATITUDE = "LATITUDE";
    private final String LONGITUDE = "LONGITUDE";

    //    =============================================Create Database for Trace Employee===================================================
    private final String TBL_TEMP_TRACE_LAT_LONG = "TBL_TEMP_TRACE_LAT_LONG";


    //    =============================================Create Database for RUJVAT_REASON===================================================
    private final String TBL_PLANTATION_RUJVAT_REASON = "TBL_PLANTATION_RUJVAT_REASON";
    private final String PLANTATION_ID_RUJVAT = "PLANTATION_ID_RUJVAT";
    private final String RUJVAT_AREA = "RUJVAT_AREA";
    private final String RUJVAT_REASON = "RUJVAT_REASON";
    private final String RUJVAT_ENTER_DATE = "RUJVAT_ENTER_DATE";
    private final String RUJVAT_USER_ID = "RUJVAT_USER_ID";
    private final String RUJVAT_BRIX = "RUJVAT_BRIX";
    private final String RUJVAT_COUNT = "RUJVAT_COUNT";
    private final String RUJVAT_LENGTH = "RUJVAT_LENGTH";
    private final String RUJVAT_TONAGE = "RUJVAT_TONAGE";
    private final String RUJVAT_DRIP = "RUJVAT_DRIP";

//    //    =============================================Create Database for RUJVAT_REASON===================================================
//    private final String TBL_TEMP_PLANTATION_RUJVAT_REASON = "TBL_TEMP_PLANTATION_RUJVAT_REASON";


    //    =============================================Create Database for EMPLOYEE_TRIP===================================================
    private final String TBL_EMPLOYEE_TRIP = "TBL_EMPLOYEE_TRIP";
    private final String DEVICE_ID_TRIP = "DEVICE_ID_TRIP";
    private final String DATE_OF_TRIP = "DATE_OF_TRIP";
    private final String START_READING = "START_READING";
    private final String START_TIME = "START_TIME";
    private final String END_READING = "END_READING";
    private final String END_TIME = "END_TIME";
    private final String START_PICK_NAME = "START_PICK_NAME";
    private final String END_PICK_NAME = "END_PICK_NAME";
    private final String FLAG = "FLAG";
    private final String IMAGE_TRIP_NAME = "IMAGE_TRIP_NAME";
    private final String START_LAT_TRIP = "START_LAT_TRIP";
    private final String START_LONG_TRIP = "START_LONG_TRIP";
    private final String END_LAT_TRIP = "END_LAT_TRIP";
    private final String END_LONG_TRIP = "END_LONG_TRIP";

    //    =============================================Create Database for EMPLOYEE_TRIP===================================================
    private final String TBL_EMPLOYEE_TRIP_TEMP = "TBL_EMPLOYEE_TRIP_TEMP";


    //    =============================================Create Database for PLANTATION DATA===================================================
    private final String TBL_PLANTATION_DATA_SUBMIT = "TBL_PLANTATION_DATA_SUBMIT";
    private final String ENTER_DATE_PL = "ENTER_DATE_PL";
    private final String GROWER_ID_PL = "GROWER_ID_PL";
    private final String FORM_NO_PL = "FORM_NO_PL";
    private final String FORM_DATE_PL = "FORM_DATE_PL";
    private final String SAREV_NO_PL = "SARVE_NO_PL";
    private final String SARVE_DATE_PL = "SARVE_DATE_PL";
    private final String AGREEMENT_NO_PL = "AGREEMENT_NO_PL";
    private final String AGREEMENT_DATE_PL = "AGREEMENT_DATE_PL";
    private final String PLANTATION_DATE_PL = "PLANTATION_DATE_PL";
    private final String VERITY_ID_PL = "VERITY_ID_PL";
    private final String CANE_TYPE_PL = "CANE_TYPE_PL";
    private final String IRRIGATION_PL = "IRRIGATION_PL";
    private final String PLANTATION_AREA_PL = "PLANTATION_AREA_PL";
    private final String PLANTATION_METHOD_PL = "PLANTATION_METHOD_PL";
    private final String TOTAL_AREA_PL = "TOTAL_AREA_PL";
    private final String SHIVAR_VILLAGE_ID_PL = "SHIVAR_VILLAGE_ID_PL";
    private final String SHIVAR_GUT_ID_PL = "SHIVAR_GUT_ID_PL";
    private final String SHIVAR_SUBGUT_ID_PL = "SHIVAR_SUBGUT_ID_PL";
    private final String DEVICE_IMEI_PL = "DEVICE_IMEI_PL";
    private final String USER_ID_PL = "USER_ID_PL";
    private final String LATITUDE_PL = "LATITUDE_PL";
    private final String LONGITUDE_PL = "LONGITUDE_PL";
    private final String PLANTATIONTEMPKEY_PL = "PLANTATIONTEMPKEY_PL";
    private final String SEASON_PL = "SEASON_PL";


    //    =============================================Create Database for PLANTATION AND RUJVAT IMAGE AND LOCATION DATA===================================================
    private final String TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT = "TBL_PLANTATION_DATA_AND_RUJVAT_SUBMIT";
    private final String PLANTATION_ID_PR = "PLANTATION_ID";
    private final String IMAGE_1_PR = "IMAGE_1";
    private final String IMAGE_2_PR = "IMAGE_2";
    private final String IMAGE_3_PR = "IMAGE_3";
    private final String IMAGE_4_PR = "IMAGE_4";
    private final String LAT_1_PR = "LAT_1";
    private final String LAT_2_PR = "LAT_2";
    private final String LAT_3_PR = "LAT_3";
    private final String LAT_4_PR = "LAT_4";
    private final String LONG_1_PR = "LONG_1";
    private final String LONG_2_PR = "LONG_2";
    private final String LONG_3_PR = "LONG_3";
    private final String LONG_4_PR = "LONG_4";
    private final String FLAGS_PR = "FLAG";


    //    =============================================PLANTATION KEY AND PLANTATION ID TABLE===================================================
    private final String TBL_PLaNTATION_KEY_AND_PLANTATION_ID = "TBL_PLaNTATION_KEY_AND_PLANTATION_ID";
    private final String PLANTATION_KEY_SET = "PLANTATION_KEY";
    private final String PLANTATION_ID_SET = "PLANTATION_ID";


    //    =============================================RUJVAT DATA TABLE===================================================
    private final String TBL_RUJVAT_DATA_SAVED_RJ = "TBL_RUJVAT_DATA_SAVED_RJ";
    private final String PLANTATION_CODE_RJ = "PLANTATION_CODE_RJ";
    private final String FARMER_NAME_RJ = "FARMER_NAME_RJ";
    private final String FARMER_CODE_RJ = "FARMER_CODE_RJ";
    private final String VILLAGE_NAME_RJ = "VILLAGE_NAME_RJ";
    private final String GUT_NAME_RJ = "GUT_NAME_RJ";
    private final String SUB_GUT_NAME_RJ = "SUB_GUT_NAME_RJ";
    private final String TALUKA_NAME_RJ = "TALUKA_NAME_RJ";
    private final String SHIVAR_VILLAGE_NAME_RJ = "SHIVAR_VILLAGE_NAME_RJ";
    private final String SHIVAR_GUT_NAME_RJ = "SHIVAR_GUT_NAME_RJ";
    private final String SARVE_NO_RJ = "SARVE_NO_RJ";
    private final String IRRE_NAME_RJ = "IRRE_NAME_RJ";
    private final String VARITY_RJ = "VARITY_RJ";
    private final String PLANTATION_DATE_RJ = "PLANTATION_DATE_RJ";
    private final String PLANTATION_TYPE_RJ = "PLANTATION_TYPE_RJ";
    private final String PLANTATION_AREA_RJ = "PLANTATION_AREA_RJ";
    private final String OBJECT_ID_RJ = "OBJECT_ID_RJ";


    //    =============================================ALL VILLAGE LIST TABLE===================================================
    private final String TBL_ALL_VILLAGE_LIST = "TBL_ALL_VILLAGE_LIST";
    private final String ALL_VILLAGE_ID = "ALL_VILLAGE_ID";
    private final String ALL_VILLAGE_LIST = "ALL_VILLAGE_LIST";

    //    =============================================ALL CANE QUALITY LIST TABLE===================================================
    private final String TBL_CANE_QUALITY = "TBL_CANE_QUALITY";
    private final String QUALITY_NAME = "QUALITY_NAME";
    private final String QUALITY_ID = "QUALITY_ID";

    //    ===================================================Other Constants=====================================================================
    private Context context;

    /**
     * Initialization of variables End
     */

    public SQLiteHelper(Context context) {
        super(context, DATABASE_NAME, null, DATABASE_VERSION);
        this.context = context;
    }

    @Override
    public void onCreate(SQLiteDatabase sqLiteDatabase) {
        try {
            createTableCaneType(sqLiteDatabase);
            createTableCaneVerity(sqLiteDatabase);
            createTablePlantationType(sqLiteDatabase);
            createTableFarmerList(sqLiteDatabase);
            createTableSugarIrrigationMaster(sqLiteDatabase);
            createTableVillageList(sqLiteDatabase);
            createTableRujvatReasonMaster(sqLiteDatabase);
            createTableLoginData(sqLiteDatabase);
            createTableTraceEmployee(sqLiteDatabase);
            createTableRujvatReason(sqLiteDatabase);
            createTableEmployeeTrip(sqLiteDatabase);
            createTablePlantationDataSubmit(sqLiteDatabase);
            createTablePlantationAndRujvatImageSubmit(sqLiteDatabase);
            createTablePlantationKeyAndID(sqLiteDatabase);
            createTableRujvatDataRJ(sqLiteDatabase);
            createTablePlantationList(sqLiteDatabase);
            createTableVehicleList(sqLiteDatabase);
            createTableHarvesterList(sqLiteDatabase);
            createTableTodChittiData(sqLiteDatabase);
            createTableAllVillageList(sqLiteDatabase);
            createTableKycForm(sqLiteDatabase);
            createTableFactoryNameList(sqLiteDatabase);
            createTableCaneQuality(sqLiteDatabase);
//            dbRead =sqLiteDatabase;
//            dbWrite = sqLiteDatabase.
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    @Override
    public void onUpgrade(SQLiteDatabase sqLiteDatabase, int oldVersion, int newVersion) {
        if (newVersion > oldVersion) {
            try {
//                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode == 15) {
////                sqLiteDatabase.execSQL("ALTER TABLE " + TBL_TRACE_LAT_LONG + " ADD COLUMN " + TRACE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ;");
//                    createTableTempTraceEmployee(sqLiteDatabase);
//                    if (insertTraceTempData(sqLiteDatabase, TBL_TEMP_TRACE_LAT_LONG, TBL_TRACE_LAT_LONG)) {
//                        dropTraceTbl(sqLiteDatabase, TBL_TRACE_LAT_LONG);
//                        createTableTraceEmployee(sqLiteDatabase);
//                        insertTraceTempData(sqLiteDatabase, TBL_TRACE_LAT_LONG, TBL_TEMP_TRACE_LAT_LONG);
//                        dropTraceTbl(sqLiteDatabase, TBL_TEMP_TRACE_LAT_LONG);
//                    }
//                }
//                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode == 22) {
//                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_RUJVAT_REASON);
//                    dropTraceTbl(sqLiteDatabase, TBL_TOD_CHITTI_DATA_SUBMIT);
//                    createTableRujvatReason(sqLiteDatabase);
//                    createTableAllVillageList(sqLiteDatabase);
//                    createTableKycForm(sqLiteDatabase);
//                    createTableFactoryNameList(sqLiteDatabase);
//                    createTableTodChittiData(sqLiteDatabase);
//                    createTableCaneQuality(sqLiteDatabase);
//                }
//                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode == 23) {
//                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_RUJVAT_REASON);
//                    dropTraceTbl(sqLiteDatabase, TBL_TOD_CHITTI_DATA_SUBMIT);
//                    createTableRujvatReason(sqLiteDatabase);
//                    createTableAllVillageList(sqLiteDatabase);
//                    createTableKycForm(sqLiteDatabase);
//                    createTableFactoryNameList(sqLiteDatabase);
//                    createTableTodChittiData(sqLiteDatabase);
//                    createTableCaneQuality(sqLiteDatabase);
//                }
//                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode == 31) {
//                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_RUJVAT_REASON);
//                    dropTraceTbl(sqLiteDatabase, TBL_TOD_CHITTI_DATA_SUBMIT);
//                    createTableRujvatReason(sqLiteDatabase);
//                    createTableAllVillageList(sqLiteDatabase);
//                    createTableKycForm(sqLiteDatabase);
//                    createTableFactoryNameList(sqLiteDatabase);
//                    createTableTodChittiData(sqLiteDatabase);
//                    createTableCaneQuality(sqLiteDatabase);
//                }
//                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode == 32) {
//                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_RUJVAT_REASON);
//                    dropTraceTbl(sqLiteDatabase, TBL_TOD_CHITTI_DATA_SUBMIT);
//                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_DATA_SUBMIT);
//                    createTableRujvatReason(sqLiteDatabase);
//                    createTableAllVillageList(sqLiteDatabase);
//                    createTableKycForm(sqLiteDatabase);
//                    createTableFactoryNameList(sqLiteDatabase);
//                    createTableTodChittiData(sqLiteDatabase);
//                    createTableCaneQuality(sqLiteDatabase);
//                    createTablePlantationDataSubmit(sqLiteDatabase);
//                }
                if (context.getPackageManager().getPackageInfo(context.getPackageName(), 0).versionCode >= 37) {
                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_RUJVAT_REASON);
                    dropTraceTbl(sqLiteDatabase, TBL_TOD_CHITTI_DATA_SUBMIT);
                    dropTraceTbl(sqLiteDatabase, TBL_PLANTATION_DATA_SUBMIT);
                    dropTraceTbl(sqLiteDatabase, TBL_EMPLOYEE_TRIP);
                    createTableRujvatReason(sqLiteDatabase);
                    createTableAllVillageList(sqLiteDatabase);
                    createTableKycForm(sqLiteDatabase);
                    createTableFactoryNameList(sqLiteDatabase);
                    createTableTodChittiData(sqLiteDatabase);
                    createTableCaneQuality(sqLiteDatabase);
                    createTablePlantationDataSubmit(sqLiteDatabase);
                    createTableEmployeeTrip(sqLiteDatabase);
                    createTableTempEmployeeTrip(sqLiteDatabase);

                    if (insertTempEmployeeTrip(sqLiteDatabase, TBL_EMPLOYEE_TRIP_TEMP, TBL_EMPLOYEE_TRIP)) {
                        dropTraceTbl(sqLiteDatabase, TBL_EMPLOYEE_TRIP);
                        createTableEmployeeTrip(sqLiteDatabase);
                        insertTempEmployeeTrip(sqLiteDatabase, TBL_EMPLOYEE_TRIP, TBL_EMPLOYEE_TRIP_TEMP);
                        dropTraceTbl(sqLiteDatabase, TBL_EMPLOYEE_TRIP_TEMP);
                    }
                }
//                sqLiteDatabase.close();
//                sqLiteDatabase.endTransaction();
            } catch (PackageManager.NameNotFoundException e) {
                e.printStackTrace();
            }
        }
//        ALTER TABLE {tableName} ADD COLUMN COLNew {type}
    }


    /**
     * Table Create Functions Start//
     */


    //==================================== METHOD FOR CREATTING TABLE FOR CANE TYPE  =====================================
    public void createTableCaneType(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_CANE_TYPE_LIST + " ( "
                    + ID_CANE_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + CANE_TYPE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR CANE VERITY  =====================================
    public void createTableCaneVerity(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_CANE_VERITY_LIST + " ( "
                    + ID_CANE_VERITY + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + CANE_VERITY_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR CANE VERITY  =====================================
    public void createTablePlantationType(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLANTATION_TYPE + " ( "
                    + ID_PLANTATION_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + PLANTATION_TYPE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //==================================== METHOD FOR CREATTING TABLE FOR Farmer LIST  =====================================
    public void createTableFarmerList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_FARMER_LIST + " ( "
                    + ID_FARMER + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + FARMER_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //==================================== METHOD FOR CREATTING TABLE FOR SUGAR IRRIGATION MASTER  =====================================
    public void createTableSugarIrrigationMaster(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_SUGAR_IRRIGATION_MASTER + " ( "
                    + ID_IRRIGATION + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + SUGAR_IRRIGATION_MASTER + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //==================================== METHOD FOR CREATTING TABLE FOR VILLAGE LIST  =====================================
    public void createTableVillageList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_VILLAGE_LIST + " ( "
                    + ID_VILLAGE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + VILLAGE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //==================================== METHOD FOR CREATTING TABLE FOR RUJVAT REASON  =====================================
    public void createTableRujvatReasonMaster(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_REASON_MASTERT + " ( "
                    + ID_REASON + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + RUJVAT_REASON_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR CREATTING TABLE LOGIN_DATA===================================================
    public void createTableLoginData(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_LOGIN_MASTERT + " ( "
                    + USER_NAME + " TEXT ,"
                    + USER_PASS + " TEXT ,"
                    + USER_ID + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR CREATTING TABLE TRACE EMPLOYEE===================================================
    public void createTableTraceEmployee(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_TRACE_LAT_LONG + " ( "
                    + TRACE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DEVICE_ID + " TEXT ,"
                    + DATE_TIME + " TEXT ,"
                    + LATITUDE + " TEXT ,"
                    + LONGITUDE + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR CREATTING TABLE TRACE TEMP EMPLOYEE===================================================
    public void createTableTempTraceEmployee(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_TEMP_TRACE_LAT_LONG + " ( "
//                    + TRACE_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + DEVICE_ID + " TEXT ,"
                    + DATE_TIME + " TEXT ,"
                    + LATITUDE + " TEXT ,"
                    + LONGITUDE + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR CREATTING TABLE TBL_EMPLOYEE_TRIP_TEMP===================================================
    public void createTableTempEmployeeTrip(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_EMPLOYEE_TRIP_TEMP + " ( "
                    + DEVICE_ID_TRIP + " TEXT ,"
                    + DATE_OF_TRIP + " TEXT ,"
                    + START_READING + " TEXT ,"
                    + START_TIME + " TEXT ,"
                    + END_READING + " TEXT ,"
                    + END_TIME + " TEXT ,"
                    + START_PICK_NAME + " TEXT ,"
                    + END_PICK_NAME + " TEXT ,"
                    + FLAG + " TEXT ,"
                    + IMAGE_TRIP_NAME + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR CREATTING TABLE RUJVAT_REASON===================================================
    public void createTableRujvatReason(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLANTATION_RUJVAT_REASON + " ( "
                    + PLANTATION_ID_RUJVAT + " TEXT ,"
                    + RUJVAT_REASON + " TEXT ,"
                    + RUJVAT_AREA + " TEXT  ,"
                    + RUJVAT_ENTER_DATE + " TEXT ,"
                    + RUJVAT_USER_ID + " TEXT ,"
                    + RUJVAT_BRIX + " TEXT ,"
                    + RUJVAT_COUNT + " TEXT ,"
                    + RUJVAT_LENGTH + " TEXT ,"
                    + RUJVAT_TONAGE + " TEXT ,"
                    + RUJVAT_DRIP + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//    //=============================================METHOD FOR CREATTING TABLE RUJVAT_REASON===================================================
//    public void createTempTableRujvatReason(SQLiteDatabase db) {
//        try {
//            String sql = "CREATE TABLE " + TBL_TEMP_PLANTATION_RUJVAT_REASON + " ( "
//                    + PLANTATION_ID_RUJVAT + " TEXT ,"
//                    + RUJVAT_REASON + " TEXT ,"
//                    + RUJVAT_AREA + " TEXT );";
//            db.execSQL(sql);
//        } catch (Exception ex) {
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
//        }
//    }

    //=============================================METHOD FOR CREATTING TABLE RUJVAT_REASON===================================================
    public void createTableEmployeeTrip(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_EMPLOYEE_TRIP + " ( "
                    + DEVICE_ID_TRIP + " TEXT ,"
                    + DATE_OF_TRIP + " TEXT ,"
                    + START_READING + " TEXT ,"
                    + START_TIME + " TEXT ,"
                    + END_READING + " TEXT ,"
                    + END_TIME + " TEXT ,"
                    + START_PICK_NAME + " TEXT ,"
                    + END_PICK_NAME + " TEXT ,"
                    + FLAG + " TEXT ,"
                    + IMAGE_TRIP_NAME + " TEXT ,"
                    + START_LAT_TRIP + " TEXT ,"
                    + START_LONG_TRIP + " TEXT ,"
                    + END_LAT_TRIP + " TEXT ,"
                    + END_LONG_TRIP + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //=============================================METHOD FOR CREATTING TABLE PLANTATION DATA===================================================
    public void createTablePlantationDataSubmit(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLANTATION_DATA_SUBMIT + " ( "
                    + ENTER_DATE_PL + " TEXT ,"
                    + GROWER_ID_PL + " TEXT ,"
                    + FORM_NO_PL + " TEXT ,"
                    + FORM_DATE_PL + " TEXT ,"
                    + SAREV_NO_PL + " TEXT ,"
                    + SARVE_DATE_PL + " TEXT ,"
                    + AGREEMENT_NO_PL + " TEXT ,"
                    + AGREEMENT_DATE_PL + " TEXT ,"
                    + PLANTATION_DATE_PL + " TEXT ,"
                    + VERITY_ID_PL + " TEXT ,"
                    + CANE_TYPE_PL + " TEXT ,"
                    + IRRIGATION_PL + " TEXT ,"
                    + PLANTATION_AREA_PL + " TEXT ,"
                    + PLANTATION_METHOD_PL + " TEXT ,"
                    + TOTAL_AREA_PL + " TEXT ,"
                    + SHIVAR_VILLAGE_ID_PL + " TEXT ,"
                    + SHIVAR_GUT_ID_PL + " TEXT ,"
                    + SHIVAR_SUBGUT_ID_PL + " TEXT ,"
                    + DEVICE_IMEI_PL + " TEXT ,"
                    + USER_ID_PL + " TEXT ,"
                    + LATITUDE_PL + " TEXT ,"
                    + LONGITUDE_PL + " TEXT ,"
                    + PLANTATIONTEMPKEY_PL + " TEXT ,"
                    + SEASON_PL + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //    =============================================METHOD FOR CREATTING PLANTATION AND RUJVAT IMAGE AND LOCATION DATA===================================================

    public void createTablePlantationAndRujvatImageSubmit(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + " ( "
                    + PLANTATION_ID_PR + " TEXT ,"
                    + IMAGE_1_PR + " TEXT ,"
                    + IMAGE_2_PR + " TEXT ,"
                    + IMAGE_3_PR + " TEXT ,"
                    + IMAGE_4_PR + " TEXT ,"
                    + LAT_1_PR + " TEXT ,"
                    + LAT_2_PR + " TEXT ,"
                    + LAT_3_PR + " TEXT ,"
                    + LAT_4_PR + " TEXT ,"
                    + LONG_1_PR + " TEXT ,"
                    + LONG_2_PR + " TEXT ,"
                    + LONG_3_PR + " TEXT ,"
                    + LONG_4_PR + " TEXT ,"
                    + FLAGS_PR + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    =============================================METHOD FOR CREATTING PLANTATION KEY AND ID TABLE===================================================
    public void createTablePlantationKeyAndID(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLaNTATION_KEY_AND_PLANTATION_ID + " ( "
                    + PLANTATION_KEY_SET + " TEXT ,"
                    + PLANTATION_ID_SET + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //    =============================================METHOD FOR CREATTING PLANTATION KEY AND ID TABLE===================================================
    public void createTableRujvatDataRJ(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_RUJVAT_DATA_SAVED_RJ + " ( "
                    + PLANTATION_CODE_RJ + " TEXT ,"
                    + FARMER_NAME_RJ + " TEXT ,"
                    + FARMER_CODE_RJ + " TEXT ,"
                    + VILLAGE_NAME_RJ + " TEXT ,"
                    + GUT_NAME_RJ + " TEXT ,"
                    + SUB_GUT_NAME_RJ + " TEXT ,"
                    + TALUKA_NAME_RJ + " TEXT ,"
                    + SHIVAR_VILLAGE_NAME_RJ + " TEXT ,"
                    + SHIVAR_GUT_NAME_RJ + " TEXT ,"
                    + SARVE_NO_RJ + " TEXT ,"
                    + IRRE_NAME_RJ + " TEXT ,"
                    + VARITY_RJ + " TEXT ,"
                    + PLANTATION_DATE_RJ + " TEXT ,"
                    + PLANTATION_TYPE_RJ + " TEXT ,"
                    + PLANTATION_AREA_RJ + " TEXT ,"
                    + OBJECT_ID_RJ + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //    =============================================METHOD FOR CREATTING PLANTATION KEY AND ID TABLE===================================================
    public void createTableAllVillageList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_ALL_VILLAGE_LIST + " ( "
                    + ALL_VILLAGE_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + ALL_VILLAGE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //    =============================================METHOD FOR CREATTING CANE QUALITY TABLE===================================================
    public void createTableCaneQuality(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_CANE_QUALITY + " ( "
                    + QUALITY_ID + "  INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + QUALITY_NAME + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Table Create Functions End */


    /**
     * Insert Query Functions Start
     */

    //====================================METHOD FOR INSERTING DATA INTO CANE TYPE =======================
    public void insertCaneTypeList(String caneTypeList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_CANE_TYPE_LIST + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(CANE_TYPE_LIST, caneTypeList);
            db.insert(TBL_CANE_TYPE_LIST, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO CANE VERITY =======================
    public void insertCaneVerityList(String caneVerityList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_CANE_VERITY_LIST + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(CANE_VERITY_LIST, caneVerityList);
            db.insert(TBL_CANE_VERITY_LIST, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO Plantation Type =======================
    public void insertCanePlantationList(String canePlantationList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_PLANTATION_TYPE + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_TYPE_LIST, canePlantationList);
            db.insert(TBL_PLANTATION_TYPE, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //====================================METHOD FOR INSERTING DATA INTO FARMER LIST =======================
    public void insertFarmerList(String farmerList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_FARMER_LIST + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(FARMER_LIST, farmerList);
            db.insert(TBL_FARMER_LIST, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO IRRIGATION MASTER =======================
    public void insertIrrigationMaster(String irrigationMasterList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_SUGAR_IRRIGATION_MASTER + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(SUGAR_IRRIGATION_MASTER, irrigationMasterList);
            db.insert(TBL_SUGAR_IRRIGATION_MASTER, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO VILLAGE LIST =======================
    public void insertVillageList(String villageList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_VILLAGE_LIST + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(VILLAGE_LIST, villageList);
            db.insert(TBL_VILLAGE_LIST, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO RUJVAT REASON =======================
    public void insertReasonList(String reasonList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_REASON_MASTERT + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(RUJVAT_REASON_LIST, reasonList);
            db.insert(TBL_REASON_MASTERT, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR INSERTING DATA INTO LOGIN_DATA===================================================

    public void insertLoginData(String userName, String userPass, String userID) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_LOGIN_MASTERT + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(USER_NAME, userName);
            cv.put(USER_PASS, userPass);
            cv.put(USER_ID, userID);
            db.insert(TBL_LOGIN_MASTERT, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR INSERTING DATA INTO TRACE_EPLOYEE===================================================

    public void insertTraceEmployeeData(String deviceID, String dateTime, String latitude, String longitude) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(DEVICE_ID, deviceID);
            cv.put(DATE_TIME, dateTime);
            cv.put(LATITUDE, latitude);
            cv.put(LONGITUDE, longitude);
            db.insert(TBL_TRACE_LAT_LONG, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//=============================================METHOD FOR INSERTING DATA INTO TRACE_EPLOYEE===================================================

    public boolean insertTraceTempData(SQLiteDatabase db, String to_table, String from_table) {
        try {
//            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + from_table + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    ContentValues cv = new ContentValues();
                    cv.put(DEVICE_ID, cursor.getString(0));
                    cv.put(DATE_TIME, cursor.getString(1));
                    cv.put(LATITUDE, cursor.getString(2));
                    cv.put(LONGITUDE, cursor.getString(3));
                    db.insert(to_table, null, cv);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            db.close();
            return true;
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }

//=============================================METHOD FOR INSERTING DATA INTO TRACE_EPLOYEE===================================================

    public boolean insertTempEmployeeTrip(SQLiteDatabase db, String to_table, String from_table) {
        try {
//            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + from_table + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    ContentValues cv = new ContentValues();
                    cv.put(DEVICE_ID_TRIP, cursor.getString(0));
                    cv.put(DATE_OF_TRIP, cursor.getString(1));
                    cv.put(START_READING, cursor.getString(2));
                    cv.put(START_TIME, cursor.getString(3));
                    cv.put(END_READING, cursor.getString(4));
                    cv.put(END_TIME, cursor.getString(5));
                    cv.put(START_PICK_NAME, cursor.getString(6));
                    cv.put(END_PICK_NAME, cursor.getString(7));
                    cv.put(FLAG, cursor.getString(8));
                    cv.put(IMAGE_TRIP_NAME, cursor.getString(9));
//                    cv.put(START_LAT_TRIP, cursor.getString(10));
//                    cv.put(START_LONG_TRIP, cursor.getString(11));
//                    cv.put(END_LAT_TRIP, cursor.getString(12));
//                    cv.put(END_LONG_TRIP, cursor.getString(13));
                    db.insert(to_table, null, cv);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
//            db.close();
            return true;
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
            return false;
        }
    }


    //=============================================METHOD FOR INSERTING DATA INTO RUJVAT REASON===================================================

    public void insertRjvatReasonData(String plantationID, String rujvarArea, String rujvatReason, String date, String id, String reading, String count, String length, String tonage, String drip) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_ID_RUJVAT, plantationID);
            cv.put(RUJVAT_AREA, rujvarArea);
            cv.put(RUJVAT_REASON, rujvatReason);
            cv.put(RUJVAT_ENTER_DATE, date);
            cv.put(RUJVAT_USER_ID, id);
            cv.put(RUJVAT_BRIX, reading);
            cv.put(RUJVAT_COUNT, count);
            cv.put(RUJVAT_LENGTH, length);
            cv.put(RUJVAT_TONAGE, tonage);
            cv.put(RUJVAT_DRIP, drip);
            db.insert(TBL_PLANTATION_RUJVAT_REASON, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //=============================================METHOD FOR INSERTING DATA INTO EMPLOYEE_TRIP===================================================

    public void insertEmployeeTripData(String device_id_trip, String dateOfTrip, String startReading, String startTime, String endReading, String endTime, String startPickName, String endPickName, String flag, String imageName, String startLat, String startLong, String endLat, String endLong) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(DEVICE_ID_TRIP, device_id_trip);
            cv.put(DATE_OF_TRIP, dateOfTrip);
            cv.put(START_READING, startReading);
            cv.put(START_TIME, startTime);
            cv.put(END_READING, endReading);
            cv.put(END_TIME, endTime);
            cv.put(START_PICK_NAME, startPickName);
            cv.put(END_PICK_NAME, endPickName);
            cv.put(FLAG, flag);
            cv.put(IMAGE_TRIP_NAME, imageName);
            cv.put(START_LAT_TRIP, startLat);
            cv.put(START_LONG_TRIP, startLong);
            cv.put(END_LAT_TRIP, endLat);
            cv.put(END_LONG_TRIP, endLong);
            db.insert(TBL_EMPLOYEE_TRIP, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR INSERTING DATA INTO EMPLOYEE_TRIP===================================================
    public void insertPlantationDataSubmit(String enterDate, String growerID, String formNo, String formDate, String sarveNo, String sarveDate, String agreementNo, String agreementDate, String plantationDate, String verityID, String caneType, String irrigation, String plantationArea, String plantationMethod, String totalArea, String shivarVillage, String shivarGut, String shivarSubgut, String deviceIMEI, String userID, String lat, String longg, String plantationTempKey, String season) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(ENTER_DATE_PL, enterDate);
            cv.put(GROWER_ID_PL, growerID);
            cv.put(FORM_NO_PL, formNo);
            cv.put(FORM_DATE_PL, formDate);
            cv.put(SAREV_NO_PL, sarveNo);
            cv.put(SARVE_DATE_PL, sarveDate);
            cv.put(AGREEMENT_NO_PL, agreementNo);
            cv.put(AGREEMENT_DATE_PL, agreementDate);
            cv.put(PLANTATION_DATE_PL, plantationDate);
            cv.put(VERITY_ID_PL, verityID);
            cv.put(CANE_TYPE_PL, caneType);
            cv.put(IRRIGATION_PL, irrigation);
            cv.put(PLANTATION_AREA_PL, plantationArea);
            cv.put(PLANTATION_METHOD_PL, plantationMethod);
            cv.put(TOTAL_AREA_PL, totalArea);
            cv.put(SHIVAR_VILLAGE_ID_PL, shivarVillage);
            cv.put(SHIVAR_GUT_ID_PL, shivarGut);
            cv.put(SHIVAR_SUBGUT_ID_PL, shivarSubgut);
            cv.put(DEVICE_IMEI_PL, deviceIMEI);
            cv.put(USER_ID_PL, userID);
            cv.put(LATITUDE_PL, lat);
            cv.put(LONGITUDE_PL, longg);
            cv.put(PLANTATIONTEMPKEY_PL, plantationTempKey);
            cv.put(SEASON_PL, season);
            db.insert(TBL_PLANTATION_DATA_SUBMIT, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//=============================================METHOD FOR INSERTING DATA INTO RUJVAT REASON===================================================

    public void insertPlantationAndRujvatImageSubmit(String plantationID, String image_1, String image_2, String image_3, String image_4, String lat_1, String lat_2, String lat_3, String lat_4, String long_1, String long_2, String long_3, String long_4, String flag) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_ID_PR, plantationID);
            cv.put(IMAGE_1_PR, image_1);
            cv.put(IMAGE_2_PR, image_2);
            cv.put(IMAGE_3_PR, image_3);
            cv.put(IMAGE_4_PR, image_4);
            cv.put(LAT_1_PR, lat_1);
            cv.put(LAT_2_PR, lat_2);
            cv.put(LAT_3_PR, lat_3);
            cv.put(LAT_4_PR, lat_4);
            cv.put(LONG_1_PR, long_1);
            cv.put(LONG_2_PR, long_2);
            cv.put(LONG_3_PR, long_3);
            cv.put(LONG_4_PR, long_4);
            cv.put(FLAGS_PR, flag);
            db.insert(TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //=============================================METHOD FOR INSERTING DATA INTO PLANTATION KEY AND RUJVAT===================================================

    public void insertPlantationKeyAndID(String plantationKey, String plantationID) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();

            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_KEY_SET, plantationKey);
            cv.put(PLANTATION_ID_SET, plantationID);
            db.insert(TBL_PLaNTATION_KEY_AND_PLANTATION_ID, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //=============================================METHOD FOR INSERTING DATA INTO RUJVAT TABLE===================================================

    public void insertRjvatDataRJ(String plantationCode, String farmerName, String farmerCode, String villageName, String gutName, String subGutName, String talukaName, String shivarVillageName, String shivarGutName, String sarveNoRJ, String irreName, String varity, String plantationDate, String plantationType, String plantationArea, String objectID) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_RUJVAT_DATA_SAVED_RJ + ";";
//        db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_CODE_RJ, plantationCode);
            cv.put(FARMER_NAME_RJ, farmerName);
            cv.put(FARMER_CODE_RJ, farmerCode);
            cv.put(VILLAGE_NAME_RJ, villageName);
            cv.put(GUT_NAME_RJ, gutName);
            cv.put(SUB_GUT_NAME_RJ, subGutName);
            cv.put(TALUKA_NAME_RJ, talukaName);
            cv.put(SHIVAR_VILLAGE_NAME_RJ, shivarVillageName);
            cv.put(SHIVAR_GUT_NAME_RJ, shivarGutName);
            cv.put(SARVE_NO_RJ, sarveNoRJ);
            cv.put(IRRE_NAME_RJ, irreName);
            cv.put(VARITY_RJ, varity);
            cv.put(PLANTATION_DATE_RJ, plantationDate);
            cv.put(PLANTATION_TYPE_RJ, plantationType);
            cv.put(PLANTATION_AREA_RJ, plantationArea);
            cv.put(OBJECT_ID_RJ, objectID);
            db.insert(TBL_RUJVAT_DATA_SAVED_RJ, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //=============================================METHOD FOR INSERTING ALL VILLAGE LIST DATA===================================================

    public void insertAllVillageList(String villageList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_ALL_VILLAGE_LIST + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(ALL_VILLAGE_LIST, villageList);
            db.insert(TBL_ALL_VILLAGE_LIST, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

//=============================================METHOD FOR INSERTING ALL CANE QUALITY LIST DATA===================================================

    public void insertAllCaneQuality(String qualityList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_CANE_QUALITY + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(QUALITY_NAME, qualityList);
            db.insert(TBL_CANE_QUALITY, null, cv);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    /**
     * Insert Query Functions End */


    /**
     * Get Data Query Functions Start
     */

    //====================================METHOD FOR GETTING DATA OF CANE TYPE =======================
    public String getCaneTypeList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_CANE_TYPE_LIST + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception ex) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return value;
    }


    //====================================METHOD FOR GETTING DATA OF CANE VERITY =======================
    public String getCaneVerityList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_CANE_VERITY_LIST + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception ex) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return value;
    }


    //====================================METHOD FOR GETTING DATA OF PLANTATION TYPE =======================
    public String getPlantationTypeList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_TYPE + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }


    //====================================METHOD FOR GETTING DATA OF FARMER LIST =======================
    public String getFarmerList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_FARMER_LIST + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;

    }

    //====================================METHOD FOR GETTING DATA OF SUGAR IRRIGATION MASTER =======================
    public String getIrrigationMaster() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_SUGAR_IRRIGATION_MASTER + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF VILLAGE LIST =======================
    public String getVillageList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_VILLAGE_LIST + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF RUJVAT REASON LIST =======================
    public String getRujvatReason() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_REASON_MASTERT + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //=============================================METHOD FOR GETTING DATA OF LOGIN_DATA===================================================
    public String getLoginData(String userName, String userPass) {
        String value = "";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT " + USER_ID + " FROM " + TBL_LOGIN_MASTERT + " WHERE " + USER_NAME + "='" + userName + "' AND " + USER_PASS + "='" + userPass + "';";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            if (!(cursor.moveToFirst()) || cursor.getCount() == 0) {
                value = "0";
            } else {
                value = cursor.getString(0);
            }
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //=============================================METHOD FOR GETTING DATA OF TRACKING_DATA===================================================
    public String getTackingData() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT " + DEVICE_ID + ", " + DATE_TIME + ", " + LATITUDE + ", " + LONGITUDE + " FROM " + TBL_TRACE_LAT_LONG + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_device", cursor.getString(0));
                    innerjsonObject.put("p_date", cursor.getString(1));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_lat", cursor.getString(2));
                    innerjsonObject.put("p_lon", cursor.getString(3));
                    jsonArray.put(innerjsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            value = jsonObject.put("TrackEntry", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }


    //=============================================METHOD FOR GETTING DATA OF TRACKING_DATA===================================================
    public String getTackingTop300Row() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT " + TRACE_ID + "," + DEVICE_ID + ", " + DATE_TIME + ", " + LATITUDE + ", " + LONGITUDE + " FROM " + TBL_TRACE_LAT_LONG + " LIMIT 300;";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_trace_id", cursor.getString(0));
                    innerjsonObject.put("p_device", cursor.getString(1));
                    innerjsonObject.put("p_date", cursor.getString(2));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_lat", cursor.getString(3));
                    innerjsonObject.put("p_lon", cursor.getString(4));
                    jsonArray.put(innerjsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            value = jsonObject.put("TrackEntry", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }

    //=============================================METHOD FOR GETTING DATA OF RUJVAT_REASON===================================================
    public String getRujvatReasonData() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_RUJVAT_REASON + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_enter_date", cursor.getString(3));
                    innerjsonObject.put("p_object_id", cursor.getString(0));
                    innerjsonObject.put("p_area", cursor.getString(1));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_reason", cursor.getString(2));
                    innerjsonObject.put("p_user_id", cursor.getString(4));
                    innerjsonObject.put("p_brix", cursor.getString(5));
                    innerjsonObject.put("p_count", cursor.getString(6));
                    innerjsonObject.put("p_length", cursor.getString(7));
                    innerjsonObject.put("p_tonage", cursor.getString(8));
                    innerjsonObject.put("p_drip", cursor.getString(9));
                    jsonArray.put(innerjsonObject);
                } catch (JSONException ex) {
                    ex.printStackTrace();
                    Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
                }
            }
            value = jsonObject.put("RujvatData", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }


    //=============================================METHOD FOR GETTING DATA OF EMPLOYEE_TRIP_DATA===================================================
    public ArrayList<String[]> getEmployeeTripData() {
        ArrayList<String[]> value = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_EMPLOYEE_TRIP + ";";
            Cursor cursor = db.rawQuery(sql, null);
            String devbiceIDTrip[] = new String[cursor.getCount()];
            String dateOfTrip[] = new String[cursor.getCount()];
            String startReading[] = new String[cursor.getCount()];
            String startTime[] = new String[cursor.getCount()];
            String endReading[] = new String[cursor.getCount()];
            String endTime[] = new String[cursor.getCount()];
            String startPickName[] = new String[cursor.getCount()];
            String endPickName[] = new String[cursor.getCount()];
            String flag[] = new String[cursor.getCount()];
            String imageName[] = new String[cursor.getCount()];
            String startLat[] = new String[cursor.getCount()];
            String startLang[] = new String[cursor.getCount()];
            String endLat[] = new String[cursor.getCount()];
            String endLang[] = new String[cursor.getCount()];
            int i = 0;
            while (cursor.moveToNext()) {
                try {
                    devbiceIDTrip[i] = cursor.getString(0);
                    dateOfTrip[i] = cursor.getString(1);
                    startReading[i] = cursor.getString(2);
                    startTime[i] = cursor.getString(3);
                    endReading[i] = cursor.getString(4);
                    endTime[i] = cursor.getString(5);
                    startPickName[i] = cursor.getString(6);
                    endPickName[i] = cursor.getString(7);
                    flag[i] = cursor.getString(8);
                    imageName[i] = cursor.getString(9);
                    startLat[i] = cursor.getString(10);
                    startLang[i] = cursor.getString(11);
                    endLat[i] = cursor.getString(12);
                    endLang[i] = cursor.getString(13);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i++;
            }
            value.add(devbiceIDTrip);
            value.add(dateOfTrip);
            value.add(startReading);
            value.add(startTime);
            value.add(endReading);
            value.add(endTime);
            value.add(startPickName);
            value.add(endPickName);
            value.add(flag);
            value.add(imageName);
            value.add(startLat);
            value.add(startLang);
            value.add(endLat);
            value.add(endLang);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value.add(new String[]{AppConstants.TABLE_NOT_AVAILABLE});
        }
        return value;
    }


    //=============================================METHOD FOR GETTING DATA OF EMPLOYEE_TRIP_DATA===================================================
    public String getEmployeeTripDataInJson() {
        String value = "";
        JSONArray jsonArray = new JSONArray();
        JSONObject jsonObject = new JSONObject();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_EMPLOYEE_TRIP + ";";
            Cursor cursor = db.rawQuery(sql, null);
            int i = 0;
            while (cursor.moveToNext()) {

                try {
                    JSONObject innerjsonObject = new JSONObject();

                    innerjsonObject.put("p_device", cursor.getString(0));
                    innerjsonObject.put("p_date", cursor.getString(1));
                    innerjsonObject.put("p_start", cursor.getString(2));
                    innerjsonObject.put("p_start_time", cursor.getString(3));
                    innerjsonObject.put("p_end", cursor.getString(4));
                    innerjsonObject.put("p_end_time", cursor.getString(5));
                    innerjsonObject.put("p_start_pic", cursor.getString(6));
                    innerjsonObject.put("p_end_pic", cursor.getString(7));
                    innerjsonObject.put("p_flag", cursor.getString(8));
                    innerjsonObject.put("p_start_lat", cursor.getString(10));
                    innerjsonObject.put("p_start_long", cursor.getString(11));
                    innerjsonObject.put("p_end_lat", cursor.getString(12));
                    innerjsonObject.put("p_end_long", cursor.getString(13));

                    jsonArray.put(innerjsonObject);

                } catch (Exception e) {
                    e.printStackTrace();
                }
                i++;
            }
            value = jsonObject.put("TripData", jsonArray).toString();

            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }


    //=============================================METHOD FOR GETTING DATA OF EMPLOYEE_TRIP_DATA===================================================
    public String getPlantationData() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_DATA_SUBMIT + ";";
            Cursor cursor = db.rawQuery(sql, null);
//            if (cursor.moveToFirst()) {
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_enter_date", cursor.getString(0));
                    innerjsonObject.put("p_grower_id", cursor.getString(1));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_form_no", cursor.getString(2));
                    innerjsonObject.put("p_form_date", cursor.getString(3));
                    innerjsonObject.put("p_serve_no", cursor.getString(4));
                    innerjsonObject.put("p_serve_date", cursor.getString(5));
                    innerjsonObject.put("p_agreement_no", cursor.getString(6));
                    innerjsonObject.put("p_agreement_date", cursor.getString(7));
                    innerjsonObject.put("p_plantation_date", cursor.getString(8));
                    innerjsonObject.put("p_varity_id", cursor.getString(9));
                    innerjsonObject.put("p_cane_type", cursor.getString(10));
                    innerjsonObject.put("p_irrigation", cursor.getString(11));
                    innerjsonObject.put("p_plantation_area", cursor.getString(12));
                    innerjsonObject.put("p_plantation_method", cursor.getString(13));
                    innerjsonObject.put("p_total_area", cursor.getString(14));
                    innerjsonObject.put("p_shivar_village_id", cursor.getString(15));
                    innerjsonObject.put("p_shivar_gut_id", cursor.getString(16));
                    innerjsonObject.put("p_shivar_sub_gut_id", cursor.getString(17));
                    innerjsonObject.put("p_device", cursor.getString(18));
                    innerjsonObject.put("p_user_id", cursor.getString(19));
                    innerjsonObject.put("p_lat", cursor.getString(20));
                    innerjsonObject.put("p_long", cursor.getString(21));
                    innerjsonObject.put("plantation_key", cursor.getString(22));
                    innerjsonObject.put("p_season", cursor.getString(23));
                    jsonArray.put(innerjsonObject);
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
//            }
            value = jsonObject.put("PlantationSubmit", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }
//128,127-0000003671
//129,130-0000003419

    //====================================METHOD FOR GETTING DATA OF SUGAR IRRIGATION MASTER =======================
    public ArrayList<String[]> getPlantationAndRujvatImageData(String flagPyara) {
        ArrayList<String[]> value = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + " WHERE " + FLAGS_PR + " = '" + flagPyara + "';";
            Cursor cursor = db.rawQuery(sql, null);
            String plantationId[] = new String[cursor.getCount()];
            String image_1[] = new String[cursor.getCount()];
            String image_2[] = new String[cursor.getCount()];
            String image_3[] = new String[cursor.getCount()];
            String image_4[] = new String[cursor.getCount()];
            String lat_1[] = new String[cursor.getCount()];
            String lat_2[] = new String[cursor.getCount()];
            String lat_3[] = new String[cursor.getCount()];
            String lat_4[] = new String[cursor.getCount()];
            String long_1[] = new String[cursor.getCount()];
            String long_2[] = new String[cursor.getCount()];
            String long_3[] = new String[cursor.getCount()];
            String long_4[] = new String[cursor.getCount()];
            String flag[] = new String[cursor.getCount()];
            int i = 0;
            while (cursor.moveToNext()) {
                try {
                    plantationId[i] = cursor.getString(0);
                    image_1[i] = cursor.getString(1);
                    image_2[i] = cursor.getString(2);
                    image_3[i] = cursor.getString(3);
                    image_4[i] = cursor.getString(4);
                    lat_1[i] = cursor.getString(5);
                    lat_2[i] = cursor.getString(6);
                    lat_3[i] = cursor.getString(7);
                    lat_4[i] = cursor.getString(8);
                    long_1[i] = cursor.getString(9);
                    long_2[i] = cursor.getString(10);
                    long_3[i] = cursor.getString(11);
                    long_4[i] = cursor.getString(12);
                    flag[i] = cursor.getString(13);
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i++;
            }
            value.add(plantationId);
            value.add(image_1);
            value.add(image_2);
            value.add(image_3);
            value.add(image_4);
            value.add(lat_1);
            value.add(lat_2);
            value.add(lat_3);
            value.add(lat_4);
            value.add(long_1);
            value.add(long_2);
            value.add(long_3);
            value.add(long_4);
            value.add(flag);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value.add(new String[]{AppConstants.TABLE_NOT_AVAILABLE});
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF SUGAR IRRIGATION MASTER =======================
    public ArrayList<String> getPlantationImageDataToRename(String plKey) {
        ArrayList<String> value = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + " WHERE " + PLANTATION_ID_PR + "=" + plKey + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    value.add(cursor.getString(1));
                    value.add(cursor.getString(2));
                    value.add(cursor.getString(3));
                    value.add(cursor.getString(4));
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value.add(AppConstants.TABLE_NOT_AVAILABLE);
        }
        return value;
    }

    //====================================METHOD FOR GETTING ALL PLANTATION AND RUJAVT IMAGE LIST =======================
    public String getAllPlantationAndRujvatImageList() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
        Cursor cursor = db.rawQuery(sql, null);
        try {
            while (cursor.moveToNext()) {

                try {
                    jsonArray.put(new JSONObject().put("img_name", cursor.getString(1)));
                    jsonArray.put(new JSONObject().put("img_name", cursor.getString(2)));
                    jsonArray.put(new JSONObject().put("img_name", cursor.getString(3)));
                    jsonArray.put(new JSONObject().put("img_name", cursor.getString(4)));
                } catch (JSONException e) {
                    e.printStackTrace();
                }
            }
            value = jsonObject.put("imgNameList", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF PLANTATION KEY AND ID =======================
    public Cursor getPlantationKeyAndID() {
        String value;
//        try {
        SQLiteDatabase db = this.getWritableDatabase();
        String sql = "SELECT a." + PLANTATION_KEY_SET + ", a." + PLANTATION_ID_SET + ", b." + PLANTATIONTEMPKEY_PL + " FROM " + TBL_PLaNTATION_KEY_AND_PLANTATION_ID + " a LEFT JOIN " + TBL_PLANTATION_DATA_SUBMIT + " b ON a." + PLANTATION_KEY_SET + "=b." + PLANTATIONTEMPKEY_PL + ";";
        Cursor cursor = db.rawQuery(sql, null);
        cursor.moveToNext();
        value = cursor.getString(1);
        System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
        db.close();
        return cursor;
    }


    //====================================METHOD FOR GETTING DATA PLANTATION KEY AND IMAGE FROM KEY =======================
    public ArrayList<PlantationKeyAndIDModel> getPlantationKey() {
//        private final String MY_QUERY = "SELECT * FROM table_a a INNER JOIN table_b b ON a.id=b.other_id WHERE b.property_id=?";
//        db.rawQuery(MY_QUERY, new String[]{String.valueOf(propertyId)});
        ArrayList<PlantationKeyAndIDModel> value = new ArrayList();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT " + PLANTATIONTEMPKEY_PL + " FROM " + TBL_PLaNTATION_KEY_AND_PLANTATION_ID + " a LEFT JOIN " + TBL_PLANTATION_DATA_SUBMIT + " b NO a." + PLANTATION_KEY_SET + "=b." + PLANTATIONTEMPKEY_PL + ";";
            String sql = "SELECT * FROM " + TBL_PLaNTATION_KEY_AND_PLANTATION_ID + ";";
            Cursor cursor = db.rawQuery(sql, null);
            int i = 0;
            while (cursor.moveToNext()) {
                PlantationKeyAndIDModel plantationKeyAndIDModel = new PlantationKeyAndIDModel();
                plantationKeyAndIDModel.setKey(cursor.getString(0));
                plantationKeyAndIDModel.setId(cursor.getString(1));
                value.add(plantationKeyAndIDModel);
//                value.get(i).setKey(cursor.getString(0));
//                value.get(i).setId(cursor.getString(1));
                i++;
            }
//            value = cursor.getString(1);
//            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value.get(0).setId("0");
        }
        return value;
    }

    public ArrayList<String> getRujvatDataRJ(String plCode) {
        ArrayList<String> value = new ArrayList<>();
//        plCode="0000542";
        try {
            SQLiteDatabase db = this.getReadableDatabase();
            String sql = "SELECT * FROM " + TBL_RUJVAT_DATA_SAVED_RJ + " WHERE " + OBJECT_ID_RJ + " = '" + plCode + "' ;";
            Cursor cursor = db.rawQuery(sql, null);
            int i = 0;
            while (cursor.moveToNext()) {
                try {
                    value.add(cursor.getString(0));
                    value.add(cursor.getString(1));
                    value.add(cursor.getString(2));
                    value.add(cursor.getString(3));
                    value.add(cursor.getString(4));
                    value.add(cursor.getString(5));
                    value.add(cursor.getString(6));
                    value.add(cursor.getString(7));
                    value.add(cursor.getString(8));
                    value.add(cursor.getString(9));
                    value.add(cursor.getString(10));
                    value.add(cursor.getString(11));
                    value.add(cursor.getString(12));
                    value.add(cursor.getString(13));
                    value.add(cursor.getString(14));
                    value.add(cursor.getString(15));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                i++;
            }
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value.add(AppConstants.TABLE_NOT_AVAILABLE);
        }
        return value;
    }


    //====================================METHOD FOR GETTING DATA PLANTATION KEY AND IMAGE FROM KEY =======================
    public String getAllVillageListData() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_ALL_VILLAGE_LIST + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA CANE QUALITY =======================
    public String getAllCaneQuality() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_CANE_QUALITY + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }


    /**
     * Get Data Query Functions End */


    /**
     * Delete Data Query Functions Start
     */


    //=============================================METHOD FOR DELETING DATA FROM TRACE_EPLOYEE===================================================
    public void deleteTraceEmployeeData() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_TRACE_LAT_LONG + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM TRACE_EPLOYEE===================================================
    public void deleteTraceEmployeeTop300(int id) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_TRACE_LAT_LONG + " WHERE " + TRACE_ID + " <= " + id + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM RUJVAR_REASON===================================================
    public void deleteRujvatReasonData() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_PLANTATION_RUJVAT_REASON + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM EMPLOYEE_TRIP_DATA===================================================
    public void deleteEmployeeTripData() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_EMPLOYEE_TRIP + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM PlantationDataSubmit===================================================
    public void deletePlantationDataSubmit() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_PLANTATION_DATA_SUBMIT + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM PlantationAndRujvatImageData===================================================
    public void deletePlantationAndRujvatImageData() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING DATA FROM RUJVAR_REASON===================================================
    public void deleteRujvatDataRJ() {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_RUJVAT_DATA_SAVED_RJ + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=============================================METHOD FOR DELETING TABLE===================================================
    public void deleteTables(String tableName) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + tableName + ";";
            db.execSQL(deleteQuery);
            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Delete Data Query Functions End */


    /**
     * Drop Table Functions Start
     */
//=============================================METHOD FOR DROP TABLE TRACKING===================================================
    public void dropTraceTbl(SQLiteDatabase db, String tbl_Name) {
        try {
            String dropQuery = "DROP TABLE IF EXISTS '" + tbl_Name + "'";
            db.execSQL(dropQuery);
//            db.close();
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Drop Table Query Functions End */

    /**
     * Update Recorde Functions Start
     */

    //====================================METHOD FOR UPDATE LIST OF PLANTATION IMAGES =======================
    public String updatePlantationImageName(String key, String img1, String img2, String img3, String img4, String plantation) {
        String value = "";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues newValues = new ContentValues();
            newValues.put(PLANTATION_ID_PR, plantation);
            newValues.put(IMAGE_1_PR, img1);
            newValues.put(IMAGE_2_PR, img2);
            newValues.put(IMAGE_3_PR, img3);
            newValues.put(IMAGE_4_PR, img4);
            db.update(TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT, newValues, PLANTATION_ID_PR + "=" + key, null);
//            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
//            Cursor cursor = db.rawQuery(sql, null);
            value = "DONE";
            System.out.print(value);
            db.close();
        } catch (Exception ex) {
            value = "FAIL";
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return value;
    }

    /**
     * Update Recorde Functions End */
//Rezorection Remex


    /**
     * Initialization of variables Start
     */

    //    =============================================Database Name and Version===================================================
//    private final static String DATABASE_NAME = "CANE_BUDDY_APP_1";
//    private final static int DATABASE_VERSION = 1;


    //    =============================================Create Database for Farmer_List===================================================
//    private final String TBL_FARMER_LIST_TC = "TBL_FARMER_LIST_TC";
//    private final String ID_FARMER_TC = "ID_TC";
//    private final String FARMER_LIST_TC = "FARMER_LIST_TC";

    //    =============================================Create Database for PLANTATION_TYPE===================================================
    private final String TBL_PLANTATION_LIST_TC = "TBL_PLANTATION_LIST_TC";
//    private final String ID_PLANTATION_TYPE = "ID_TC";
//    private final String PLANTATION_TYPE_LIST = "PLANTATION_TYPE_LIST";

    //    =============================================Create Database for VEHICLE_TYPE===================================================
    private final String TBL_VEHICLE_LIST_TC = "TBL_VEHICLE_LIST_TC";
    private final String ID_VEHICLE_TYPE = "ID_TC";
    private final String VEHICLE_TYPE_LIST = "VEHICLE_TYPE_LIST";

    //    =============================================Create Database for Get_HARVESTER_TYPE===================================================
    private final String TBL_HARVESTER_LIST_TC = "TBL_HARVESTER_LIST_TC";
    private final String ID_HARVESTER_TYPE = "ID_TC";
    private final String HARVESTER_TYPE_LIST = "HARVESTER_TYPE_LIST";

    //    =============================================Create Database for Get_FARMER_NAME_LIST===================================================
    private final String TBL_GROWER_FACTORY_NAME_LIST_TC = "TBL_GROWER_FACTORY_NAME_LIST_TC";
    private final String FACTORY_NAME_ID = "FACTORY_NAME_ID";
    private final String FACTORY_NAME_LIST = "FACTORY_NAME_LIST";

    //    =============================================Create Database for TOD CHITTI DATA===================================================
    private final String TBL_TOD_CHITTI_DATA_SUBMIT = "TBL_TOD_CHITTI_DATA_SUBMIT";
    private final String SR_NO_TC = "SR_NO_TC";
    private final String TOD_CHITTI_ID_TC = "TOD_CHITTI_ID_TC";
    private final String PLANTATION_CODE_TC = "PLANTATION_CODE_TC";
    private final String FARMER_CODE_TC = "FARMER_CODE_TC";
    private final String FARMER_NAME_TC = "FARMER_NAME_TC";
    private final String FARMER_VILLAGE_TC = "FARMER_VILLAGE_TC";
    private final String FARMER_GUT_TC = "FARMER_GUT_TC";
    private final String FARMER_SUBGUT_TC = "FARMER_SUBGUT_TC";
    private final String FARMER_TALUKA_TC = "FARMER_TALUKA_TC";
    private final String SHIVAR_VILLAGE_TC = "SHIVAR_VILLAGE_TC";
    private final String SHIVAR_GUT_TC = "SHIVAR_GUT_TC";
    private final String SURVEY_NUMBER_TC = "SURVEY_NUMBER_TC";
    private final String IRRIGATION_TC = "IRRIGATION_TC";
    private final String CANE_VERITY_TC = "CANE_VERITY_TC";
    private final String PLANTATION_DATE_TC = "PLANTATION_DATE_TC";
    private final String PLANTATION_TYPE_TC = "PLANTATION_TYPE_TC";
    private final String VEHICAL_NUMBER_TC = "VEHICAL_NUMBER_TC";
    private final String VEHICAL_TYPE_TC = "VEHICAL_TYPE_TC";
    private final String TRANSPORTER_NAME_TC = "TRANSPORTER_NAME_TC";
    private final String HARVEStER_CODE_TC = "HARVEStER_CODE_TC";
    private final String TRAILER_1 = "TRAILER_1";
    private final String TRAILER_2 = "TRAILER_2";
    //    private final String FLAG = "FLAG";
    private final String OBJECT_ID_TC = "OBJECT_ID_TC";
    private final String FARMER_ID_TC = "FARMER_ID_TC";
    private final String VNO_OBJECT_ID_TC = "VNO_OBJECT_ID_TC";
    private final String TRANSPORTER_ID_TC = "TRANSPORTER_ID_TC";
    private final String VEHICAL_TYPE_NO_TC = "VEHICAL_TYPE_NO_TC";
    private final String DATE_TIME_TC = "DATE_TIME_TC";
    private final String FLAG_ONLINE_SUBMIT_TC = "FLAG_ONLINE_SUBMIT_TC";
    private final String FACTORY_NAME_TC = "FACTORY_NAME_TC";
    private final String LAT_TC = "LAT_TC";
    private final String LONG_TC = "LONG_TC";
    private final String CUTTING_OBJECT_ID_TC = "CUTTING_OBJECT_ID_TC";
    private final String HRV_CODE_TC = "HRV_CODE_TC";
    private final String HRV_NAME_TC = "HRV_NAME_TC";
    private final String PLOT_START_DATE_TC = "PLOT_START_DATE_TC";
    private final String PLANT_GUT_ID_TC = "PLANT_GUT_ID_TC";
    private final String CANE_QUALITY_TC = "CANE_QUALITY_TC";
    private final String DRIVER_NAME_TC = "DRIVER_NAME_TC";
    private final String ROP_TYPE_TC = "ROP_TYPE_TC";

    //========================================= KYC form ===================================================

    private final String KYC_FORM_TBL = "KYC_FORM_TBL";
    private final String KYC_RECORD_ID = "KYC_RECORD_ID";
    private final String GROWER_FIRST_NAME = "GROWER_FIRST_NAME";
    private final String GROWER_MIDDLE_NAME = "GROWER_MIDDLE_NAME";
    private final String GROWER_LAST_NAME = "GROWER_LAST_NAME";
    private final String GROWER_MOBILE_NUMBER = "GROWER_MOBILE_NUMBER";
    private final String VILLAGE_ID = "VILLAGE_ID";
    private final String AADHAR_CARD = "AADHAR_CARD";
    private final String BANK_PASSBOOK = "BANK_PASSBOOK";
    private final String VOTING_CARD = "VOTING_CARD";
    private final String PAN_CARD = "PAN_CARD";
    private final String AADHAR_CARD_NO = "AADHAR_CARD_NO";
    private final String BANK_PASSBOOK_NO = "BANK_PASSBOOK_NO";
    private final String VOTING_CARD_NO = "VOTING_CARD_NO";
    private final String PAN_CARD_NO = "PAN_CARD_NO";
    private final String USER_ID_KYC = "USER_ID_KYC";
    private final String DATE_KYC = "DATE_KYC";
    private final String LAT_KYC = "LAT_KYC";
    private final String LONG_KYC = "LONG_KYC";
    private final String IMEI_KYC = "IMEI_KYC";


    /**
     * Initialization of variables End
     */


    /**
     * Table Create Functions Start
     */

    //==================================== METHOD FOR CREATTING TABLE FOR Farmer LIST  =====================================
//    public void createTableFarmerList(SQLiteDatabase db) {
//
//        String sql = "CREATE TABLE " + TBL_FARMER_LIST_TC + " ( "
//                + ID_FARMER_TC + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
//                + FARMER_LIST_TC + " TEXT );";
//        db.execSQL(sql);
//
//    }


    //==================================== METHOD FOR CREATTING TABLE FOR PLANTATION LIST  =====================================
    public void createTableKycForm(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + KYC_FORM_TBL + " ( "
                    + KYC_RECORD_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + GROWER_FIRST_NAME + " TEXT ,"
                    + GROWER_MIDDLE_NAME + " TEXT ,"
                    + GROWER_LAST_NAME + " TEXT ,"
                    + GROWER_MOBILE_NUMBER + " TEXT ,"
                    + VILLAGE_ID + " TEXT ,"
                    + AADHAR_CARD + " TEXT ,"
                    + BANK_PASSBOOK + " TEXT ,"
                    + VOTING_CARD + " TEXT ,"
                    + PAN_CARD + " TEXT ,"
                    + AADHAR_CARD_NO + " TEXT ,"
                    + BANK_PASSBOOK_NO + " TEXT ,"
                    + VOTING_CARD_NO + " TEXT ,"
                    + PAN_CARD_NO + " TEXT ,"
                    + USER_ID_KYC + " TEXT ,"
                    + DATE_KYC + " TEXT ,"
                    + LAT_KYC + " TEXT ,"
                    + LONG_KYC + " TEXT ,"
                    + IMEI_KYC + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR PLANTATION LIST  =====================================
    public void createTablePlantationList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_PLANTATION_LIST_TC + " ( "
                    + ID_PLANTATION_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + PLANTATION_TYPE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR Vehicle LIST  =====================================
    public void createTableVehicleList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_VEHICLE_LIST_TC + " ( "
                    + ID_VEHICLE_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + VEHICLE_TYPE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {                   //exception handling
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR Harvester LIST  =====================================
    public void createTableHarvesterList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_HARVESTER_LIST_TC + " ( "
                    + ID_HARVESTER_TYPE + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + HARVESTER_TYPE_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR Factory Name LIST  =====================================
    public void createTableFactoryNameList(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_GROWER_FACTORY_NAME_LIST_TC + " ( "
                    + FACTORY_NAME_ID + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + FACTORY_NAME_LIST + " TEXT );";
            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //==================================== METHOD FOR CREATTING TABLE FOR TOD CHITT DATA  =====================================
    public void createTableTodChittiData(SQLiteDatabase db) {
        try {
            String sql = "CREATE TABLE " + TBL_TOD_CHITTI_DATA_SUBMIT + " ( "
                    + SR_NO_TC + " INTEGER PRIMARY KEY AUTOINCREMENT ,"
                    + TOD_CHITTI_ID_TC + " TEXT ,"
                    + PLANTATION_CODE_TC + " TEXT ,"
                    + FARMER_CODE_TC + " TEXT ,"
                    + FARMER_NAME_TC + " TEXT ,"
                    + FARMER_VILLAGE_TC + " TEXT ,"
                    + FARMER_GUT_TC + " TEXT ,"
                    + FARMER_SUBGUT_TC + " TEXT ,"
                    + FARMER_TALUKA_TC + " TEXT ,"
                    + SHIVAR_VILLAGE_TC + " TEXT ,"
                    + SHIVAR_GUT_TC + " TEXT ,"
                    + SURVEY_NUMBER_TC + " TEXT ,"
                    + IRRIGATION_TC + " TEXT ,"
                    + CANE_VERITY_TC + " TEXT ,"
                    + PLANTATION_DATE_TC + " TEXT ,"
                    + PLANTATION_TYPE_TC + " TEXT ,"
                    + VEHICAL_NUMBER_TC + " TEXT ,"
                    + VEHICAL_TYPE_TC + " TEXT ,"
                    + TRANSPORTER_NAME_TC + " TEXT ,"
                    + HARVEStER_CODE_TC + " TEXT ,"
                    + TRAILER_1 + " TEXT ,"
                    + TRAILER_2 + " TEXT ,"
                    + FLAG + " TEXT ,"
                    + OBJECT_ID_TC + " TEXT ,"
                    + FARMER_ID_TC + " TEXT ,"
                    + VNO_OBJECT_ID_TC + " TEXT ,"
                    + TRANSPORTER_ID_TC + " TEXT ,"
                    + VEHICAL_TYPE_NO_TC + " TEXT ,"
                    + FLAG_ONLINE_SUBMIT_TC + " TEXT ,"
                    + DATE_TIME_TC + " TEXT ,"
                    + FACTORY_NAME_TC + " TEXT ,"
                    + LAT_TC + " TEXT ,"
                    + LONG_TC + " TEXT ,"
                    + CUTTING_OBJECT_ID_TC + " TEXT ,"
                    + HRV_CODE_TC + " TEXT ,"
                    + HRV_NAME_TC + " TEXT ,"
                    + PLOT_START_DATE_TC + " TEXT ,"
                    + PLANT_GUT_ID_TC + " TEXT ,"
                    + CANE_QUALITY_TC + " TEXT ,"
                    + DRIVER_NAME_TC + " TEXT ,"
                    + ROP_TYPE_TC + " TEXT );";

            db.execSQL(sql);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    /**
     * Table Create Functions End */


    /**
     * Insert Query Functions Start
     */

    //====================================METHOD FOR INSERTING DATA INTO FARMER LIST =======================
//    public void insertFarmerList(String farmerList) {
//        SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_FARMER_LIST_TC + ";";
//        db.execSQL(deleteQuery);
//        ContentValues cv = new ContentValues();
//        cv.put(FARMER_LIST_TC, farmerList);
//        db.insert(TBL_FARMER_LIST_TC, null, cv);
//    }

    //====================================METHOD FOR INSERTING DATA INTO PLANTATION LIST =======================
    public void insertPlantationList(String farmerList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_PLANTATION_LIST_TC + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(PLANTATION_TYPE_LIST, farmerList);
            db.insert(TBL_PLANTATION_LIST_TC, null, cv);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //====================================METHOD FOR INSERTING DATA INTO VEHICLE LIST =======================
    public void insertVehicleList(String farmerList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_VEHICLE_LIST_TC + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(VEHICLE_TYPE_LIST, farmerList);
            db.insert(TBL_VEHICLE_LIST_TC, null, cv);
        } catch (Exception ex) {
            ex.printStackTrace();
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //====================================METHOD FOR INSERTING DATA INTO HARVESTER LIST =======================
    public void insertHarvesterList(String farmerList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_HARVESTER_LIST_TC + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(HARVESTER_TYPE_LIST, farmerList);
            db.insert(TBL_HARVESTER_LIST_TC, null, cv);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //====================================METHOD FOR INSERTING DATA INTO FACTORY NAME LIST =======================
    public void insertFactoryNameList(String factoryNameList) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String deleteQuery = "DELETE FROM " + TBL_GROWER_FACTORY_NAME_LIST_TC + ";";
            db.execSQL(deleteQuery);
            ContentValues cv = new ContentValues();
            cv.put(FACTORY_NAME_LIST, factoryNameList);
            db.insert(TBL_GROWER_FACTORY_NAME_LIST_TC, null, cv);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    //=====================================INSERT DATA INTO KYC TABLE==========================================

    public void insertKycForm(String name, String middleName, String lastName, String mobileNumber, String villageName, String districtName, String gutName, String subGutName, String aadharCard, String bankPassbook, String votingCard, String panCard, String taluka, String aadharCardNo, String bankPassbookNo, String votingCardNo, String panCardNo, String userid, String date, String lat, String longg, String imei) {
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues cv = new ContentValues();
            cv.put(GROWER_FIRST_NAME, name);
            cv.put(GROWER_MIDDLE_NAME, middleName);
            cv.put(GROWER_LAST_NAME, lastName);
            cv.put(GROWER_MOBILE_NUMBER, mobileNumber);
            cv.put(VILLAGE_ID, villageName);
            cv.put(AADHAR_CARD, aadharCard);
            cv.put(BANK_PASSBOOK, bankPassbook);
            cv.put(VOTING_CARD, votingCard);
            cv.put(PAN_CARD, panCard);
            cv.put(AADHAR_CARD_NO, aadharCardNo);
            cv.put(BANK_PASSBOOK_NO, bankPassbookNo);
            cv.put(VOTING_CARD_NO, votingCardNo);
            cv.put(PAN_CARD_NO, panCardNo);
            cv.put(USER_ID_KYC, userid);
            cv.put(DATE_KYC, date);
            cv.put(LAT_KYC, lat);
            cv.put(LONG_KYC, longg);
            cv.put(IMEI_KYC, imei);
            db.insert(KYC_FORM_TBL, null, cv);
        } catch (Exception ex) {
            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }


    //====================================METHOD FOR INSERTING DATA INTO TOD CHITTI DATA =======================
    public boolean insertTodChittiData(String ID_TC, String plantation_TC, String farmerCode, String farmerName, String farmerVillage, String farmerGut, String farmerSubgut, String farmerTaluka, String shivarVillage, String shivarGut, String surveyNumber, String irrigation, String caneVerity, String plantationDate, String plantationType, String vehicalNumber, String vehicalType, String transporterName, String harvesterCode, String trailer_1, String trailer_2, String plantation_id, String farmer_id, String vnoID, String transporter_id, String vehical_id, String dateTime, String factoryName, String lat, String langg, String cuttingID, String hrvCode, String hrvName, String startDate, String gutID, String quality, String driver, String ropType) {
        SQLiteDatabase db = this.getWritableDatabase();
//        String deleteQuery = "DELETE FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + ";";
//        db.execSQL(deleteQuery);
        try {
            ContentValues cv = new ContentValues();
            cv.put(TOD_CHITTI_ID_TC, ID_TC);
            cv.put(PLANTATION_CODE_TC, plantation_TC);
            cv.put(FARMER_CODE_TC, farmerCode);
            cv.put(FARMER_NAME_TC, farmerName);
            cv.put(FARMER_VILLAGE_TC, farmerVillage);
            cv.put(FARMER_GUT_TC, farmerGut);
            cv.put(FARMER_SUBGUT_TC, farmerSubgut);
            cv.put(FARMER_TALUKA_TC, farmerTaluka);
            cv.put(SHIVAR_VILLAGE_TC, shivarVillage);
            cv.put(SHIVAR_GUT_TC, shivarGut);
            cv.put(SURVEY_NUMBER_TC, surveyNumber);
            cv.put(IRRIGATION_TC, irrigation);
            cv.put(CANE_VERITY_TC, caneVerity);
            cv.put(PLANTATION_DATE_TC, plantationDate);
            cv.put(PLANTATION_TYPE_TC, plantationType);
            cv.put(VEHICAL_NUMBER_TC, vehicalNumber);
            cv.put(VEHICAL_TYPE_TC, vehicalType);
            cv.put(TRANSPORTER_NAME_TC, transporterName);
            cv.put(HARVEStER_CODE_TC, harvesterCode);
            cv.put(TRAILER_1, trailer_1);
            cv.put(TRAILER_2, trailer_2);
            cv.put(FLAG, "N");
            cv.put(OBJECT_ID_TC, plantation_id);
            cv.put(FARMER_ID_TC, farmer_id);
            cv.put(VNO_OBJECT_ID_TC, vnoID);
            cv.put(TRANSPORTER_ID_TC, transporter_id);
            cv.put(VEHICAL_TYPE_NO_TC, vehical_id);
            cv.put(DATE_TIME_TC, dateTime);
            cv.put(FLAG_ONLINE_SUBMIT_TC, "N");
            cv.put(FACTORY_NAME_TC, factoryName);
            cv.put(LAT_TC, lat);
            cv.put(LONG_TC, langg);
            cv.put(CUTTING_OBJECT_ID_TC, cuttingID);
            cv.put(HRV_CODE_TC, hrvCode);
            cv.put(HRV_NAME_TC, hrvName);
            cv.put(PLOT_START_DATE_TC, startDate);
            cv.put(PLANT_GUT_ID_TC, gutID);
            cv.put(CANE_QUALITY_TC, quality);
            cv.put(DRIVER_NAME_TC, driver);
            cv.put(ROP_TYPE_TC, ropType);
            db.insert(TBL_TOD_CHITTI_DATA_SUBMIT, null, cv);
            return true;
        } catch (Exception ex) {
            return false;
        }
    }


//9595953721 //

    /**
     * Insert Query Functions End */


    /**
     * Get Data Query Functions Start
     */

    //====================================METHOD FOR GETTING DATA OF FARMER LIST =======================
//    public String getFarmerList() {
//        String value;
//        try {
//            SQLiteDatabase db = this.getWritableDatabase();
//            String sql = "SELECT * FROM " + TBL_FARMER_LIST_TC + ";";
//            Cursor cursor = db.rawQuery(sql, null);
//            cursor.moveToNext();
//            value = cursor.getString(1);
//            System.out.print(value);
//        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
//        }
//        return value;
//     
//    }

    //====================================METHOD FOR GETTING DATA OF PLANTATION LIST =======================
    public String getPlanatationList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_PLANTATION_LIST_TC + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF VEHICLE LIST =======================
    public String getVehicleList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_VEHICLE_LIST_TC + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF HARVESTER LIST =======================
    public String getHarvesterList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_HARVESTER_LIST_TC + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF FACTORY NAME LIST =======================
    public String getFactoryNameList() {
        String value;
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_GROWER_FACTORY_NAME_LIST_TC + ";";
            Cursor cursor = db.rawQuery(sql, null);
            cursor.moveToNext();
            value = cursor.getString(1);
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }


    //====================================METHOD FOR GETTING DATA OF KYC FORM ==============================

    public String getKycData() {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + KYC_FORM_TBL + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_first_name", cursor.getString(1));
                    innerjsonObject.put("p_middel_name", cursor.getString(2));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_last_name", cursor.getString(3));
                    innerjsonObject.put("p_mob", cursor.getString(4));
                    innerjsonObject.put("p_village_id", cursor.getString(5));
                    innerjsonObject.put("p_aadhar_name", cursor.getString(6));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_pan_name", cursor.getString(7));
                    innerjsonObject.put("p_bank_name", cursor.getString(8));
                    innerjsonObject.put("p_vote_name", cursor.getString(9));
                    innerjsonObject.put("p_aadharno", cursor.getString(10));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_panno", cursor.getString(11));
                    innerjsonObject.put("p_voterid", cursor.getString(12));
                    innerjsonObject.put("p_accountno", cursor.getString(13));
                    innerjsonObject.put("p_user_id", cursor.getString(14));
                    innerjsonObject.put("p_date", cursor.getString(15));
                    innerjsonObject.put("p_lat", cursor.getString(16));
                    innerjsonObject.put("p_lng", cursor.getString(17));
                    innerjsonObject.put("p_device", cursor.getString(18));
                    jsonArray.put(innerjsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            value = jsonObject.put("KYCInfo", jsonArray).toString();
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.TABLE_NOT_AVAILABLE;
        }
        return value;
    }

    public ArrayList<KYCModel> getKycDataArrayList() {
        KYCModel kycModel = new KYCModel();
        ArrayList<KYCModel> kycModelArrayList = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + KYC_FORM_TBL + ";";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    kycModel.setP_first_name(cursor.getString(1));
                    kycModel.setP_middel_name(cursor.getString(2));
                    kycModel.setP_last_name(cursor.getString(3));
                    kycModel.setP_mob(cursor.getString(4));
                    kycModel.setP_village_id(cursor.getString(5));
                    kycModel.setP_aadhar_name(cursor.getString(6));
                    kycModel.setP_pan_name(cursor.getString(7));
                    kycModel.setP_bank_name(cursor.getString(8));
                    kycModel.setP_vote_name(cursor.getString(9));
                    kycModel.setP_aadharno(cursor.getString(10));
                    kycModel.setP_panno(cursor.getString(11));
                    kycModel.setP_voterid(cursor.getString(12));
                    kycModel.setP_accountno(cursor.getString(13));
                    kycModel.setP_user_id(cursor.getString(14));
                    kycModel.setP_date(cursor.getString(15));
                    kycModel.setP_lat(cursor.getString(16));
                    kycModel.setP_lng(cursor.getString(17));
                    kycModel.setP_device(cursor.getString(18));
                    kycModelArrayList.add(kycModel);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            db.close();
        } catch (Exception e) {

        }
        return kycModelArrayList;
    }

    //====================================METHOD FOR GETTING DATA OF TOD CHITTI LIST =======================


    public ArrayList<PlantationDataOfTCModel> getTodChittiList() {
        ArrayList<PlantationDataOfTCModel> value = new ArrayList<>();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + " WHERE " + FLAG + " = 'N'  ORDER BY " + SR_NO_TC + " DESC ;";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                PlantationDataOfTCModel plantationDataOfTCModel = new PlantationDataOfTCModel();
                try {
                    plantationDataOfTCModel.setTOD_CHITTI_ID(cursor.getString(1));
                    plantationDataOfTCModel.setPLANTATION_CODE_TC(cursor.getString(2));
                    plantationDataOfTCModel.setFARMER_CODE_TC(cursor.getString(3));
                    plantationDataOfTCModel.setFARMER_NAME_TC(cursor.getString(4));
                    plantationDataOfTCModel.setFARMER_VILLAGE_TC(cursor.getString(5));
                    plantationDataOfTCModel.setFARMER_GUT_TC(cursor.getString(6));
                    plantationDataOfTCModel.setFARMER_SUBGUT_TC(cursor.getString(7));
                    plantationDataOfTCModel.setFARMER_TALUKA_TC(cursor.getString(8));
                    plantationDataOfTCModel.setSHIVAR_VILLAGE_TC(cursor.getString(9));
                    plantationDataOfTCModel.setSHIVAR_GUT_TC(cursor.getString(10));
                    plantationDataOfTCModel.setSURVEY_NUMBER_TC(cursor.getString(11));
                    plantationDataOfTCModel.setIRRIGATION_TC(cursor.getString(12));
                    plantationDataOfTCModel.setCANE_VERITY_TC(cursor.getString(13));
                    plantationDataOfTCModel.setPLANTATION_DATE_TC(cursor.getString(14));
                    plantationDataOfTCModel.setPLANTATION_TYPE_TC(cursor.getString(15));
                    plantationDataOfTCModel.setVEHICAL_NUMBER_TC(cursor.getString(16));
                    plantationDataOfTCModel.setVEHICAL_TYPE_TC(cursor.getString(17));
                    plantationDataOfTCModel.setTRANSPORTER_NAME_TC(cursor.getString(18));
                    plantationDataOfTCModel.setHARVEStER_CODE_TC(cursor.getString(19));
                    plantationDataOfTCModel.setTRAILER_1(cursor.getString(20));
                    plantationDataOfTCModel.setTRAILER_2(cursor.getString(21));
                    plantationDataOfTCModel.setFLAG(cursor.getString(22));
                    plantationDataOfTCModel.setOBJECT_ID(cursor.getString(23));
                    plantationDataOfTCModel.setFARMER_ID(cursor.getString(24));
                    plantationDataOfTCModel.setVNO_OBJECT_ID(cursor.getString(25));
                    plantationDataOfTCModel.setTRANSPORTER_ID(cursor.getString(26));
                    plantationDataOfTCModel.setVEHICAL_TYPE(cursor.getString(27));
                    plantationDataOfTCModel.setFLAG_ONLINE_SUBMIT_TC(cursor.getString(28));
                    plantationDataOfTCModel.setDATE_TIME_TC(cursor.getString(29));
                    plantationDataOfTCModel.setFACTORY_NAME_TC(cursor.getString(30));
                    plantationDataOfTCModel.setLAT_TC(cursor.getString(31));
                    plantationDataOfTCModel.setLONG_TC(cursor.getString(32));
                    plantationDataOfTCModel.setCUTTING_OBJECT_ID_TC(cursor.getString(33));
                    plantationDataOfTCModel.setHRV_CODE_TC(cursor.getString(34));
                    plantationDataOfTCModel.setHRV_NAME_TC(cursor.getString(35));
                    plantationDataOfTCModel.setPLOT_START_DATE_TC(cursor.getString(36));
                    plantationDataOfTCModel.setPLANT_GUT_ID_TC(cursor.getString(37));
                    plantationDataOfTCModel.setCANE_QUALITY_TC(cursor.getString(38));
                    plantationDataOfTCModel.setDRIVER_NAME_TC(cursor.getString(39));
                    plantationDataOfTCModel.setROP_TYPE_TC(cursor.getString(40));
                } catch (Exception e) {
                    e.printStackTrace();
                }
                value.add(plantationDataOfTCModel);
            }
            System.out.print(value);
            db.close();
        } catch (Exception e) {
//            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    //====================================METHOD FOR GETTING DATA OF TOD CHITTI LIST =======================
    public String getTodChittiListSubmit(String deviceID) {
        String value = "";
        JSONObject jsonObject = new JSONObject();
        JSONArray jsonArray = new JSONArray();
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            String sql = "SELECT * FROM " + TBL_TOD_CHITTI_DATA_SUBMIT + " WHERE " + FLAG_ONLINE_SUBMIT_TC + " = 'N' ;";
            Cursor cursor = db.rawQuery(sql, null);
            while (cursor.moveToNext()) {
                try {
                    JSONObject innerjsonObject = new JSONObject();
                    innerjsonObject.put("p_date", cursor.getString(28));
                    innerjsonObject.put("p_device_id", deviceID);//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_user_id", cursor.getString(2));
                    innerjsonObject.put("p_farmer_id", cursor.getString(24));
                    innerjsonObject.put("p_plantation", cursor.getString(23));
                    innerjsonObject.put("p_trasporter_id", cursor.getString(26));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_veh_id", cursor.getString(25));
                    innerjsonObject.put("p_harvestor_id", cursor.getString(19));
                    innerjsonObject.put("p_tr_1", cursor.getString(20));
                    innerjsonObject.put("p_tr_2", cursor.getString(21));//dd-MMM-yyyy HH:mm:ss);
                    innerjsonObject.put("p_slip", cursor.getString(1));
                    innerjsonObject.put("p_cane_quality", cursor.getString(38));
                    innerjsonObject.put("p_driver_name", cursor.getString(39));
                    innerjsonObject.put("p_rop_type", cursor.getString(40));
                    jsonArray.put(innerjsonObject);
                } catch (Exception e) {
                    e.printStackTrace();
                }
            }
            value = jsonObject.put("SlipSubmit", jsonArray).toString();
//p_date,p_device_id,p_user_id,p_farmer_id,p_plantation,p_trasporter_id,p_veh_id,p_harvestor_id,p_tr_1,p_tr_2,p_slip,
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = AppConstants.DATA_BASE_NOT_AVAILABLE;
        }
        return value;
    }

    /**
     * Get Data Query Functions End */


    /**
     * Delete Data Query Functions Start
     */

//  ========

    /**
     * Delete Data Query Functions End */

    /**
     * Update Recorde Functions Start
     */

    //====================================METHOD FOR UPDATE LIST OF PLANTATION IMAGES =======================
    public String updateTodChittiData(String todChittiID, String flag) {
        String value = "";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues newValues = new ContentValues();
            newValues.put(FLAG, flag);
//            int update = db.update(TBL_TOD_CHITTI_DATA_SUBMIT, newValues, OBJECT_ID_TC + "=" + plantationID, null);
            int update = db.update(TBL_TOD_CHITTI_DATA_SUBMIT, newValues, TOD_CHITTI_ID_TC + "=" + todChittiID, null);
//            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
//            Cursor cursor = db.rawQuery(sql, null);
            value = "DONE" + update;
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = "FAIL";
        }
        return value;
    }


    //====================================METHOD FOR UPDATE LIST OF PLANTATION IMAGES =======================
    public String updateTodChittiOnlineFlag(String slipId, String flag) {
        String value = "";
        try {
            SQLiteDatabase db = this.getWritableDatabase();
            ContentValues newValues = new ContentValues();
            newValues.put(FLAG_ONLINE_SUBMIT_TC, flag);
            int update = db.update(TBL_TOD_CHITTI_DATA_SUBMIT, newValues, TOD_CHITTI_ID_TC + "=" + slipId, null);
//            String sql = "SELECT * FROM " + TBL_PLANTATION_AND_RUJVAT_IMAGE_LOCATION_SUBMIT + ";";
//            Cursor cursor = db.rawQuery(sql, null);
            value = "DONE" + update;
            System.out.print(value);
            db.close();
        } catch (Exception e) {
            value = "FAIL";
        }
        return value;
    }

    public String getPAN_CARD_NO() {
        return PAN_CARD_NO;
    }

//  ========

    /**
     * Update Recorde Functions End */


}


