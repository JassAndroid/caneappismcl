package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.Manifest;
import android.app.ProgressDialog;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.graphics.Typeface;
import android.os.AsyncTask;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.RequiresApi;
import android.support.design.widget.Snackbar;
import android.support.design.widget.TextInputLayout;
import android.support.v4.app.ActivityCompat;
import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentTransaction;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.Toolbar;
import android.telephony.TelephonyManager;
import android.text.Editable;
import android.text.TextWatcher;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.EditText;
import android.widget.LinearLayout;
import android.widget.TextView;
import android.widget.Toast;

import com.google.gson.Gson;
import com.praful.jass.ismclcaneapp.Activity.Phase1.GetGeoLocationAndImagesActivity;
import com.praful.jass.ismclcaneapp.DBHelper.SQLiteHelper;
import com.praful.jass.ismclcaneapp.ManagerClasses.JSONEManager;
import com.praful.jass.ismclcaneapp.ManagerClasses.XMLManager;
import com.praful.jass.ismclcaneapp.Models.CaneTypeListModel;
import com.praful.jass.ismclcaneapp.Models.CustomLocationModel;
import com.praful.jass.ismclcaneapp.Models.SeasonNameListModel;
import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.Utils.AppConstants;
import com.praful.jass.ismclcaneapp.Utils.ChangeLocaleLanguage;
import com.praful.jass.ismclcaneapp.Utils.Preferences;
import com.praful.jass.ismclcaneapp.Utils.SystemDateTime;
import com.praful.jass.ismclcaneapp.services.CallSoap;

import org.json.JSONArray;
import org.json.JSONException;
import org.json.JSONObject;

import java.text.DateFormat;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.Objects;

public class PlantationRegistrationFragment extends Fragment {

    Button btn_submit;
    CallSoap cs;
    XMLManager xmlManager;
    LinearLayout layout_grower_name, layout_shivar_village, layout__irrigation_source, layout_cane_varity, layout_cane_type, layout_plantation_type, layout_rujvat_reason, layout_rujvat_reason_outer, layout_rujvat_area, layout_plantation_area, layout_season;
    TextInputLayout txtInpLVillage;

    //==============================================List Text View================================================
    TextView grower_name, shivar_village, irrigation_source, cane_varity, cane_type, plantation_type, season_list;

    //==============================================View Text View================================================
    TextView txt_village, txt_gut, txt_subgut, txt_taluka, txt_shivar_gut, txt_title;

//    ================================================Edit Textr================================================

    EditText txt_surveyNumber, txt_surveyDate, txt_plantationDate, txt_plantationArea; //txt_formNumber, txt_formDate, txt_agrementNumber, txt_agrementDate,

    //=============Variable for gps========================
    private static final int REQUEST_CODE_PERMISSION = 2;
    String mPermission = Manifest.permission.ACCESS_FINE_LOCATION;
    //    GPSTracker gps;
    private FragmentManager fragmentManager;
    OptionListFragment fragment;
    SQLiteHelper sqLiteHelper;
    ArrayList<String> namLlist = new ArrayList();
    ArrayList<String> growarCode = new ArrayList();
    ArrayList<CaneTypeListModel> deatilsList;
    int selectListType = 0;
    Bundle bundle;
    private Toolbar toolbar;
    public ProgressDialog pdLoading;
    String user_id, grower_id, form_no, sarve_no, agrement_no, varity_id, cane_type_id, irrigation, plantation_method, total_area, shivar_village_id, shivar_gut_id, shivar_sub_gut_id, user_idp, selected_season;
    String lat, longi;
    String device;
    String enter_date, form_date, sarve_date, plantation_date, agrement_date, plantation_area;
    SystemDateTime dateTime = new SystemDateTime();
    TelephonyManager telephonyManager;
    ArrayList<String> season = new ArrayList<>();
    ArrayList<SeasonNameListModel> seasonList = new ArrayList<>();


    @Override
    public View onCreateView(@NonNull LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {

        final View view = inflater.inflate(R.layout.fragment_plantation_registration, container, false);
        try {
            // Inflate the layout for this fragment
            telephonyManager = (TelephonyManager) Objects.requireNonNull(getActivity()).getSystemService(Context.TELEPHONY_SERVICE);

            toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
            toolbar.setTitle(getString(R.string.nav_plantation));

            bundle = this.getArguments();
            init(view);
            //setInputLayoutColor();
            btn_submit.setOnClickListener(new View.OnClickListener() {
                @RequiresApi(api = Build.VERSION_CODES.M)
                @Override
                public void onClick(View v) {
                    if (validateData()) {
                        pdLoading.show();
                        if (setValues()) {
                            int plID = Preferences.getPlantationIDValueKey() + AppConstants.PLANTATIONID_ADDER_PL;
                            sqLiteHelper.insertPlantationDataSubmit(enter_date, grower_id, form_no, form_date, sarve_no, sarve_date, agrement_no, agrement_date, plantation_date, varity_id, cane_type_id, irrigation, plantation_area, plantation_method, total_area, shivar_village_id, shivar_gut_id, shivar_sub_gut_id, device, user_id, lat, longi, plID + "", selected_season);
                            Snackbar.make(view.findViewById(R.id.activity_registration), getString(R.string.submitted_successfully), Snackbar.LENGTH_LONG).show();
                            Intent intent = new Intent(getContext(), GetGeoLocationAndImagesActivity.class);
                            intent.putExtra("plantationID", plID + "");
                            intent.putExtra("flag", AppConstants.PLANTATION_CONSTANT);
                            Preferences.setPlantationIDValueKey(plID);
                            startActivity(intent);
                        } else
                            Toast.makeText(getContext(), getString(R.string.plantation_fail_submit), Toast.LENGTH_SHORT).show();
                        pdLoading.dismiss();
                    }
                }
            });

            layout_season.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    selectListType = AppConstants.SEASON_SELECT;
                    new AsyncCaller().execute();
                }
            });
            layout_grower_name.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.GROWER_SELECT;
                    new AsyncCaller().execute();
                }
            });

            layout_shivar_village.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.SHIVAR_SELECT;
                    new AsyncCaller().execute();

                }
            });

            layout__irrigation_source.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.IRRIGATION_SELECT;
                    new AsyncCaller().execute();
                }
            });

            layout_cane_varity.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.CAN_VERIETY_SELECT;
                    new AsyncCaller().execute();
                }
            });

            layout_cane_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.CAN_TYPE_SELECT;
                    new AsyncCaller().execute();
                }
            });

            layout_plantation_type.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    selectListType = AppConstants.PLANTATION_TYPE_SELECT;
                    new AsyncCaller().execute();
                }
            });

            txt_surveyNumber.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
//                if (txt_surveyNumber.getText().toString().trim().equals("") || txt_surveyNumber.getText().toString().trim().equals("Survey Number")) {
                    txt_surveyNumber.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    txt_surveyNumber.setTypeface(null, Typeface.BOLD);
//                }
                }
            });
            txt_surveyDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    txt_surveyDate.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    txt_surveyDate.setTypeface(null, Typeface.BOLD);
                }
            });
            txt_plantationDate.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
//                    if (b) {
                    txt_plantationDate.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    txt_plantationDate.setTypeface(null, Typeface.BOLD);
//                    } else {
//                        selectListType = AppConstants.CAN_TYPE_SELECT;
//                        new AsyncCaller().execute();
//                    }
                }

            });

            txt_plantationArea.setOnFocusChangeListener(new View.OnFocusChangeListener() {
                @Override
                public void onFocusChange(View view, boolean b) {
                    txt_plantationArea.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                    txt_plantationArea.setTypeface(null, Typeface.BOLD);
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return view;
    }

    public void init(View view) {
        try {
            btn_submit = (Button) view.findViewById(R.id.btn_submit);
            grower_name = (TextView) view.findViewById(R.id.grower_name);
            shivar_village = (TextView) view.findViewById(R.id.shivar_village);
            irrigation_source = (TextView) view.findViewById(R.id.irrigation_source);
            cane_varity = (TextView) view.findViewById(R.id.cane_varity);
            cane_type = (TextView) view.findViewById(R.id.cane_type);
            plantation_type = (TextView) view.findViewById(R.id.plantation_type);
            txt_village = (TextView) view.findViewById(R.id.txt_village);
            txt_gut = (TextView) view.findViewById(R.id.txt_gut);
            txt_subgut = (TextView) view.findViewById(R.id.txt_subgut);
            txt_taluka = (TextView) view.findViewById(R.id.txt_taluka);
            txt_shivar_gut = (TextView) view.findViewById(R.id.txt_shivar_gut);
            season_list = (TextView) view.findViewById(R.id.season_list);

            layout_grower_name = (LinearLayout) view.findViewById(R.id.layout_grower_name);
            layout_shivar_village = (LinearLayout) view.findViewById(R.id.layout_shivar_village);
            layout__irrigation_source = (LinearLayout) view.findViewById(R.id.layout__irrigation_source);
            layout_cane_varity = (LinearLayout) view.findViewById(R.id.layout_cane_varity);
            layout_cane_type = (LinearLayout) view.findViewById(R.id.layout_cane_type);
            layout_plantation_type = (LinearLayout) view.findViewById(R.id.layout_plantation_type);
            layout_season = (LinearLayout) view.findViewById(R.id.layout_season);
//            layout_rujvat_reason = (LinearLayout) view.findViewById(R.id.layout_rujvat_reason);
            //layout_plantation_area = (LinearLayout) view.findViewById(R.id.layout_plantation_area);

            txt_surveyNumber = (EditText) view.findViewById(R.id.txt_surveyNumber);
            txt_surveyDate = (EditText) view.findViewById(R.id.txt_surveyDate);
            txt_plantationDate = (EditText) view.findViewById(R.id.txt_plantationDate);
            txt_plantationArea = (EditText) view.findViewById(R.id.txt_plantationArea);

            txt_surveyDate.addTextChangedListener(surveyDateWatcher);
            txt_plantationDate.addTextChangedListener(plantationDateWatcher);

            pdLoading = new ProgressDialog(getActivity());
            pdLoading.setCancelable(false);
            cs = new CallSoap();
            fragmentManager = getActivity().getSupportFragmentManager();
            xmlManager = new XMLManager();
            sqLiteHelper = new SQLiteHelper(getContext());

            txtInpLVillage = view.findViewById(R.id.txtInpL_village);

        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    TextWatcher surveyDateWatcher = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8) {
                        clean = clean + ddmmyyyy.substring(clean.length());
                    } else {
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int day = Integer.parseInt(clean.substring(0, 2));
                        int mon = Integer.parseInt(clean.substring(2, 4));
                        int year = Integer.parseInt(clean.substring(4, 8));

                        if (mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 2000) ? 2000 : (year > 2200) ? 2200 : year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012


                        day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                        clean = String.format("%02d%02d%02d", day, mon, year);
                    }

                    clean = String.format("%s/%s/%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    txt_surveyDate.setText(current);
                    txt_surveyDate.setSelection(sel < current.length() ? sel : current.length());
                }
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };
    TextWatcher plantationDateWatcher = new TextWatcher() {
        private String current = "";
        private String ddmmyyyy = "DDMMYYYY";
        private Calendar cal = Calendar.getInstance();

        @Override
        public void onTextChanged(CharSequence s, int start, int before, int count) {
            try {
                if (!s.toString().equals(current)) {
                    String clean = s.toString().replaceAll("[^\\d.]", "");
                    String cleanC = current.replaceAll("[^\\d.]", "");

                    int cl = clean.length();
                    int sel = cl;
                    for (int i = 2; i <= cl && i < 6; i += 2) {
                        sel++;
                    }
                    //Fix for pressing delete next to a forward slash
                    if (clean.equals(cleanC)) sel--;

                    if (clean.length() < 8) {
                        clean = clean + ddmmyyyy.substring(clean.length());
                    } else {
                        //This part makes sure that when we finish entering numbers
                        //the date is correct, fixing it otherwise
                        int day = Integer.parseInt(clean.substring(0, 2));
                        int mon = Integer.parseInt(clean.substring(2, 4));
                        int year = Integer.parseInt(clean.substring(4, 8));

                        if (mon > 12) mon = 12;
                        cal.set(Calendar.MONTH, mon - 1);
                        year = (year < 2000) ? 2000 : (year > 2200) ? 2200 : year;
                        cal.set(Calendar.YEAR, year);
                        // ^ first set year for the line below to work correctly
                        //with leap years - otherwise, date e.g. 29/02/2012
                        //would be automatically corrected to 28/02/2012

                        day = (day > cal.getActualMaximum(Calendar.DATE)) ? cal.getActualMaximum(Calendar.DATE) : day;
                        clean = String.format("%02d%02d%02d", day, mon, year);
                    }

                    clean = String.format("%s/%s/%s", clean.substring(0, 2),
                            clean.substring(2, 4),
                            clean.substring(4, 8));

                    sel = sel < 0 ? 0 : sel;
                    current = clean;
                    txt_plantationDate.setText(current);
                    txt_plantationDate.setSelection(sel < current.length() ? sel : current.length());
                }
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        public void beforeTextChanged(CharSequence charSequence, int i, int i1, int i2) {

        }

        @Override
        public void afterTextChanged(Editable editable) {

        }
    };

    private ArrayList<String> getJsoneManager(String jsone) {
        try {
            new JSONEManager().getJSONEResults(new JSONObject(jsone), CaneTypeListModel.class, new JSONEManager.APIManagerInterface() {
                @Override
                public void onSuccess(Object resultObj) {
                    deatilsList = new ArrayList();
                    if (resultObj instanceof ArrayList) {
                        if (selectListType == AppConstants.GROWER_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getFarmerName());
                                growarCode.add(i, deatilsList.get(i).getFarmerCode());
                            }
                        } else if (selectListType == AppConstants.SHIVAR_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getVillageName());
                            }
                        } else if (selectListType == AppConstants.IRRIGATION_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getIrrigationMasterName());
                            }
                        } else if (selectListType == AppConstants.PLANTATION_TYPE_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getPlantationName());
                            }
                        } else if (selectListType == AppConstants.CAN_VERIETY_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getCaneVerityName());
                            }
                        } else if (selectListType == AppConstants.CAN_TYPE_SELECT) {
                            namLlist = new ArrayList();
                            growarCode = new ArrayList();
                            deatilsList = (ArrayList<CaneTypeListModel>) resultObj;
                            for (int i = 0; i < deatilsList.size(); i++) {
                                namLlist.add(i, deatilsList.get(i).getCaneTypeName());
                            }
                        }
                    }
                }

                @Override
                public void onError(String strError) {

                }
            });
        } catch (JSONException ex) {
            ex.printStackTrace();
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return namLlist;
    }

    @RequiresApi(api = Build.VERSION_CODES.M)
    public boolean setValues() {
        boolean returnVal = false;
        try {
//        getCurrentLocation();
            CustomLocationModel locationModel = new Gson().fromJson(Preferences.getUserLocation(), CustomLocationModel.class);
            ChangeLocaleLanguage changeLocaleLanguage = new ChangeLocaleLanguage();
            changeLocaleLanguage.changeLocale(AppConstants.ENGLISH, getContext());
            enter_date = dateTime.systemDateTime("dd-MMM-yyyy");//dd-MMM-yyyy HH:mm:ss
            form_no = "";//txt_formNumber.getText().toString();
            form_date = "";//changeDateFormate(txt_formDate.getText().toString());
            user_id = Preferences.getCurrentUserId() + "";
            agrement_no = "";//txt_agrementNumber.getText().toString();
            agrement_date = "";//changeDateFormate(txt_agrementDate.getText().toString());
            sarve_no = txt_surveyNumber.getText().toString();
            sarve_date = changeDateFormate(txt_surveyDate.getText().toString());
            plantation_date = changeDateFormate(txt_plantationDate.getText().toString());
            plantation_area = txt_plantationArea.getText().toString();
            changeLocaleLanguage.changeLocale(Preferences.getLocalLanguage(), getContext());
            if (ActivityCompat.checkSelfPermission(getContext(), Manifest.permission.READ_PHONE_STATE) != PackageManager.PERMISSION_GRANTED) {
                // TODO: Consider calling

//                return false;
            }
//        device = telephonyManager.getDeviceId(1); //(API level 23)
            if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                device = telephonyManager.getDeviceId(2);
            } else {
                device = telephonyManager.getDeviceId();//getDeviceId(1);
            }
            lat = locationModel.getLatitude() + "";
            longi = locationModel.getLongitude() + "";
            returnVal = true;
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            returnVal = false;
        }
        return returnVal;
    }

    private void showOptionList(ArrayList<String> listArray, final TextView textView) {
        try {
            final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);

            fragment = new OptionListFragment();
            fragment.updateList(listArray, textView.getText().toString(), growarCode);
            if (selectListType == AppConstants.GROWER_SELECT) {
                fragment.getFarmerNameList(growarCode);
            }
            fragment.setListTitle("kljklkllk");

            fragment.listner = new OptionListFragment.OptionListInterface() {
                @Override
                public void onListDismiss() {
                    try {
                        removeOptionList();
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }

                @Override
                public void onListItemSelected(Object model) {

                    try {
                        // you can use this model to show selected value in the text
                        if (model instanceof String) {
                            if (selectListType == AppConstants.GROWER_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                txt_village.setText(deatilsList.get(AppConstants.selectItemIndex).getFarmerVillageName());
                                txt_gut.setText(deatilsList.get(AppConstants.selectItemIndex).getFarmerGutName());
                                txt_subgut.setText(deatilsList.get(AppConstants.selectItemIndex).getFarmerSubgutName());
                                txt_taluka.setText(deatilsList.get(AppConstants.selectItemIndex).getFarmerTalukaName());
                                grower_id = deatilsList.get(AppConstants.selectItemIndex).getFarmerID();
//                        varity_id = Integer.parseInt(deatilsList.get(AppConstants.selectItemIndex).getFarmerObjectID());
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                txt_village.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                txt_gut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                txt_subgut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                txt_taluka.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                txt_village.setTypeface(null, Typeface.BOLD);
                                txt_gut.setTypeface(null, Typeface.BOLD);
                                txt_subgut.setTypeface(null, Typeface.BOLD);
                                txt_taluka.setTypeface(null, Typeface.BOLD);
                                //setInputLayoutColor();
                            } else if (selectListType == AppConstants.SHIVAR_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                txt_shivar_gut.setText(deatilsList.get(AppConstants.selectItemIndex).getGutName());
                                shivar_village_id = deatilsList.get(AppConstants.selectItemIndex).getObjectID();
                                shivar_gut_id = deatilsList.get(AppConstants.selectItemIndex).getGutID();
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                txt_shivar_gut.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                txt_shivar_gut.setTypeface(null, Typeface.BOLD);
                            } else if (selectListType == AppConstants.IRRIGATION_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                irrigation = deatilsList.get(AppConstants.selectItemIndex).getIrrigationMasterID();
                            } else if (selectListType == AppConstants.CAN_VERIETY_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
//                        cane_type = Integer.parseInt(deatilsList.get(AppConstants.selectItemIndex).getCaneVerityID());
                                varity_id = deatilsList.get(AppConstants.selectItemIndex).getCaneVerityID();
                            } else if (selectListType == AppConstants.CAN_TYPE_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                cane_type_id = deatilsList.get(AppConstants.selectItemIndex).getCaneTypeID();
                            } else if (selectListType == AppConstants.PLANTATION_TYPE_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                plantation_method = deatilsList.get(AppConstants.selectItemIndex).getPlantationID();
                            } else if (selectListType == AppConstants.SEASON_SELECT) {
                                String value = (String) model;
                                textView.setText(value);
                                textView.setTextColor(ContextCompat.getColor(getContext(), R.color.colorSelectedValueInPlantationAndRujvat));
                                textView.setTypeface(null, Typeface.BOLD);
                                selected_season = seasonList.get(AppConstants.selectItemIndex).getSeasonID();
                            }
                        }
                        AppConstants.selectItemIndex = 0;
                        removeOptionList();
                    } catch (Exception ex) {
                        Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
                    }
                }
            };
            ft.add(R.id.activity_registration, fragment);
            ft.commit();
            pdLoading.dismiss();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    void removeOptionList() {
        try {
            final FragmentTransaction ft = (FragmentTransaction) fragmentManager.beginTransaction();
            ft.setCustomAnimations(R.anim.slide_up, R.anim.slide_down);
            ft.remove(fragment).commit();
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private class AsyncCaller extends AsyncTask<Void, Void, Void> {

//        pdLoading=new ProgressDialog(LoginActivity.this);

        @Override
        protected void onPreExecute() {
            try {
                super.onPreExecute();
                //this method will be running on UI thread
                pdLoading.setMessage("\t" + getString(R.string.loading));
                pdLoading.show();
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
        }

        @Override
        protected Void doInBackground(Void... params) {
            try {
                //this method will be running on background thread so don't update UI frome here
                //do your long running http tasks here,you dont want to pass argument and u can access the parent class' variable url over here
                if (selectListType == AppConstants.SEASON_SELECT) {

                    JSONObject jsnobject = new JSONObject(Preferences.getSeasonList());
                    JSONArray list = jsnobject.getJSONArray("seasonList");
                    season.clear();
                    seasonList.clear();
                    Gson gson = new Gson();
                    for (int i = 0; i < list.length(); i++) {
                        SeasonNameListModel model = gson.fromJson(list.getString(i), SeasonNameListModel.class);
                        season.add(model.getSeasonName());
                        seasonList.add(model);
                    }
                    showOptionList(season, season_list);
                } else if (selectListType == AppConstants.GROWER_SELECT) {
                    String farmerList = sqLiteHelper.getFarmerList();
                    if (farmerList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.farmer_list_not_found));
                    else
                        showOptionList(getJsoneManager(farmerList), grower_name);
                } else if (selectListType == AppConstants.SHIVAR_SELECT) {
                    String villageList = sqLiteHelper.getVillageList();
                    if (villageList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.village_list_not_found));
                    else
                        showOptionList(getJsoneManager(villageList), shivar_village);
                } else if (selectListType == AppConstants.IRRIGATION_SELECT) {
                    String irrigationMaster = sqLiteHelper.getIrrigationMaster();
                    if (irrigationMaster.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.irrigation_list_not_found));
                    else
                        showOptionList(getJsoneManager(irrigationMaster), irrigation_source);
                } else if (selectListType == AppConstants.CAN_VERIETY_SELECT) {
                    String caneVerityList = sqLiteHelper.getCaneVerityList();
                    if (caneVerityList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.cane_verity_not_found));
                    else
                        showOptionList(getJsoneManager(caneVerityList), cane_varity);
                } else if (selectListType == AppConstants.CAN_TYPE_SELECT) {
                    String caneTypeList = sqLiteHelper.getCaneTypeList();
                    if (caneTypeList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.cane_type_not_found));
                    else {
                        showOptionList(getJsoneManager(caneTypeList), cane_type);
//                        validatePlantationDate(txt_plantationDate.getText().toString(), getJsoneManager(caneTypeList));
                    }
                } else if (selectListType == AppConstants.PLANTATION_TYPE_SELECT) {
                    String plantationTypeList = sqLiteHelper.getPlantationTypeList();
                    if (plantationTypeList.equals(AppConstants.DATA_BASE_NOT_AVAILABLE))
                        showMessage(getString(R.string.plantation_type_list_not_found));
                    else
                        showOptionList(getJsoneManager(plantationTypeList), plantation_type);
                }
            } catch (Exception ex) {
                Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
            }
            return null;
        }

        @Override
        protected void onPostExecute(Void result) {
            super.onPostExecute(result);

//            pdLoading.dismiss();
        }
    }


    private boolean validateData() {
        try {
            if (grower_name.getText().toString().equals(getString(R.string.grower_name_txt))) {
                Toast.makeText(getContext(), getString(R.string.select_grower_name), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (shivar_village.getText().toString().equals(getString(R.string.village))) {
                Toast.makeText(getContext(), getString(R.string.select_shivar_name), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (txt_surveyNumber.getText().toString().length() == 0) {
                Toast.makeText(getContext(), getString(R.string.select_survey_number), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (txt_surveyDate.getText().toString().length() == 0) {
                Toast.makeText(getContext(), getString(R.string.select_survey_date), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (irrigation_source.getText().toString().equals(getString(R.string.irrigation_source))) {
                Toast.makeText(getContext(), getString(R.string.select_irrigation_source), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (cane_varity.getText().toString().equals(getString(R.string.cane_variety))) {
                Toast.makeText(getContext(), getString(R.string.select_cane_variety), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (cane_type.getText().toString().equals(getString(R.string.cane_type))) {
                Toast.makeText(getContext(), getString(R.string.select_cane_type), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (txt_plantationDate.getText().toString().length() == 0) {
                Toast.makeText(getContext(), getString(R.string.enter_plantation_date), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (plantation_type.getText().toString().equals(getString(R.string.plantation_type))) {
                Toast.makeText(getContext(), getString(R.string.select_plantation_type), Toast.LENGTH_SHORT).show();
                return false;
            }
            if (txt_plantationArea.getText().toString().length() == 0) {
                Toast.makeText(getContext(), getString(R.string.enter_plantation_area), Toast.LENGTH_SHORT).show();
                return false;
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return true;
    }

    private String validatePlantationDate(String pDate, ArrayList<String> listArray) {
        String returnVal = "";
        Date dateTOCompare = changeDateFormateForPlantation(pDate);
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM");
//            Date dateP = sdf.parse(pDate);
            Date adsaliFrom = sdf.parse("16-07");
            Date adsaliTo = sdf.parse("31-08");
            Date preSeasonalFrom = sdf.parse("01-09");
            Date preseasonalTo = sdf.parse("30-11");
            Date seasonalFrom = sdf.parse("01-12");
            Date seasonalTo = sdf.parse("29-02");
            Date ratoonFrom = sdf.parse("01-03");
            Date ratoonTo = sdf.parse("30-06");

            if (adsaliFrom.after(dateTOCompare) && adsaliTo.before(dateTOCompare)) {
                returnVal = "";
            }
            if (preSeasonalFrom.after(dateTOCompare) && preseasonalTo.before(dateTOCompare)) {
                returnVal = "";
            }
            if (seasonalFrom.after(dateTOCompare) && seasonalTo.before(dateTOCompare)) {
                returnVal = "";
            }
            if (ratoonFrom.after(dateTOCompare) && ratoonTo.before(dateTOCompare)) {
                returnVal = "";
            }
        } catch (ParseException e) {
            e.printStackTrace();
        }
        return returnVal;
    }

    public void showMessage(final String message) {
        try {
            getActivity().runOnUiThread(new Runnable() {
                @Override
                public void run() {
                    pdLoading.dismiss();
                    Toast.makeText(getContext(), message, Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
    }

    private String changeDateFormate(String startDateString) {
//        startDateString = "27/06/2007";
        String newDateString = "";
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat df1 = new SimpleDateFormat("dd-MMM-yyyy");
            Date startDate;
            try {
                startDate = df.parse(startDateString);
                newDateString = df1.format(startDate);
                System.out.println(newDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return newDateString;
    }

    private Date changeDateFormateForPlantation(String startDateString) {
//        startDateString = "27/06/2007";
        Date returnVal = new Date();
        try {
            DateFormat df = new SimpleDateFormat("dd/MM/yyyy");
            DateFormat df1 = new SimpleDateFormat("dd/MM");
            Date startDate;
            try {
                startDate = df.parse(startDateString);
                String newDateString = df1.format(startDate);
                returnVal = df1.parse(newDateString);
                System.out.println(newDateString);
            } catch (ParseException e) {
                e.printStackTrace();
            }
        } catch (Exception ex) {
            Toast.makeText(getContext(), ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return returnVal;
    }
}