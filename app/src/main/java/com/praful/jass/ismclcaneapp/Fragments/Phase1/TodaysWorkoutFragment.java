package com.praful.jass.ismclcaneapp.Fragments.Phase1;

import android.annotation.SuppressLint;
import android.content.BroadcastReceiver;
import android.content.Context;
import android.content.Intent;
import android.content.IntentFilter;
import android.graphics.Color;
import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.support.v7.widget.Toolbar;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import com.google.android.gms.location.LocationRequest;
import com.google.android.gms.maps.CameraUpdateFactory;
import com.google.android.gms.maps.GoogleMap;
import com.google.android.gms.maps.OnMapReadyCallback;
import com.google.android.gms.maps.SupportMapFragment;
import com.google.android.gms.maps.model.LatLng;
import com.google.android.gms.maps.model.Marker;
import com.google.android.gms.maps.model.MarkerOptions;
import com.google.android.gms.maps.model.PolylineOptions;

import com.praful.jass.ismclcaneapp.R;
import com.praful.jass.ismclcaneapp.services.GoogleService;

public class TodaysWorkoutFragment extends Fragment implements OnMapReadyCallback {
    //    List<LatLng> list;
//    private static final LatLng LOWER_MANHATTAN = new LatLng(21.139939, 78.991827);
//    private static final LatLng TIMES_SQUARE = new LatLng(21.142500, 79.009336);
//    private static final LatLng BROOKLYN_BRIDGE0 = new LatLng(21.141700, 79.023584);
//    private static final LatLng BROOKLYN_BRIDGE1 = new LatLng(21.147463, 79.034399);
//    private static final LatLng BROOKLYN_BRIDGE2 = new LatLng(21.141700, 79.040235);
//    private static final LatLng BROOKLYN_BRIDGE3 = new LatLng(21.135295, 79.048818);
//    private static final LatLng BROOKLYN_BRIDGE4 = new LatLng(21.126836, 79.050358);
//    private static final LatLng BROOKLYN_BRIDGE5 = new LatLng(21.126371, 79.059634);
//    private static final LatLng BROOKLYN_BRIDGE6 = new LatLng(21.127115, 79.066815);
    private Toolbar toolbar;

//    private GoogleMap googleMap;

//    @Override
//    public View onCreateView(LayoutInflater inflater, ViewGroup container,
//                             Bundle savedInstanceState) {
//        // Inflate the layout for this fragment
//        View view = inflater.inflate(R.layout.fragment_todays_workout, container, false);
//        list = new LinkedList<>();
//        list.add(0, LOWER_MANHATTAN);
//        list.add(1, TIMES_SQUARE);
//        list.add(2, BROOKLYN_BRIDGE0);
//        list.add(3, BROOKLYN_BRIDGE1);
//        list.add(4, BROOKLYN_BRIDGE2);
//        list.add(5, BROOKLYN_BRIDGE3);
//        list.add(6, BROOKLYN_BRIDGE4);
//        list.add(7, BROOKLYN_BRIDGE5);
//        list.add(8, BROOKLYN_BRIDGE6);
//        setUpMapIfNeeded();
//
//        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
//        toolbar.setTitle("Todays Workout");
//
//        return view;
//    }
//
//    private void setUpMapIfNeeded() {
//        // check if we have got the googleMap already
////        if (googleMap == null) {
//        ((SupportMapFragment) this.getChildFragmentManager().findFragmentById(R.id.map)).getMapAsync(this);//getMap();
////            if (googleMap != null) {
////                addLines();
////            }
////        }
//    }
//
//    private void addLines(GoogleMap googleMap) {
//
//        googleMap
//                .addPolyline((new PolylineOptions())
////                        .add(LOWER_MANHATTAN, TIMES_SQUARE, BROOKLYN_BRIDGE0, BROOKLYN_BRIDGE1, BROOKLYN_BRIDGE2, BROOKLYN_BRIDGE3, BROOKLYN_BRIDGE4, BROOKLYN_BRIDGE5, BROOKLYN_BRIDGE6).width(5).color(Color.BLUE)
//                        .addAll(list).width(list.size()).color(Color.RED)
//                        .geodesic(true));
//        googleMap.addMarker(new MarkerOptions().position(LOWER_MANHATTAN)
//                .title("Rahul Start Time:- 10.54 AM"));
//        googleMap.addMarker(new MarkerOptions().position(BROOKLYN_BRIDGE6)
//                .title("Rahul Stop Time :- 5.30 PM"));
//        // move camera to zoom on map
//        googleMap.moveCamera(CameraUpdateFactory.newLatLngZoom(LOWER_MANHATTAN, 12));
//    }
//
//    @Override
//    public void onMapReady(GoogleMap googleMap) {
//
//        addLines(googleMap);
//    }
    private GoogleMap mMap;
    boolean isZoom = false;
    double tLat = 0.0, tLang = 0.0, nLang, nLat;
    LatLng previousLatLng, currentLatLng;
    LocationRequest locationRequest;
    Marker currentMarker;

//    @Override
//    protected void onCreate(Bundle savedInstanceState) {
//        super.onCreate(savedInstanceState);
//        setContentView(R.layout.activity_maps);
//        // Obtain the SupportMapFragment and get notified when the map is ready to be used.
//        SupportMapFragment mapFragment = (SupportMapFragment) getSupportFragmentManager()
//                .findFragmentById(R.id.map);
//        mapFragment.getMapAsync(this);
//        createLocationService();
//        Intent intent = new Intent(getApplicationContext(), GoogleService.class);
//        intent.putExtra("accuracy",getIntent().getDoubleExtra("accuracy",0.0));
//        startService(intent);
//        isZoom=true;
//
//    }

    @Override
    public View onCreateView(LayoutInflater inflater, ViewGroup container,
                             Bundle savedInstanceState) {
        // Inflate the layout for this fragment
        View view = inflater.inflate(R.layout.fragment_todays_workout, container, false);

        SupportMapFragment mapFragment = (SupportMapFragment) this.getChildFragmentManager()
                .findFragmentById(R.id.map);
        mapFragment.getMapAsync(this);
        createLocationService();
        Intent intent = new Intent(getContext(), GoogleService.class);
        intent.putExtra("accuracy", getActivity().getIntent().getDoubleExtra("accuracy", 0.0));
        getActivity().startService(intent);
        isZoom = true;

        toolbar = (Toolbar) getActivity().findViewById(R.id.toolbar);
        toolbar.setTitle(getString(R.string.nav_todays_workout));

        return view;
    }

    @SuppressLint("RestrictedApi")
    public void createLocationService() {
        locationRequest = new LocationRequest();
        locationRequest.setInterval(100);
        locationRequest.setSmallestDisplacement(0.05f);
        locationRequest.setFastestInterval(100);
        locationRequest.setPriority(LocationRequest.PRIORITY_HIGH_ACCURACY);
    }

    private BroadcastReceiver gpsreceiver = new BroadcastReceiver() {
        @Override
        public void onReceive(Context context, Intent intent) {
            Bundle bundle = intent.getExtras();
            if (bundle != null) {
                nLat = bundle.getDouble("lat");
                nLang = bundle.getDouble("lag");


                if (tLat != nLat && tLang != nLang) {
                    //  mMap.clear();
                    currentLatLng = new LatLng(nLat, nLang);
                    // LatLng sydney = new LatLng(bundle.getDouble("lat"), bundle.getDouble("lag"));
                    if (isZoom) {

                        mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(bundle.getDouble("lat"), bundle.getDouble("lag")), 17.0f));
                        isZoom = false;
                        previousLatLng = new LatLng(nLat, nLang);
                        mMap.addMarker(new MarkerOptions().position(currentLatLng).title(getString(R.string.starting_location)));

                    }
                    tLat = nLat;
                    tLang = nLang;
                    if (currentMarker != null) {
                        currentMarker.remove();
                    }
                    mMap.addPolyline((new PolylineOptions()).add(previousLatLng, currentLatLng).width(5).color(Color.BLUE).geodesic(true));
                    currentMarker = mMap.addMarker(new MarkerOptions().position(currentLatLng).title(getString(R.string.current_location)));
                    previousLatLng = currentLatLng;
//                    Toast.makeText(getContext(), "Location Changed", Toast.LENGTH_SHORT).show();
                } else {
//                    Toast.makeText(getContext(), "same Location", Toast.LENGTH_SHORT).show();
                }

            }
        }
    };

    @Override
    public void onDetach() {
        super.onDetach();
        getContext().unregisterReceiver(gpsreceiver);
    }

    @Override
    public void onAttach(Context context) {
        super.onAttach(context);
        context.registerReceiver(gpsreceiver, new IntentFilter("com.praful.jass.ismclcaneapp"));
    }


    @Override
    public void onMapReady(GoogleMap googleMap) {
        mMap = googleMap;
        mMap.animateCamera(CameraUpdateFactory.zoomTo(15), 2000, null);
// mMap.animateCamera(CameraUpdateFactory.newLatLngZoom(new LatLng(location.getLatitude(), location.getLongitude()), 12.0f));

//        // Add a marker in Sydney and move the camera
//        LatLng sydney = new LatLng(-34, 151);
//        mMap.addMarker(new MarkerOptions().position(sydney).title("Marker in Sydney"));
//        mMap.moveCamera(CameraUpdateFactory.newLatLng(sydney));
    }
}
