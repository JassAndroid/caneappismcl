package com.praful.jass.ismclcaneapp.Utils;

import java.text.SimpleDateFormat;
import java.util.Calendar;

/**
 * Created by Praful on 15-06-2017.
 */

public class SystemDateTime {
    String formattedDate = "";

    public String systemDateTime(String dateFormate) {
        try {
            Calendar c = Calendar.getInstance();
            System.out.println("Current time => " + c.getTime());
            SimpleDateFormat df = new SimpleDateFormat(dateFormate);//"dd-MMM-yyyy"
            formattedDate = df.format(c.getTime());
        } catch (Exception ex) {
//            Toast.makeText(context, ex.getMessage(), Toast.LENGTH_SHORT).show();
        }
        return formattedDate;
    }
}
