package com.praful.jass.ismclcaneapp.Models;

public class HarvestingForCuttingOrderModel {
    private String H_ID;

    private String H_NAME;

    private String H_CODE;

    public String getH_ID ()
    {
        return H_ID;
    }

    public void setH_ID (String H_ID)
    {
        this.H_ID = H_ID;
    }

    public String getH_NAME ()
    {
        return H_NAME;
    }

    public void setH_NAME (String H_NAME)
    {
        this.H_NAME = H_NAME;
    }

    public String getH_CODE ()
    {
        return H_CODE;
    }

    public void setH_CODE (String H_CODE)
    {
        this.H_CODE = H_CODE;
    }

    @Override
    public String toString()
    {
        return "ClassPojo [H_ID = "+H_ID+", H_NAME = "+H_NAME+", H_CODE = "+H_CODE+"]";
    }
}
